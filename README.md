### Fs19
***
<div style="text-align: right;">
Version 1.4.0
</div>

#### Table of contents
***
1. Introduction
1. How to use
1. Distribution file
***
#### 1. Introduction
This software was created by being impressed by the software called "FD" used in the MS-DOS era.

#### 2. How to build

Generate build file
```bash
cmake . -B build -DCMAKE_BUILD_TYPE=Release
```
Actual build
```bash
msbuild -m build/fs19.sln -p:Configuration=Release
```

#### 3. How to use
| Key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Feature Description |
| :--- | :--- | :--- |
| `A` | Attribute | Change file attributes (read only, hidden, system, datetime). |
| `E` | Eject | Remove the removable media such as USB memory. |
| `Z` | Execution | Run the executable file. |
| `Shift` + `Enter` | Editor | If you have not registered a text editor, Notepad will start, and if you have registered, the registered software will start and you can edit the text file at the cursor position. |
| `Space` | Select | Select the file on the cursor position. If selected, cancel. |
| `Ctrl` + `Insert`  | Copy | Copies the file at the cursor position or the selected file to the clipboard. |
| `Shift` + `Insert` | paste | Paste the copied file to the clipboard. |
| `Delete` | Delete | Deletes the file at the cursor position or the selected file. |
| `]` | Change drive | You can change the drive. You can also move the directory. |

#### 4. Distribution file

One file is OK.
```bash
build/release/Fs19.exe
```
