/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: com_button.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _COM_BUTTON_H_
#define _COM_BUTTON_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define ICOMBUTTON_KEY  0x8A00

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class com_button : public xButton
{
public:
  com_button();
  ~com_button();

public:
  void OnKeyDown(UINT vk, BOOL fDown, int cRepeat, UINT flags);
  int  DrawItem(const DRAWITEMSTRUCT *pDIS);
  void FillRect(HDC hDC, const RECT *pRC, COLORREF fg);

public:
  int icon_no;
};

#endif /* _COM_BUTTON_H_ */
