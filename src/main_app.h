/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: main_app.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _MAIN_APP_H_
#define _MAIN_APP_H_

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class main_app : public sub_app
{
  // --
  typedef int (main_app::*PT_FUNC_TYPE)(TCHAR *cp);

protected:
  void  OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  BOOL  OnCreate(LPCREATESTRUCT lpCreateStruct);
  void  OnShowWindow(BOOL fShow, UINT status);
  void  DoContextMenu(int x, int y);
  void  DoContextMenu(int x, int y, const TCHAR *target);
  void  OnInitMenuPopup(HMENU hMenu, UINT item, BOOL fSystemMenu);
  void  OnDrawItem(const DRAWITEMSTRUCT * lpDrawItem);
  void  OnMeasureItem(MEASUREITEMSTRUCT * lpMeasureItem);
  void  OnDestroy();
  void  OnKeyDown(UINT vk, BOOL fDown, int cRepeat, UINT flags);
  void  OnDropFiles(HDROP hDrop);
  int   ReadClip();
  void  LetsCopy(TCOPYACTION *act, int id);

protected:
  HBRUSH OnCtlColorEditr(HDC hdc, HWND hwndChild, int type);

private:
  TCHAR *GetDesktopFolder(void);

public:
  int  Dummy(TCHAR *param = NULL) { return 0; }
  int  FuncEditor(TCHAR *param = NULL);
  int  FuncLogDsk(TCHAR *param = NULL);
  int  FuncRefresh(TCHAR *param = NULL);
  int  FuncExec(TCHAR *param = NULL);
  int  FuncBack(TCHAR *param = NULL);
  int  FuncStep(TCHAR *param = NULL);
  int  FuncEject(TCHAR *param = NULL);
  int  FuncCopy(TCHAR *param = NULL);
  int  FuncPast(TCHAR *param = NULL);
  int  FuncRename(TCHAR *param = NULL);
  int  FuncOption(TCHAR *param = NULL);
  int  FuncSelExe(TCHAR *param = NULL);
  int  FuncDelete(TCHAR *param = NULL);
  int  FuncInsert(TCHAR *param = NULL);
  int  FuncApps(TCHAR *param = NULL);
  int  FuncProperties(TCHAR *param = NULL);
  int  FuncAttribute(TCHAR *param = NULL);
  int  FuncShortcut1(TCHAR *param = NULL);
  int  FuncShortcut2(TCHAR *param = NULL);
  int  FuncShortcut3(TCHAR *param = NULL);
  int  FuncShortcut4(TCHAR *param = NULL);
  int  FuncShortcut5(TCHAR *param = NULL);
  int  FuncShortcut6(TCHAR *param = NULL);
  int  FuncShortcut7(TCHAR *param = NULL);
  int  FuncShortcut8(TCHAR *param = NULL);
  int  FuncShortcut9(TCHAR *param = NULL);
  int  FuncFind(TCHAR *param = NULL);
  int  FuncSort(TCHAR *param = NULL);
  int  FuncColumn(TCHAR *param = NULL);

private:
  char *OpenProcessToMemory(TCHAR *param);
  int   OpenProcessToFile(TCHAR *param, TCHAR *outp);
  int   xgit_status(TCHAR *dir);
  int   xgit_ls_files(TCHAR *dir);
  int   xgit_log(TCHAR *path, char *hash1, char *hash2);
  int   xgit_rev_parse(TCHAR *path, size_t len);
  int   xgit_diff(TCHAR *path, char *hash, TCHAR *temp);
  int   xgit_log_to_file(TCHAR *path, TCHAR *out);
  int   xgit_get_rev_parse_path(dirent *entry, TCHAR *path, size_t len);
  int   xgit_diff_2nd(dirent *entry, uint64_t *hash, TCHAR **temp);
  int   xgit_diff_2nd(dirent *entry, TCHAR *hash1, TCHAR *hash2, TCHAR **temp);
  int   LogListMode(TCHAR *fnam, dirent *entry);
  int   Desktop(void);

private:
  PT_FUNC_TYPE *mpFuncs;

private:
  TCHAR  m_desktopfolder[MAX_PATH];

public:
  main_app();
  ~main_app();

public:
  UINT  mOsMessageMode;
  HWND  mhNextClipWnd;
};

#endif /* _MAIN_APP_H_ */
