/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_logdsk.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "resource.h"
#include "com_list.h"
#include "com_edit.h"
#include "com_button.h"
#include "sub_funcs.h"
#include "history.h"
#include "xpt.h"
#include "logf.h"
#include "icon.h"
#include "dlg_logdsk.h"

/* ----------------------------------------------------------------- */
dlg_logdsk::dlg_logdsk(HWND hParent) : xDialog(_T("IDD_LOGDSK"), hParent)
{
  mpLogList = new com_list;
  mpEdit    = new com_edit;
  mpFont    = new xFont;
  mpOK      = new com_button;
  mpCancel  = new com_button;

  IcoSetPsMode(&mpOK->icon_no, &mpCancel->icon_no);

  ZeroMemory(logdrv, sizeof(logdrv));
}

/* ----------------------------------------------------------------- */
dlg_logdsk::~dlg_logdsk()
{
  delete mpLogList;
  delete mpEdit;
  delete mpFont;
  delete mpOK;
  delete mpCancel;
}

/* ----------------------------------------------------------------- */
void dlg_logdsk::OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  TCHAR buf[MAX_PATH2];
  int   mark = 0;
  DWORD date = 0;
  int   index;

  switch (id)
    {
      case IDC_EDIT1:
        if ((codeNotify & 0xFF00) != ICOMEDIT_KEY)
          {
            break;
          }

        mpLogList->SendMessage(WM_KEYDOWN, codeNotify & 0xFF, 0);
        mpEdit->SetSel(0, -1);

        if ((codeNotify & 0xFF) != VK_INSERT)
          {
            break;
          }

        id = mpLogList->GetCurSel();

        if ((index = (int)mpLogList->GetItemData(id)) < 0) {
          break;
        }

        mpLogList->GetText(id, buf);

        if (HitGetAt(index, &mark, &date) == NULL) {
          break;
        }

        if (mark != 0) {
          mark = 0;
        }
        else {
          mark = 1;
        }

        HitSetAt(index, mark);

        mpLogList->DeleteString(id);

        mpLogList->InsertString(id, buf);
        mpLogList->SetItemData(id, index);
        mpLogList->SetCurSel(id);

        break;

      case IDC_LIST1:
        id = mpLogList->GetCurSel();

        mpLogList->GetText(id, buf);

        mpEdit->SetWindowText(buf);

        if (codeNotify != LBN_DBLCLK)
          {
            break;
          }

        /* Through*/

      case IDOK:
      case IDOK2:
        mpEdit->GetText(logdrv, MAX_PATH2);

        if (_tcslen(logdrv) < 3)
          {
            /* Drive name correction */

            logdrv[1] = ':';
            logdrv[2] = '\\';
            logdrv[3] = '\0';
          }

        if (!CheckPathRules(logdrv))
          {
            _stprintf_s(buf, MAX_PATH2, _T("\"%s\" Invalid path !!"), logdrv);
            ::MessageBox(mhWnd, buf, _T("Failure(-o-;"), MB_ICONSTOP);
            break;
          }

        if (CheckPath(logdrv, 2, mhWnd, 1) < 2)
          {
            break;
          }

        if (!PathIsRoot(logdrv) && !PathIsUNC(logdrv) && !PathFileExists(logdrv))
          {
            if (::MessageBox(mhWnd,
                             _T("There is no folder. Do you want to make it? "),
                             _T("What will you do?"),
                             MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 ) == IDNO)
              {
                break;
              }

            PathAddBackslash(logdrv);

            if (SHCreateDirectoryEx(mhWnd, logdrv, 0) != ERROR_SUCCESS)
              {
                ::MessageBox(mhWnd,
                             _T("Failed to create folder !!"),
                             _T("Failure(-o-;"),
                             MB_ICONSTOP);
                break;
              }
          }

        EndDialog(IDOK);
        break;

      case IDCANCEL:
      case IDCANCEL2:
        EndDialog(IDCANCEL);
        break;

      default:
        break;
    }
}

/* ----------------------------------------------------------------- */
BOOL dlg_logdsk::OnInitDialog(HWND hwndFocus, LPARAM lParam)
{
  TCHAR buf[MAX_PATH2];
  IPT  *pt = xPt();
  RECT  rc;
  xDC   dc(mhWnd);
  int   h_edit;

  Attach(*mpLogList, IDC_LIST1);
  Attach(*mpEdit,    IDC_EDIT1);
  Attach(*mpOK,      IDOK2);
  Attach(*mpCancel,  IDCANCEL2);

  /* Rearrange controls */

  GetClientRect(&rc);

  int x = 8;
  int y = 8;
  int w = rc.right - 16;
  int h = rc.bottom;

  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  dc.GetTextMetrics();

  y += 16 + 4;
  y += 2;

  /* EditBox */

  h_edit = dc.tm.tmHeight + 8;

  mpEdit->MoveWindow(x, y, w, h_edit);

  y += h_edit;
  y += 10;

  /* Button */

  mpOK->GetClientRect(&rc);

  mpOK->MoveWindow(w - (rc.right * 2 + 8), h - (rc.bottom + 8), rc.right, rc.bottom);

  mpCancel->MoveWindow(w - rc.right, h - (rc.bottom + 8), rc.right, rc.bottom);

  /* ListBox */

  mpLogList->MoveWindow(x, y, w, h - (y + rc.bottom + 16));

  /* Initialize list of Logs */

  SetList();

  /* Move to center */

  CenterWindow();

  /* Initialize EditBox */

  mpFont->Create(pt->fonts[4].name, pt->fonts[4].size);

  mpEdit->SetFocus();
  mpEdit->SetFont(*mpFont);

  if (logdrv[0] == '\0')
    {
      mpLogList->SetCurSel(0);
      mpLogList->GetText(0, buf);
      mpEdit->SetText(buf);
    }
  else
    {
      mpEdit->SetText(logdrv);
    }

  mpEdit->SetSel(0, -1);

  return 0;
}

/* ----------------------------------------------------------------- */
void dlg_logdsk::SetCurrent(const TCHAR *name)
{
  _tcscpy_s(logdrv, MAX_PATH2, name);
}

/* ----------------------------------------------------------------- */
void dlg_logdsk::OnPaint(void)
{
  xPaintDC  dc(mhWnd);
  IPT      *pt = xPt();
  RECT      rc;

  GetClientRect(&rc);

  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  dc.SetBkMode(TRANSPARENT);

  dc.SetTextColor(pt->color.title);

  dc.FillRect(&rc, pt->color.base_bar);

  dc.TextOut(28, 8, _T("Enter the drive name or folder name to be changed."));

  IcoDrawIconFromIndex(dc, 8, 6, 105, TRUE);
}

/* ----------------------------------------------------------------- */
static REMOTE_NAME_INFO *GetUniversalName(TCHAR *cp)
{
  REMOTE_NAME_INFO *inf = NULL;
  DWORD             len = 0;
  DWORD             res;

  res = WNetGetUniversalName(cp,
                             REMOTE_NAME_INFO_LEVEL,
                             inf,
                             &len);
  if (res != ERROR_MORE_DATA)
    {
      LOGE("WNetGetUniversalName:%d", res);
      return NULL;
    }

  inf = (REMOTE_NAME_INFO *)malloc(sizeof(REMOTE_NAME_INFO) + len);

  if (inf == NULL)
    {
      LOGE("malloc");
      return NULL;
    }

  res = WNetGetUniversalName(cp,
                             REMOTE_NAME_INFO_LEVEL,
                             inf,
                             &len);
  if (res != NO_ERROR)
    {
      LOGE("WNetGetUniversalName:%d", res);
      free(inf);
      inf = NULL;
    }

  return inf;
}

/* ----------------------------------------------------------------- */
void dlg_logdsk::SetList(void)
{
  int      i;
  int      cur = 0;
  int      cnt = mpLogList->GetCount();
  TCHAR   *buf;
  TCHAR    drv[MAX_PATH2];
  TCHAR   *cp;

  for (i = 0; i < cnt; i++)
    {
      mpLogList->DeleteString(0);
    }

  /* Get list of drives */

  DWORD len = GetLogicalDriveStrings(0, NULL);

  buf = (TCHAR *)alloca((len + 1 /* Null */) * sizeof(TCHAR));

  if (buf == NULL)
    {
      ::MessageBox(mhWnd,
                   _T("Failed to memory allocate !!"),
                   _T("Failure(-o-;"),
                   MB_ICONSTOP);
      return;
    }

  GetLogicalDriveStrings(len, buf);

  /* Get current drive name */

  _stprintf_s(drv, MAX_PATH2, _T("%s"), logdrv);

  HitPush(logdrv);

  PathStripToRoot(drv);

  for (i = 0, cp = buf; *cp != '\0'; cp += (lstrlen(cp) + 1))
    {
      if (GetDriveType(cp) == DRIVE_REMOTE)
        {
          REMOTE_NAME_INFO *info = GetUniversalName(cp);

          if (info)
            {
              mpLogList->SetLogData(i, info->lpConnectionName, -1);
              free(info);
            }
          else
            {
              mpLogList->SetLogData(i, cp, -1);
            }
        }
      else
        {
          if (!PathFileExists(cp))
            {
              continue;
            }

          mpLogList->SetLogData(i, cp, -1);
        }

      if (_tcsicmp(drv, cp) == 0)
        {
          mpLogList->SetCurSel(i);
        }

      i++;
    }

  for (i = 0; i < IPT_HISTORY_EVENT_NUM; i++) {
    const TCHAR *path;
    int          mark = 0;
    DWORD        date = 0;

    if ((path = HitGetAt(i, &mark, &date)) == NULL) {
      break;
    }

    mpLogList->SetLogData(0, path, i);

    cur = i;
  }

  /* Set the cursor to the end and then set the cursor to the current! */

  mpLogList->SetCurSel(mpLogList->GetCount() - 1);
  mpLogList->SetCurSel(cur);
}

/* ----------------------------------------------------------------- */
LRESULT dlg_logdsk::Message(UINT msg, WPARAM wParam, LPARAM lParam)
{
  LRESULT res = 0;

  switch(msg) {
  case WM_LBUTTONDOWN:
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, lParam);
    break;
  default:
    res = xDialog::Message(msg, wParam, lParam);
    break;
  }

  return res;
}
