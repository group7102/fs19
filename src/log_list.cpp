/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: log_list.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#include "_box.h"
#include "xpt.h"
#include "logf.h"
#include "dirent.h"
#include "icon.h"
#include "command.h"
#include "log_list.h"

/* ----------------------------------------------------------------- */
log_list::log_list() : xListBox(102)
{
  uint32_t style = 0
                 | WS_VISIBLE
                 | WS_CHILD
                 | LBS_MULTIPLESEL
                 | WS_VSCROLL
//               | WS_HSCROLL
//               | LBS_EXTENDEDSEL
                 | LBS_OWNERDRAWFIXED
//               | LBS_OWNERDRAWVARIABLE
//               | LBS_HASSTRINGS
//               | LBS_NOINTEGRALHEIGHT
//               | WS_BORDER
                 | LBS_HASSTRINGS
                 | 0;

  SetStyleEx(0);
  SetStyle(style);

  mpFont = new xFont;

  m_enable = FALSE;

  m_entry = FALSE;
}

/* ----------------------------------------------------------------- */
log_list::~log_list()
{
  m_enable = FALSE;

  Destroy();

  delete mpFont;
}

/* ----------------------------------------------------------------- */
HWND log_list::Create(HWND hParentWnd, int x, int y, int cx, int cy)
{
  /* Initialize */

  IPT *pt = xPt();

  /* Set font */

  mpFont->Create(pt->fonts[4].name, pt->fonts[4].size);

  HWND hWnd = xListBox::Create(hParentWnd, x, y, cx, cy);

  SetFont(*mpFont);

  m_enable = TRUE;

  return hWnd;
}

/* ----------------------------------------------------------------- */
void log_list::OnDestroy(void)
{
  xListBox::OnDestroy();
}

/* ----------------------------------------------------------------- */
int log_list::PumpMessage(void)
{
  MSG  msg;
  int  res = 0;

  m_hash1[0] = L'\0';
  m_hash2[0] = L'\0';

  for (;;)
    {
      if (!PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
        {
          WaitMessage();
          continue;
        }

      if (msg.message == WM_QUIT)
        {
          res = -1;
          break;
        }

      if (msg.message == WM_KEYDOWN)
        {
          if (msg.wParam == VK_TAB)
            {
              break;
            }
        }

      if (GetMessage(&msg, NULL, 0, 0) <= 0)
        {
          break;
        }

      if (msg.message == WM_KEYDOWN)
        {
          if (msg.wParam == VK_ESCAPE || msg.wParam == VK_BACK)
            {
              break;
            }

          if (msg.wParam == 'D')
            {
              res = GetSelectHash(m_hash1, m_hash2);
              break;
            }
        }

      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }

  return res;
}

/* ----------------------------------------------------------------- */
HWND log_list::SetFocus()
{
  HWND hWnd = NULL;

  if (m_enable)
    {
      hWnd = xListBox::SetFocus();
    }

  return hWnd;
}

/* ----------------------------------------------------------------- */
int log_list::GetSelectCount(void)
{
  int num = GetCount();
  int cnt = 0;

  for (int i = 0; i < num; i++)
    {
      if (GetSel(i))
        {
          cnt++;
        }
    }

  return cnt;
}

/* ----------------------------------------------------------------- */
void log_list::OnKeyDown(UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
  if (vk == VK_SPACE)
    {
      int cnt = GetSelectCount();
      int idx = GetCurSel();
      int sel = GetSel(idx);

      if (sel)
        {
          SetSel(0, idx);
        }
      else
        {
          if (cnt < 2)
            {
              SetSel(1, idx);
            }
        }

      vk = VK_DOWN;
    }

  if (vk == VK_HOME || vk == VK_END)
    {
      int num = GetCount();

      for (int i = 0; i < num; i++)
        {
          if (GetSel(i))
            {
              SetSel(0, i);
            }
        }

      return;
    }

  xListBox::OnKeyDown(vk, fDown, cRepeat, flags);
}

/* ----------------------------------------------------------------- */
int log_list::GetHash(int idx, TCHAR *hash)
{
  int len = GetTextLen(idx);

  if (len == 0) {
    return -1;
  }

  TCHAR *buf = (TCHAR *)alloca((len + 1) * sizeof(TCHAR));

  if (buf == NULL) {
    return -1;
  }

  GetText(idx, buf);

  TCHAR *tmp;
  TCHAR *cp = _tcstok_s(buf, L" ", &tmp);

  if (cp == NULL) {
    return -1;
  }

  __snprintf(hash, 41, "%s", cp);

  return 0;
}

/* ----------------------------------------------------------------- */
uint64_t log_list::GetHash(int idx)
{
  uint64_t hash = 0;

  int len = GetTextLen(idx);

  if (len == 0)
    {
      return 0;
    }

  TCHAR *buf = (TCHAR *)alloca((len + 1) * sizeof(TCHAR));

  if (buf == NULL)
    {
      return 0;
    }

  GetText(idx, buf);

  TCHAR *tmp;
  TCHAR *cp = _tcstok_s(buf, L" ", &tmp);

  if (cp)
    {
      hash = _tcstoull(cp, NULL, 16);
    }

  return hash;
}

/* ----------------------------------------------------------------- */
int log_list::GetSelectHash(TCHAR *hash1, TCHAR *hash2)
{
  int  num = GetCount();
  int  cnt = 0;
  int  idx;

  hash1[0] = L'\0';
  hash2[1] = L'\0';

  for (idx = 0; idx < num; idx++) {
    if (!GetSel(idx)) {
      continue;
    }

    if (cnt == 0) {
      GetHash(idx, hash1);
    }
    else if (cnt == 1) {
      GetHash(idx, hash2);
    }
    else {
      break;
    }

    cnt++;
  }

  if (cnt > 0) {
    return cnt;
  }

  idx = GetCurSel();

  return GetHash(idx, hash1) == 0 ? 1 : 0;
}

/* ----------------------------------------------------------------- */
int log_list::DrawItem(const DRAWITEMSTRUCT *pDIS)
{
  int idx = pDIS->itemID;

  if (idx < 0)
    {
      return 0;
    }

  /* Get info */

  TCHAR *buf = (TCHAR *)alloca((GetTextLen(idx) + 1) * sizeof(TCHAR));
  TCHAR *argv[5] = {0};
  TCHAR *cp = buf;
  TCHAR *context = NULL;

  GetText(idx, buf);

  for (int i = 0; i < 5; i++)
    {
      argv[i] = _tcstok_s(cp, L"*", &context);

      if (argv[i] == NULL)
        {
          return 0;
        }

      cp = NULL;
    }

  /* Set paint rec */

  RECT rc;

  rc.left   = 0;
  rc.top    = 0;
  rc.right  = pDIS->rcItem.right - pDIS->rcItem.left;
  rc.bottom = pDIS->rcItem.bottom - pDIS->rcItem.top;

  xTmpDC dc(pDIS->hDC);
  IPT   *pt = xPt();
  xBmpDC bmp(dc, rc.right, rc.bottom);
  int    chk = 0;

  /* Background fill */

  COLORREF col1 = pt->color.base_bar;
  COLORREF col2 = pt->color.base_bar;

  if (pDIS->itemAction & ODA_DRAWENTIRE)
    {
      m_entry = TRUE;
    }

  if (m_entry == FALSE)
    {
      return 0;
    }

  if (pDIS->itemState & ODS_SELECTED)
    {
      col1 = pt->color.selected_cur;
      col2 = pt->color.selected_cur;
      chk  = 1;
    }

  if (pDIS->itemState & ODS_FOCUS)
    {
      col1 = pt->color.selected_frame;
      col2 = pt->color.selected;
    }

  bmp.Rectangle(&rc, col1 , col2);

  /* Set text */

  bmp.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);
  bmp.SetBkMode(TRANSPARENT);

  SIZE size;
  int  xx = 8;
  int  yy = 3;

  bmp.SetTextColor(RGB(120, 90, 0));
  bmp.TextOut(xx, yy, argv[1]);

  bmp.GetTextExtentPoint32(argv[1], &size);

  xx += size.cx;
  xx += 8;

  bmp.SetTextColor(0);
  bmp.TextOut(xx, yy, argv[4]);

  bmp.GetTextExtentPoint32(argv[4], &size);

  xx += size.cx;
  xx += 8;

  bmp.SetTextColor(RGB(120, 90, 0));
  bmp.TextOut(xx, yy, argv[2]);

  /* Set check */

  if (chk)
    {
      IcoDrawIconFromIndex(bmp, 0, 0, 44, 1);
    }

  dc.BitBlt(pDIS->rcItem.left,
            pDIS->rcItem.top,
            rc.right,
            rc.bottom,
            bmp,
            0,
            0);

  return 0;
}

/* ----------------------------------------------------------------- */
int log_list::MeasureItem(MEASUREITEMSTRUCT *lpMeasureItem)
{
  return 1;
}

/* ----------------------------------------------------------------- */
int log_list::AddTail(TCHAR *str)
{
  StrTrim(str, L"\r\n");

  int no = InsertString(-1, str);

  SetItemData(no, (UINT_PTR)this);

  return no;
}
