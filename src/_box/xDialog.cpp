/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: xDialog.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    コントロールの基底クラス
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <tchar.h>
#include <assert.h>
#include "_boxCore.h"
#include "_boxMsgCore.h"
#include "xControls.h"
#include "xDialog.h"

/* ----------------------------------------------------------------- *
 * ダイアログボックスのメインのコールバック
 * ----------------------------------------------------------------- */
INT_PTR CALLBACK xDialog::DialogMainProc(HWND   hDlg,
                                         UINT   msg,
                                         WPARAM wParam,
                                         LPARAM lParam)
{
  xDialog*  pDlg = (xDialog*)::GetWindowLongPtr(hDlg, GWLP_USERDATA);

  assert(pDlg);

  pDlg->ResultDialog(-1);

  INT_PTR res = pDlg->Message(msg, wParam, lParam);

  if (pDlg->mResult >= 0)
    {
      res = pDlg->mResult;
    }

  return res;
}

/* ----------------------------------------------------------------- *
 * ダイアログボックスの初期コールバック
 * ----------------------------------------------------------------- */
INT_PTR CALLBACK xDialog::DialogInitProc(HWND   hDlg,
                                         UINT   msg,
                                         WPARAM wParam,
                                         LPARAM lParam)
{
  if (msg != WM_INITDIALOG)
    {
      return FALSE;
    }

  xDialog *pDlg = (xDialog *)lParam;

  assert(pDlg);

  pDlg->mhWnd = hDlg;

  ::SetWindowLongPtr(hDlg, DWLP_DLGPROC, (LPARAM)DialogMainProc);
  ::SetWindowLongPtr(hDlg, GWLP_USERDATA, (LONG_PTR)pDlg);

  return pDlg->Message(msg, wParam, lParam);
}

/* ----------------------------------------------------------------- */
xDialog::xDialog(const TCHAR *pTemplate, HWND hParent)
{
  mpTemplate = pTemplate;
  mhParent   = hParent;
}

/* ----------------------------------------------------------------- */
xDialog::xDialog(const TCHAR *pTemplate, _boxCore *pParent)
{
  mpTemplate = pTemplate;
  mhParent   = pParent->mhWnd;
}

/* ----------------------------------------------------------------- */
xDialog::~xDialog()
{
}

/* ----------------------------------------------------------------- */
HWND xDialog::Create()
{
  return ::CreateDialogParam(::GetModuleHandle(NULL),
                             mpTemplate,
                             mhParent,
                             DialogInitProc,
                             (LPARAM)this);
}

/* ----------------------------------------------------------------- */
LRESULT xDialog::DefaultMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  return FALSE;
}

/* ----------------------------------------------------------------- */
int xDialog::OnMessage(UINT msg, WPARAM wParam, LPARAM lParam)
{
  return FALSE;
}

/* ----------------------------------------------------------------- */
void xDialog::PumpMessage()
{
  MSG   msg;

  while (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (!::IsDialogMessage(mhWnd, &msg))
        {
          if (msg.message == WM_PAINT || msg.message == WM_SYSCOMMAND)
            {
              ::TranslateMessage(&msg);
              ::DispatchMessage (&msg);
            }
        }
    }
}

/* ----------------------------------------------------------------- */
int xDialog::Modal()
{
  return (int)::DialogBoxParam(::GetModuleHandle(NULL),
                               mpTemplate,
                               mhParent,
                               DialogInitProc,
                               (LPARAM)this);
}

/* ----------------------------------------------------------------- */
BOOL xDialog::EndDialog(int result)
{
  return ::EndDialog(mhWnd, result);
}

/* ----------------------------------------------------------------- */
HWND xDialog::GetDlgItem(int iID)
{
  return ::GetDlgItem(mhWnd, iID);
}

/* ----------------------------------------------------------------- */
BOOL xDialog::CenterWindow()
{
  RECT  rcThis;
  RECT  rcOwner;
  int   width;
  int   height;

  ::GetWindowRect(mhParent, &rcOwner);
  GetWindowRect(&rcThis);

  width  = rcOwner.right  - rcOwner.left;
  height = rcOwner.bottom - rcOwner.top;

  width  -= (rcThis.right  - rcThis.left);
  height -= (rcThis.bottom - rcThis.top);

  width  /= 2;
  height /= 2;

  return MoveWindow(rcOwner.left  + width,
                    rcOwner.top   + height,
                    rcThis.right  - rcThis.left,
                    rcThis.bottom - rcThis.top);
}

/* ----------------------------------------------------------------- */
int xDialog::Attach(_boxCore& obj, int id)
{
  return obj.Attach(GetDlgItem(id));
}

/* ----------------------------------------------------------------- */
int xDialog::Attach(_boxCore* obj, int id)
{
  return obj->Attach(GetDlgItem(id));
}

/* ----------------------------------------------------------------- */
void xDialog::OnDrawItem(const DRAWITEMSTRUCT *pDIS)
{
  xControls *pCtl = NULL;

  BOOL res = ::GetWindowSubclass((HWND)pDIS->hwndItem,
                                 MainWindowFunc,
                                 0,
                                 (DWORD_PTR *)&pCtl);
  if (res && pCtl)
    {
      mResult = pCtl->DrawItem(pDIS);
    }
}

/* ----------------------------------------------------------------- */
void xDialog::OnMeasureItem(MEASUREITEMSTRUCT *pMIS)
{
  xControls *pCtl = NULL;

  BOOL res = ::GetWindowSubclass((HWND)GetDlgItem(pMIS->CtlID),
                                 MainWindowFunc,
                                 0,
                                 (DWORD_PTR *)&pCtl);
  if (res && pCtl)
    {
      mResult = pCtl->MeasureItem(pMIS);
    }
}
