/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: xThread.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    コントロールの基底クラス
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include <windows.h>
#include <windowsx.h>
#include "xThread.h"

/* ----------------------------------------------------------------- */
xThread::xThread()
{
  mhThread = NULL;
}

/* ----------------------------------------------------------------- */
xThread::~xThread()
{
  End();
}

/* ----------------------------------------------------------------- */
void xThread::Create(DWORD creationFlags)
{
  mIsEnd   = FALSE;
  mhThread = ::CreateThread(NULL,
                            0,
                            (LPTHREAD_START_ROUTINE)ThreadFunc,
                            (LPVOID)this,
                            creationFlags,
                            &mThID);
}

/* ----------------------------------------------------------------- */
void xThread::End()
{
  mIsEnd = TRUE;    // スレッド終了させなさい

  if (mhThread)
    {
      /* スレッドが終了するまで待つ */

      ::WaitForSingleObject(mhThread, INFINITE);
      ::CloseHandle(mhThread);

      mhThread = NULL;
    }
}

/* ----------------------------------------------------------------- */
DWORD WINAPI xThread::ThreadFunc(LPVOID obj)
{
  ((xThread*)obj)->Main();

  ::ExitThread(0);
}

/* ----------------------------------------------------------------- */
int xThread::Wait(DWORD dwMilliseconds)
{
  int   res = 0;

  switch (::WaitForSingleObject(mhThread, dwMilliseconds))
    {
      case WAIT_TIMEOUT:
        res = 1;
        break;

      default:
        break;
    }

  return res;
}
