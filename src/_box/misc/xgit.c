/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2019 ****, Inc.
 *
 *  File Name: xgit.cpp
 *  Created  : 19/11/23
 *
 *  Function
 *    Scan folder
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:48 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include <stdio.h>
#include <tchar.h>
#include <sys/stat.h>
#include <shlwapi.h>
#include <time.h>
#include <fcntl.h>
#include <bcrypt.h>
#include "xgit.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

typedef struct _SAVE_INDEX_T {
  struct _SAVE_INDEX_T *next;
  time_t                mtime;
  TCHAR                 path[1];
}
SAVE_INDEX_T;

/* ----------------------------------------------------------------- *
 * Private data
 * ----------------------------------------------------------------- */

static SAVE_INDEX_T _top_idx = {
  .next  = NULL,
  .mtime = 0,
};

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static BYTE get_byte(const BYTE** pp_data)
{
  BYTE        data;
  const BYTE* bp = *pp_data;

  data = *bp;

  bp++;

  *pp_data = bp;

  return data;
}

/* ----------------------------------------------------------------- */
static WORD get_word(const BYTE** pp_data)
{
  WORD  data = 0;

  data |= get_byte(pp_data);
  data <<= 8;
  data |= get_byte(pp_data);

  return data;
}

/* ----------------------------------------------------------------- */
static DWORD get_dword(const BYTE** pp_data)
{
  DWORD data = 0;

  data |= get_word(pp_data);
  data <<= 16;
  data |= get_word(pp_data);

  return data;
}

/* ----------------------------------------------------------------- */
static const BYTE *Header(const BYTE *bp, int *total, DWORD *version)
{
  DWORD       _version;
  DWORD       _total;

  if (get_dword(&bp) != 0x44495243)   /* 0x43524944 */
    {
      return NULL;
    }

      _version = get_dword(&bp);
      _total   = get_dword(&bp);

      if (version)
        {
          *version = _version;
        }

      if (total)
        {
          *total = _total;
        }

  return bp;
}

/* ----------------------------------------------------------------- */
static const BYTE *Body(const BYTE *bp, const TCHAR *root, GIT_NODE *g_idx)
{
  WORD         len;
  int          i;
  CHAR         cc[MAX_PATH];
  const BYTE  *top = bp;
  DWORD        temp;

  /* ctime  */

  temp = get_dword(&bp); get_dword(&bp);

  g_idx->ctime = temp;

  /* mtime  */

  temp = get_dword(&bp); get_dword(&bp);

  g_idx->mtime = temp;

  /* dev */

  g_idx->dev = get_dword(&bp);

  /* inode */

  g_idx->inode = get_dword(&bp);

  /* mode */

  g_idx->mode = get_dword(&bp);

  /* uid */

  g_idx->uid = get_dword(&bp);

  /* guid */

  g_idx->guid = get_dword(&bp);

  /* file size */

  g_idx->size = get_dword(&bp);

  /* sha1 */

  for (i = 0; i < 20; i++)
    {
      g_idx->sha1[i] = get_byte(&bp);
    }

  /* path length */

  len = get_word(&bp);

  /* path */

  for (i = 0; i < len; i++)
    {
      cc[i] = get_byte(&bp);
    }

  cc[i] = '\0';

  /* path terminate */

  get_byte(&bp);

  /* path conv */

  _stprintf_s(g_idx->path, MAX_PATH, _T("%s\\"), root);

  MultiByteToWideChar(CP_UTF8, 0U,
                      cc,
                      -1,
                      g_idx->path + _tcsclen(g_idx->path),
                      MAX_PATH);

  /* next point */

  bp = (BYTE*)(top + ((bp - top + 7) & ~7));

  return bp;
}

/* ----------------------------------------------------------------- */
static void debug_git_index(GIT_NODE *g_idx)
{
  /* ctime  */

  _tprintf(L"ctime = %08llX\n", g_idx->ctime);

  /* mtime  */

  _tprintf(L"mtime = %08llX\n", g_idx->mtime);

  /* dev */

  _tprintf(L"%08X\n", g_idx->dev);

  /* inode */

  _tprintf(L"%08X\n", g_idx->inode);

  /* mode */

  _tprintf(L"mode = %08X\n", g_idx->mode);

  /* uid */

  _tprintf(L"%08X\n", g_idx->uid);

  /* guid */

  _tprintf(L"%08X\n", g_idx->guid);

  /* sha1 */

  _tprintf(L"sha1:"
            "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\n",
    g_idx->sha1[0],
    g_idx->sha1[1],
    g_idx->sha1[2],
    g_idx->sha1[3],
    g_idx->sha1[4],
    g_idx->sha1[5],
    g_idx->sha1[6],
    g_idx->sha1[7],
    g_idx->sha1[8],
    g_idx->sha1[9],
    g_idx->sha1[10],
    g_idx->sha1[11],
    g_idx->sha1[12],
    g_idx->sha1[13],
    g_idx->sha1[14],
    g_idx->sha1[15],
    g_idx->sha1[16],
    g_idx->sha1[17],
    g_idx->sha1[18],
    g_idx->sha1[19]);

  _tprintf(L"[%s]\n", g_idx->path);
}

/* ----------------------------------------------------------------- */
static int search(const TCHAR *target, time_t mtime)
{
  SAVE_INDEX_T *idx = _top_idx.next;
  int           res = 0;

  while (idx)
    {
      if (_tcscmp(idx->path, target) == 0)
        {
          if (idx->mtime == mtime)
            {
              return 0;
            }

          idx->mtime = mtime;

          return 1;
        }

      idx = idx->next;
    }

  size_t bsize = _tcslen(target) + 1;

  idx = (SAVE_INDEX_T *)malloc(sizeof(SAVE_INDEX_T) + bsize * sizeof(TCHAR));

  if (idx == NULL)
    {
      return -1;
    }

  _stprintf_s(idx->path, bsize, _T("%s"), target);

  idx->mtime = mtime;

  idx->next = _top_idx.next;

  _top_idx.next = idx;

  return 1;
}

/* ----------------------------------------------------------------- */
static time_t From_FILETIME_to_time_t(FILETIME *from_time)
{
  /* Constant for converting from windows time to linux time */

  const time_t unix_epoch_offset = 116444736000000000LL;

  union _tag {
    FILETIME times;
    time_t   qpart;
  }
  *update = (union _tag *)from_time;

  return (update->qpart - unix_epoch_offset) / 10000000;
}

/* ----------------------------------------------------------------- */
static HANDLE open_git_index(const TCHAR *input, TCHAR *git_path, size_t size, int force)
{
  TCHAR   path[MAX_PATH];
  HANDLE  hFile = INVALID_HANDLE_VALUE;

  _tcscpy_s(git_path, size, input);

  PathRemoveFileSpec(git_path);

  do
    {
      _stprintf_s(path, MAX_PATH, _T("%s\\.git\\index"), git_path);

      hFile = CreateFile(path,
                         GENERIC_READ,
                         FILE_SHARE_READ,
                         NULL,
                         OPEN_EXISTING,
                         FILE_ATTRIBUTE_NORMAL,
                         NULL);

      if (hFile == INVALID_HANDLE_VALUE)
        {
          continue;
        }

      FILETIME times;

      GetFileTime(hFile, NULL, NULL, &times);

      if (search(path, From_FILETIME_to_time_t(&times)) <= 0)
        {
          if (!force)
            {
              CloseHandle(hFile);
              hFile = NULL;
            }
        }

      break;
    }
  while (PathRemoveFileSpec(git_path));

  return hFile;
}

/* ----------------------------------------------------------------- */
static int scan_git_index(const BYTE        *bp,
                          const TCHAR       *git_path,
                          xgit_node_callback cb,
                          void              *p_param)
{
  int       total = 0;
  DWORD     version;
  GIT_NODE  g_idx;

  bp = Header(bp, &total, &version);

  if (!bp)
    {
      return 0;
    }

  g_idx.total = total;

  for (int i = 0; i < total; i++)
    {
      bp = Body(bp, git_path, &g_idx);

      g_idx.cnt = i + 1;

      /* Add file info */

      if (cb)
        {
          if (cb(&g_idx, git_path, p_param) < 0)
            {
              break;
            }
        }
    }

  return total;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
int check_git_hash(const TCHAR *path, BYTE *sha1)
{
  BCRYPT_ALG_HANDLE  h_alg  = NULL;
  BCRYPT_HASH_HANDLE h_hash = NULL;
  NTSTATUS           status = 0;
  int                res    = -1;
  HANDLE             hFile  = INVALID_HANDLE_VALUE;

  /* Open target file */

  hFile = CreateFile(path,
                     GENERIC_READ,
                     FILE_SHARE_READ,
                     NULL,
                     OPEN_EXISTING,
                     FILE_ATTRIBUTE_NORMAL,
                     NULL);

  if (hFile == INVALID_HANDLE_VALUE)
    {
      _tprintf(_T("*** File(%s:%d) not found\n"), path, errno);
      goto errout;
    }

  /* Open algorithm provider */

  if ((status = BCryptOpenAlgorithmProvider(&h_alg,
                                            BCRYPT_SHA1_ALGORITHM,
                                            NULL,
                                            0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptOpenAlgorithmProvider()=%d\n"), status);
      goto errout;
    }

  /* Create hash */

  if ((status = BCryptCreateHash(h_alg,
                                 &h_hash,
                                 NULL,
                                 0,
                                 NULL,
                                 0,
                                 0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptCreateHash()=%d\n"), status);
      goto errout;
    }

  /* Create hash value */

  BYTE  fdata[1024];
  DWORD fsize = GetFileSize(hFile, NULL);

  /* header section */

  fsize = snprintf(fdata, sizeof(fdata), "blob %d", fsize);

  if ((status = BCryptHashData(h_hash,
                               (PBYTE)fdata,
                               fsize + 1,
                               0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptHashData()=%d\n"), status);
      goto errout;
    }

  /* file section */

  for (;;)
    {
      if (ReadFile(hFile, fdata, sizeof(fdata), &fsize, NULL) == FALSE)
        {
          break;
        }

      /* hash some data */

      if ((status = BCryptHashData(h_hash,
                                   (PBYTE)fdata,
                                   fsize,
                                   0)) < 0)
        {
          _tprintf(_T("*** Failed to BCryptHashData()=%d\n"), status);
          goto errout;
        }

      if (fsize == 0)
        {
          break;
        }
    }

  /* Close hash */

  BYTE hash[20] = {0};

  if ((status = BCryptFinishHash(h_hash,
                                 hash,
                                 sizeof(hash),
                                 0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptFinishHash()=%d\n"), status);
      goto errout;
    }

  /* Successful acquisition of hash value */

  res = memcmp(hash, sha1, sizeof(hash)) ? 1 : 0;

#if 0
  _tprintf(L"path:%s\n", path);

  _tprintf(L"sha1:"
            "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\n",
    sha1[0],
    sha1[1],
    sha1[2],
    sha1[3],
    sha1[4],
    sha1[5],
    sha1[6],
    sha1[7],
    sha1[8],
    sha1[9],
    sha1[10],
    sha1[11],
    sha1[12],
    sha1[13],
    sha1[14],
    sha1[15],
    sha1[16],
    sha1[17],
    sha1[18],
    sha1[19]);
  _tprintf(L"sha2:"
            "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\n",
    hash[0],
    hash[1],
    hash[2],
    hash[3],
    hash[4],
    hash[5],
    hash[6],
    hash[7],
    hash[8],
    hash[9],
    hash[10],
    hash[11],
    hash[12],
    hash[13],
    hash[14],
    hash[15],
    hash[16],
    hash[17],
    hash[18],
    hash[19]);
#endif

errout:
  /* Cleanup */

  if(h_alg)
    {
      BCryptCloseAlgorithmProvider(h_alg,0);
    }

  if (h_hash)
    {
      BCryptDestroyHash(h_hash);
    }

  if (hFile != INVALID_HANDLE_VALUE)
    {
      CloseHandle(hFile);
    }

  return res;
}

/* ----------------------------------------------------------------- */
int check_git_params(GIT_NODE *node)
{
  BCRYPT_ALG_HANDLE  h_alg  = NULL;
  BCRYPT_HASH_HANDLE h_hash = NULL;
  NTSTATUS           status = 0;
  int                res    = -1;
  HANDLE             hFile  = INVALID_HANDLE_VALUE;

  /* Open target file */

  hFile = CreateFile(node->path,
                     GENERIC_READ,
                     FILE_SHARE_READ,
                     NULL,
                     OPEN_EXISTING,
                     FILE_ATTRIBUTE_NORMAL,
                     NULL);

  if (hFile == INVALID_HANDLE_VALUE)
    {
      _tprintf(_T("*** File(%s:%d) not found\n"), node->path, errno);
      goto errout;
    }

  /* Get file status */

  FILETIME times;

  GetFileTime(hFile, NULL, NULL, &times);

  /* Compare file modified time */

  res = difftime(From_FILETIME_to_time_t(&times), node->mtime) ? -1 : 0;

  if (res == 0)
    {
      goto errout;
    }

  /* Open algorithm provider */

  if ((status = BCryptOpenAlgorithmProvider(&h_alg,
                                            BCRYPT_SHA1_ALGORITHM,
                                            NULL,
                                            0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptOpenAlgorithmProvider()=%d\n"), status);
      goto errout;
    }

  /* Create hash */

  if ((status = BCryptCreateHash(h_alg,
                                 &h_hash,
                                 NULL,
                                 0,
                                 NULL,
                                 0,
                                 0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptCreateHash()=%d\n"), status);
      goto errout;
    }

  /* Create hash value */

  BYTE  fdata[1024];
  DWORD fsize = GetFileSize(hFile, NULL);

  /* header section */

  fsize = snprintf(fdata, sizeof(fdata), "blob %d", fsize);

  if ((status = BCryptHashData(h_hash,
                               (PBYTE)fdata,
                               fsize + 1,
                               0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptHashData()=%d\n"), status);
      goto errout;
    }

  /* file section */

  for (;;)
    {
      if (ReadFile(hFile, fdata, sizeof(fdata), &fsize, NULL) == FALSE)
        {
          break;
        }

      /* hash some data */

      if ((status = BCryptHashData(h_hash,
                                   (PBYTE)fdata,
                                   fsize,
                                   0)) < 0)
        {
          _tprintf(_T("*** Failed to BCryptHashData()=%d\n"), status);
          goto errout;
        }

      if (fsize == 0)
        {
          break;
        }
    }

  /* Close hash */

  BYTE hash[20] = {0};

  if ((status = BCryptFinishHash(h_hash,
                                 hash,
                                 sizeof(hash),
                                 0)) < 0)
    {
      _tprintf(_T("*** Failed to BCryptFinishHash()=%d\n"), status);
      goto errout;
    }

  /* Successful acquisition of hash value */

  res = memcmp(hash, node->sha1, sizeof(hash)) ? 1 : 0;

errout:
  /* Cleanup */

  if(h_alg)
    {
      BCryptCloseAlgorithmProvider(h_alg,0);
    }

  if (h_hash)
    {
      BCryptDestroyHash(h_hash);
    }

  if (hFile != INVALID_HANDLE_VALUE)
    {
      CloseHandle(hFile);
    }

  return res;
}

/* ----------------------------------------------------------------- */
int scan_git_dir(const TCHAR dir[], int force, xgit_node_callback cb, void *p_param)
{
  HANDLE  hFile;
  TCHAR   path[MAX_PATH];
  DWORD   size_lo;
  DWORD   size_hi;
  DWORD   size_ot;
  BYTE   *bp;
  int     total = 0;

  /* Get git index file */

  hFile = open_git_index(dir, path, MAX_PATH, force);

  if (hFile == INVALID_HANDLE_VALUE)
    {
      return 0;
    }

  /* .git/index reading analysis */

  size_lo = GetFileSize(hFile, &size_hi);

  if (size_lo == INVALID_FILE_SIZE)
    {
      /* File size acquisition failure! */

      goto errout;
    }

  if (size_hi > 0)
    {
      /* File size is too large! */

      goto errout;
    }

  if ((bp = (BYTE *)malloc(size_lo)) == NULL)
    {
      /* Memory allocate error! */

      goto errout;
    }

  if (ReadFile(hFile, bp, size_lo, &size_ot, NULL))
    {
      /* Analysis */

      total = scan_git_index(bp, path, cb, p_param);
    }

  free(bp);

errout:
  CloseHandle(hFile);

  return total;
}
