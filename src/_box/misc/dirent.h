/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: scandir.h
 *  Created  : 11/03/03(水) 09:46:44
 *
 *  Function
 *    Scan folder
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:48 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DIRENT_H_
#define _DIRENT_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Public type declarations
 * ----------------------------------------------------------------- */

typedef struct dirent
{
  size_t         size;
  int            d_cnt;
  struct dirent *next;
  int            depth;
  int            state;
  void          *param;
  DWORD          dwFileAttributes;
  time_t         ctime;
  time_t         atime;
  time_t         mtime;
  DWORD          nFileSizeHigh;
  DWORD          nFileSizeLow;
  DWORD          bookMark;
  DWORD          userData;
  HANDLE         hHandle;
  TCHAR          cAlternateFileName[14];
  TCHAR          d_name[1];
}
dirent;

#ifdef __cplusplus
}
#endif

#endif /* _DIRENT_H_ */
