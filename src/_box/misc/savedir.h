/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: savedir.h
 *  Created  : 11/03/03(水) 09:46:44
 *
 *  Function
 *    パス名を保持する
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:48 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _SAVEDIR_H_
#define _SAVEDIR_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Public type declarations
 * ----------------------------------------------------------------- */

typedef struct DIR_NODE_T
{
  struct DIR_NODE_T *next;
  struct DIR_NODE_T *subd;
  struct DIR_NODE_T *parent;
  DWORD              d_no;
  time_t             mtime;
  HANDLE             hHandle;
  HANDLE             hHeap;
  BYTE               sha1[20];
  union {
    struct {
      DWORD64        g_idx : 8;
      DWORD64        gtime : 56;
    };
    DWORD64          param;
  };
  TCHAR              d_name[1];
}
D_NODE;

typedef int (*savedir_node_callback)(D_NODE *p_node, void *p_param);

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

void    savedir_delete(void);
D_NODE *savedir_node_search(D_NODE *p_parent, int depth, TCHAR *elem);
D_NODE *savedir(const TCHAR *path);
D_NODE *savedir_search(D_NODE *p_node, savedir_node_callback cb, void *p_param);

#ifdef __cplusplus
}
#endif

#endif /* _SAVEDIR_H_ */
