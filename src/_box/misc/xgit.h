/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2019 ****, Inc.
 *
 *  File Name: xgit.h
 *  Created  : 19/11/23
 *
 *  Function
 *    Scan folder
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:48 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _XGIT_H_
#define _XGIT_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Public types
 * ----------------------------------------------------------------- */

typedef struct
{
  time_t  ctime;
  time_t  mtime;
  DWORD   dev;
  DWORD   inode;
  DWORD   mode;
  DWORD   uid;
  DWORD   guid;
  DWORD   size;
  BYTE    sha1[20];
  TCHAR   path[MAX_PATH];
  int     cnt;
  int     total;
}
GIT_NODE;

typedef int (*xgit_node_callback)(GIT_NODE *p_node, const TCHAR *git_dir, void *p_param);

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

int scan_git_dir(const TCHAR dir[], int force, xgit_node_callback cb, void *p_param);
int check_git_params(GIT_NODE *node);
int check_git_hash(const TCHAR *path, BYTE *sha1);

#ifdef __cplusplus
}
#endif

#endif /* _XGIT_H_ */
