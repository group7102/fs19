/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2019 ****, Inc.
 *
 *  File Name: savedir.cpp
 *  Created  : 19/12/01(日) 09:46:44
 *
 *  Function
 *    Preserve path name
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:48 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

/* ----------------------------------------------------------------- *
 * Included files
 * ----------------------------------------------------------------- */

#include <windows.h>
#include <string.h>
#include <malloc.h>
#include <tchar.h>
#include "savedir.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private data
 * ----------------------------------------------------------------- */

/* Top node */

static D_NODE*  _node_top = NULL;

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static D_NODE* Malloc(HANDLE hHeap, TCHAR *target)
{
  D_NODE* p_node;
  size_t  size;
  size_t  str_len = _tcslen(target) + 1;

  size = sizeof(D_NODE) + str_len * sizeof(TCHAR);

  p_node = (D_NODE *)HeapAlloc(hHeap, HEAP_NO_SERIALIZE, size);

  if (p_node)
    {
      _tcscpy_s(p_node->d_name, str_len, target);
      p_node->hHeap    = hHeap;
      p_node->parent   = NULL;
      p_node->subd     = NULL;
      p_node->mtime    = 0;
      p_node->gtime    = 0;
      p_node->g_idx    = 0;
      p_node->d_no     = 0;
      memset(p_node->sha1, 0, sizeof(p_node->sha1));
    }

  return p_node;
}

/* ----------------------------------------------------------------- */
static D_NODE *create(void)
{
  if (_node_top)
    {
      return _node_top;
    }

  /* Create memory heap */

  HANDLE  hHeap = HeapCreate(HEAP_NO_SERIALIZE, 0, 0);

  if (!hHeap)
    {
      return NULL;
    }

  /* Initialize top node */

  D_NODE  *p_topd = (D_NODE *)HeapAlloc(hHeap, HEAP_NO_SERIALIZE, sizeof(D_NODE));

  if (p_topd)
    {
      p_topd->parent    = NULL;
      p_topd->next      = NULL;
      p_topd->subd      = NULL;
      p_topd->hHeap     = hHeap;
      p_topd->d_name[0] = _T('\0');
      _node_top         = p_topd;
    }
  else
    {
      /* Although heap allocation was possible, memory allocation failed */

      HeapDestroy(hHeap);
    }

  return p_topd;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
D_NODE *savedir_node_search(D_NODE *p_parent, int depth, TCHAR *elem)
{
  D_NODE *p_node = p_parent->subd;
  HANDLE  hHeap  = p_parent->hHeap;

  while (1)
    {
      if (p_node == NULL)
        {
          p_node = Malloc(hHeap, elem);

          if (p_node == NULL)
            {
              break;
            }

          p_node->parent = p_parent;
          p_node->next   = p_parent->subd;
          p_parent->subd = p_node;

          break;
        }

      if (_tcsicmp(p_node->d_name, elem) == 0)
        {
          break;
        }

      p_node = p_node->next;
    }

  return p_node;
}

/* ----------------------------------------------------------------- */
void savedir_delete(void)
{
  if (_node_top)
    {
      HeapDestroy(_node_top->hHeap);
      _node_top = NULL;
    }
}

/* ----------------------------------------------------------------- */
static D_NODE *savedir_sub(D_NODE *p_node, const TCHAR *path)
{
  TCHAR   *cp;
  TCHAR   *buf;
  TCHAR   *ctxt = NULL;
  size_t   len;

  len = _tcslen(path) + 1;

  buf = (TCHAR *)alloca(len * sizeof(TCHAR));

  _tcscpy_s(buf, len, path);

  cp = _tcstok_s(buf, L"\\/", &ctxt);

  for (int i = 0; p_node && cp; cp = _tcstok_s(NULL, L"\\/", &ctxt), i++)
    {
        p_node = savedir_node_search(p_node, i, cp);
    }

  return p_node;
}

/* ----------------------------------------------------------------- */
D_NODE *savedir(const TCHAR *path)
{
  D_NODE *p_node = create();

  return savedir_sub(p_node, path);
}

/* ----------------------------------------------------------------- */
static D_NODE *next_search(D_NODE *p_node, savedir_node_callback cb, void *p_param)
{
  while (p_node)
    {
      if (cb && cb(p_node, p_param) != 0)
        {
          break;
        }

      if (p_node->subd)
        {
          next_search(p_node->subd, cb, p_param);
        }

      p_node = p_node->next;
    }

  return p_node;
}

/* ----------------------------------------------------------------- */
D_NODE *savedir_search(D_NODE *p_node, savedir_node_callback cb, void *p_param)
{
  if (p_node == NULL)
    {
      return NULL;
    }

  if (cb && cb(p_node, p_param) != 0)
    {
      return p_node;
    }

  return next_search(p_node->subd, cb, p_param);
}
