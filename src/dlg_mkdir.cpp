/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_mkdir.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    ディレクトリの作成
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "resource.h"
#include "com_edit.h"
#include "com_button.h"
#include "xpt.h"
#include "dirent.h"
#include "icon.h"
#include "sub_funcs.h"
#include "dlg_mkdir.h"

/* ----------------------------------------------------------------- */
dlg_mkdir::dlg_mkdir(HWND hParent) : xDialog(_T("IDD_MKDIRDLG"), hParent)
{
  m_mode = 0;
  mpFont = new xFont;
  mpEdit = new com_edit;

  mpOK     = new com_button;
  mpCancel = new com_button;

  IcoSetPsMode(&mpOK->icon_no, &mpCancel->icon_no);
}

/* ----------------------------------------------------------------- */
dlg_mkdir::~dlg_mkdir()
{
  delete mpFont;
  delete mpEdit;
  delete mpOK;
  delete mpCancel;
}

/* ----------------------------------------------------------------- *
 * Dialog initialization
 * ----------------------------------------------------------------- */
BOOL dlg_mkdir::OnInitDialog(HWND hwndFocus, LPARAM lParam)
{
  CenterWindow();   // Move in the middle

  Attach(mpEdit, IDC_EDIT1);
  mpEdit->SetFont(*mpFont);
  mpEdit->SetFocus();
  mpEdit->SendKeyDown(VK_UP, TRUE, 0, 0);

  Attach(mpOK,     IDOK2);
  Attach(mpCancel, IDCANCEL2);

  return 0;
}

/* ----------------------------------------------------------------- *
 * Command processing
 * ----------------------------------------------------------------- */
void dlg_mkdir::OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  TCHAR         buf[MAX_PATH];
  SYSTEMTIME    systime;
  const TCHAR*  ext[] = { _T(""), _T(".txt") };
  HANDLE        hFile;
  int           mkdir;

  switch(id)
    {
      case IDC_EDIT1:
        if ((codeNotify & 0xFF00) != ICOMEDIT_KEY)
          {
            break;
          }

        ::GetLocalTime(&systime);

        wsprintf(buf, _T("%02d%02d%02d_%02d%02d%02d_%03d%s"),
          systime.wYear % 100,
          systime.wMonth,
          systime.wDay,
          systime.wHour,
          systime.wMinute,
          systime.wSecond,
          systime.wMilliseconds % 1000, ext[m_mode]);

        mpEdit->SetText(buf);
        mpEdit->SetSel(0, 17);

        DispTitle(m_mode);

        m_mode = !m_mode;

        break;

      case IDOK:
      case IDOK2:
        mpEdit->GetText(buf, MAX_PATH - 1);

        wsprintf(m_buf, _T("%s%s"), m_dir, buf);

        if (m_mode)
          {
            PathAddBackslash(m_buf);
            mkdir = ::SHCreateDirectoryEx(mhWnd, m_buf, 0);

            if (mkdir == ERROR_SUCCESS)
              {
                ;
              }
            else if (mkdir == ERROR_BAD_PATHNAME)
              {
                break;
              }
            else if (mkdir == ERROR_FILENAME_EXCED_RANGE)
              {
                break;
              }
            else if (mkdir == ERROR_PATH_NOT_FOUND)
              {
                break;
              }
            else if (mkdir == ERROR_FILE_EXISTS)
              {
                break;
              }
            else if (mkdir == ERROR_ALREADY_EXISTS)
              {
                break;
              }
            else if (mkdir == ERROR_CANCELLED)
              {
                break;
              }
            else
              {
                break;
              }
          }
        else
          {
            hFile = ::CreateFile(
                  m_buf,
                  GENERIC_READ,
                  0, //FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
                  NULL,
                  CREATE_NEW,
                  FILE_ATTRIBUTE_NORMAL,
                  NULL);

            if (hFile == INVALID_HANDLE_VALUE)
              {
                break;
              }

            ::CloseHandle(hFile);
          }

        EndDialog(IDOK);
        break;

      case IDCANCEL:
      case IDCANCEL2:
        EndDialog(IDCANCEL);
        break;

      default:
        break;
    }
}

/* ----------------------------------------------------------------- */
void dlg_mkdir::SetFont(const TCHAR *name, int size)
{
  mpFont->Create(name, size);
}

/* ----------------------------------------------------------------- */
void dlg_mkdir::SetDirectory(const TCHAR *name)
{
  _tcsncpy_s(m_dir, MAX_PATH, name, MAX_PATH);
  PathAddBackslash(m_dir);
}

/* ----------------------------------------------------------------- */
TCHAR *dlg_mkdir::GetSel(void)
{
  StrTrimRight(m_buf, _T("\\"));
  return m_buf;
}

/* ----------------------------------------------------------------- */
void dlg_mkdir::OnPaint()
{
  xPaintDC dc(mhWnd);
  RECT     rc;
  IPT     *pt = xPt();

  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(pt->color.title);
  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  GetClientRect(&rc);

  dc.FillRect(&rc, pt->color.base_bar);

  DispTitle(0);
}

/* ----------------------------------------------------------------- */
void dlg_mkdir::DispTitle(int mode)
{
  xDC   dc(mhWnd);
  RECT  rc;
  RECT  rc_2;
  IPT  *pt = xPt();

  GetClientRect(&rc);

  dc.SetTextColor(pt->color.title);
  dc.SetBkColor(pt->color.base_bar);
  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  rc_2 = rc;
  rc_2.bottom = rc_2.top + 24;

  dc.FillRect(&rc_2, pt->color.base_bar);

  if (mode) {
    dc.TextOut(rc.left + 28, rc.top + 4, L"Enter the file name");
    IcoDrawIconFromIndex(dc, 4, rc.top + 4, 103, TRUE);
  }
  else {
    dc.TextOut(rc.left + 28, rc.top + 4, L"Enter the directory name");
    IcoDrawIconFromIndex(dc, 4, rc.top + 4, 102, TRUE);
  }
}

/* ----------------------------------------------------------------- */
LRESULT dlg_mkdir::Message(UINT msg, WPARAM wParam, LPARAM lParam)
{
  LRESULT res = 0;

  switch(msg) {
  case WM_LBUTTONDOWN:
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, lParam);
    break;
  default:
    res = xDialog::Message(msg, wParam, lParam);
    break;
  }

  return res;
}
