/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: history.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _HISTORY_H_
#define _HISTORY_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

void HitPush(const TCHAR *path);

const TCHAR *HitGetAt(int idx, int *mark, DWORD *date);

const TCHAR *HitSetAt(int idx, int mark);

DWORD HitGetLocalDateTime(void);

const TCHAR *HitGetEventFromPath(const TCHAR *path, int *mark, DWORD *date);

void HitDelete(int idx);

#ifdef __cplusplus
}
#endif

#endif /* _HISTORY_H_ */
