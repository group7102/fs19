#include <shlobj_core.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h> 
#include "dirent.h"
#include "logf.h"
#include "command.h"
#include "sub_functions.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private parameters
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
HGLOBAL MakeHDropFiles2nd(dirent *top_entry)
{
  size_t     len = 0;
  HDROP      hDrop = NULL;
  DROPFILES *pFiles;
  dirent    *entry;

  /* Add header size part in advance */

  for (entry = top_entry; entry; entry = entry->next) {
    len += (entry->size - sizeof(dirent));
  }

  len += sizeof(DROPFILES);
  len += 2;  /* terminate */

  hDrop = (HDROP)GlobalAlloc(GHND, len);

  if (hDrop == NULL) {
    LOGE("GlobalAlloc:%d", len);
    return NULL;
  }

  pFiles = (DROPFILES*)GlobalLock(hDrop);

  ZeroMemory(pFiles, len);

  pFiles->pFiles  = sizeof(DROPFILES);
  pFiles->pt.x  = 0;
  pFiles->pt.y  = 0;
  pFiles->fNC   = 0;
  pFiles->fWide = 1;
  pFiles++;

  TCHAR *cp = (TCHAR *)pFiles;

  for (entry = top_entry; entry; entry = entry->next) {
    cp += __snprintf(cp,
                     (entry->size - sizeof(dirent)) / sizeof(TCHAR),
                     "%s",
                     entry->d_name);
    cp++;
  }

  *cp++ = '\0';

  GlobalUnlock(hDrop);

  return hDrop;
}
