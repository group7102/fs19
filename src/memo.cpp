/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: memo.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#include "_box.h"
#include "dirent.h"
#include "xpt.h"
#include "logf.h"
#include "cmd_exec.h"
#include "memo.h"

/* ----------------------------------------------------------------- */
memo::memo(int number) : xEdit(101)
{
  SetStyleEx(0);
  SetStyle(WS_VISIBLE | WS_CHILD | ES_MULTILINE | ES_AUTOVSCROLL);

  no = number;

  mpFont = new xFont;
}

/* ----------------------------------------------------------------- */
memo::~memo()
{
  Destroy();

  delete mpFont;
}

/* ----------------------------------------------------------------- */
int memo::GetSha1(const PBYTE buf, DWORD len, PBYTE sha1)
{
  BCRYPT_ALG_HANDLE  h_alg  = NULL;
  BCRYPT_HASH_HANDLE h_hash = NULL;
  NTSTATUS           status = 0;
  int                res    = -1;

  /* Open algorithm provider */

  if ((status = BCryptOpenAlgorithmProvider(&h_alg,
                                            BCRYPT_SHA1_ALGORITHM,
                                            NULL,
                                            0)) < 0)
    {
      LOGE("*** Failed to BCryptOpenAlgorithmProvider()=%d\n", status);
      goto errout;
    }

  /* Create hash */

  if ((status = BCryptCreateHash(h_alg,
                                 &h_hash,
                                 NULL,
                                 0,
                                 NULL,
                                 0,
                                 0)) < 0)
    {
      LOGE("*** Failed to BCryptCreateHash()=%d\n", status);
      goto errout;
    }

  /* Create hash value */

  if ((status = BCryptHashData(h_hash,
                               buf,
                               len,
                               0)) < 0)
    {
      LOGE("*** Failed to BCryptHashData()=%d\n", status);
      goto errout;
    }

  /* Close hash */

  if ((status = BCryptFinishHash(h_hash,
                                 sha1,
                                 20,
                                 0)) < 0)
    {
      LOGE("*** Failed to BCryptFinishHash()=%d\n", status);
      goto errout;
    }

  /* Successful acquisition of hash value */

  res = 0;

errout:
  /* Cleanup */

  if(h_alg)
    {
      BCryptCloseAlgorithmProvider(h_alg,0);
    }

  if (h_hash)
    {
      BCryptDestroyHash(h_hash);
    }

  return res;
}

/* ----------------------------------------------------------------- */
void memo::SetUtf8ForWindowText(char *buf)
{
  size_t  size;
  TCHAR  *out;
  int     len = MultiByteToWideChar(CP_UTF8,
                                    0U,
                                    buf,
                                    -1,
                                    NULL,
                                    0);
  size = len * sizeof(TCHAR);
  out  = (TCHAR *)malloc(size);

  if (out == NULL)
    {
      LOGE("malloc");
      return;
    }

  memset(out, 0, size);

  MultiByteToWideChar(CP_UTF8,
                      0U,
                      buf,
                      -1,
                      out,
                      (int)size);
  SetWindowText(out);

  /* Get SHA1 */

  GetSha1((PBYTE)out, len * sizeof(TCHAR), sha1_1);

  /* Clean up */

  free(out);
}

/* ----------------------------------------------------------------- */
char *memo::ReadFile(size_t *p_len)
{
  /* Set memo file name */

  IPT *pt = xPt();

  TCHAR *memo = no == 0 ? pt->memo.file1 : pt->memo.file2;

  if (memo[0] == L'\0')
    {
      TCHAR  temp[MAX_PATH];

      GetModuleFileName(NULL, temp, sizeof(temp));
      PathRemoveFileSpec(temp);
      PathAddBackslash(temp);
      _stprintf_s(memo, MAX_PATH, L"%smemo%d.txt", temp, no);
    }

  /* Get memo file */

  FILE *fp  = NULL;
  CHAR *buf = NULL;

  if (_tfopen_s(&fp, memo, _T("rb")) != 0 || fp == NULL)
    {
      if (_tfopen_s(&fp, memo, _T("wb")) != 0 || fp == NULL)
        {
          LOGE("_tfopen_s/fp=%p", fp);
          goto errout;
        }
    }

  struct _stat st;

  if (_tstat(memo, &st) != 0 || st.st_size == 0)
    {
      LOGE("_tstat");
      goto errout;
    }

  size_t  bsize = st.st_size + 1;

  buf = (char *)malloc(bsize);

  if (buf == NULL)
    {
      LOGE("malloc");
      goto errout;
    }

  bsize = fread(buf, 1, bsize, fp);

  fclose(fp);

  if (bsize == 0)
    {
      LOGE("fread");
      goto errout;
    }

  /* Success */

  if (p_len)
    {
      *p_len = bsize;
    }

  return buf;

errout:
  /* Clean up */

  if (fp)
    {
      fclose(fp);
    }

  if (buf)
    {
      free(buf);
    }

  return NULL;
}

/* ----------------------------------------------------------------- */
char *memo::WindowTextToUTF8(size_t *p_len)
{
  TCHAR *buf = NULL;
  char  *cp  = NULL;

  /* Get text */

  int len = GetWindowTextLength() + 1;

  buf = (TCHAR *)malloc(len * sizeof(TCHAR));

  if (buf == NULL)
    {
      goto errout;
    }

  GetWindowText(buf, len);

  /* Get SHA1 */

  GetSha1((PBYTE)buf, len * sizeof(TCHAR), sha1_2);

  /* To UTF8 */

  len = WideCharToMultiByte(CP_UTF8, 0, buf, -1, 0, 0, NULL, NULL);

  cp = (char *)malloc(len + 1);

  if (cp == NULL)
    {
      goto errout;
    }

  WideCharToMultiByte(CP_UTF8, 0, buf, -1, cp, len, 0, 0);

  free(buf);

  /* Success */

  *p_len = (size_t)len;

  return cp;

errout:
  /* Clean up */

  if (buf)
    {
      free(buf);
    }

  if (cp)
    {
      free(cp);
    }

  return NULL;
}

/* ----------------------------------------------------------------- */
int memo::Update(BOOL is_force)
{
  int  res = IDNO;

  /* Get text */

  char  *buf;
  size_t len;

  if ((buf = WindowTextToUTF8(&len)) == NULL)
    {
      return res;
    }

  if (!is_force)
    {
      /* Check if it's updated */

      if (memcmp(sha1_1, sha1_2, sizeof(sha1_1)))
        {
          res = MessageBox(L"Updated. Save?",
                           L"Fs19(64bit)",
                           MB_YESNOCANCEL | MB_ICONINFORMATION);
          if (res == IDYES)
            {
              is_force = TRUE;
            }
        }
    }

  if (is_force)
    {
      /* Set memo file */

      FILE *fp = NULL;
      IPT  *pt = xPt();

      if (_tfopen_s(&fp, no == 0 ? pt->memo.file1 : pt->memo.file2, _T("wb")) == 0 && fp)
        {
          fwrite(buf, 1, len, fp);
          fclose(fp);

          /* Set sha1 */

          memcpy(sha1_1, sha1_2, sizeof(sha1_1));
        }
    }

  free(buf);

  return res;
}

/* ----------------------------------------------------------------- */
HWND memo::Create(HWND hParentWnd, int x, int y, int cx, int cy)
{
  /* Initialize */

  HWND hWnd = xControls::Create(hParentWnd, x, y, cx, cy);

  /* Set font */

  IPT *pt = xPt();

  mpFont->Create(pt->fonts[1].name, pt->fonts[1].size);

  SetFont(*mpFont);

  /* Set memo file */

  char *cp = ReadFile();

  if (cp)
    {
      SetUtf8ForWindowText(cp);
      free(cp);
    }

  /* Set caret */

  int cur = no == 0 ? pt->memo.cur1 : pt->memo.cur2;

  SetSel(cur >> 16, cur & 0x0000FFFF);

  Edit_ScrollCaret(mhWnd);

  return hWnd;
}

/* ----------------------------------------------------------------- */
void memo::OnDestroy(void)
{
  IPT *pt = xPt();

  if (no == 0)
    {
      pt->memo.cur1 = GetSel();
    }
  else
    {
      pt->memo.cur2 = GetSel();
    }

  xEdit::OnDestroy();
}

/* ----------------------------------------------------------------- */
int memo::PumpMessage(void)
{
  MSG  msg;
  int  res = 0;

  for (;;)
    {
      if (!PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
        {
          WaitMessage();
          continue;
        }

      if (msg.message == WM_QUIT)
        {
          break;
        }

      if (GetMessage(&msg, NULL, 0, 0) <= 0)
        {
          break;
        }

      if (msg.message == WM_SYSKEYDOWN)
        {
          /* Alt + Q */

          if (msg.wParam == 0x51)
            {
              if (Update() != IDCANCEL)
                {
                  break;
                }
            }

          /* Alt + A */

          if (msg.wParam == 0x41)
            {
              Update(TRUE);
              continue;
            }
        }
      else if (msg.message == WM_SETFOCUS)
        {
        }
      else if (msg.message == WM_KEYDOWN)
        {
          if (msg.wParam == VK_ESCAPE)
            {
              if (Update() != IDCANCEL)
                {
                  break;
                }
            }
          else if (msg.wParam == VK_TAB)
            {
              _boxMsgCore *parent = GetParent();

              parent->SendMessage(WM_KEYDOWN, msg.wParam, msg.lParam);
              parent->SetFocus();
              break;
            }
          else if (msg.wParam == VK_RETURN && HIBYTE(GetKeyState(VK_SHIFT)))
            {
              FuncEditor();
              break;
            }
        }

      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }

  /* IME forced off */

  HIMC himc = ImmGetContext(mhWnd);

  ImmSetOpenStatus(himc, FALSE);
  ImmReleaseContext(mhWnd, himc);

  return res;
}

/* ----------------------------------------------------------------- */
int memo::FuncEditor(void)
{
  IPT    *pt = xPt();
  TCHAR  *buf;
  TCHAR  *path = (no == 0) ? pt->memo.file1 : pt->memo.file2;
  size_t  len;

  len = _tcslen(pt->setting.editor) + 1 + 1 + _tcslen(path) + 1 + 1;

  buf = (TCHAR *)alloca(len * sizeof(TCHAR));

  _sntprintf_s(buf, len, _TRUNCATE, L"%s \"%s\"", pt->setting.editor, path);

  ExecCommand(buf);

  return 0;
}
