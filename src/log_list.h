/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: log_list.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _LOG_LIST_H_
#define _LOG_LIST_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */
class log_list : public xListBox
{
public:
  log_list();
  ~log_list();

public:
  HWND Create(HWND hParentWnd, int x, int y, int cx, int cy);
  void OnKeyDown(UINT vk, BOOL fDown, int cRepeat, UINT flags);

public:
  int  DrawItem(const DRAWITEMSTRUCT *lpDrawItem);
  int  MeasureItem(MEASUREITEMSTRUCT *lpMeasureItem);

public:
  int  PumpMessage(void);
  HWND SetFocus(void);
  int  GetSelectCount(void);
  int  GetSelectHash(TCHAR *hash1, TCHAR *hash2);
  int  AddTail(TCHAR *str);

public:
  TCHAR m_hash1[41];
  TCHAR m_hash2[41];

protected:
  void OnDestroy(void);

protected:
  xFont *mpFont;

private:
  BOOL m_enable;
  BOOL m_entry;

private:
  uint64_t GetHash(int idx);
  int      GetHash(int idx, TCHAR *hash);
};

#endif /* _LOG_LIST_H_ */
