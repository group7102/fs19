/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: com_edit.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _COM_EDIT_H_
#define _COM_EDIT_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define ICOMEDIT_KEY  0x8A00

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */
class com_edit : public xEdit
{
public:
  void OnKeyDown(UINT vk, BOOL fDown, int cRepeat, UINT flags);
};

#endif /* _COM_EDIT_H_ */
