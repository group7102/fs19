#ifndef _XPT_H_
#define _XPT_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

#define IPT_FONT_INDEX_NUM    (5)
#define IPT_SORT_FOLDERS_NUM  (30)
#define IPT_LIBRALY_NUM       (8)
#define IPT_QUEUE_FOLDER_NUM  (16)
#define IPT_HISTORY_EVENT_NUM (30)

/* ----------------------------------------------------------------- *
 * Public types
 * ----------------------------------------------------------------- */

typedef struct {
  /* Placement */

  WINDOWPLACEMENT place;

  /* Fonts */

  struct FONT_T {
    TCHAR name[LF_FACESIZE];
    int   size;
  }
  fonts[IPT_FONT_INDEX_NUM];

  /* Sort folders */

  struct SORT_FOLDERS_T {
    int num;
    struct {
      TCHAR path[MAX_PATH];
      int   type;
      int   order;
      int   column;
    }
    folders[IPT_SORT_FOLDERS_NUM];
  }
  sort;

  /* Colors */

  struct COLOR_T {
    COLORREF base_bar;
    COLORREF scroll_bar;
    COLORREF scroll_bar_sel;
    COLORREF base_bar_low;
    COLORREF split_line;
    COLORREF split_line2;
    COLORREF split_line_top;
    COLORREF split_line_btm;
    COLORREF split_line3;
    COLORREF selected;
    COLORREF selected_frame;
    COLORREF selected_cur;
    COLORREF progress_bar;
    COLORREF remain_bar_blue;
    COLORREF remain_bar_red;
    COLORREF title;
    COLORREF file_attr_system;
    COLORREF file_attr_hidden;
    COLORREF file_attr_readonly;
    COLORREF file_attr_directory;
    COLORREF file_attr_normal;
  }
  color;

  /* Setting */

  struct SETTING_T {
    TCHAR editor[MAX_PATH];
    TCHAR shortcut[MAX_PATH];
    TCHAR path1[MAX_PATH];
    TCHAR path2[MAX_PATH];
    int   col_mode;
    TCHAR wclist[MAX_PATH];
    TCHAR difftool[MAX_PATH];
    int   column;
    int   page_no;
    int   tool_size;
    int   line_pitch;
    int   sort_part;
    int   sort_order;
    int   ps_mode;
  }
  setting;

  /* Memo */

  struct MEMO_T {
    TCHAR file1[MAX_PATH];
    TCHAR file2[MAX_PATH];
    DWORD cur1;
    DWORD cur2;
  }
  memo;

  struct LIBRARY_T {
    int num;
    struct {
      TCHAR name[MAX_PATH];
      TCHAR path[MAX_PATH];
    }
    folders[IPT_LIBRALY_NUM];
  }
  library;

  struct SEARCH_T {
    int num;
    struct {
      int   mark;
      TCHAR path[MAX_PATH];
    }
    folders[IPT_QUEUE_FOLDER_NUM];
  }
  queue;

  struct HISTORY_T {
    int num;
    struct EVENT_T {
      int   mark;
      TCHAR path[MAX_PATH];
      DWORD date;
    }
    events[IPT_HISTORY_EVENT_NUM];
  }
  history;
}
IPT;

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

IPT  *xPtInit(void);
void  xPtFin(void);
IPT  *xPt(void);
void  xPtSetSortInfo(IPT *pt, TCHAR *path, int  column, int  type, int  order);
int   xPtGetSortInfo(IPT *pt, TCHAR *path, int *column, int *type, int *order);

#ifdef __cplusplus
}
#endif

#endif /* _XPT_H_ */
