#include <windows.h>
#include <tchar.h>
#include <shlwapi.h>
#include <stdio.h>
#include <stdint.h>
#include "logf.h"
#include "cmd_exec.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private parameters
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
HANDLE OpenProcessToFileHandle(const TCHAR *cmd, DWORD tmo, PUMP_CALLBACK cb, void *p_param)
{
  /* Make temporary file name */

  TCHAR  buf[MAX_PATH];
  TCHAR  tmp[MAX_PATH];

  GetTempPath(MAX_PATH, buf);
  GetTempFileName(buf, L"FS0", 0, tmp);

  /* Open file */

  HANDLE  hFile;
  HANDLE  hStdOutput = INVALID_HANDLE_VALUE;

  hFile = CreateFile(tmp,
                     GENERIC_WRITE | GENERIC_READ,
                     0,
                     NULL,
                     CREATE_ALWAYS,
                     FILE_ATTRIBUTE_NORMAL,
                     NULL);

  if (hFile == INVALID_HANDLE_VALUE)
    {
      LOGE("CreateFile");
      return INVALID_HANDLE_VALUE;
    }

  BOOL res = DuplicateHandle(GetCurrentProcess(),
                             hFile,
                             GetCurrentProcess(),
                             &hStdOutput,
                             0,
                             TRUE,
                             DUPLICATE_SAME_ACCESS);
  CloseHandle(hFile);

  if (!res)
    {
      LOGE("DuplicateHandle");
      return INVALID_HANDLE_VALUE;
    }

  /* Initialize parameters */

  STARTUPINFO         si;
  PROCESS_INFORMATION pi;

  memset(&si, 0, sizeof(si));
  memset(&pi, 0, sizeof(pi));

  si.cb          = sizeof(STARTUPINFO); 
  si.dwFlags     = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
  si.hStdOutput  = hStdOutput;
  si.wShowWindow = SW_HIDE;

  /* Set command */

  _stprintf_s(buf, MAX_PATH, L"%s", cmd);

  /* Exec command */

  res =  CreateProcess(NULL,
                       buf,
                       NULL,
                       NULL,
                       TRUE,
                       CREATE_NO_WINDOW,
                       NULL,
                       NULL,
                       &si,
                       &pi);
  if (!res)
    {
      LOGE("CreateProcess");
      goto errout;
    }

  /* Wait indefinitely for process to finish */

  DWORD  code;

  do
    {
      code = WaitForSingleObject(pi.hProcess, tmo);

      if (cb)
        {
          cb(p_param);
        }
    }
  while (code == WAIT_TIMEOUT);

  /* Close process handle */

  CloseHandle(pi.hThread);
  CloseHandle(pi.hProcess);

  if (code != WAIT_OBJECT_0)
    {
      LOGE("WaitForSingleObject(%d)", code);
      goto errout;
    }

  /* success */

  SetFilePointer(hStdOutput, 0, NULL, FILE_BEGIN);

  return hStdOutput;

errout:
  /* failure */

  CloseHandle(hStdOutput);

  return INVALID_HANDLE_VALUE;
}

/* ----------------------------------------------------------------- */
int OpenProcessToFile(const TCHAR  *cmd,
                      TCHAR        *file,
                      DWORD         len,
                      DWORD         tmo,
                      PUMP_CALLBACK cb,
                      void         *p_param)
{
  HANDLE h = OpenProcessToFileHandle(cmd, tmo, cb, p_param);

  if (h == INVALID_HANDLE_VALUE)
    {
      return -1;
    }

  /* Get file name from file handle */

  int res = GetFinalPathNameByHandle(h, file, len, 0) > 0 ? 0 : -1;

  CloseHandle(h);

  return res;
}

/* ----------------------------------------------------------------- */
char *OpenProcessToMemory(const TCHAR  *cmd, DWORD tmo, PUMP_CALLBACK cb, void *p_param)
{
  HANDLE h = OpenProcessToFileHandle(cmd, tmo, cb, p_param);

  if (h == INVALID_HANDLE_VALUE)
    {
      return NULL;
    }

  /* Extract file into memory */

  DWORD buf_size = GetFileSize(h, NULL);
  char *buf = NULL;
  DWORD size;

  if (buf_size == 0)
    {
      goto errout;
    }

  /* Allocate output data area */

  buf = (char *)malloc(buf_size + 1);

  if (buf == NULL)
    {
      LOGE("malloc");
      goto errout;
    }

  if (!ReadFile(h, buf, buf_size, &size, NULL))
    {
      LOGE("ReadFile");
      free(buf);
      buf = NULL;
    }

  buf[size] = '\0';

errout:
  TCHAR  tmp[MAX_PATH];

  DWORD res = GetFinalPathNameByHandle(h, tmp, MAX_PATH, 0);

  CloseHandle(h);

  if (res > 0)
    {
      DeleteFile(tmp);
    }

  return buf;
}

/* ----------------------------------------------------------------- */
int ExecProcess(const TCHAR *cmd, DWORD tmo, PUMP_CALLBACK cb, void *p_param)
{
  /* Set command */

  TCHAR  buf[MAX_PATH];

  _stprintf_s(buf, MAX_PATH, L"%s", cmd);

  /* Running child process */

  STARTUPINFO         si;
  PROCESS_INFORMATION pi;

  memset(&si, 0, sizeof(si));
  memset(&pi, 0, sizeof(pi));

  if (!CreateProcess(NULL,
                     buf,
                     NULL,
                     NULL,
                     FALSE,
                     0,
                     NULL,
                     NULL,
                     &si,
                     &pi))
    {
      LOGE("Merge tool failed to start!");
      return -1;
    }

  /* Wait for process end */

  DWORD  code;

  do
    {
      code = WaitForSingleObject(pi.hProcess, tmo);

      if (cb)
        {
          cb(p_param);
        }
    }
  while (code == WAIT_TIMEOUT);

  /* Close process handle */

  CloseHandle(pi.hThread);
  CloseHandle(pi.hProcess);

  if (code != WAIT_OBJECT_0)
    {
      LOGE("WaitForSingleObject(%d)", code);
      return -1;
    }

  return 0;
}

/* ----------------------------------------------------------------- */
BOOL ExecCommand(TCHAR *pCmdline)
{
  STARTUPINFO           startInfo;
  PROCESS_INFORMATION   procInfo;          /* Startup window state */
  BOOL                  is;

  /* Same state as the current process */

  GetStartupInfo(&startInfo);

  is = CreateProcess(NULL,                 /* Process path (EXE path) */
                     pCmdline,             /* Command line for process */
                     NULL,                 /* Handle inheritance (security for process) */
                     NULL,                 /* Handle inheritance (security for thread) */
                     FALSE,                /* Whether to inherit handles */
                     HIGH_PRIORITY_CLASS,  /* Process Type and Priority */
                     NULL,                 /* Environment block pointer */
                     NULL,                 /* Process path */
                     &startInfo,           /* Startup status */
                     &procInfo);           /* Process information */
  if (is) {
    /* Wait until the process is started */

    WaitForInputIdle(procInfo.hProcess, INFINITE);

    CloseHandle(&procInfo);
    CloseHandle(&startInfo);
  }

  return is;
}
