/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_setting.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    ディレクトリの作成
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DLG_SETTING_H_
#define _DLG_SETTING_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define DLG_SETTING_EDIT_CTRL_NUM  (5)
#define DLG_SETTING_STATE_CTRL_NUM (8)

/* ----------------------------------------------------------------- *
 * Class of use
 * ----------------------------------------------------------------- */

class xComboBox;
class xEdit;
class xStatic;
class com_button;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class xComboBoxEx : public xComboBox
{
public:
  xComboBoxEx();

public:
  int DrawItem(const DRAWITEMSTRUCT *pDIS);
  int MeasureItem(MEASUREITEMSTRUCT *lpMeasureItem);
  int Command(int id, UINT codeNotify);

  TCHAR mFontName[LF_FACESIZE];
  int   mFontSize;

  int GetItemInt(UINT itemID);
  int DrawItemA(const DRAWITEMSTRUCT *pDIS);
  int DrawItemB(const DRAWITEMSTRUCT *pDIS);

public:
  TCHAR *GetText(int idx = -1);
  int    GetFontName(TCHAR *name, size_t bsize = LF_FACESIZE, int idx = -1);
  int    GetFontSize(int idx = -1);
};

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class xStaticEx : public xStatic
{
public:
  xStaticEx();

public:
  void  OnPaint(void);

public:
  int index;
  int column;
};

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class dlg_setting : public xDialog
{
public:
#if 0
  BOOL SetFontDialog(TCHAR *face, int *size, int fixed = 0);
#endif
  void SetLogFont(int no, const TCHAR *fnam, int size);
  void GetLogFont(int no, TCHAR *face, int len, int *size);

public:
  dlg_setting(HWND hParent);
  ~dlg_setting();

public:
  BOOL  OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void  OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  void  OnPaint(void);

public:
  LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam);

private:
  void  SetListFontName(xComboBoxEx *cmb_a, xComboBoxEx *cmb_b, const TCHAR *cp);
  void  SetListFontSize(xComboBoxEx *cmb_a, xComboBoxEx *cmb_b, int size);
  void  SetOkElement(void);

protected:
  xComboBox *mpCmbo1;
  xComboBox *mpCmbo2;
  xComboBox *mpCmbo3;

  /* Font info */

  TCHAR  mFontName[DLG_SETTING_EDIT_CTRL_NUM][LF_FACESIZE];
  int    mFontSize[DLG_SETTING_EDIT_CTRL_NUM];

public:
  xComboBoxEx *mpCmboA[DLG_SETTING_EDIT_CTRL_NUM];
  xComboBoxEx *mpCmboB[DLG_SETTING_EDIT_CTRL_NUM];
  xStaticEx   *mpState[DLG_SETTING_STATE_CTRL_NUM];
  xStaticEx   *mpICons;
  xStaticEx   *mpColumn;
  com_button  *mpButton1;
  com_button  *mpButton2;
};

#endif /* _DLG_SETTING_H_ */
