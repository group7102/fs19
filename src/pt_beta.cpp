/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: pt_beta.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 */

#include <windows.h>
#include <stdio.h>
#include <shlwapi.h>
#include <tchar.h>
#include <locale.h>
#include "parson.h"
#include "history.h"
#include "logf.h"
#include "pt_beta.h"

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * private types
 * ----------------------------------------------------------------- */

/* Key function */
/*
typedef struct {
  //
  int   vk;
  int   ctl;
  int   sft;
  int   alt;
  int   f_no;
}
KEYS_T;
*/

/* ----------------------------------------------------------------- *
 * private parameters
 * ----------------------------------------------------------------- */

/* Window */

int       ipt::OK                       = 0;
int       ipt::x                        = CW_USEDEFAULT;
int       ipt::y                        = CW_USEDEFAULT;
int       ipt::cx                       = CW_USEDEFAULT;
int       ipt::cy                       = CW_USEDEFAULT;
int       ipt::show                     = SW_SHOWNORMAL;
int       ipt::column                   = 2;
int       ipt::page_no                  = 0;
int       ipt::tool_size                = 200;
int       ipt::line_pitch               = 0;
int       ipt::sort_part                = 0;
int       ipt::sort_order               = 1;

/* Colors */

COLORREF  ipt::COL_BASE_BAR             = RGB(255, 251, 240);
COLORREF  ipt::COL_SCROLL_BAR           = RGB(205, 205, 205);
COLORREF  ipt::COL_SCROLL_BAR_SEL       = RGB(166, 166, 166);
COLORREF  ipt::COL_BASE_BAR_LOW         = RGB(250, 250, 255);
COLORREF  ipt::COL_SPLIT_LINE           = RGB(217, 217, 217);
COLORREF  ipt::COL_SPLIT_LINE2          = RGB(192, 192, 192);
COLORREF  ipt::COL_SPLIT_LINE_TOP       = RGB(  7,   5,   5);
COLORREF  ipt::COL_SPLIT_LINE_BTM       = RGB(137, 145, 145);
COLORREF  ipt::COL_SPLIT_LINE3          = RGB(107, 105, 105);
COLORREF  ipt::COL_SELECTED             = RGB(204, 232, 255);
COLORREF  ipt::COL_SELECTED_FRAME       = RGB(153, 209, 255);
COLORREF  ipt::COL_SELECTED_CUR         = RGB(229, 243, 255);
COLORREF  ipt::COL_PROGRESS_BAR         = RGB(  0, 114, 198);
COLORREF  ipt::COL_REMAIN_BAR_BLUE      = RGB( 38, 160, 218);
COLORREF  ipt::COL_REMAIN_BAR_RED       = RGB(218,  38,  38);
COLORREF  ipt::COL_TITLE                = RGB( 0,   51, 153);
COLORREF  ipt::COL_FILE_ATTR_SYSTEM     = RGB( 15,   4,   0);
COLORREF  ipt::COL_FILE_ATTR_HIDDEN     = RGB( 15,   4,   0);
COLORREF  ipt::COL_FILE_ATTR_READONLY   = RGB( 15,   4,   0);
COLORREF  ipt::COL_FILE_ATTR_DIRECTORY  = RGB( 15,   4,   0);
COLORREF  ipt::COL_FILE_ATTR_NORMAL     = RGB( 15,   4,   0);

/* Font */

int       ipt::fontsize1                = -12;
TCHAR     ipt::fontname1[MAX_PATH]      = _T("ＭＳ ゴシック");
int       ipt::fontsize2                = -12;
TCHAR     ipt::fontname2[MAX_PATH]      = _T("ＭＳ ゴシック");
int       ipt::fontsize3                = -12;
TCHAR     ipt::fontname3[MAX_PATH]      = _T("ＭＳ ゴシック");
int       ipt::fontsize4                = -12;
TCHAR     ipt::fontname4[MAX_PATH]      = _T("ＭＳ ゴシック");
int       ipt::fontsize5                = -12;
TCHAR     ipt::fontname5[MAX_PATH]      = _T("ＭＳ ゴシック");

/* Setting */

TCHAR     ipt::editor[MAX_PATH]         = _T("notepad.exe");
TCHAR     ipt::shortcut[MAX_PATH]       = _T("");
TCHAR     ipt::path1[MAX_PATH]          = _T("");
TCHAR     ipt::path2[MAX_PATH]          = _T("");
int       ipt::COL_MODE                 = 0;
TCHAR     ipt::wclist[MAX_PATH]         = _T("");
TCHAR     ipt::difftool[MAX_PATH]       = _T("C:\\usr\\bin\\WinMerge\\WinMergeU.exe");

/* memo */

TCHAR     ipt::memo_file1[MAX_PATH]     = _T("");
TCHAR     ipt::memo_file2[MAX_PATH]     = _T("");
DWORD     ipt::memo_cur1                = 0;
DWORD     ipt::memo_cur2                = 0;

/* List */

int       ipt::libraly_num = 0;
TCHAR     ipt::libraly_name[30][MAX_PATH];
TCHAR     ipt::libraly_path[30][MAX_PATH];
int       ipt::search_folder_num = 0;
TCHAR     ipt::search_folders[8][MAX_PATH];
int       ipt::sort_folder_num = 0;
D_SORT_T  ipt::sort_folders[PT_SORT_FOLDERS_NUM];

/* Key function */
/*
KEYS_T keys[] = {
  {VK_RETURN, 0, 1, 0, PT_FUNC_EDITOR},
  {'L',       0, 0, 0, PT_FUNC_LOGDISK},
  {221,       0, 0, 0, PT_FUNC_LOGDISK},
  {VK_F5,     0, 0, 0, PT_FUNC_REFLESH},
  {'Z',       0, 0, 0, PT_FUNC_EXEC},
  {VK_RETURN, 1, 0, 0, PT_FUNC_BACK},
  {VK_DELETE, 1, 0, 0, PT_FUNC_STEP},
  {'E',       0, 0, 0, PT_FUNC_EJECT},
  {VK_INSERT, 1, 0, 0, PT_FUNC_COPY},
  {VK_INSERT, 0, 1, 0, PT_FUNC_PAST},
  {VK_F2,     0, 0, 0, PT_FUNC_RENAME},
  {'X',       0, 0, 0, PT_FUNC_SELEXE},
  {VK_INSERT, 0, 0, 0, PT_FUNC_INSERT},
  {VK_DELETE, 0, 0, 0, PT_FUNC_DELETE},
  {VK_APPS,   0, 0, 0, PT_FUNC_APPS},
  {'A',       0, 0, 0, PT_FUNC_ATTRIBUTE},
  {VK_F1,     0, 0, 0, PT_FUNC_SHORTCUT1},
  {VK_F11,    0, 0, 0, PT_FUNC_SHORTCUT2},
  {'Q',       0, 0, 0, PT_FUNC_SHORTCUT3},
  {'G',       0, 0, 0, PT_FUNC_SHORTCUT4},
  {'F',       0, 0, 0, PT_FUNC_FIND},
  {'1',       0, 0, 0, PT_FUNC_SORT},
  {'2',       0, 0, 0, PT_FUNC_SORT},
  {'3',       0, 0, 0, PT_FUNC_SORT},
  {'4',       0, 0, 0, PT_FUNC_SORT},
  {'1',       0, 1, 0, PT_FUNC_COLUMN},
  {'2',       0, 1, 0, PT_FUNC_COLUMN},
  {'3',       0, 1, 0, PT_FUNC_COLUMN},
  {'4',       0, 1, 0, PT_FUNC_COLUMN},
  {'D',       0, 0, 0, PT_FUNC_SHORTCUT5},
  {'D',       0, 1, 0, PT_FUNC_SHORTCUT5},
  {'D',       1, 0, 0, PT_FUNC_SHORTCUT5},
  {'M',       0, 0, 0, PT_FUNC_SHORTCUT6},
  {'L',       0, 0, 0, PT_FUNC_SHORTCUT7},
  {226,       0, 0, 0, PT_FUNC_APPS},
  {0,         0, 0, 0, PT_FUNC_NON},
};
*/

/* ----------------------------------------------------------------- *
 * private function                                                  *
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static const char *WtoC(const TCHAR *wbuf, char *cbuf, size_t bsize)
{
#ifdef _UNICODE
  WideCharToMultiByte(CP_UTF8,
                      0,
                      wbuf,
                      -1,
                      cbuf,
                      (int)bsize,
                      NULL,
                      NULL);

  return cbuf;
#else
  return strncpy(cbuf, wbuf, bsize);
#endif
}

/* ----------------------------------------------------------------- */
static const TCHAR *CtoW(const char *cbuf, TCHAR *wbuf, size_t bsize)
{
  if (cbuf == NULL)
    {
      return NULL;
    }
#ifdef _UNICODE
  MultiByteToWideChar(CP_UTF8,
                      0,
                      cbuf,
                      -1,
                      wbuf,
                      (int)bsize);
  return wbuf;
#else
  return strncpy(wbuf, cbuf, bsize);
#endif
}

/* ----------------------------------------------------------------- */
static char *ItoS16(int param)
{
  static char buf[MAX_PATH];

  snprintf(buf, MAX_PATH, "%08X", param);

  return buf;
}

/* ----------------------------------------------------------------- */
static int S16toI(const char *param)
{
  return strtol(param, NULL, 16);
}

/* ----------------------------------------------------------------- *
 * public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
ipt::ipt()
{
  if (OK)
    {
      return;
    }

  _tsetlocale(LC_ALL, _T(""));  // ロケールを定義します。

  Read();

  OK = 1;
}

/* ----------------------------------------------------------------- */
ipt::~ipt()
{
}

/* ----------------------------------------------------------------- */
bool ipt::Read(void)
{
  TCHAR wbuf[MAX_PATH];
  char  cbuf[MAX_PATH];

  JSON_Value  *val;
  JSON_Object *obj;
  JSON_Array  *interests_array = NULL;

  /* Initialize default path */

  ::GetCurrentDirectory(MAX_PATH, path1);
  ::GetCurrentDirectory(MAX_PATH, path2);

  /* parse from file */

  TCHAR ext[] = _T(".json");

  ::GetModuleFileName(NULL, wbuf, sizeof(wbuf) - sizeof(ext));

  PathRenameExtension(wbuf, ext);

  val = json_parse_file(WtoC(wbuf, cbuf, sizeof(cbuf)));

  if (val == NULL) {
    return false;
  }

  obj = json_value_get_object(val);

  /* Window */

  x           = (int)json_object_dotget_number(obj, "WINDOW.X");
  y           = (int)json_object_dotget_number(obj, "WINDOW.Y");
  cx          = (int)json_object_dotget_number(obj, "WINDOW.CX");
  cy          = (int)json_object_dotget_number(obj, "WINDOW.CY");
  show        = (int)json_object_dotget_number(obj, "WINDOW.SHOW");
  column      = (int)json_object_dotget_number(obj, "WINDOW.COLUMN");
  page_no     = (int)json_object_dotget_number(obj, "WINDOW.PAGE_NO");
  line_pitch  = (int)json_object_dotget_number(obj, "WINDOW.LINE_PITCH");
  tool_size   = (int)json_object_dotget_number(obj, "WINDOW.TOOLBER_SIZE");
  sort_part   = (int)json_object_dotget_number(obj, "WINDOW.SORT_PART");
  sort_order  = (int)json_object_dotget_number(obj, "WINDOW.SORT_ORDER");

  /* Font */

  fontsize1 = (int)json_object_dotget_number(obj, "FONT.SIZE1");
  CtoW(json_object_dotget_string(obj, "FONT.NAME1"), fontname1, sizeof(fontname1));
  fontsize2 = (int)json_object_dotget_number(obj, "FONT.SIZE2");
  CtoW(json_object_dotget_string(obj, "FONT.NAME2"), fontname2, sizeof(fontname2));
  fontsize3 = (int)json_object_dotget_number(obj, "FONT.SIZE3");
  CtoW(json_object_dotget_string(obj, "FONT.NAME3"), fontname3, sizeof(fontname3));
  fontsize4 = (int)json_object_dotget_number(obj, "FONT.SIZE4");
  CtoW(json_object_dotget_string(obj, "FONT.NAME4"), fontname4, sizeof(fontname4));
  fontsize4 = (int)json_object_dotget_number(obj, "FONT.SIZE5");
  CtoW(json_object_dotget_string(obj, "FONT.NAME5"), fontname5, sizeof(fontname5));

  /* Color */

  COL_BASE_BAR            = S16toI(json_object_dotget_string(obj, "COLOR.BASE_BAR"));
  COL_SCROLL_BAR          = S16toI(json_object_dotget_string(obj, "COLOR.SCROLL_BAR"));
  COL_SCROLL_BAR_SEL      = S16toI(json_object_dotget_string(obj, "COLOR.SCROLL_BAR_SEL"));
  COL_BASE_BAR_LOW        = S16toI(json_object_dotget_string(obj, "COLOR.BASE_BAR_LOW"));
  COL_SPLIT_LINE          = S16toI(json_object_dotget_string(obj, "COLOR.SPLIT_LINE"));
  COL_SPLIT_LINE2         = S16toI(json_object_dotget_string(obj, "COLOR.SPLIT_LINE2"));
  COL_SPLIT_LINE3         = S16toI(json_object_dotget_string(obj, "COLOR.SPLIT_LINE3"));
  COL_SPLIT_LINE_TOP      = S16toI(json_object_dotget_string(obj, "COLOR.SPLIT_LINE_TOP"));
  COL_SPLIT_LINE_BTM      = S16toI(json_object_dotget_string(obj, "COLOR.SPLIT_LINE_BTM"));
  COL_SELECTED            = S16toI(json_object_dotget_string(obj, "COLOR.SELECTED"));
  COL_SELECTED_FRAME      = S16toI(json_object_dotget_string(obj, "COLOR.SELECTED_FRAME"));
  COL_SELECTED_CUR        = S16toI(json_object_dotget_string(obj, "COLOR.SELECTED_CUR"));
  COL_FILE_ATTR_SYSTEM    = S16toI(json_object_dotget_string(obj, "COLOR.FILE_ATTR_SYSTEM"));
  COL_FILE_ATTR_HIDDEN    = S16toI(json_object_dotget_string(obj, "COLOR.FILE_ATTR_HIDDEN"));
  COL_FILE_ATTR_READONLY  = S16toI(json_object_dotget_string(obj, "COLOR.FILE_ATTR_READONLY"));
  COL_FILE_ATTR_DIRECTORY = S16toI(json_object_dotget_string(obj, "COLOR.FILE_ATTR_DIRECTORY"));
  COL_FILE_ATTR_NORMAL    = S16toI(json_object_dotget_string(obj, "COLOR.FILE_ATTR_NORMAL"));

  /* Setting */

  CtoW(json_object_dotget_string(obj, "SETTING.EDITOR"),   editor,   sizeof(editor));
  CtoW(json_object_dotget_string(obj, "SETTING.SHORTCUT"), shortcut, sizeof(shortcut));
  CtoW(json_object_dotget_string(obj, "SETTING.PATH1"),    path1,    sizeof(path1));
  CtoW(json_object_dotget_string(obj, "SETTING.PATH2"),    path2,    sizeof(path2));
  COL_MODE = (int)json_object_dotget_number(obj, "SETTING.COL_MODE");
  CtoW(json_object_dotget_string(obj, "SETTING.WCLIST"),   wclist,   sizeof(wclist));
  CtoW(json_object_dotget_string(obj, "SETTING.DIFFTOOL"), difftool, sizeof(difftool));

  /* memo */

  CtoW(json_object_dotget_string(obj, "MEMO.PATH1"), memo_file1, sizeof(memo_file1));
  CtoW(json_object_dotget_string(obj, "MEMO.PATH2"), memo_file2, sizeof(memo_file2));
  memo_cur1 = (int)json_object_dotget_number(obj, "MEMO.CUR1");
  memo_cur2 = (int)json_object_dotget_number(obj, "MEMO.CUR2");

  /* List */

  history     his;
  TCHAR       buf[MAX_PATH];
  int         i;
  const char *param;

  /* History */

  interests_array = json_object_dotget_array(obj, "LIST.HISTORY");

  int cnt = (int)json_array_get_count(interests_array);

  for (i = 0; i < cnt; i++)
    {
      param = json_array_get_string(interests_array, i);

      CtoW(param + HISTORY_PATH_OFFSET, buf, sizeof(buf));

      his.Push(buf, *param == '*', S16toI(param + 1));
    }

  /* Library */

  interests_array = json_object_dotget_array(obj, "LIST.LIBRARY");

  libraly_num = (int)json_array_get_count(interests_array);

  for (i = 0; i < libraly_num; i++)
    {
      JSON_Value  *sub_val = json_array_get_value(interests_array, i);
      JSON_Object *sub_obj = json_value_get_object(sub_val);

      CtoW(json_object_get_string(sub_obj, "NAME"), libraly_name[i], MAX_PATH);
      CtoW(json_object_get_string(sub_obj, "PATH"), libraly_path[i], MAX_PATH);
    }

  /* Search folder */

  interests_array = json_object_dotget_array(obj, "LIST.SEACH_FOLDER");

  search_folder_num = (int)json_array_get_count(interests_array);

  for (i = 0; i < search_folder_num; i++)
    {
      param = json_array_get_string(interests_array, i);

      CtoW(param, search_folders[i], MAX_PATH);
    }

  /* Sort folder */

  interests_array = json_object_dotget_array(obj, "LIST.SORT_FOLDER");

  sort_folder_num = (int)json_array_get_count(interests_array);

  for (i = 0; i < sort_folder_num; i++)
    {
      JSON_Value  *sub_val = json_array_get_value(interests_array, i);
      JSON_Object *sub_obj = json_value_get_object(sub_val);

      CtoW(json_object_get_string(sub_obj, "PATH"), sort_folders[i].path, MAX_PATH);

      sort_folders[i].type   = (int)json_object_get_number(sub_obj, "TYPE");
      sort_folders[i].order  = (int)json_object_get_number(sub_obj, "ORDER");
      sort_folders[i].column = (int)json_object_get_number(sub_obj, "COLUMN");
    }

  /* Clean up*/

  json_value_free(val);

  return true;
}

/* ----------------------------------------------------------------- */
bool ipt::Write(void)
{
  TCHAR wbuf[MAX_PATH];
  char  cbuf[MAX_PATH];

  JSON_Value  *val;
  JSON_Object *obj;
  JSON_Array  *array = NULL;

  val = json_value_init_object();
  obj = json_value_get_object(val);

  /* Window */

  json_object_dotset_number(obj, "WINDOW.X",            x);
  json_object_dotset_number(obj, "WINDOW.Y",            y);
  json_object_dotset_number(obj, "WINDOW.CX",           cx);
  json_object_dotset_number(obj, "WINDOW.CY",           cy);
  json_object_dotset_number(obj, "WINDOW.SHOW",         show);
  json_object_dotset_number(obj, "WINDOW.COLUMN",       column);
  json_object_dotset_number(obj, "WINDOW.PAGE_NO",      page_no);
  json_object_dotset_number(obj, "WINDOW.LINE_PITCH",   line_pitch);
  json_object_dotset_number(obj, "WINDOW.TOOLBER_SIZE", tool_size);
  json_object_dotset_number(obj, "WINDOW.SORT_PART",    sort_part);
  json_object_dotset_number(obj, "WINDOW.SORT_ORDER",   sort_order);

  /* Font */

  json_object_dotset_number(obj, "FONT.SIZE1", fontsize1);
  json_object_dotset_string(obj, "FONT.NAME1", WtoC(fontname1, cbuf, sizeof(cbuf)));
  json_object_dotset_number(obj, "FONT.SIZE2", fontsize2);
  json_object_dotset_string(obj, "FONT.NAME2", WtoC(fontname2, cbuf, sizeof(cbuf)));
  json_object_dotset_number(obj, "FONT.SIZE3", fontsize3);
  json_object_dotset_string(obj, "FONT.NAME3", WtoC(fontname3, cbuf, sizeof(cbuf)));
  json_object_dotset_number(obj, "FONT.SIZE4", fontsize4);
  json_object_dotset_string(obj, "FONT.NAME4", WtoC(fontname4, cbuf, sizeof(cbuf)));
  json_object_dotset_number(obj, "FONT.SIZE5", fontsize5);
  json_object_dotset_string(obj, "FONT.NAME5", WtoC(fontname5, cbuf, sizeof(cbuf)));

  /* Color */

  json_object_dotset_string(obj, "COLOR.BASE_BAR",            ItoS16(COL_BASE_BAR));
  json_object_dotset_string(obj, "COLOR.SCROLL_BAR",          ItoS16(COL_SCROLL_BAR));
  json_object_dotset_string(obj, "COLOR.SCROLL_BAR_SEL",      ItoS16(COL_SCROLL_BAR_SEL));
  json_object_dotset_string(obj, "COLOR.BASE_BAR_LOW",        ItoS16(COL_BASE_BAR_LOW));
  json_object_dotset_string(obj, "COLOR.SPLIT_LINE",          ItoS16(COL_SPLIT_LINE));
  json_object_dotset_string(obj, "COLOR.SPLIT_LINE2",         ItoS16(COL_SPLIT_LINE2));
  json_object_dotset_string(obj, "COLOR.SPLIT_LINE3",         ItoS16(COL_SPLIT_LINE3));
  json_object_dotset_string(obj, "COLOR.SPLIT_LINE_TOP",      ItoS16(COL_SPLIT_LINE_TOP));
  json_object_dotset_string(obj, "COLOR.SPLIT_LINE_BTM",      ItoS16(COL_SPLIT_LINE_BTM));
  json_object_dotset_string(obj, "COLOR.SELECTED",            ItoS16(COL_SELECTED));
  json_object_dotset_string(obj, "COLOR.SELECTED_FRAME",      ItoS16(COL_SELECTED_FRAME));
  json_object_dotset_string(obj, "COLOR.SELECTED_CUR",        ItoS16(COL_SELECTED_CUR));
  json_object_dotset_string(obj, "COLOR.FILE_ATTR_SYSTEM",    ItoS16(COL_FILE_ATTR_SYSTEM));
  json_object_dotset_string(obj, "COLOR.FILE_ATTR_HIDDEN",    ItoS16(COL_FILE_ATTR_HIDDEN));
  json_object_dotset_string(obj, "COLOR.FILE_ATTR_READONLY",  ItoS16(COL_FILE_ATTR_READONLY));
  json_object_dotset_string(obj, "COLOR.FILE_ATTR_DIRECTORY", ItoS16(COL_FILE_ATTR_DIRECTORY));
  json_object_dotset_string(obj, "COLOR.FILE_ATTR_NORMAL",    ItoS16(COL_FILE_ATTR_NORMAL));

  /* Setting */

  json_object_dotset_string(obj, "SETTING.EDITOR",   WtoC(editor, cbuf, sizeof(cbuf)));
  json_object_dotset_string(obj, "SETTING.SHORTCUT", WtoC(shortcut, cbuf, sizeof(cbuf)));
  json_object_dotset_string(obj, "SETTING.PATH1",    WtoC(path1, cbuf, sizeof(cbuf)));
  json_object_dotset_string(obj, "SETTING.PATH2",    WtoC(path2, cbuf, sizeof(cbuf)));
  json_object_dotset_number(obj, "SETTING.COL_MODE", COL_MODE);
  json_object_dotset_string(obj, "SETTING.WCLIST",   WtoC(wclist, cbuf, sizeof(cbuf)));
  json_object_dotset_string(obj, "SETTING.DIFFTOOL", WtoC(difftool, cbuf, sizeof(cbuf)));

  /* Memo*/

  json_object_dotset_string(obj, "MEMO.FILE1", WtoC(memo_file1, cbuf, sizeof(cbuf)));
  json_object_dotset_string(obj, "MEMO.FILE2", WtoC(memo_file2, cbuf, sizeof(cbuf)));
  json_object_dotset_number(obj, "MEMO.CUR1", memo_cur1);
  json_object_dotset_number(obj, "MEMO.CUR2", memo_cur2);

  /* List */

  history   his;
  int       i;
  int       cnt;

  /* History */

  json_object_dotset_value(obj, "LIST.HISTORY", json_value_init_array());

  array = json_object_dotget_array(obj, "LIST.HISTORY");

  cnt = his.GetCount();

  for (i = cnt - 1; i >= 0; i--)
    {
      json_array_append_string(array, WtoC(his.GetAt(i), cbuf, sizeof(cbuf)));
    }

  /* Library */

  json_object_dotset_value(obj, "LIST.LIBRARY", json_value_init_array());

  array = json_object_dotget_array(obj, "LIST.LIBRARY");

  JSON_Value *val_with_parent = json_value_init_null();;

  for (i = 0; i < libraly_num; i++)
    {
      JSON_Value  *sub_val = json_value_init_object();
      JSON_Object *sub_obj = json_value_get_object(sub_val);

      json_object_set_string(sub_obj, "NAME", WtoC(libraly_name[i], cbuf, sizeof(cbuf)));
      json_object_set_string(sub_obj, "PATH", WtoC(libraly_path[i], cbuf, sizeof(cbuf)));
      json_array_append_value(array, sub_val);
    }

  /* Search folder */

  json_object_dotset_value(obj, "LIST.SEACH_FOLDER", json_value_init_array());

  array = json_object_dotget_array(obj, "LIST.SEACH_FOLDER");

  for (i = 0; i < search_folder_num; i++)
    {
      json_array_append_string(array, WtoC(search_folders[i], cbuf, sizeof(cbuf)));
    }

  /* Sort folder */

  json_object_dotset_value(obj, "LIST.SORT_FOLDER", json_value_init_array());

  array = json_object_dotget_array(obj, "LIST.SORT_FOLDER");

  for (i = 0; i < sort_folder_num; i++)
    {
      JSON_Value  *sub_val = json_value_init_object();
      JSON_Object *sub_obj = json_value_get_object(sub_val);

      json_object_set_string(sub_obj, "PATH",   WtoC(sort_folders[i].path, cbuf, sizeof(cbuf)));
      json_object_set_number(sub_obj, "TYPE",   sort_folders[i].type);
      json_object_set_number(sub_obj, "ORDER",  sort_folders[i].order);
      json_object_set_number(sub_obj, "COLUMN", sort_folders[i].column);
      json_array_append_value(array, sub_val);
    }

  /*serialize to file */

  TCHAR ext[] = _T(".json");

  ::GetModuleFileName(NULL, wbuf, sizeof(wbuf) - sizeof(ext));

  PathRenameExtension(wbuf, ext);

  json_serialize_to_file_pretty(val, WtoC(wbuf, cbuf, sizeof(cbuf)));

  /* Clean up*/

  json_value_free(val);

  return true;
}

/* ----------------------------------------------------------------- */
/*
void ipt::SetPlacement(HWND hWnd)
{
  WINDOWPLACEMENT place;

  ::GetWindowPlacement(hWnd, &place);

  x    = place.rcNormalPosition.left;
  y    = place.rcNormalPosition.top;
  cx   = place.rcNormalPosition.right  - place.rcNormalPosition.left;
  cy   = place.rcNormalPosition.bottom - place.rcNormalPosition.top;
  show = place.showCmd;
}
*/
/* ----------------------------------------------------------------- */
/*
int ipt::GetFuncNo(int vk, int ctrl, int shift, int alt)
{
  int   i;
  int   f_no = 0;

  ctrl  = (ctrl > 0);
  shift = (shift > 0);
  alt   = (alt > 0);

  for (i = 0; keys[i].vk; i++)
    {
      if (keys[i].vk  == vk
       && keys[i].ctl == ctrl
       && keys[i].sft == shift
       && keys[i].alt == alt)
        {
          f_no = keys[i].f_no;
        }
    }

  return f_no;
}
*/
/* ----------------------------------------------------------------- */
D_SORT_T *ipt::SearchInfo(TCHAR *path)
{
  if (path == NULL || path[0] == '\0')
    {
      return NULL;
    }

  D_SORT_T *info = NULL;

  for (int i = 0; i < sort_folder_num; i++)
    {
      if (_tcsicmp(sort_folders[i].path, path) != 0)
        {
          continue;
        }

      info = &sort_folders[i];

      break;
    }

  return info;
}

/* ----------------------------------------------------------------- */
void ipt::SetSortInfo(TCHAR *path, int column, int type, int order)
{
  D_SORT_T *info = SearchInfo(path);

  if (info == NULL)
    {
      for (int i = PT_SORT_FOLDERS_NUM - 1; i > 0; i--)
        {
          sort_folders[i] = sort_folders[i - 1];
        }

      info = &sort_folders[0];

      _tcscpy_s(info->path, MAX_PATH, path);

      if (column < 0)
        {
          info->column = ipt::column;
        }
      else
        {
          info->column = column;
        }

      if (type < 0)
        {
          info->type   = ipt::sort_part;
          info->order  = ipt::sort_order;
        }
      else
        {
          info->type   = type;
          info->order  = order;
        }

      if (sort_folder_num < PT_SORT_FOLDERS_NUM)
        {
          sort_folder_num++;
        }

    }
  else
    {
      if (column >= 0)
        {
          info->column = column;
        }

      if (type >= 0)
        {
          info->type   = type;
          info->order  = order;
        }
    }
}

/* ----------------------------------------------------------------- */
int ipt::GetSortInfo(TCHAR *path, int *column, int *type, int *order)
{
  D_SORT_T *info = SearchInfo(path);

  if (info == NULL)
    {
      return -1;
    }

  if (column)
    {
      *column = info->column;
    }

  if (type)
    {
      *type = info->type;
    }

  if (order)
    {
      *order = info->order;
    }

  return 0;
}
