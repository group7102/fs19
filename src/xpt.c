#include <Windows.h>
#include <stdio.h>
#include <malloc.h>
#include <tchar.h>
#include "logf.h"
#include "parson.h"
#include "xpt.h"

/* ----------------------------------------------------------------- *
 * Private data
 * ----------------------------------------------------------------- */

static IPT _ini = {
  /* placement */

  .place = {
    .length                  = sizeof(WINDOWPLACEMENT),
    .flags                   = 0,
    .showCmd                 = SW_SHOWNORMAL,
    .ptMinPosition.x         = -1,
    .ptMinPosition.y         = -1,
    .ptMaxPosition.x         = -1,
    .ptMaxPosition.y         = -1,
    .rcNormalPosition.left   = 16,
    .rcNormalPosition.top    = 16,
    .rcNormalPosition.right  = 16 + 800,
    .rcNormalPosition.bottom = 16 + 600,
  },

  /* Font */

  .fonts = {
    {
      .name = L"ＭＳ ゴシック",
      .size = -12,
    },
    {
      .name = L"ＭＳ ゴシック",
      .size = -12,
    },
    {
      .name = L"ＭＳ ゴシック",
      .size = -12,
    },
    {
      .name = L"ＭＳ ゴシック",
      .size = -12,
    },
    {
      .name = L"ＭＳ ゴシック",
      .size = -12,
    },
  },

  /* Sort folders */

  .sort = {
    .num = 1,
    .folders = {
      {
        .path   = L"C:\\WORK",
        .type   = 1,
        .order  = 2,
        .column = 3,
      }
    }
  },

  /* Colors */

  .color = {
    .base_bar            = RGB(251, 251, 251),
    .scroll_bar          = RGB(205, 205, 205),
    .scroll_bar_sel      = RGB(166, 166, 166),
    .base_bar_low        = RGB(250, 250, 255),
    .split_line          = RGB(217, 217, 217),
    .split_line2         = RGB(192, 192, 192),
    .split_line_top      = RGB(  7,   5,   5),
    .split_line_btm      = RGB(137, 145, 145),
    .split_line3         = RGB(107, 105, 105),
    .selected            = RGB(204, 232, 255),
    .selected_frame      = RGB(  0, 180, 255),
    .selected_cur        = RGB(229, 243, 255),
    .progress_bar        = RGB(  0, 114, 198),
    .remain_bar_blue     = RGB( 38, 160, 218),
    .remain_bar_red      = RGB(218,  38,  38),
    .title               = RGB( 0,   51, 153),
    .file_attr_system    = RGB( 15,   4,   0),
    .file_attr_hidden    = RGB( 15,   4,   0),
    .file_attr_readonly  = RGB( 15,   4,   0),
    .file_attr_directory = RGB( 15,   4,   0),
    .file_attr_normal    = RGB( 15,   4,   0),
  },

  /* Setting */

  .setting = {
    .editor     = L"notepad.exe",
    .shortcut   = L"",
    .path1      = L"",
    .path2      = L"",
    .col_mode   = 0,
    .wclist     = L"",
    .difftool   = L"C:\\usr\\bin\\WinMerge\\WinMergeU.exe",
    .column     = 2,
    .page_no    = 0,
    .tool_size  = 200,
    .line_pitch = 0,
    .sort_part  = 0,
    .sort_order = 1,
    .ps_mode    = 0,
  },

  /* Memo */

  .memo = {
    .file1 = _T(""),
    .file2 = _T(""),
    .cur1  = 0,
    .cur2  = 0,
  },

  /* Library folders */

  .library = {
    .num = 1,
    .folders = {
      {
        .name = _T("Root"),
        .path = _T("C:\\"),
      }
    },
  },

  /* Queue folders */

  .queue.num = 0,

  /* history */

  .history.num = 0,
};

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
JSON_Status JObjDotSetForUtf8(JSON_Object *obj, const char *prop, TCHAR *str)
{
  int len = WideCharToMultiByte(CP_UTF8,
                                0,
                                str,
                                -1,
                                NULL,
                                0,
                                NULL,
                                NULL) + 1;

  char *cp = (char *)alloca(len);

  if (cp == NULL) {
    LOGE("alloca");
    return JSONFailure;
  }

  /* TCHAR to char */

  WideCharToMultiByte(CP_UTF8,
                      0,
                      str,
                      -1,
                      cp,
                      len,
                      NULL,
                      NULL);

  return json_object_dotset_string(obj, prop, cp);
}

/* ----------------------------------------------------------------- */
TCHAR *JObjDotGetForUtf8(JSON_Object *obj, const char *prop, TCHAR *str, size_t len)
{
  if (json_object_dothas_value_of_type(obj, prop, JSONString) == 0) {
    LOGW("json_object_dothas_value_of_type");
    LOGA("%s", prop);
    return NULL;
  }

  const char *cp = json_object_dotget_string(obj, prop);

  if (cp == NULL) {
    LOGE("json_object_dotget_string");
    return NULL;
  }

  /* char to TCHAR */

  MultiByteToWideChar(CP_UTF8,
                      0,
                      cp,
                      -1,
                      str,
                      (int)len);
  return str;
}

/* ----------------------------------------------------------------- */
static void SetPlacement(WINDOWPLACEMENT *p, JSON_Object *root)
{
  json_object_dotset_number(root, "place.length",                  p->length);
  json_object_dotset_number(root, "place.flags",                   p->flags);
  json_object_dotset_number(root, "place.showCmd",                 p->showCmd);
  json_object_dotset_number(root, "place.ptMinPosition.x",         p->ptMinPosition.x);
  json_object_dotset_number(root, "place.ptMinPosition.y",         p->ptMinPosition.y);
  json_object_dotset_number(root, "place.ptMaxPosition.x",         p->ptMaxPosition.x);
  json_object_dotset_number(root, "place.ptMaxPosition.y",         p->ptMaxPosition.y);
  json_object_dotset_number(root, "place.rcNormalPosition.left",   p->rcNormalPosition.left);
  json_object_dotset_number(root, "place.rcNormalPosition.top",    p->rcNormalPosition.top);
  json_object_dotset_number(root, "place.rcNormalPosition.right",  p->rcNormalPosition.right);
  json_object_dotset_number(root, "place.rcNormalPosition.bottom", p->rcNormalPosition.bottom);
}

/* ----------------------------------------------------------------- */
static int JObjDotGetNumber(JSON_Object *root, const char *prop, int *number)
{
  if (json_object_dothas_value_of_type(root, prop, JSONNumber) == 0) {
    LOGW("json_object_dothas_value_of_type");
    LOGA("%s", prop);
    return -1;
  }

  *number = (int)json_object_dotget_number(root, prop);

  return 0;
}

/* ----------------------------------------------------------------- */
static void GetPlacement(WINDOWPLACEMENT *p, JSON_Object *root)
{
  JObjDotGetNumber(root, "place.length",                  &p->length);
  JObjDotGetNumber(root, "place.flags",                   &p->flags);
  JObjDotGetNumber(root, "place.showCmd",                 &p->showCmd);
  JObjDotGetNumber(root, "place.ptMinPosition.x",         &p->ptMinPosition.x);
  JObjDotGetNumber(root, "place.ptMinPosition.y",         &p->ptMinPosition.y);
  JObjDotGetNumber(root, "place.ptMaxPosition.x",         &p->ptMaxPosition.x);
  JObjDotGetNumber(root, "place.ptMaxPosition.y",         &p->ptMaxPosition.y);
  JObjDotGetNumber(root, "place.rcNormalPosition.left",   &p->rcNormalPosition.left);
  JObjDotGetNumber(root, "place.rcNormalPosition.top",    &p->rcNormalPosition.top);
  JObjDotGetNumber(root, "place.rcNormalPosition.right",  &p->rcNormalPosition.right);
  JObjDotGetNumber(root, "place.rcNormalPosition.bottom", &p->rcNormalPosition.bottom);
}

/* ----------------------------------------------------------------- */
static void SetFonts(struct FONT_T *font, JSON_Object *root)
{
  JSON_Value  *array_val = json_value_init_array();
  JSON_Array  *array_obj = json_value_get_array(array_val);
  JSON_Value  *val;
  JSON_Object *obj;

  for (int i = 0; i < IPT_FONT_INDEX_NUM; i++) {
    val = json_value_init_object();
    obj = json_value_get_object(val);

    JObjDotSetForUtf8(obj, "name", font[i].name);
    json_object_dotset_number(obj, "size", font[i].size);

    json_array_append_value(array_obj, val);
  }

  json_object_dotset_value(root, "fonts", array_val);
}

/* ----------------------------------------------------------------- */
static void GetFonts(struct FONT_T *font, JSON_Object *root)
{
  JSON_Array *array = json_object_dotget_array(root, "fonts");

  size_t cnt = json_array_get_count(array);

  if (cnt < 0) {
    LOGE("json_array_get_count");
    return;
  }

  for (int i = 0; i < cnt && i < IPT_FONT_INDEX_NUM; i++) {
    JSON_Value  *val = json_array_get_value(array, i);
    JSON_Object *obj = json_value_get_object(val);

    JObjDotGetForUtf8(obj, "name", font[i].name, sizeof(font[i].name));
    JObjDotGetNumber(obj, "size", &font[i].size);
  }
}

/* ----------------------------------------------------------------- */
static void SetSortFolders(struct SORT_FOLDERS_T *sort, JSON_Object *root)
{
  json_object_dotset_number(root, "sort.num", sort->num);

  JSON_Value *array_val = json_value_init_array();
  JSON_Array *array     = json_value_get_array(array_val);

  /* Add array */

  json_object_dotset_value(root, "sort.folders", array_val);

  /* Add object to array */

  for (int i = 0; i < sort->num && i < IPT_SORT_FOLDERS_NUM; i++) {
    JSON_Value  *val = json_value_init_object();
    JSON_Object *obj = json_value_get_object(val);

    JObjDotSetForUtf8(obj, "path", sort->folders[i].path);
    json_object_set_number(obj, "type",   sort->folders[i].type);
    json_object_set_number(obj, "order",  sort->folders[i].order);
    json_object_set_number(obj, "column", sort->folders[i].column);

    json_array_append_value(array, val);
  }
}

/* ----------------------------------------------------------------- */
static void GetSortFolders(struct SORT_FOLDERS_T *sort, JSON_Object *root)
{
  JSON_Array *array = json_object_dotget_array(root, "sort.folders");

  int cnt = (int)json_array_get_count(array);

  if (cnt < 0) {
    LOGE("json_array_get_count");
    return;
  }

  for (int i = 0; i < cnt && i < IPT_SORT_FOLDERS_NUM; i++) {
    JSON_Value  *val = json_array_get_value(array, i);
    JSON_Object *obj = json_value_get_object(val);

    JObjDotGetForUtf8(obj, "path", sort->folders[i].path, sizeof(sort->folders[i].path));
    JObjDotGetNumber(obj, "type",   &sort->folders[i].type);
    JObjDotGetNumber(obj, "order",  &sort->folders[i].order);
    JObjDotGetNumber(obj, "column", &sort->folders[i].column);
  }

  sort->num = cnt;
}

/* ----------------------------------------------------------------- */
static void SetColorName(JSON_Object *root, const char *prop, COLORREF color)
{
  char buf[64];

  snprintf(buf, sizeof(buf), "color.%s.r",  prop);
  json_object_dotset_number(root, buf, GetRValue(color));
  snprintf(buf, sizeof(buf), "color.%s.g",  prop);
  json_object_dotset_number(root, buf, GetGValue(color));
  snprintf(buf, sizeof(buf), "color.%s.b",  prop);
  json_object_dotset_number(root, buf, GetBValue(color));
}

/* ----------------------------------------------------------------- */
static void SetColor(struct COLOR_T *color, JSON_Object *root)
{
  SetColorName(root, "base_bar",            color->base_bar);
  SetColorName(root, "scroll_bar",          color->scroll_bar);
  SetColorName(root, "scroll_bar_sel",      color->scroll_bar_sel);
  SetColorName(root, "base_bar_low",        color->base_bar_low);
  SetColorName(root, "split_line",          color->split_line);
  SetColorName(root, "split_line2",         color->split_line2);
  SetColorName(root, "split_line_top",      color->split_line_top);
  SetColorName(root, "split_line_btm",      color->split_line_btm);
  SetColorName(root, "split_line3",         color->split_line3);
  SetColorName(root, "selected",            color->selected);
  SetColorName(root, "selected_frame",      color->selected_frame);
  SetColorName(root, "selected_cur",        color->selected_cur);
  SetColorName(root, "progress_bar",        color->progress_bar);
  SetColorName(root, "remain_bar_blue",     color->remain_bar_blue);
  SetColorName(root, "remain_bar_red",      color->remain_bar_red);
  SetColorName(root, "title",               color->title);
  SetColorName(root, "file_attr_system",    color->file_attr_system);
  SetColorName(root, "file_attr_hidden",    color->file_attr_hidden);
  SetColorName(root, "file_attr_readonly",  color->file_attr_readonly);
  SetColorName(root, "file_attr_directory", color->file_attr_directory);
  SetColorName(root, "file_attr_normal",    color->file_attr_normal);
}

/* ----------------------------------------------------------------- */
static void GetColorName(JSON_Object *root, const char *prop, COLORREF *color)
{
  char buf[64];
  int  r;
  int  g;
  int  b;

  snprintf(buf, sizeof(buf), "color.%s.b",  prop);
  if (JObjDotGetNumber(root, buf, &b) < 0) {
    return;
  }

  snprintf(buf, sizeof(buf), "color.%s.g",  prop);
  if (JObjDotGetNumber(root, buf, &g) < 0) {
    return;
  }

  snprintf(buf, sizeof(buf), "color.%s.r",  prop);
  if (JObjDotGetNumber(root, buf, &r) < 0) {
    return;
  }

  *color = RGB(LOBYTE(r), LOBYTE(g), LOBYTE(b));
}

/* ----------------------------------------------------------------- */
static void GetColor(struct COLOR_T *color, JSON_Object *root)
{
  GetColorName(root, "base_bar",            &color->base_bar);
  GetColorName(root, "scroll_bar",          &color->scroll_bar);
  GetColorName(root, "scroll_bar_sel",      &color->scroll_bar_sel);
  GetColorName(root, "base_bar_low",        &color->base_bar_low);
  GetColorName(root, "split_line",          &color->split_line);
  GetColorName(root, "split_line2",         &color->split_line2);
  GetColorName(root, "split_line_top",      &color->split_line_top);
  GetColorName(root, "split_line_btm",      &color->split_line_btm);
  GetColorName(root, "split_line3",         &color->split_line3);
  GetColorName(root, "selected",            &color->selected);
  GetColorName(root, "selected_frame",      &color->selected_frame);
  GetColorName(root, "selected_cur",        &color->selected_cur);
  GetColorName(root, "progress_bar",        &color->progress_bar);
  GetColorName(root, "remain_bar_blue",     &color->remain_bar_blue);
  GetColorName(root, "remain_bar_red",      &color->remain_bar_red);
  GetColorName(root, "title",               &color->title);
  GetColorName(root, "file_attr_system",    &color->file_attr_system);
  GetColorName(root, "file_attr_hidden",    &color->file_attr_hidden);
  GetColorName(root, "file_attr_readonly",  &color->file_attr_readonly);
  GetColorName(root, "file_attr_directory", &color->file_attr_directory);
  GetColorName(root, "file_attr_normal",    &color->file_attr_normal);
}

/* ----------------------------------------------------------------- */
static void SetSetting(struct SETTING_T *setting, JSON_Object *root)
{
  JObjDotSetForUtf8(root, "setting.editor",   setting->editor);
  JObjDotSetForUtf8(root, "setting.shortcut", setting->shortcut);
  JObjDotSetForUtf8(root, "setting.path1",    setting->path1);
  JObjDotSetForUtf8(root, "setting.path2",    setting->path2);
  JObjDotSetForUtf8(root, "setting.wclist",   setting->wclist);
  JObjDotSetForUtf8(root, "setting.difftool", setting->difftool);

  json_object_dotset_number(root, "setting.col_mode",   setting->col_mode);
  json_object_dotset_number(root, "setting.column",     setting->column);
  json_object_dotset_number(root, "setting.page_no",    setting->page_no);
  json_object_dotset_number(root, "setting.tool_size",  setting->tool_size);
  json_object_dotset_number(root, "setting.line_pitch", setting->line_pitch);
  json_object_dotset_number(root, "setting.sort_part",  setting->sort_part);
  json_object_dotset_number(root, "setting.sort_order", setting->sort_order);
  json_object_dotset_number(root, "setting.ps_mode",    setting->ps_mode);
}

/* ----------------------------------------------------------------- */
static void GetSetting(struct SETTING_T *setting, JSON_Object *root)
{
  JObjDotGetForUtf8(root, "setting.editor",   setting->editor,   sizeof(setting->editor));
  JObjDotGetForUtf8(root, "setting.shortcut", setting->shortcut, sizeof(setting->shortcut));
  JObjDotGetForUtf8(root, "setting.path1",    setting->path1,    sizeof(setting->path1));
  JObjDotGetForUtf8(root, "setting.path2",    setting->path2,    sizeof(setting->path2));
  JObjDotGetForUtf8(root, "setting.wclist",   setting->wclist,   sizeof(setting->wclist));
  JObjDotGetForUtf8(root, "setting.difftool", setting->difftool, sizeof(setting->difftool));

  JObjDotGetNumber(root, "setting.col_mode",   &setting->col_mode);
  JObjDotGetNumber(root, "setting.column",     &setting->column);
  JObjDotGetNumber(root, "setting.page_no",    &setting->page_no);
  JObjDotGetNumber(root, "setting.tool_size",  &setting->tool_size);
  JObjDotGetNumber(root, "setting.line_pitch", &setting->line_pitch);
  JObjDotGetNumber(root, "setting.sort_part",  &setting->sort_part);
  JObjDotGetNumber(root, "setting.sort_order", &setting->sort_order);
  JObjDotGetNumber(root, "setting.ps_mode",    &setting->ps_mode);
}

/* ----------------------------------------------------------------- */
static void SetMemo(struct MEMO_T *memo, JSON_Object *root)
{
  JObjDotSetForUtf8(root, "memo.file1", memo->file1);
  JObjDotSetForUtf8(root, "memo.file2", memo->file2);

  json_object_dotset_number(root, "memo.cur1", memo->cur1);
  json_object_dotset_number(root, "memo.cur2", memo->cur2);
}

/* ----------------------------------------------------------------- */
static void GetMemo(struct MEMO_T *memo, JSON_Object *root)
{
  JObjDotGetForUtf8(root, "memo.file1", memo->file1, sizeof(memo->file1));
  JObjDotGetForUtf8(root, "memo.file2", memo->file2, sizeof(memo->file2));

  JObjDotGetNumber(root, "memo.cur1", &memo->cur1);
  JObjDotGetNumber(root, "memo.cur2", &memo->cur2);
}

/* ----------------------------------------------------------------- */
static void SetLibrary(struct LIBRARY_T *library, JSON_Object *root)
{
  json_object_dotset_number(root, "library.num", library->num);

  JSON_Value *array_val = json_value_init_array();
  JSON_Array *array     = json_value_get_array(array_val);

  /* Add array */

  json_object_dotset_value(root, "library.folders", array_val);

  /* Add object to array */

  for (int i = 0; i < library->num && i < IPT_LIBRALY_NUM; i++) {
    JSON_Value  *val = json_value_init_object();
    JSON_Object *obj = json_value_get_object(val);

    JObjDotSetForUtf8(obj, "name", library->folders[i].name);
    JObjDotSetForUtf8(obj, "path", library->folders[i].path);

    json_array_append_value(array, val);
  }
}

/* ----------------------------------------------------------------- */
static void GetLibrary(struct LIBRARY_T *library, JSON_Object *root)
{
  JSON_Array *array = json_object_dotget_array(root, "library.folders");

  int cnt = (int)json_array_get_count(array);

  if (cnt < 0) {
    LOGE("json_array_get_count");
    return;
  }

  for (int i = 0; i < cnt && i < IPT_LIBRALY_NUM; i++) {
    JSON_Value  *val = json_array_get_value(array, i);
    JSON_Object *obj = json_value_get_object(val);

    JObjDotGetForUtf8(obj, "name", library->folders[i].name, sizeof(library->folders[i].name));
    JObjDotGetForUtf8(obj, "path", library->folders[i].path, sizeof(library->folders[i].path));
  }

  library->num = cnt;
}

/* ----------------------------------------------------------------- */
static void SetQueue(struct SEARCH_T *queue, JSON_Object *root)
{
  json_object_dotset_number(root, "queue.num", queue->num);

  JSON_Value *array_val = json_value_init_array();
  JSON_Array *array     = json_value_get_array(array_val);

  /* Add array */

  json_object_dotset_value(root, "queue.folders", array_val);

  /* Add object to array */

  for (int i = 0; i < queue->num && i < IPT_QUEUE_FOLDER_NUM; i++) {
    JSON_Value  *val = json_value_init_object();
    JSON_Object *obj = json_value_get_object(val);

    json_object_set_number(obj, "mark", queue->folders[i].mark);
    JObjDotSetForUtf8(obj, "path", queue->folders[i].path);

    json_array_append_value(array, val);
  }
}

/* ----------------------------------------------------------------- */
static void GetQueue(struct SEARCH_T *queue, JSON_Object *root)
{
  JSON_Array *array = json_object_dotget_array(root, "queue.folders");

  int cnt = (int)json_array_get_count(array);

  if (cnt < 0) {
    LOGE("json_array_get_count");
    return;
  }

  for (int i = 0; i < cnt && i < IPT_QUEUE_FOLDER_NUM; i++) {
    JSON_Value  *val = json_array_get_value(array, i);
    JSON_Object *obj = json_value_get_object(val);

    JObjDotGetNumber(obj, "mark", &queue->folders[i].mark);
    JObjDotGetForUtf8(obj, "path", queue->folders[i].path, sizeof(queue->folders[i].path));
  }

  queue->num = cnt;
}

/* ----------------------------------------------------------------- */
static void SetHistory(struct HISTORY_T *history, JSON_Object *root)
{
  json_object_dotset_number(root, "history.num", history->num);

  JSON_Value *array_val = json_value_init_array();
  JSON_Array *array     = json_value_get_array(array_val);

  /* Add array */

  json_object_dotset_value(root, "history.events", array_val);

  /* Add object to array */

  for (int i = 0; i < history->num && i < IPT_HISTORY_EVENT_NUM; i++) {
    JSON_Value  *val = json_value_init_object();
    JSON_Object *obj = json_value_get_object(val);

    JObjDotSetForUtf8(obj, "path", history->events[i].path);
    json_object_set_number(obj, "mark", history->events[i].mark);
    json_object_set_number(obj, "date", history->events[i].date);

    json_array_append_value(array, val);
  }
}

/* ----------------------------------------------------------------- */
static void GetHistory(struct HISTORY_T *history, JSON_Object *root)
{
  JSON_Array *array = json_object_dotget_array(root, "history.events");

  int cnt = (int)json_array_get_count(array);

  if (cnt < 0) {
    LOGE("json_array_get_count");
    return;
  }

  for (int i = 0; i < cnt && i < IPT_HISTORY_EVENT_NUM; i++) {
    JSON_Value  *val = json_array_get_value(array, i);
    JSON_Object *obj = json_value_get_object(val);

    JObjDotGetForUtf8(obj, "path", history->events[i].path, sizeof(history->events[i].path));
    JObjDotGetNumber(obj, "mark", &history->events[i].mark);
    JObjDotGetNumber(obj, "date", &history->events[i].date);
  }

  history->num = cnt;
}

/* ----------------------------------------------------------------- */
static char *GetBaseName(void)
{
  const TCHAR ext[] = L"_2nd.json";

  int extlen = _countof(ext) + 1;

  /* Get base file name */

  TCHAR *temp = (TCHAR *)alloca(MAX_PATH + extlen);

  if (GetModuleFileName(NULL, temp, MAX_PATH) == 0) {
    LOGE("GetModuleFileName");
    return NULL;
  }

  TCHAR *str = NULL;
  TCHAR *cp;

  for (cp = temp; *cp; cp++) {
    if (*cp == L'.') {
      str = cp;
    }
  }

  if (str == NULL) {
    str = cp;
  }

  _sntprintf_s(str, extlen, _TRUNCATE, L"%s", ext);

  int len = WideCharToMultiByte(CP_UTF8,
                                0,
                                temp,
                                -1,
                                NULL,
                                0,
                                NULL,
                                NULL) + 1;

  char *basename = (char *)malloc(len);

  if (basename == NULL) {
    LOGE("malloc");
    return NULL;
  }

  WideCharToMultiByte(CP_UTF8,
                      0,
                      temp,
                      -1,
                      basename,
                      len,
                      NULL,
                      NULL);
  return basename;
}

/* ----------------------------------------------------------------- */
static JSON_Status JsonSerializeToFilePretty(JSON_Value *val)
{
  char *basename = GetBaseName();

  if (basename == NULL) {
    return JSONFailure;
  }

  JSON_Status status = json_serialize_to_file_pretty(val, basename);

  free(basename);

  return status;
}

/* ----------------------------------------------------------------- */
static JSON_Value *JsonParseBaseFile(void)
{
  char *basename = GetBaseName();

  if (basename == NULL) {
    return NULL;
  }

  JSON_Value *val = json_parse_file(basename);

  free(basename);

  return val;
}

/* ----------------------------------------------------------------- */
static int SearchInfo(struct SORT_FOLDERS_T *sort, TCHAR *path)
{
  for (int i = 0; i < sort->num; i++) {
    if (_tcsicmp(sort->folders[i].path, path) == 0) {
      return i;
    }
  }

  return -1;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
IPT *xPtInit(void)
{
  /* parse from file */

  JSON_Value *val = JsonParseBaseFile();

  if (val == NULL) {
    LOGE("json_parse_file");
    return &_ini;
  }

  JSON_Object *obj = json_value_get_object(val);

  if (obj == NULL) {
    LOGE("json_value_get_object");
    return &_ini;
  }

  /* Get placement */

  GetPlacement(&_ini.place, obj);

  /* Get font */

  GetFonts(_ini.fonts, obj);

  /* Get sort folders */

  GetSortFolders(&_ini.sort, obj);

  /* Get color */

  GetColor(&_ini.color, obj);

  /* Get setting */

  GetSetting(&_ini.setting, obj);

  /* Get memo */

  GetMemo(&_ini.memo, obj);

  /* Get library */

  GetLibrary(&_ini.library, obj);

  /* Get queue */

  GetQueue(&_ini.queue, obj);

  /* Get history */

  GetHistory(&_ini.history, obj);

  /* Clean up*/

  json_value_free(val);

  return &_ini;
}

/* ----------------------------------------------------------------- */
void xPtFin(void)
{
  JSON_Value  *val = json_value_init_object();
  JSON_Object *obj = json_value_get_object(val);

  /* Placement */

  SetPlacement(&_ini.place, obj);

  /* font */

  SetFonts(_ini.fonts, obj);

  /* Set sort folders */

  SetSortFolders(&_ini.sort, obj);

  /* Set color */

  SetColor(&_ini.color, obj);

  /* Set setting */

  SetSetting(&_ini.setting, obj);

  /* Set memo */

  SetMemo(&_ini.memo, obj);

  /* Set library */

  SetLibrary(&_ini.library, obj);

  /* Set queue */

  SetQueue(&_ini.queue, obj);

  /* Set history */

  SetHistory(&_ini.history, obj);

  /* Serialize to file */

  JsonSerializeToFilePretty(val);

  /* Clean up*/

  json_value_free(val);
}

/* ----------------------------------------------------------------- */
IPT *xPt(void)
{
  return &_ini;
}

/* ----------------------------------------------------------------- */
void xPtSetSortInfo(IPT *pt, TCHAR *path, int column, int type, int order)
{
  if (path == NULL || path[0] == '\0') {
    return;
  }

  struct SORT_FOLDERS_T *sort    = &pt->sort;
  struct SETTING_T      *setting = &pt->setting;

  int info = SearchInfo(sort, path);

  if (info < 0) {
    for (int i = IPT_SORT_FOLDERS_NUM - 1; i > 0; i--) {
      sort->folders[i] = sort->folders[i - 1];
    }

    _sntprintf_s(sort->folders[0].path,
                 _countof(sort->folders[0].path),
                 _TRUNCATE,
                 L"%s",
                 path);

    if (column < 0) {
      sort->folders[0].column = setting->column;
    }
    else {
      sort->folders[0].column = column;
    }

    if (type < 0) {
      sort->folders[0].type  = setting->sort_part;
      sort->folders[0].order = setting->sort_order;
    }
    else {
      sort->folders[0].type  = type;
      sort->folders[0].order = order;
    }

    if (sort->num < IPT_SORT_FOLDERS_NUM) {
      sort->num++;
    }
  }
  else {
    if (column >= 0) {
      sort->folders[info].column = column;
    }

    if (type >= 0) {
      sort->folders[info].type   = type;
      sort->folders[info].order  = order;
    }
  }
}

/* ----------------------------------------------------------------- */
int xPtGetSortInfo(IPT *pt, TCHAR *path, int *column, int *type, int *order)
{
  if (path == NULL || path[0] == '\0') {
    return -1;
  }

  struct SORT_FOLDERS_T *sort = &pt->sort;

  int info = SearchInfo(sort, path);

  if (info < 0) {
    return -1;
  }

  if (column) {
    *column = sort->folders[info].column;
  }

  if (type) {
    *type = sort->folders[info].type;
  }

  if (order) {
    *order = sort->folders[info].order;
  }

  return 0;
}
