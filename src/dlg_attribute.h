/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_attribute.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DLG_ATTRIBUTE_H_
#define _DLG_ATTRIBUTE_H_

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

class xFont;
class com_button;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class dlg_attribute : public xDialog
{
public:
  void  SetAttribute(dirent *entry);
  DWORD GetAttribute() { return mp_entry->dwFileAttributes; }
  BOOL  GetCheckDir() { return mbEnableDir; }
  void  SetAttributes(void);

public:
  dlg_attribute(HWND hParent);
  ~dlg_attribute();

private:
  void OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void OnPaint();

private:
  LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam);

private:
  xButton *mpCheckBoxs[5];
  dirent  *mp_entry;
  xFont   *mpFont;
  BOOL     mbEnableDir;
  DWORD    mAttribute;

private:
  com_button *mpOK;
  com_button *mpCancel;
};

#endif /* _DLG_ATTRIBUTE_H_ */
