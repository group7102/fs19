/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: _simple_que.c
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <tchar.h>
#include <shlwapi.h>
#include "_simple_que.h"

/* ----------------------------------------------------------------- *
 * define
 * ----------------------------------------------------------------- */

#define MAX_LENGTH_OF_STRING  (MAX_PATH + 1 + 1)

/* ----------------------------------------------------------------- *
 * typedef
 * ----------------------------------------------------------------- */

typedef struct _ELEMENT_T_ {
  struct _ELEMENT_T_ *next;
  struct _ELEMENT_T_ *prev;
  int                 cnt;
  TCHAR               str[1];
}
ELEMENT_T;

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */

static ELEMENT_T *AllocElement(const TCHAR *str)
{
  size_t len = _tcsnlen(str, MAX_LENGTH_OF_STRING);

  if (len >= MAX_LENGTH_OF_STRING)
    {
      return NULL;
    }

  len++;

  ELEMENT_T *elm = (ELEMENT_T *)malloc(sizeof(ELEMENT_T) + sizeof(TCHAR) * len);

  if (elm == NULL)
    {
      return NULL;
    }

  _sntprintf_s(elm->str, len, _TRUNCATE, L"%s", str);

  return elm;
}

/* ----------------------------------------------------------------- */
static int AddTail(ELEMENT_T *top, ELEMENT_T *elm)
{
  if (elm == NULL || top == NULL)
    {
      return -1;
    }

  elm->next = NULL;
  elm->prev = top->prev;

  if (elm->prev) {
    elm->prev->next = elm;
  }

  top->prev = elm;

  if (top->next == NULL)
    {
      top->next = elm;
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int AddHead(ELEMENT_T *top, ELEMENT_T *elm)
{
  if (elm == NULL || top == NULL)
    {
      return -1;
    }

  elm->next = top->next;
  elm->prev = NULL;

  if (elm->next) {
    elm->next->prev = elm;
  }

  top->next = elm;

  if (top->prev == NULL)
    {
      top->prev = elm;
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int Remove(ELEMENT_T *top, ELEMENT_T *target)
{
  if (target->prev)
    {
      target->prev->next = target->next;
    }
  else
    {
      top->next = target->next;
    }

  if (target->next)
    {
      target->next->prev = target->prev;
    }
  else
    {
      top->prev = target->prev;
    }

  return 0;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
int SimpleQueOpen(void **handle)
{
  if (handle == NULL)
    {
      return -1;
    }

  *handle = NULL;

  ELEMENT_T *elm = (ELEMENT_T *)malloc(sizeof(ELEMENT_T));

  if (elm == NULL)
    {
      return -1;
    }

  elm->next = NULL;
  elm->prev = NULL;
  elm->cnt  = 0;

  elm->str[0] = L'\0';

  *handle = (void *)elm;

  return 0;
}

/* ----------------------------------------------------------------- */
int SimpleQueClose(void *handle)
{
  ELEMENT_T *elm = (ELEMENT_T *)handle;

  while (elm)
    {
      ELEMENT_T *tmp = elm->next;
      free(elm);
      elm = tmp;
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int SimpleQuePush(void *handle, const TCHAR *str)
{
  ELEMENT_T *top = (ELEMENT_T *)handle;

  if (top == NULL) {
    return -1;
  }

  /* Add backslash to end of path */

  size_t len = _tcsnlen(str, MAX_LENGTH_OF_STRING);

  if (len >= MAX_LENGTH_OF_STRING) {
    return -1;
  }

  len += (1 + 1 /* + Bach slash */);

  TCHAR *buf = (TCHAR *)alloca(len * sizeof(TCHAR));

  _sntprintf_s(buf, len, _TRUNCATE, L"%s", str);

  PathAddBackslash(buf);

  /* Search the list for the same data */

  ELEMENT_T *target = top->next;

  while (target) {
    if (_tcscmp(target->str, buf) == 0) {
      break;
    }

    if (_tcsncmp(target->str, buf, _tcslen(buf)) == 0) {
//    return 0;
    }

    target = target->next;
  }

  int  res = 0;

  if (target) {
    /* Bring existing data to the beginning */

    Remove(top, target);
    AddHead(top, target);
  }
  else {
    /* Add new data to the beginning */

    if ((res = AddHead(top, AllocElement(buf))) == 0) {
      top->cnt++;
    }
  }

  return res;
}

/* ----------------------------------------------------------------- */
int SimpleQuePop(void *handle, TCHAR *str, size_t len)
{
  ELEMENT_T *top = (ELEMENT_T *)handle;

  if (top == NULL || top->next == NULL)
    {
      return -1;
    }

  ELEMENT_T *elm = top->next;

  if (str)
    {
      _sntprintf_s(str, len, _TRUNCATE, L"%s", elm->str);
    }

  Remove(top, elm);

  free(elm);

  top->cnt--;

  return 0;
}

/* ----------------------------------------------------------------- */
int SimpleQueGetAt(void *handle, int index, const TCHAR **str)
{
  ELEMENT_T *top = (ELEMENT_T *)handle;

  if (top == NULL || top->next == NULL || str == NULL || top->cnt <= index)
    {
      return -1;
    }

  for (int i = 0; i <= index; i++)
    {
      top = top->next;
    }

  *str = top->str;

  return 0;
}

/* ----------------------------------------------------------------- */
int SimpleQueGetCount(void *handle)
{
  ELEMENT_T *top = (ELEMENT_T *)handle;

  if (top == NULL)
    {
      return -1;
    }

  return top->cnt;
}

/* ----------------------------------------------------------------- */
int SimpleQueClear(void *handle)
{
  ELEMENT_T *top = (ELEMENT_T *)handle;

  if (top == NULL)
    {
      return -1;
    }

  ELEMENT_T *elm = top->next;

  while (elm)
    {
      ELEMENT_T *tmp = elm->next;
      free(elm);
      elm = tmp;
    }

  top->next = NULL;
  top->prev = NULL;
  top->cnt  = 0;

  return 0;
}
