/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_ejectdsk.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "resource.h"
#include "com_list.h"
#include "sub_funcs.h"
#include "icon.h"
#include "xpt.h"
#include "com_button.h"
#include "dlg_ejectdsk.h"

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Eject disc
 * ----------------------------------------------------------------- */
static BOOL DiscEjectSub(TCHAR path[])
{
  return EjectVolume(path[0]);
}

/* ----------------------------------------------------------------- *
 * Construction of class
 * ----------------------------------------------------------------- */
dlg_ejectdsk::dlg_ejectdsk(HWND hParent) : xDialog(_T("IDD_EJECTDSK"), hParent)
{
  mpLogList = NULL;

  mpOK     = new com_button;
  mpCancel = new com_button;

  IcoSetPsMode(&mpOK->icon_no, &mpCancel->icon_no);
}

/* ----------------------------------------------------------------- *
 * class destruction
 * ----------------------------------------------------------------- */
dlg_ejectdsk::~dlg_ejectdsk()
{
  delete mpLogList;
  delete mpOK;
  delete mpCancel;
}

/* ----------------------------------------------------------------- *
 * Command processing
 * ----------------------------------------------------------------- */
void dlg_ejectdsk::OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  int   cmd  = 0;
  TCHAR buf[MAX_PATH];

  switch (id)
    {
      case IDC_LIST1:
        if (cmd != LBN_DBLCLK)
          {
            break;
          }
      case IDOK:
      case IDOK2:
        id = mpLogList->GetCurSel();

        mpLogList->GetText(id, buf);

        if (DiscEject(buf) == FALSE)
          {
            break;
          }

        EndDialog(IDOK);
        break;

      case IDCANCEL:
      case IDCANCEL2:
        //
        EndDialog(IDCANCEL);
        break;

      default:
        break;
    }
}

/* ----------------------------------------------------------------- *
 * Dialog initialization
 * ----------------------------------------------------------------- */
BOOL dlg_ejectdsk::OnInitDialog(HWND hwndFocus, LPARAM lParam)
{
  TCHAR   buf[MAX_PATH];
  int     type;
  TCHAR*  cp;

  mpLogList = new com_list;

  Attach(*mpLogList, IDC_LIST1);
  Attach(*mpOK,      IDOK2);
  Attach(*mpCancel,  IDCANCEL2);

  /* Get list of drives */

  GetLogicalDriveStrings(MAX_PATH, buf);

  for (cp = buf; *cp != '\0'; cp += (lstrlen(cp) + 1))
    {
      type = GetDriveType(cp);

      if (!(type == DRIVE_CDROM || type == DRIVE_REMOVABLE))
        {
          continue;
        }

      if (!PathFileExists(cp))
        {
          continue;
        }

      mpLogList->SetLogData(-1, cp, 0);
    }

  /* Move in the middle */

  CenterWindow();

  mpLogList->SetCurSel(0);
  mpLogList->SetFocus();

  return 0;
}

/* ----------------------------------------------------------------- *
 * Eject disc
 * ----------------------------------------------------------------- */
BOOL dlg_ejectdsk::DiscEject(TCHAR *drv)
{
  BOOL  res = FALSE;

  res = DiscEjectSub(drv);

  if (res)
    {
      MessageBox(_T("Ejected. Unplug it."), _T("Success!"), MB_ICONINFORMATION);
    }
  else
    {
      MessageBox(_T("Could not eject (; _;)\r\nClose other applications."), _T("Error!"), MB_ICONSTOP);
    }

  return res;
}

/* ----------------------------------------------------------------- *
 * Loading a disc
 * ----------------------------------------------------------------- */
BOOL dlg_ejectdsk::DiscLoad(int c)
{
  return FALSE;
}

/* ----------------------------------------------------------------- */
BOOL dlg_ejectdsk::CheckEjectDrive(void)
{
  TCHAR   buf[MAX_PATH];
  BOOL    res = FALSE;
  int     type;
  TCHAR*  cp;

  /* Get list of drives */

  GetLogicalDriveStrings(MAX_PATH, buf);

  for (cp = buf; *cp != '\0'; cp += (lstrlen(cp) + 1))
    {
      type = GetDriveType(cp);

      if (!(type == DRIVE_CDROM || type == DRIVE_REMOVABLE))
        {
          continue;
        }

      if (!PathFileExists(cp))
        {
          continue;
        }

      res = TRUE;
      break;
    }

  return res;
}

/* ----------------------------------------------------------------- */
void dlg_ejectdsk::OnPaint()
{
  xPaintDC dc(mhWnd);
  RECT     rc;
  IPT     *pt = xPt();

  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(pt->color.title);
  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  GetClientRect(&rc);

  dc.FillRect(&rc, pt->color.base_bar);

  dc.TextOut(rc.left + 4, rc.top + 4, L"Select the drive to eject");
}
