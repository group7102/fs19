/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_history.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DLG_HISTORY_H_
#define _DLG_HISTORY_H_

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */
CLASS_INHERITANCE(dlg_history, xDialog)

/* ----------------------------------------------------------------- *
 * Public types
 * ----------------------------------------------------------------- */

struct HitItemData {
  int    argc;
  TCHAR *argv[100];
  TCHAR  str[1];
};

/* ----------------------------------------------------------------- *
 * Private parameters
 * ----------------------------------------------------------------- */

xListBox *mpList;

/* Directry history(new) */

void  *handle_back;
void  *handle_step;

/* Current path */

TCHAR *mpCurrentPath;

/* Target path */

const TCHAR *mpTargetPath;

/* Current data */

HitItemData *mpCurrentData;

/* Font info */

TEXTMETRIC tm;

int mOffset1;
int mOffset2;
int mHeight;

/* logical pixsel */

int mLogPixel;

/* ----------------------------------------------------------------- */
dlg_history(HWND hParent) : xDialog(_T("IDD_HISTORY_VIEW"), hParent)
{
  mpList = new xListBox;

  mpCurrentPath = NULL;
  mpCurrentData = NULL;
  mpTargetPath  = NULL;

  mOffset1 = 0;
  mOffset2 = 0;
  mHeight  = 0;
}

/* ----------------------------------------------------------------- */
~dlg_history()
{
  delete mpList;

  if (mpCurrentPath) {
    free(mpCurrentPath);
  }

  if (mpCurrentData) {
    free(mpCurrentData);
  }
}

/* ----------------------------------------------------------------- */
void SetSimpleQueHandle(void *back, void *step)
{
  handle_back = back;
  handle_step = step;
}

/* ----------------------------------------------------------------- */
HitItemData *SetItemData(const TCHAR *str, bool is_relative = true)
{
  HitItemData *p_item = NULL;

  TCHAR buf[MAX_PATH];

  if (is_relative) {
    if (PathRelativePathTo(buf,
                           mpTargetPath,
                           FILE_ATTRIBUTE_DIRECTORY,
                           str,
                           FILE_ATTRIBUTE_DIRECTORY)) {
      str = buf;
    }
  }

  size_t len   = _tcslen(str) + 1;
  size_t bsize = sizeof(HitItemData) + len * sizeof(TCHAR);

  p_item = (HitItemData *)malloc(bsize);

  if (p_item) {
    _sntprintf_s(p_item->str, len, _TRUNCATE, L"%s", str);

    const TCHAR tok[] = L"\\/";
    TCHAR      *cp;
    TCHAR      *contex;
    int         i = 0;

    cp = _tcstok_s(p_item->str, tok, &contex);

    for(; cp; cp = _tcstok_s(NULL, tok, &contex), i++) {
      p_item->argv[i] = cp;
    }

    p_item->argc = i;
  }

  return p_item;
}

/* ----------------------------------------------------------------- */
BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam)
{
  IPT   *pt = xPt();

  /* Attach control */

  Attach(mpList, IDC_LIST1);

  CenterWindow();   /* Move to center */

  xDC  dc(mhWnd);

  mLogPixel = -MulDiv(GetDeviceCaps(dc, LOGPIXELSY), 9, 72);

  dc.SetSelectFont(pt->fonts[4].name, mLogPixel);

  if (dc.tm.tmHeight < 16) {
    mHeight = 16;
  }
  else {
    mHeight = dc.tm.tmHeight;
  }

  mHeight += 2;

  mOffset1 = (mHeight - dc.tm.tmHeight) / 2;
  mOffset2 = (mHeight - 16) / 2;

  /* Set size of listbox */

  RECT  rc;

  GetClientRect(&rc);

  mpList->MoveWindow(mOffset1,
                     mHeight,
                     rc.right - rc.left - mOffset1 * 2,
                     rc.bottom - rc.top - mOffset1- mHeight);

  /* Set item of listbox */

  int          cnt;
  const TCHAR *str;

  if (handle_back == NULL) {
    goto errout;
  }

  cnt = SimpleQueGetCount(handle_back);

  if (cnt <= 0) {
    goto errout;
  }

  /* Save current folder */

  if (mpTargetPath == NULL) {
    SimpleQueGetAt(handle_back, 0, &mpTargetPath);
  }

  mpCurrentData = SetItemData(mpTargetPath);

  /* Show current folder */

  SetWindowText(mpTargetPath);

  for (int i = 0; i < cnt; i++) {
    str = NULL;

    if (SimpleQueGetAt(handle_back, i, &str) != 0) {
      LOGE("SimpleQueGetAt(%d)", i);
      continue;
    }

    TCHAR buf[MAX_PATH];

    if (PathRelativePathTo(buf,
                           mpTargetPath,
                           FILE_ATTRIBUTE_DIRECTORY,
                           str,
                           FILE_ATTRIBUTE_DIRECTORY)) {
      PathRemoveBackslash(buf);

      if (_tcscmp(PathFindFileName(buf), L"..") == 0) {
        continue;
      }
    }

    mpList->InsertString(0, str);
  }

  cnt = mpList->GetCount();

  if (cnt <= 0) {
    LOGE("GetCount");
    goto errout;
  }

  if (mpList->SetCurSel(cnt - 1) < 0) {
    LOGE("SetCurSel");
  }

  /* Success! */

errout:
  return 0;
}

/* ----------------------------------------------------------------- */
void MeasureItem(MEASUREITEMSTRUCT *pMIS)
{
  pMIS->itemHeight = mHeight;
}

/* ----------------------------------------------------------------- */
void DrawItem(const DRAWITEMSTRUCT *pDrawItem)
{
  if (pDrawItem->CtlID != IDC_LIST1) {
    return;
  }

  if (pDrawItem->itemID < 0) {
    LOGE("GetCurSel %d", pDrawItem->itemID);
    return;
  }

  int len = mpList->GetTextLen(pDrawItem->itemID);

  TCHAR *buf = (TCHAR *)alloca((len + 1) * sizeof(TCHAR));

  if (buf == NULL) {
    LOGE("malloc %d", len);
    return;
  }

  mpList->GetText(pDrawItem->itemID, buf);

  IPT   *pt = xPt();
  xTmpDC dc(pDrawItem->hDC);

  int x  = pDrawItem->rcItem.left;
  int y  = pDrawItem->rcItem.top;
  int cx = pDrawItem->rcItem.right  - pDrawItem->rcItem.left;
  int cy = pDrawItem->rcItem.bottom - pDrawItem->rcItem.top;

  dc.SetSelectFont(pt->fonts[4].name, mLogPixel);

  if (pDrawItem->itemState & ODS_SELECTED) {
    dc.Rectangle(x, y, cx, cy, pt->color.selected_frame, pt->color.selected);
  }
  else {
    dc.FillRect(x, y, cx, cy, pt->color.base_bar);
  }

  if (mpCurrentData == NULL) {
    return;
  }

  HitItemData *p_item = SetItemData(buf);

  if (p_item == NULL) {
    LOGE("SetItemData");
    return;
  }

  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(RGB(128, 128, 128));

  int  i = 0;
  int  first = 0;
  SIZE size;

  x += 4;

  for (; i < p_item->argc - 1; i++) {
    if (first == 0 && i < mpCurrentData->argc) {
      if (_tcscmp(mpCurrentData->argv[i], p_item->argv[i]) != 0) {
        dc.SetTextColor(0);
        first = 1;
      }
    }

    dc.GetTextExtentPoint32(p_item->argv[i], &size);
    dc.TextOut(x, y + mOffset1, p_item->argv[i]);
    x += size.cx;
    IcoDrawIconFromIndex(dc, x, y + mOffset2, 22, TRUE);
    x += 16;
  }

  if (first == 0) {
    dc.SetTextColor(0);
  }

  dc.TextOut(x, y + mOffset1, p_item->argv[i]);

  free(p_item);
}

/* ----------------------------------------------------------------- */
void OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  int sel = mpList->GetCurSel();

  if (sel < 0) {
    return;
  }

  int len = mpList->GetTextLen(sel);

  if (len < 0) {
    return;
  }

  TCHAR *buf = (TCHAR *)malloc((len + 1) * sizeof(TCHAR));

  if (buf == NULL) {
    return;
  }

  mpList->GetText(sel, buf);
  PathRemoveBackslash(buf);

  xDC dc(mhWnd);

  SetTitle(dc, buf);

  SetWindowText(buf);
}

/* ----------------------------------------------------------------- */
LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam)
{
  LRESULT res = 0;

  int sel;
  int len;
  int result = IDCANCEL;

  switch(msg) {
  case WM_SYSKEYUP:
    if (wParam != VK_CONTROL) {
      res = xDialog::Message(msg, wParam, lParam);
      break;
    }

    sel = mpList->GetCurSel();

    if (sel < 0) {
      LOGE("GetCurSel %d", sel);
      EndDialog(IDCANCEL);
      break;
    }

    if (mpCurrentPath) {
      free(mpCurrentPath);
    }

    len = mpList->GetTextLen(sel);

    mpCurrentPath = (TCHAR *)malloc((len + 1) * sizeof(TCHAR));

    if (mpCurrentPath) {
      mpList->GetText(sel, mpCurrentPath);

      if (_tcscmp(mpCurrentPath, mpTargetPath) != 0) {
        result = IDOK;
      }
    }
    else {
      LOGE("malloc %d", len);
    }

    EndDialog(result);

    break;

  case WM_KEYDOWN:
    if (wParam == VK_ESCAPE) {
      EndDialog(IDCANCEL);
    }
    else {
      res = xDialog::Message(msg, wParam, lParam);
    }
    break;

  case WM_SYSKEYDOWN:
    if (wParam == VK_UP
     || wParam == VK_DOWN) {
      mpList->SendMessage(WM_KEYDOWN, wParam, 0);
    }
    break;

  case WM_MEASUREITEM:
    MeasureItem((MEASUREITEMSTRUCT *)lParam);
    break;

  case WM_DRAWITEM:
    DrawItem((const DRAWITEMSTRUCT *)lParam);
    break;

  default:
    res = xDialog::Message(msg, wParam, lParam);
    break;
  }

  return res;
}

/* ----------------------------------------------------------------- */
void OnPaint(void)
{
  xPaintDC  dc(mhWnd);
  RECT      rc;
  IPT      *pt = xPt();

  GetClientRect(&rc);
  dc.FillRect(&rc, pt->color.base_bar);

  TCHAR buf[MAX_PATH];

  GetWindowText(buf, _countof(buf));

  SetTitle(dc, buf);
}

/* ----------------------------------------------------------------- */
void SetTitle(HDC hDC, TCHAR *str)
{
  xTmpDC    dc(hDC);
  RECT      rc;
  IPT      *pt = xPt();
  xBmpDC bmp;

  GetClientRect(&rc);

  int x  = rc.left + 8 + 16;
  int y1 = rc.top + mOffset1;
  int y2 = rc.top + mOffset2;
  int cx = rc.right  - rc.left;
  int cy = mHeight;

  bmp.Create(dc, cx, cy);
  bmp.SetBkMode(TRANSPARENT);
  bmp.SetTextColor(pt->color.title);
  bmp.SetSelectFont(pt->fonts[4].name, mLogPixel);
  bmp.FillRect(0, 0, cx, cy, pt->color.base_bar);

  HitItemData *p_item = SetItemData(str, false);
  SIZE         size;
  int          i;

  IcoDrawIconFromIndex(bmp, mOffset1, y2, 75, TRUE);

  for (i = 0; i < p_item->argc - 1; i++) {
    bmp.GetTextExtentPoint32(p_item->argv[i], &size);
    bmp.TextOut(x, y1, p_item->argv[i]);
    x += size.cx;
    IcoDrawIconFromIndex(bmp, x, y2, 22, TRUE);
    x += 16;
  }

  bmp.TextOut(x, y1, p_item->argv[i]);

  dc.BitBlt(rc.left, rc.top, cx, cy, bmp, 0, 0, SRCCOPY);

  free(p_item);
}

CLASS_END

#endif /* _DLG_HISTORY_H_ */
