#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"

int main(int argc, char **argv)
{
  sqlite3      *db   = NULL;
  sqlite3_stmt *stmt = NULL;
  int           ret;
  char         *msg;

  /* Open */

  ret = sqlite3_open("db.bin", &db);

  if (SQLITE_OK != ret) {
    printf("*** error:%d:sqlite3_open\n", ret);
    return -1;
  }

  /* CREATE TABLE */

  ret = sqlite3_exec(db,
                     "CREATE TABLE Test(id INTEGER, name CHAR(32))",
                     NULL,
                     NULL,
                     &msg);

  if (SQLITE_OK != ret) {
    printf("sqlite3_exec %s\n", msg);
  }

  /* INSERT INTO */

  ret = sqlite3_exec(db,
                     "INSERT INTO Test VALUES(1, 'shino1')",
                     NULL,
                     NULL,
                     &msg);

  if (SQLITE_OK != ret) {
    printf("*** error:sqlite3_exec %s\n", msg);
  }

  /* INSERT INTO */

  ret = sqlite3_exec(db,
                     "INSERT INTO Test VALUES(2, 'shino2')",
                     NULL,
                     NULL,
                     &msg);

  if (SQLITE_OK != ret) {
    printf("*** error:sqlite3_exec %s\n", msg);
  }

  /* SELECT */

  ret = sqlite3_prepare_v2(db,
                           "SELECT * FROM Test",
                           64,
                           &stmt,
                           NULL);
  if (SQLITE_OK != ret) {
    printf("*** error:sqlite3_prepare_v2\n");
  }
  else {
    for (;;) {
      ret = sqlite3_step(stmt);

      if (ret != SQLITE_ROW) {
        break;
      }

      printf("%d %s\n",
              sqlite3_column_int (stmt, 0),
              sqlite3_column_text(stmt, 1));
    }

    sqlite3_finalize(stmt);
  }

  /* Close */

  ret = sqlite3_close(db);

  if (SQLITE_OK != ret) {
    printf("*** error:%d:sqlite3_close\n", ret);
  }

  return 0;
}
