/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: formatinc.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 */

#ifndef _FORMATINC_H_
#define _FORMATINC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Functions
 * ----------------------------------------------------------------- */

DWORD  FormatColor(dirent *entry);
int    FormatInc(TCHAR buf[], dirent *entry, int max_len, int str_len, int *str_offset);

#ifdef __cplusplus
}
#endif

#endif /* _FORMATINC_H_ */
