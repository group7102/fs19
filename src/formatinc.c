/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: formatinc.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

/* ----------------------------------------------------------------- *
 * Included files
 * ----------------------------------------------------------------- */

#include <windows.h>
#include <malloc.h>
#include <time.h>
#include "dirent.h"
#include "logf.h"
#include "xpt.h"
#include "formatinc.h"

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define EXT_MAX_WIDTH  5

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static TCHAR *CheckFileSize(TCHAR *cp, DWORD hi, DWORD lo)
{
  static const TCHAR hs[]  = _T("BKMGT");
  static const TCHAR ss1[] = _T(" 123456789");
  static const TCHAR ss2[] = _T("0123456789");

  int           i = 0;
  DWORD         poi;
  DWORD         mod;
  LARGE_INTEGER tmp;

  tmp.HighPart  = hi;
  tmp.LowPart   = lo;
  tmp.QuadPart *= 10;

  while (tmp.QuadPart >= 10000) {
    tmp.QuadPart /= 1024;
    i++;
  }

  poi = (DWORD)(tmp.LowPart / 10);
  mod = (DWORD)(tmp.LowPart % 10);

  if (i == 0) {
    *cp++ = ' ';
    *cp++ = ' ';
  }

  *cp++ = ' ';

  if (poi < 10) {
    *cp++ = ' ';
    *cp++ = ' ';
    *cp++ = ss2[poi];
  }
  else if (poi < 100) {
    *cp++ = ' ';
    *cp++ = ss1[poi / 10];
    *cp++ = ss2[poi % 10];
  }
  else if (poi < 1000) {
    *cp++ = ss1[poi / 100];
    *cp++ = ss2[(poi /  10) % 10];
    *cp++ = ss2[poi %  10];
  }

  if (i != 0) {
    *cp++ = '.';
    *cp++ = ss2[mod];
  }

  *cp++ = hs[i];

  return cp;
}

/* ----------------------------------------------------------------- */
static TCHAR *CheckFileData(TCHAR *cp, int dat, int flag)
{
  static const TCHAR ss[] = _T("0123456789");

  if (dat < 10) {
    if (flag) {
      *cp++ = ' ';
    }
    else {
      *cp++ = '0';
    }

    *cp++ = ss[dat];
  }
  else if (dat < 100) {
    *cp++ = ss[dat / 10];
    *cp++ = ss[dat % 10];
  }
  else {
    *cp++ = '#';
    *cp++ = '#';
  }

  return cp;
}

/* ----------------------------------------------------------------- */
static TCHAR *CheckFileAttr(TCHAR *cp, DWORD attr)
{
  DWORD attrs[] = {
    0,
    FILE_ATTRIBUTE_SYSTEM,
    FILE_ATTRIBUTE_HIDDEN,
    FILE_ATTRIBUTE_READONLY,
    FILE_ATTRIBUTE_ARCHIVE,
    FILE_ATTRIBUTE_DIRECTORY
  };
  TCHAR marks[] = _T(" SHRAD");
  int   i;

  for (i = 0; i < 6; i++) {
    if (attrs[i] == 0) {
      *cp++ = marks[i];
    }
    else if (attr & attrs[i]) {
      *cp++ = marks[i];
    }
    else {
      *cp++ = '-';
    }
  }

  return cp;
}

/* ----------------------------------------------------------------- */
static int CheckNewStringLen(int len, int *max)
{
  int   len_cnt[] = { 0, 7, 9, 6, 6 };
  int   i;

  for (i = 0; i < 5; i++) {
    if (len > *max - len_cnt[i]) {
      break;
    }

    *max -= len_cnt[i];
  }

  return i;
}

/* ----------------------------------------------------------------- */
static int strlenw(TCHAR *str, int width, int *o_w, int *o_t)
{
  int   len;
  int   lend;
  int   cnt;
  int   i;
  int   total;
  int   res = 0;
  WORD *types;
  WORD  flags = C3_HALFWIDTH | C3_SYMBOL;

  total = (int)_tcsclen(str);

  types = (WORD*)alloca(sizeof(WORD) * (total + 1));

  GetStringTypeEx(LANG_USER_DEFAULT, CT_CTYPE3, str, total, types);

  for (lend = 0, len = 0, cnt = 0, i = 0; i < total; i++) {
    if (types[i] & flags) {
      len++;
    }
    else {
      len += 2;
    }

    if (width - 3 >= len) {
      cnt++;
      lend = len;
    }
    else if (width < len) {
      i   = cnt;
      len = lend;
      res = 1;
      break;
    }
  }

  *o_w = len;
  *o_t = i;

  return res;
}

/* ----------------------------------------------------------------- */
static TCHAR *CheckFileName(TCHAR *cp, TCHAR *src, int len)
{
  TCHAR *src1;
  int    i;
  TCHAR  b;
  int    w;
  int    t;

  if (strlenw(src, len, &w, &t)) {
    b = _T('.');
  }
  else {
    b = _T(' ');
  }

  for (i = 0; i < t; i++) {
    src1 = CharNext(src);

    while (src1 > src) {
      *cp++ = *src++;
    }
  }

  for (i = 0; i < 3 && w < len; w++, i++) {
    *cp++ = b;
  }

  for (; w < len; w++) {
    *cp++ = _T(' ');
  }

  *cp = '\0';

  return cp;
}

/* ----------------------------------------------------------------- */
static TCHAR *PathFindFileName(TCHAR *str)
{
  TCHAR *cp = str;

  for (int i = 0; str[i] && i < MAX_PATH; i++) {
    if (str[i] == '/' || str[i] == '\\') {
      cp = &str[i + 1];
    }
  }

  return cp;
}

/* ----------------------------------------------------------------- */
static TCHAR *PathFindExtension(TCHAR *str)
{
  TCHAR *cp = str;

  for (int i = 0; str[i] && i < MAX_PATH; i++) {
    if (str[i] == '.') {
      cp = &str[i];
    }
  }

  return cp;
}

/* ----------------------------------------------------------------- */
static DWORD FormatColorAsAttr(DWORD attr)
{
  DWORD color;
  IPT  *pt = xPt();

  if (attr & FILE_ATTRIBUTE_SYSTEM) {
    color = pt->color.file_attr_system;
  }
  else if (attr & FILE_ATTRIBUTE_HIDDEN) {
    color = pt->color.file_attr_hidden;
  }
  else if (attr & FILE_ATTRIBUTE_READONLY) {
    color = pt->color.file_attr_readonly;
  }
  else if (attr & FILE_ATTRIBUTE_DIRECTORY) {
    color = pt->color.file_attr_directory;
  }
  else {
    /* FILE_ATTRIBUTE_NORMAL */

    color = pt->color.file_attr_normal;
  }

  return color;
}

/* ----------------------------------------------------------------- *
 * public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
DWORD FormatColor(dirent *entry)
{
  return FormatColorAsAttr(entry->dwFileAttributes);
}

/* ----------------------------------------------------------------- */
int FormatInc(TCHAR buf[], dirent *entry, int max_len, int str_len, int *str_offset)
{
  TCHAR     *cp;
  TCHAR     *ext;
  TCHAR     *path;
  struct tm  local;
  int        res;
  int        len;
  TCHAR      name[MAX_PATH];

  /* convert time_t to local time */

  localtime_s(&local, &entry->mtime);

  _sntprintf_s(name, _countof(name), _TRUNCATE, L"%s", entry->d_name);

  path = PathFindFileName(name);
  ext  = PathFindExtension(path);

  if (path == ext || _tcscmp(path, _T("..")) == 0) {
    ext += _tcslen(ext);
  }

  res = CheckNewStringLen(str_len, &max_len);

  do {
    cp = buf;

    len = (int)_tcslen(ext);

    if (len == 0 || len > EXT_MAX_WIDTH || max_len <= 8) {
      cp = CheckFileName(cp, path, max_len);
    }
    else {
      ext[0] = '\0';
      cp = CheckFileName(cp, path, max_len - EXT_MAX_WIDTH);
      ext[0] = ' ';
      cp = CheckFileName(cp, ext,  EXT_MAX_WIDTH);
    }

    *str_offset = (int)(cp - buf);

    if (res < 2) {
      break;
    }

    if (entry->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
      *cp++ = ' '; *cp++ = ' ';
      *cp++ = '<'; *cp++ = 'D';
      *cp++ = 'I'; *cp++ = 'R';
      *cp++ = '>';
    }
    else {
      cp = CheckFileSize(cp, entry->nFileSizeHigh, entry->nFileSizeLow);
    }

    if (res < 3) {
      break;
    }

    *cp++ = ' ';
    cp = CheckFileData(cp, (local.tm_year + 1900) % 100, 0);
    *cp++ = '-';
    cp = CheckFileData(cp, local.tm_mon + 1, 0);
    *cp++ = '-';
    cp = CheckFileData(cp, local.tm_mday, 0);

    if (res < 4) {
      break;
    }

    *cp++ = ' ';
    cp = CheckFileData(cp, local.tm_hour, 1);
    *cp++ = ':';
    cp = CheckFileData(cp, local.tm_min, 0);

    if (res < 5) {
      break;
    }

    cp = CheckFileAttr(cp, entry->dwFileAttributes);
  }
  while(0);

  *cp = '\0';

  return max_len;
}
