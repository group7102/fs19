#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h> 
#include "command.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private parameters
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
size_t __strnlen(const wchar_t *str, size_t len)
{
  return wcsnlen(str, len);
}

/* ----------------------------------------------------------------- */
size_t __strlen(const wchar_t *str)
{
  return wcslen(str);
}

/* ----------------------------------------------------------------- */
int __strncmp(const wchar_t *str1, const wchar_t *str2, size_t len)
{
  return wcsncmp(str1, str2, len);
}

/* ----------------------------------------------------------------- */
int __strcmp(const wchar_t *str1, const wchar_t *str2)
{
  return wcscmp(str1, str2);
}

/* ----------------------------------------------------------------- */
int ___snprintf(wchar_t *buf, size_t size, const wchar_t *fmt, ...)
{
  va_list  ap;

  va_start(ap, fmt);

  int len = _vsnwprintf_s(buf, size, _TRUNCATE, fmt, ap);

  if (len < 0) {
    len = (int)size - 1;
  }

  va_end(ap);

  return len;
}
