/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: copy_funcs.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "dlg_confirm.h"
#include "dlg_rename.h"
#include "sub_funcs.h"
#include "logf.h"
#include "command.h"
#include "copy_funcs.h"

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define ACTION  TCOPYACTION

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static void Message(HWND hWnd, int id, ACTION *act)
{
  ::SendMessage(hWnd,
                WM_COMMAND,
                MAKEWPARAM((UINT)(ICOPY_BASE_ID),(UINT)(id)),
                (LPARAM)act);
}

/* ----------------------------------------------------------------- */
static TCHAR *PathAppend2nd(TCHAR *buf, const TCHAR *path1, const TCHAR *path2)
{
  size_t len1 = __strnlen(path1, MAX_PATH) + 1;

  if (len1 > MAX_PATH) {
    LOGE("__strnlen:%d", len1);
    goto errout;
  }

  size_t len2 = __strnlen(path2, MAX_PATH) + 1;

  if (len2 > MAX_PATH) {
    LOGE("__strnlen:%d", len2);
    goto errout;
  }

  TCHAR *tmp = (TCHAR *)realloc(buf, (len1 + len2) * sizeof(TCHAR));

  if (tmp == NULL) {
    LOGE("realloc");
    goto errout;
  }

  __snprintf(tmp, len1, "%s", path1);

  PathAppend(tmp, path2);

  return tmp;

errout:
  if (buf) {
    free(buf);
  }

  return NULL;
}

/* ----------------------------------------------------------------- */
static int PumpMessage(HWND hWnd)
{
  MSG   msg;
  int   res = 0;

  while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
    {
      if (msg.message == WM_QUIT)
        {
          res = 1;
          break;
        }
      else if (msg.message == WM_KEYDOWN)
        {
          if (msg.wParam == VK_ESCAPE)
            {
              if (::MessageBox(hWnd,
                               _T("Copying in progress. Do you want to cancel it?"),
                               _T("what will you do?"),
                               MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
                {
                  res = 1;
                  break;
                }
            }
        }

      if (GetMessage(&msg, NULL, 0, 0) <= 0)
        {
          break;
        }

      ::TranslateMessage(&msg);
      ::DispatchMessage (&msg);
    }

  return res;
}

/* ----------------------------------------------------------------- */
static time_t From_FILETIME_to_time_t(FILETIME *from_time)
{
  /* Constant for converting from windows time to linux time */

  const time_t unix_epoch_offset = 116444736000000000LL;

  union _tag {
    FILETIME times;
    time_t   qpart;
  }
  *update = (union _tag *)from_time;

  return (update->qpart - unix_epoch_offset) / 10000000;
}

/* ----------------------------------------------------------------- */
static dirent *CreateEntry(HANDLE         hHeap,
                           dirent        *top,
                           const TCHAR   *name,
                           DWORD          attr,
                           time_t         time,
                           LARGE_INTEGER *size)
{
  dirent* elm;
  size_t  len = _tcslen(name) + 1 ;

  elm = (dirent *)::HeapAlloc(hHeap, 0, sizeof(dirent) + len * sizeof(TCHAR));

  if (elm)
    {
      _tcscpy_s(elm->d_name, len, name);
      elm->hHandle          = hHeap;
      elm->dwFileAttributes = attr;
      elm->ctime            = time;
      elm->nFileSizeHigh    = size->HighPart;
      elm->nFileSizeLow     = size->LowPart;
      elm->next             = top;
      elm->d_cnt            = (int)(PathFindFileName(elm->d_name) - elm->d_name);
      elm->state            = -1;
      elm->bookMark         = TCOPYBMARK_FAIL_IF_EXISTS;
      elm->param            = elm;
    }

  return elm;
}

/* ----------------------------------------------------------------- */
static dirent *CreateEntry(HANDLE           hHeap,
                           dirent          *top,
                           const TCHAR     *dir,
                           WIN32_FIND_DATA *fd,
                           int              d_len = 0)
{
  dirent* elm = NULL;
  size_t  len = _tcslen(dir) + 1 ;

  if (len < MAX_PATH)
    {
      elm = (dirent *)::HeapAlloc(hHeap, 0, sizeof(dirent) + len * sizeof(TCHAR));
    }

  if (elm)
    {
      _tcscpy_s(elm->d_name, len, dir);

      elm->hHandle          = hHeap;
      elm->dwFileAttributes = fd->dwFileAttributes;
      elm->ctime            = From_FILETIME_to_time_t(&fd->ftLastWriteTime);
      elm->nFileSizeHigh    = fd->nFileSizeHigh;
      elm->nFileSizeLow     = fd->nFileSizeLow;
      elm->next             = top;
      elm->d_cnt            = d_len;
      elm->state            = -1;
      elm->bookMark         = TCOPYBMARK_FAIL_IF_EXISTS;
      elm->param            = elm;
    }

  return elm;
}

/* ----------------------------------------------------------------- */
static dirent *open_sub(dirent *top)
{
  HANDLE          hFile = INVALID_HANDLE_VALUE;
  WIN32_FIND_DATA fd;
  TCHAR          *src = NULL;
  dirent         *elm = top->next;

  src = PathAppend2nd(src, top->d_name, _T("*.*"));

  if (src == NULL) {
    LOGE("PathAppend2nd");
    goto errout;
  }

  hFile = FindFirstFile(src, &fd);

  if (hFile == INVALID_HANDLE_VALUE) {
    LOGE("FindFirstFile");
    goto errout;
  }

  do {
    if (_tcscmp(fd.cFileName, _T("."))== 0 || _tcscmp(fd.cFileName, _T("..")) == 0) {
      continue;
    }

    src = PathAppend2nd(src, top->d_name, fd.cFileName);

    if (src == NULL) {
      LOGE("PathAppend2nd");
      break;
    }

    elm = CreateEntry(top->hHandle, elm, src, &fd, top->d_cnt);

    if (elm == NULL) {
      break;
    }
  } while (FindNextFile(hFile, &fd));

errout:
  /* Clean up  */

  if (hFile != INVALID_HANDLE_VALUE) {
    FindClose(hFile);
  }

  if (src) {
    free(src);
  }

  return elm;
}

/* ----------------------------------------------------------------- */
static BOOL Rename(dirent *elm, TCHAR *new_name)
{
  dirent *trans = NULL;
  BOOL    res   = FALSE;
  size_t  len;
  size_t  size;
  TCHAR   buf[MAX_PATH];

  _tcscpy_s(buf, MAX_PATH, elm->d_name);

  PathRemoveFileSpec(buf);
  PathRemoveBackslash(buf);

  if (_tcslen(buf) + _tcslen(new_name) + 1 >= MAX_PATH)
    {
      return FALSE;
    }

  PathAppend(buf, new_name);

  /* String length = Path name without file name
   *               + delimiter
   *               + new file name
   *               + delimiter
   */

  len = _tcslen(buf) + 1;

  /* Number of bytes to reserve for the number of characters */

  size = sizeof(dirent) + len * sizeof(TCHAR);

  trans = (dirent*)::HeapAlloc(elm->hHandle, 0, size);

  if (!trans)
    {
      return FALSE;
    }

  /* New file name copy */

  ZeroMemory(trans, size);

  *trans          = *elm;
  trans->next     = elm->next;
  elm->next       = trans;
  elm->param      = trans;
  elm->bookMark   = TCOPYBMARK_RENAME;
  trans->param    = NULL;
  trans->bookMark = TCOPYBMARK_NULL;

  _tcscpy_s(trans->d_name, len, buf);

  return TRUE;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static BOOL GetFileInfo(const TCHAR *name, DWORD *attr, time_t *time, LARGE_INTEGER *size)
{
  HANDLE hFile = CreateFile(name,
                            0,
                            FILE_SHARE_READ,
                            NULL,
                            OPEN_EXISTING,
                            FILE_FLAG_BACKUP_SEMANTICS,
                            0);

  if (hFile == INVALID_HANDLE_VALUE) {
    return FALSE;
  }

  BY_HANDLE_FILE_INFORMATION info;

  BOOL res = GetFileInformationByHandle(hFile, &info);

  CloseHandle(hFile);

  if (res == FALSE) {
    LOGE("GetFileInformationByHandle");
    return FALSE;
  }

  if (attr) {
    *attr = info.dwFileAttributes;
  }

  if (time) {
    *time = From_FILETIME_to_time_t(&info.ftLastWriteTime);
  }

  if (size) {
    size->LowPart  = info.nFileSizeLow;
    size->HighPart = info.nFileSizeHigh;
  }

  return TRUE;
}

/* ----------------------------------------------------------------- */
dirent *ReadClips(HWND hWnd, HANDLE hHeap, const TCHAR *dir_path)
{
  int           cnt;
  int           i;
  TCHAR         src[MAX_PATH * 2];
  HDROP         hDrop;
  DWORD         attr;
  time_t        time;
  LARGE_INTEGER size;
  dirent       *elm;
  dirent       *top = NULL;

  if (!IsClipboardFormatAvailable(CF_HDROP))
    {
      return FALSE;
    }

  OpenClipboard(hWnd);

  hDrop = (HDROP)GetClipboardData(CF_HDROP);

  if (hDrop != NULL)
    {
      /* Get number of files */

      cnt = ::DragQueryFile(hDrop, (unsigned)-1, NULL, 0);

      for (i = 0; i < cnt; i++)
        {
          ::DragQueryFile(hDrop, i, src, MAX_PATH * 2);

          attr = 0;
          GetFileInfo(src, &attr, &time, &size);
          top = CreateEntry(hHeap, top, src, attr, time, &size);
        }
    }

  CloseClipboard();

  for (elm = top; elm; elm = elm->next)
    {
      if (elm->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
          elm->next = open_sub(elm);
        }
    }

  return top;
}

/* ----------------------------------------------------------------- */
int CheckClips(HWND hWnd)
{
  int     cnt = 0;
  HDROP   hDrop;

  if (!IsClipboardFormatAvailable(CF_HDROP))
    {
      return 0;
    }

  OpenClipboard(hWnd);

  hDrop = (HDROP)GetClipboardData(CF_HDROP);

  if (hDrop != NULL)
    {
      /* Get number of files */

      cnt = ::DragQueryFile(hDrop, (unsigned)-1, NULL, 0);
    }

  CloseClipboard();

  return cnt;
}

/* ----------------------------------------------------------------- */
BOOL CheckCopyInfo(HWND hWnd, dirent* top, const TCHAR *dir)
{
  TCHAR        *trans = NULL;
  dirent       *elm;
  dlg_confirm  *confirm;
  BOOL          res = TRUE;
  time_t        time;
  int           theAll   = 0;
  int           selected = 0;
  DWORD         attr     = 0;
  LARGE_INTEGER size;

  confirm = new dlg_confirm(hWnd);

  if (confirm == NULL) {
    LOGE("dlg_confirm");
    return FALSE;
  }

  LOGI("To dir :%s", dir);

  for (elm = top; elm; elm = elm->next) {
    trans = PathAppend2nd(trans, dir, elm->d_name + elm->d_cnt);

    if (trans == NULL) {
      break;
    }

    LOGI("To path:%s", trans);

    if (!GetFileInfo(trans, &attr, &time, &size)) {
      continue;
    }

    if ((elm->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
     && attr == elm->dwFileAttributes) {
      if (PathFileExists(trans)) {
        elm->bookMark = TCOPYBMARK_EXIST_DIRECTORY;
      }
      continue;
    }

    /* Process when there is a file with the same name */

    confirm->SetSourceFileInfo(elm->d_name,
                               &elm->ctime,
                               elm->nFileSizeHigh,
                               elm->nFileSizeLow);
    confirm->SetTransFileInfo(trans, &time, &size);

    if (theAll == 0) {
      confirm->selected = selected;

      if (confirm->Modal() == IDCANCEL) {
        res = FALSE;
        break;
      }

      if (confirm->thatsall) {
        theAll = 1;
      }

      selected = confirm->selected;
    }

    /* Processing result */

    if (selected == IKAKU_RENAME) {
      Rename(elm, confirm->mNewPath);
    }
    else if (selected == IKAKU_NEW) {
      if (difftime(elm->ctime, time) > 0) {
        /* The destination file is older */

        elm->bookMark = TCOPYBMARK_OVERWRITE;
      }
      else {
        elm->bookMark = TCOPYBMARK_NOT_COPY_FOR_DATE;
      }
    }
    else if (selected == IKAKU_OVERWRITE) {
      elm->bookMark = TCOPYBMARK_OVERWRITE;
    }
    else if (selected == IKAKU_NOTCOPY) {
      elm->bookMark = TCOPYBMARK_NOT_COPY_FOR_FILE;
    }
  }

  if (trans) {
    free(trans);
  }

  delete confirm;

  return res;
}
