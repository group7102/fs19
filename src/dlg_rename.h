/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_rename.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    リネームダイアログ
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DLG_RENAME_H_
#define _DLG_RENAME_H_

/* ----------------------------------------------------------------- *
 * Class of use
 * ----------------------------------------------------------------- */

class xFont;
class com_button;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class dlg_rename : public xDialog
{
public:
  void  SetFileInfo(dirent* pFD);
  TCHAR rename[MAX_PATH];

public:
  dlg_rename(HWND hParent);
  ~dlg_rename();

private:
  BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  void OnPaint();

private:
  LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam);

private:
  xEdit     mEdit;
  xFont*    mpFont;
  TCHAR     m_name[MAX_PATH];
  TCHAR     m_dir[MAX_PATH];
  dirent    m_entry;

private:
  com_button *mpOK;
  com_button *mpCancel;
};

#endif /* _DLG_RENAME_H_ */
