/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: view_title.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _VIEW_TITLE_H_
#define _VIEW_TITLE_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "command.h"
#include "logf.h"

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */
CLASS(view_title)

/* ----------------------------------------------------------------- *
 * Private parameters
 * ----------------------------------------------------------------- */

TCHAR mhTitle[128];

/* ----------------------------------------------------------------- *
 * Functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
view_title()
{
  _stprintf_s(mhTitle, _countof(mhTitle), L"%s - [%s %s]", L"F's", _T(__DATE__), _T(__TIME__));
}

/* ----------------------------------------------------------------- */
~view_title()
{
}

/* ----------------------------------------------------------------- */
static int Localtime(struct tm *now)
{
  time_t    t = time(NULL);
  time_t    specific;
  errno_t   ercd;
  struct tm switch2 = {0};
  int       ret;

  /* nintendo switch2 announcement date */

  switch2.tm_year = 2025 - 1900;
  switch2.tm_mon  = 4 - 1;
  switch2.tm_mday = 2;
  switch2.tm_hour = 22;
  switch2.tm_min  = 0;
  switch2.tm_sec  = 0;

  specific = mktime(&switch2);

  if (specific > t) {
    specific = specific - t;
    ret = 1;
  }
  else {
    specific = t - specific;
    ret = -1;
  }

  if ((ercd = gmtime_s(now, &specific)) < 0) {
    LOGE("localtime_s()=%d", ercd);
    ret = 0;
  }

  return ret;
}

/* ----------------------------------------------------------------- */
void Draw(HWND hWnd, HDC hDC, int xx, int yy, int cx, int cy)
{
  struct tm tm;

  int ret = Localtime(&tm);

  if (ret == 0) {
    return;
  }

  WINDOWPLACEMENT place;

  GetWindowPlacement(hWnd, &place);

  xTmpDC  dc(hDC);
  xBmpDC  bmp;
  IPT    *pt = xPt();
  SIZE    size;
  int     x = 0;
  int     y = 0;

  bmp.Create(dc, cx, cy);
  bmp.SetBkMode(TRANSPARENT);
  bmp.SetTextColor(pt->color.title);
  bmp.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);
  bmp.FillRect(0, 0, cx, cy, pt->color.base_bar);

  if (place.showCmd == SW_SHOWMAXIMIZED) {
    __snprintf(mhTitle,
               _countof(mhTitle),
               "%d day(s) %d:%d:%d",
               tm.tm_yday,
               tm.tm_hour,
               tm.tm_min,
               tm.tm_sec);
    y += 2;
    x += 4;
    x += IcoDrawIconFromIndex(bmp, x, y, 109, TRUE);
    x += IcoDrawIconFromIndex(bmp, x, y, 110, TRUE);
    x += 4;

    bmp.GetTextExtentPoint32(mhTitle, &size);

    bmp.TextOut(x, y + (16 - size.cy) / 2 + 1, mhTitle);

    bmp.Line(cx - 3, 0, cx - 3, cy, pt->color.split_line);
  }
  else {
    if (ret > 0) {
      __snprintf(mhTitle,
                 _countof(mhTitle),
                 "%d day(s), %d hour(s), %d minute(s) and %d second(s) left until the "
                 "Nintendo Switch2 announcement",
                 tm.tm_yday,
                 tm.tm_hour,
                 tm.tm_min,
                 tm.tm_sec);
    }
    else {
      __snprintf(mhTitle,
                 _countof(mhTitle),
                 "%d day(s) have passed since the Nintendo Switch2 was announced",
                 tm.tm_yday);
    }

    x += 8;
    x += IcoDrawIconFromIndex(bmp, x, y, 109, TRUE);
    x += IcoDrawIconFromIndex(bmp, x, y, 110, TRUE);
    x += 8;

    bmp.TextOut(x, y, mhTitle);

    bmp.GetTextExtentPoint32(mhTitle, &size);

    x += size.cx;
    x += 8;

    for (int i = 0; i < 7; i++) {
      x += IcoDrawIconFromIndex(bmp, x, y, 111 + i, TRUE);
    }
  }

  dc.BitBlt(xx, yy, cx, cy, bmp, 0, 0, SRCCOPY);
}

CLASS_END

#endif /* _VIEW_TITLE_H_ */
