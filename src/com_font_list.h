/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: com_font_list.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _COM_FONT_LIST_H_
#define _COM_FONT_LIST_H_

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class com_font_list : public xListBox
{
public:
  void  SetLogData(int index, const TCHAR *pDispName, int type = -1);

public:
  com_font_list();
  ~com_font_list();

private:
  int   DrawItem(const DRAWITEMSTRUCT *lpDrawItem);
  int   MeasureItem(MEASUREITEMSTRUCT *lpMeasureItem);

private:
  void  DispDirectory(HDC hDC, int x, int cy, int height, TCHAR *str);

protected:
  void  FillRect(HDC hDC, const RECT *pRC, COLORREF fg);
  void  FillRect(HDC hDC, const RECT *pRC, COLORREF pen, COLORREF fg);

private:
  DWORD date_time;

};

#endif /* _COM_FONT_LIST_H_ */
