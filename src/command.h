#ifndef _COMMAND_H_
#define _COMMAND_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

#define __snprintf(buf, len, fmt, ...)  ___snprintf(buf, len, L"" fmt "", ##__VA_ARGS__)

/* ----------------------------------------------------------------- *
 * Public types
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

size_t __strnlen(const wchar_t *cmd, size_t len);
size_t __strlen(const wchar_t *str);
int __strncmp(const wchar_t *str1, const wchar_t *str2, size_t len);
int __strcmp(const wchar_t *str1, const wchar_t *str2);
int ___snprintf(wchar_t *buf, size_t len, const wchar_t *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif /* _COMMAND_H_ */
