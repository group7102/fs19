/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: logf.cpp
 *  Created  : 08/06/04 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include <windows.h>
#include <stdio.h>
#include "logf.h"

/* ----------------------------------------------------------------- *
 * Static parameters
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Static function
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static TCHAR *GetBaseName(void)
{
  static TCHAR _basename[MAX_PATH + 5/* +ext */] = L"";

  if (_basename[0] != L'\0') {
    return _basename;
  }

  memset(_basename, 0, sizeof(_basename));

  GetModuleFileName(NULL, _basename, MAX_PATH);

  TCHAR *str = NULL;
  TCHAR *cp;

  for (cp = _basename; *cp; cp++) {
    if (*cp == L'.') {
      str = cp;
    }
  }

  if (str == NULL) {
    str = cp;
  }

  *str++ = L'.';
  *str++ = L'l';
  *str++ = L'o';
  *str++ = L'g';
  *str++ = L'\0';

  return _basename;
}

/* ----------------------------------------------------------------- */
static FILE *log_open(void)
{
  static int  _first = 1;

  FILE *fp;

  if (_tfopen_s(&fp, GetBaseName(), L"a") != 0) {
    return NULL;
  }

  if (_first) {
    SYSTEMTIME  systime;

    GetLocalTime(&systime);

    _ftprintf(fp,
              L"------ First log. %02d-%02d-%02d %02d:%02d:%02d ------\n",
              systime.wYear % 100,
              systime.wMonth,
              systime.wDay,
              systime.wHour,
              systime.wMinute,
              systime.wSecond);

    _first = 0;
  }

  return fp;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
int __logf(const TCHAR *wfmt, const char *fmt, ...)
{
  FILE  *fp;

  if ((fp = log_open()) == NULL) {
    return -1;
  }

  va_list  ap;

  va_start(ap, fmt);

  if (wfmt) {
    _vftprintf(fp, wfmt, ap);
  }
  else if (fmt) {
    vfprintf(fp, fmt, ap);
  }

  fflush(fp);

  fclose(fp);

#ifdef _DEBUG
  if (wfmt) {
    _vtprintf(wfmt, ap);
  }
  else if (fmt) {
    vprintf(fmt, ap);
  }
  fflush(stdout);
#endif

  va_end(ap);

  return 0;
}

/* ----------------------------------------------------------------- */
const TCHAR *logf_strstr_w(const TCHAR *str)
{
  const TCHAR *cp = str;

  for (int i = 0; str[i] && i < MAX_PATH; i++) {
    if (str[i] == '/' || str[i] == '\\') {
      cp = &str[i + 1];
    }
  }

  return cp;
}

/* ----------------------------------------------------------------- */
const char *logf_strstr_a(const char *str)
{
  const char *cp = str;

  for (int i = 0; str[i] && i < MAX_PATH; i++) {
    if (str[i] == '/' || str[i] == '\\') {
      cp = &str[i + 1];
    }
  }

  return cp;
}
