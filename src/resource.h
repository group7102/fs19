﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// fs19.rc で使用
//
#define IDC_MYICON                      2
#define IDOK2                           3
#define IDCANCEL2                       4
#define IDOK3                           5
#define IDD_FS19_DIALOG                 102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_FS19                        107
#define IDI_SMALL                       108
#define IDC_FS19                        109
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     130
#define IDC_COMBO1                      1000
#define IDC_COMBO2                      1001
#define IDC_COMBO3                      1002
#define IDC_LIST1                       1003
#define IDC_COMBO4                      1003
#define IDC_EDIT1                       1004
#define IDC_STATIC3                     1004
#define IDC_EDIT2                       1005
#define IDC_STATIC4                     1005
#define IDC_EDIT3                       1006
#define IDC_STATIC1                     1006
#define IDC_CHECK1                      1007
#define IDC_STATIC2                     1007
#define IDC_EDIT4                       1007
#define IDC_BUTTON1                     1008
#define IDC_CHECK2                      1008
#define IDC_STATIC0                     1008
#define IDC_STATIC5                     1008
#define IDC_BUTTON2                     1009
#define IDC_CHECK3                      1009
#define IDC_PROGRESS1                   1009
#define IDC_STATIC6                     1009
#define IDC_CHECK4                      1010
#define IDC_BUTTON3                     1010
#define IDC_STATIC7                     1010
#define IDC_CHECK5                      1011
#define IDC_BUTTON4                     1011
#define IDC_STATIC8                     1011
#define IDC_COMBO5                      1012
#define IDC_COMBO6                      1013
#define IDC_COMBO7                      1014
#define IDC_COMBO8                      1015
#define IDC_COMBO9                      1016
#define IDC_COMBO10                     1017
#define IDC_COMBO11                     1018
#define IDC_STATIC9                     1019
#define IDC_COMBO12                     1020
#define IDC_COMBO13                     1021
#define IDC_STATIC10                    1022
#define MENUID_FONT1                    40002
#define MENUID_SYS_ICON                 40004
#define MENUID_DELETE                   40019
#define MENUID_EXIT                     40020
#define MENUID_FONT                     40021
#define MENUID_ROW1                     40022
#define MENUID_ROW2                     40023
#define MENUID_ROW3                     40024
#define MENUID_ROW4                     40025
#define MENUID_ROW5                     40026
#define MENUID_UP                       40027
#define MENUID_DOWN                     40028
#define MENUID_NAME                     40030
#define MENUID_EXT                      40032
#define MENUID_SIZE                     40033
#define MENUID_TIME                     40034
#define MENUID_UPFOLDER                 40036
#define MENUID_FINE_NAME_COPY           40037
#define MENUID_COPY                     40039
#define MENUID_PAST                     40040
#define MENUID_QUICK_JUMP               40042
#define MENUID_LOG_DRIVE                40044
#define MENUID_ABOUT                    40046
#define MENUID_SEARCH                   40047
#define IDC_STATIC                      -1

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
