/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_confirm.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _DLG_CONFIRM_H_
#define _DLG_CONFIRM_H_

/* ----------------------------------------------------------------- *
 * Public type declarations
 * ----------------------------------------------------------------- */

enum
{
  IKAKU_NEW,
  IKAKU_RENAME,
  IKAKU_OVERWRITE,
  IKAKU_NOTCOPY,
  IKAKU_MAX,
};

/* ----------------------------------------------------------------- *
 * Class of use
 * ----------------------------------------------------------------- */

class iComListEx;
class com_button;
class xFont;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class dlg_confirm : public xDialog
{
public:
  void  SetSourceFileInfo(const TCHAR *src, time_t *time, LARGE_INTEGER *size);
  void  SetTransFileInfo(const TCHAR *trans, time_t *time, LARGE_INTEGER *size);
  void  SetSourceFileInfo(const TCHAR *src, time_t *time, DWORD high, DWORD low);
  void  SetFiles(const TCHAR *src, const TCHAR *trans);
  int   selected;
  BOOL  thatsall;
  TCHAR mNewPath[MAX_PATH];

public:
  dlg_confirm(HWND hParent);
  ~dlg_confirm();

private:
  BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  void OnPaint(void);

private:
  LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam);

private:
  iComListEx *mpLogList;
  com_button *mpButton1;
  com_button *mpButton2;
  com_button *mpButton3;
  xFont       mFont;
  int         mHeight;
  int         mFraem_cy;
};

#endif /* _DLG_CONFIRM_H_ */
