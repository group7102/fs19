/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_find.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    ファインダイアログ
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DLG_FIND_H_
#define _DLG_FIND_H_

/* ----------------------------------------------------------------- *
 * Class of use
 * ----------------------------------------------------------------- */

class xFont;
class xComboBox;
class com_button;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class dlg_find : public xDialog
{
public:
  void  SetFileInfo(dirent* pFD);
  void  SetFileInfo(const TCHAR *name);
  void  SetFont(const TCHAR *name, int size);
  TCHAR rename[MAX_PATH];
  void  SetList(void);
  void  SaveList(void);

public:
  TCHAR m_target[MAX_PATH];

public:
  dlg_find(HWND hParent);
  ~dlg_find();

private:
  BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  void OnPaint();

private:
  LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam);

private:
  xComboBox*  mpCmbo;
  xFont*      mpFont;
  com_button *mpOK;
  com_button *mpCancel;
};

#endif /* _DLG_FIND_H_ */
