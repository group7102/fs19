/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: _simple_que.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef __SIMPLE_QUE_H_
#define __SIMPLE_QUE_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef void* SimpleQueHandle;

int SimpleQueOpen(SimpleQueHandle *handle);
int SimpleQueClose(SimpleQueHandle handle);
int SimpleQuePush(SimpleQueHandle handle, const TCHAR *str);
int SimpleQuePop(SimpleQueHandle handle, TCHAR *str, size_t len);
int SimpleQueGetAt(SimpleQueHandle handle, int index, const TCHAR **str);
int SimpleQueGetCount(SimpleQueHandle handle);
int SimpleQueClear(void *handle);

#ifdef __cplusplus
}
#endif

#endif /* __SIMPLE_QUE_H_ */
