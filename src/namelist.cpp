/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: namelist.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

/* ----------------------------------------------------------------- *
 * Included files
 * ----------------------------------------------------------------- */

#include <shlwapi.h>
#include <tchar.H>
#include <malloc.H>
#include <time.h>
#include "dirent.h"
#include "scandir.h"
#include "savedir.h"
#include "strverscmp.h"
#include "sub_funcs.h"
#include "logf.h"
#include "xpt.h"
#include "_simple_que.h"
#include "command.h"
#include "namelist.h"

/* ----------------------------------------------------------------- *
 * private types
 * ----------------------------------------------------------------- */

enum {
  MARK_GIT = 2,
  MARK_CVS,
  MARK_SVN,
  MARK_MODIFY,
  MARK_ADD,
  MARK_MARGE,
  MARK_CONF,
};

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
namelist::namelist()
{
  m_namelist = NULL;
  m_total    = 0;
  m_cancel   = FALSE;
  m_lock     = FALSE;
  m_isRemote = FALSE;

  ZeroMemory(m_dir, sizeof(m_dir));

  m_seach_mode = INAM_MODE_NORMAL;

  for (int i = 0; i < WC_LIST_MAX; i++)
    {
      m_argv[i] = NULL;
    }

  m_argc = 0;
  m_fcnt = 0;
  m_dcnt = 0;

  mCopeingFileName[0]         = '\0';
  mCopeingCurrentDirectory[0] = '\0';
  mCopeingFileIndex           = -1;
  mCopeingCounter             = 0;

  m_name_order = 1;
  m_time_order = -1;
  m_size_order = 1;
  m_extn_order = 1;
  m_kind  = INAM_KIND_EXT;

  /* Directry history(new) */

  SimpleQueOpen(&handle_back);
  SimpleQueOpen(&handle_step);
}

/* ----------------------------------------------------------------- */
namelist::~namelist()
{
  delete_scandir(m_namelist);
}

/* ----------------------------------------------------------------- *
 * private function                                                  *
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static TCHAR *RemoveFileSpec(TCHAR *buf, const TCHAR *name)
{
  _tcscpy_s(buf, MAX_PATH, name);

  PathRemoveFileSpec(buf);

  return PathFindFileName(name);
}

/* ----------------------------------------------------------------- */
static int sort_up(const TCHAR *pFn1, const TCHAR *pFn2)
{
  int   res = 0;

  if (_tcscmp(PathFindFileName(pFn1), _T("..")) == 0)
    {
      res = -1;
    }
  else if (_tcscmp(PathFindFileName(pFn2), _T("..")) == 0)
    {
      res = 1;
    }

  return res;
}

/* ----------------------------------------------------------------- */
static int sort_attr(DWORD attr1, DWORD attr2)
{
  if (attr1 & FILE_ATTRIBUTE_SYSTEM)
    {
      attr1 = FILE_ATTRIBUTE_SYSTEM;
    }
  else if (attr1 & FILE_ATTRIBUTE_HIDDEN)
    {
      attr1 = FILE_ATTRIBUTE_HIDDEN;
    }
  else if (attr1 & FILE_ATTRIBUTE_READONLY)
    {
      attr1 = FILE_ATTRIBUTE_READONLY;
    }
  else
    {
      attr1 = 0;
    }

  if (attr2 & FILE_ATTRIBUTE_SYSTEM)
    {
      attr2 = FILE_ATTRIBUTE_SYSTEM;
    }
  else if (attr2 & FILE_ATTRIBUTE_HIDDEN)
    {
      attr2 = FILE_ATTRIBUTE_HIDDEN;
    }
  else if (attr2 & FILE_ATTRIBUTE_READONLY)
    {
      attr2 = FILE_ATTRIBUTE_READONLY;
    }
  else
    {
      attr2 = 0;
    }

  return attr2 - attr1;
}

/* ----------------------------------------------------------------- */
static int sort_dir(DWORD attr1, DWORD attr2)
{
  attr1 &= FILE_ATTRIBUTE_DIRECTORY;
  attr2 &= FILE_ATTRIBUTE_DIRECTORY;

  return attr2 - attr1;
}

/* ----------------------------------------------------------------- *
 * CVS対応ファイルでソート
 * ----------------------------------------------------------------- */
static int sort_cvs(int cvs1, int cvs2)
{
  return cvs2 - cvs1;
}

/* ----------------------------------------------------------------- *
 * CVS対応ファイルでソート
 * ----------------------------------------------------------------- */
static int sort_cvs3(int cvs1, int cvs2)
{
  if (cvs1)
    {
      cvs1 = 1;
    }

  if (cvs2)
    {
      cvs2 = 1;
    }

  return cvs2 - cvs1;
}

/* ----------------------------------------------------------------- *
 * CVS対応ファイルでソート
 * ----------------------------------------------------------------- */
static int sort_cvs2(int cvs1, int cvs2)
{
  if (cvs2 == MARK_MODIFY ||
      cvs2 == MARK_ADD    ||
      cvs2 == MARK_MARGE  ||
      cvs2 == MARK_CONF)
    {
      cvs2 = 1;
    }
  else
    {
      cvs2 = 0;
    }

  if (cvs1 == MARK_MODIFY ||
      cvs1 == MARK_ADD    ||
      cvs1 == MARK_MARGE  ||
      cvs1 == MARK_CONF)
    {
      cvs1 = 1;
    }
  else
    {
      cvs1 = 0;
    }

  return cvs2 - cvs1;
}

/* ----------------------------------------------------------------- */
static int sort_idx(int idx1, int idx2)
{
  return idx2 -idx1;
}

/* ----------------------------------------------------------------- */
static int sort_time(const dirent *dir1, const dirent *dir2)
{
  return (int)difftime(dir1->mtime, dir2->mtime);
}

/* ----------------------------------------------------------------- */
static int sort_size(const dirent *dir1, const dirent *dir2)
{
  int   res = 0;

  ULARGE_INTEGER size1;
  ULARGE_INTEGER size2;

  size1.HighPart = dir1->nFileSizeHigh;
  size1.LowPart  = dir1->nFileSizeLow;
  size2.HighPart = dir2->nFileSizeHigh;
  size2.LowPart  = dir2->nFileSizeLow;

  if (size1.QuadPart > size2.QuadPart)
    {
      res = -1;
    }
  else if (size1.QuadPart < size2.QuadPart)
    {
      res = 1;
    }
  else
    {
      res = 0;
    }

  return res;
}

/* ----------------------------------------------------------------- */
static int Compar(void *param, const dirent **entry1, const dirent **entry2)
{
  const dirent* dir1 = *entry1;
  const dirent* dir2 = *entry2;
  int           res  = 0;
  TCHAR*        nam1 = (TCHAR*)PathFindFileName(dir1->d_name);
  TCHAR*        nam2 = (TCHAR*)PathFindFileName(dir2->d_name);
  TCHAR*        ext1 = (TCHAR*)PathFindExtension(dir1->d_name);
  TCHAR*        ext2 = (TCHAR*)PathFindExtension(dir2->d_name);
  namelist     *post = (namelist *)param;

  /* If file name and extension name are same, file name prioritize and sort */

  if (nam1 == ext1)
    {
      ext1 = _T("");
    }

  if (nam2 == ext2)
    {
      ext2 = _T("");
    }

  /* Directory ".." */

  if ((res = sort_up(nam1, nam2)))
    {
      return res;
    }

  /* Directory */

  if ((res = sort_dir(dir1->dwFileAttributes, dir2->dwFileAttributes)))
    {
      return res;
    }

  /* Attributey */

  if ((res = sort_attr(dir1->dwFileAttributes, dir2->dwFileAttributes)))
    {
      return res;
    }

  /* Overray icon */

  if ((res = sort_idx(dir1->userData >> 24, dir2->userData >> 24)))
    {
      return res;
    }

  /* Overray icon */

  if ((res = sort_idx((dir1->userData >> 16) & 0xFF, (dir2->userData >> 16) & 0xFF)))
    {
      return res;
    }

  if (post->m_kind == INAM_KIND_SIZE)
    {
      /* sort by file size */

      if ((res = sort_size(dir1, dir2) * post->m_size_order))
        {
          return res;
        }

      /* Extention */

      if ((res = _tcsicmp(ext1, ext2)))
        {
          return res;
        }

      /* Filename */

      return strverscmp(nam1, nam2);
    }

  if (post->m_kind == INAM_KIND_TIME)
    {
      /* sort by file update time */

      if ((res = sort_time(dir1, dir2) * post->m_time_order))
        {
          return res;
        }

      /* Extention */

      if ((res = _tcsicmp(ext1, ext2)))
        {
          return res;
        }

      /* Filename */

      return strverscmp(nam1, nam2);
    }

  if (post->m_kind == INAM_KIND_NAME)
    {
      /* sort by filename */

      if ((res = strverscmp(nam1, nam2) * post->m_name_order))
        {
          return res;
        }

      /* Extention */

      return _tcsicmp(ext1, ext2);
    }

  /* sort by extention */

  if ((res = _tcsicmp(ext1, ext2) * post->m_extn_order))
    {
      return res;
    }

  return 0;
}

/* ----------------------------------------------------------------- */
static int Compar1(void *param, const dirent **entry1, const dirent **entry2)
{
  const dirent *dir1 = *entry1;
  const dirent *dir2 = *entry2;
  TCHAR         buf1[MAX_PATH * 2];
  TCHAR         buf2[MAX_PATH * 2];
  int           res;
  TCHAR        *nam1 = (TCHAR*)PathFindFileName (dir1->d_name);
  TCHAR        *nam2 = (TCHAR*)PathFindFileName (dir2->d_name);
  TCHAR        *ext1 = (TCHAR*)PathFindExtension(dir1->d_name);
  TCHAR        *ext2 = (TCHAR*)PathFindExtension(dir2->d_name);

  res = sort_up(nam1, nam2);

  if (res)
    {
      return res;
    }

  RemoveFileSpec(buf1, dir1->d_name);
  RemoveFileSpec(buf2, dir2->d_name);

  res = strverscmp(buf1, buf2);

  if (res)
    {
      return res;
    }

  res = sort_dir(dir1->dwFileAttributes, dir2->dwFileAttributes);

  if (res)
    {
      return res;
    }

  res = sort_attr(dir1->dwFileAttributes, dir2->dwFileAttributes);

  if (res)
    {
      return res;
    }

  res = _tcsicmp(ext1, ext2);

  if (res)
    {
      return res;
    }

  return strverscmp(nam1, nam2);
}

/* ----------------------------------------------------------------- */
static int Compar2(void *param, const dirent **entry1, const dirent **entry2)
{
  int res = Compar(param, entry1, entry2);

  if (res) {
    return res;
  }

  /* Filename(fullpath) */

  const dirent *dir1 = *entry1;
  const dirent *dir2 = *entry2;

  return strverscmp(dir1->d_name, dir2->d_name);
}

/* ----------------------------------------------------------------- */
static int PumpMessage(HWND hWnd)
{
  MSG   msg;
  int   res = 0;

  while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
    {
      if (msg.message == WM_QUIT)
        {
          res = 1;
          break;
        }
      else if (msg.message == WM_KEYDOWN)
        {
          if (msg.wParam == VK_ESCAPE)
            {
              if (::MessageBox(hWnd,
                               _T("Searching. Would you like to cancel?"),
                               _T("What are you doing?"),
                               MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
                {
                  res = 1;
                  break;
                }
            }
        }

      if (GetMessage(&msg, NULL, 0, 0) <= 0)
        {
          break;
        }

      ::TranslateMessage(&msg);
      ::DispatchMessage (&msg);
    }

  return res;
}

/* ----------------------------------------------------------------- */
static int Filter1(dirent *entry)
{
  TCHAR*    path = PathFindFileName(entry->d_name);
  int       res  = 0;
  TACTION*  act  = (TACTION*)entry->param;

  if (entry->cAlternateFileName[0] == _T('\0'))
    {
      memcpy(entry->cAlternateFileName, path, sizeof(entry->cAlternateFileName));
    }

  entry->bookMark = 0;

  if (entry->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
    {
      if(_tcscmp(path, _T("..")) == 0)
        {
          if(entry->depth == 0)
            {
              res = DENT_FLT_MALLOC;
            }
        }
      else
        {
          if (entry->depth < 20)
            {
              res = DENT_FLT_RECURSION/* | DENT_FLT_MALLOC*/;
            }
          else
            {
              res = DENT_FLT_MALLOC;
            }
        }
    }
  else
    {
      if (act->argc > 0)
        {
          for (int i = 0; i < act->argc; i++)
            {
              if (PathMatchSpec(path, act->argv[i]))
                {
                  res = DENT_FLT_MALLOC;
                  break;
                }
            }
        }
    }

  if (act)
    {
      if (*(act->cancel))
        {
          res = DENT_FLT_CANCEL;
        }
      else
        {
          if (PumpMessage(act->hWnd))
            {
              *(act->cancel) = 1;
              res = DENT_FLT_CANCEL;
            }
        }
    }

  act->entry = NULL;

  act->base->NamelistAction(act);

  return res;
}

/* ----------------------------------------------------------------- */
static int Compar0(void *param, const dirent **entry1, const dirent **entry2)
{
  int res = Compar(param, entry1, entry2);

  if (res) {
    return res;
  }

  /* Filename */

  const dirent *dir1 = *entry1;
  const dirent *dir2 = *entry2;

  return strverscmp(PathFindFileName(dir1->d_name), PathFindFileName(dir2->d_name));
}

/* ----------------------------------------------------------------- */
static int Filter0(dirent *entry)
{
  TCHAR*    path = PathFindFileName(entry->d_name);
  TACTION*  act  = (TACTION*)entry->param;

  if (entry->cAlternateFileName[0] == _T('\0'))
    {
      memcpy(entry->cAlternateFileName, path, sizeof(entry->cAlternateFileName));
    }

  entry->bookMark = 0;

  act->base->NamelistAction(act);

  return DENT_FLT_MALLOC;
}

/* ----------------------------------------------------------------- */
static int D_Filter0(dirent *entry)
{
  D_NODE*   d_node;
  D_NODE*   d_parent;
  int       i;
  int       d_cnt = 0;
  dirent*   top;
  TACTION*  act;

  if (!entry)
    {
      return 0;
    }

  d_parent = savedir(entry->d_name);
  act      = (TACTION *)entry->param;

  if (!d_parent)
    {
      goto exitout;
    }

  entry->hHandle   = d_parent->hHandle;
  entry->userData  = d_parent->d_no;
  entry->userData |= (int)d_parent->g_idx << 16;

  for (top = entry->next, i = 0; i < entry->d_cnt && top && i < 1000; i++, top = top->next)
    {
      if ((d_node = savedir(top->d_name)) == NULL)
        {
          break;
        }

      if (d_node->hHandle == NULL)
        {
          if (top->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
              IcoGetTempDirectoryIconIndex((HANDLE *)&d_node->hHandle, &d_node->d_no);
            }
          else
            {
              d_cnt++;
              IcoGetTempFileIconIndex((HANDLE *)&d_node->hHandle, &d_node->d_no);
            }
        }

      top->hHandle   = d_node->hHandle;
      top->userData  = d_node->d_no;
      top->userData |= (int)d_node->g_idx << 16;
    }

  for (; i < entry->d_cnt && top; i++, top = top->next)
    {
      if (top->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
          IcoGetTempDirectoryIconIndex((HANDLE *)&top->hHandle, &top->userData);
        }
      else
        {
          d_cnt++;
          IcoGetTempFileIconIndex((HANDLE *)&top->hHandle, &top->userData);
        }
    }

exitout:
  /* Exit */

  act->entry = entry;
  act->base->NamelistAction(act);

  return 0;
}

/* ----------------------------------------------------------------- */
int namelist::Scandir(const TCHAR dir[], const TCHAR *prev_dir, void *param, int seach_mode)
{
  int             i;
  int             total;
  dirent        **namelist;
  int             current = 0;
  TACTION         act;
  static TCHAR    buf[MAX_PATH];
  const TCHAR    *cp;
  IPT            *pt = xPt();
  ULARGE_INTEGER  size;
  int (*filter)(dirent *entry);
  int (*compar)(void *, const dirent **entry1, const dirent **entry2);

  m_cancel = FALSE;

  if (seach_mode < 0)
    {
      seach_mode = m_seach_mode;
    }

  act.cancel = &m_cancel;
  act.hWnd   = (HWND)param;
  act.base   = this;
  act.time   = 0/*GetLocalTimeD()*/;
  act.argc   = m_argc;
  act.argv   = m_argv;
  act.s_mode = seach_mode;
  act.entry  = NULL;
  act.stat   = 0;

  if (m_dir[0] != dir[0])
    {
      TCHAR drive[8];
      TCHAR net_path[MAX_PATH];
      DWORD length = MAX_PATH;

      m_isRemote = TRUE;

      cp = dir;

      if (!PathIsUNC(dir))
        {
          drive[0] = dir[0];
          drive[1] = dir[1];
          drive[2] = '\0';

          if (GetDriveType(drive) == DRIVE_REMOTE)
            {
              if (WNetGetConnection(drive, net_path, &length) == NO_ERROR)
                {
                  /* Convert remotely mapped drive letter to network path */

                  cp = net_path;
                }
            }
          else
            {
              m_isRemote = FALSE;
            }
        }

      /* Note:
       * If it is wsl folder, special processing will be applied. */

      if (_tcsncmp(cp, _T("\\\\wsl"), 5) == 0)
        {
          m_isRemote = FALSE;
        }
    }

  /* Search exception folders */

  for (i = 0; i < pt->sort.num; i++)
    {
      if (_tcsstr(dir, pt->sort.folders[i].path))
        {
          m_isRemote = FALSE;
        }
    }

  if (_tcscmp(dir, _T("..")) == 0)
    {
      PathCombine(buf, m_dir, _T(".."));
    }
  else
    {
      _tcscpy_s(buf, MAX_PATH, dir);
    }

  if (seach_mode == INAM_MODE_SEACH)
    {
      filter = Filter1;
      compar = Compar1;
    }
  else
    {
      filter = Filter0;
      compar = Compar0;
    }

  /* Current folder search */

  total = scandir(buf, &namelist, filter, D_Filter0, NULL, &act);

  /* Current folder sort */

  int type;
  int order;

  if (xPtGetSortInfo(pt, buf, NULL, &type, &order) == 0)
    {
      SetSortMode(order, type);
    }
  else
    {
      SetSortMode(pt->setting.sort_order, pt->setting.sort_part);
    }

  qsort_s(namelist,
          total,
          sizeof(dirent *),
          (int(*)(void *,const void *, const void *))compar,
          this);

  if (total > 0)
    {
      _tcscpy_s(m_dir, MAX_PATH, buf);

      m_dcnt = 0;
      m_fcnt = 0;

      m_fsize.QuadPart = 0;

      if (prev_dir)
        {
          for (i = 0; i < total; i++)
            {
              if (_tcsicmp(namelist[i]->d_name, prev_dir) == 0)
                {
                  current = i;
                  break;
                }
            }
        }

      for (i = 0; i < total; i++)
        {
          if (namelist[i]->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
              if (_tcscmp(PathFindFileName(namelist[i]->d_name), _T("..")) != 0)
                {
                  m_dcnt++;
                }
            }
          else
            {
              m_fcnt++;
              size.LowPart  = namelist[i]->nFileSizeLow;
              size.HighPart = namelist[i]->nFileSizeHigh;
              m_fsize.QuadPart += size.QuadPart;
            }
        }

      if (m_total && total)
        {
          int   len = (int)(PathFindFileName(m_namelist[0]->d_name) - m_namelist[0]->d_name);

          if (StrCmpNI(m_namelist[0]->d_name, dir, len) == 0)
            {
              for (i = 0; i < m_total; i++)
                {
                  if(m_namelist[i]->bookMark == 0)
                    {
                      continue;
                    }

                  for (int j = 0; j < total; j++)
                    {
                      if (namelist[j]->bookMark
                       || _tcsicmp(m_namelist[i]->d_name + len, namelist[j]->d_name + len))
                        {
                          continue;
                        }

                      namelist[j]->bookMark = m_namelist[i]->bookMark;
                      break;
                    }
                }
            }
        }

      delete_scandir(m_namelist);

      m_namelist = namelist;
      m_total    = total;

      m_seach_mode = seach_mode;

      for (mCopeingFileIndex = -1, i = 0; i < total; i++)
        {
          if (!(namelist[i]->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
              if(_tcsicmp(mCopeingFileName, namelist[i]->d_name) == 0)
                {
                  mCopeingFileIndex = i;
                  break;
                }

              continue;
            }

          if ((cp = StrStr(mCopeingFileName, namelist[i]->d_name)) != NULL)
            {
              if (*(cp + _tcslen(namelist[i]->d_name)) == '\\')
                {
                  mCopeingFileIndex = i;
                  break;
                }
            }
        }
    }
  else
    {
      if(*act.cancel)
        {
          current = -1;
        }
      else
        {
          _tcscpy_s(m_dir, MAX_PATH, buf);
          m_total = 0;
        }
    }

  m_lock = FALSE;

  return current;
}

/* ----------------------------------------------------------------- */
void namelist::Cancel(void)
{
  m_cancel = TRUE;
}

/* ----------------------------------------------------------------- */
void namelist::Push(const TCHAR *path)
{
  if (!IsPushDir(path))
    {
      return;
    }

  SimpleQuePush(handle_back, path);
}

/* ----------------------------------------------------------------- */
TCHAR *namelist::Back(const TCHAR **back_path)
{
  const TCHAR *temp;

  if (SimpleQueGetCount(handle_back) <= 1)
    {
      return NULL;
    }

  if (SimpleQueGetAt(handle_back, 0, &temp) < 0)
    {
      return NULL;
    }

  if (SimpleQuePush(handle_step, temp) < 0)
    {
      return NULL;
    }

  if (SimpleQuePop(handle_back, NULL, 0) < 0)
    {
      return NULL;
    }

  if (back_path)
    {
      if (SimpleQueGetAt(handle_step, 0, back_path) < 0)
        {
          return NULL;
        }
    }

  if (SimpleQueGetAt(handle_back, 0, &temp) < 0)
    {
      return NULL;
    }

  return (TCHAR *)temp;
}

/* ----------------------------------------------------------------- */
TCHAR *namelist::Step(void)
{
  const TCHAR *temp;

  if (SimpleQueGetAt(handle_step, 0, &temp) < 0)
    {
      return NULL;
    }

  if (SimpleQuePush(handle_back, temp) < 0)
    {
      return NULL;
    }

  if (SimpleQuePop(handle_step, NULL, 0) < 0)
    {
      return NULL;
    }

  if (SimpleQueGetAt(handle_back, 0, &temp) < 0)
    {
      return NULL;
    }

  return (TCHAR *)temp;
}

/* ----------------------------------------------------------------- */
BOOL namelist::IsPushDir(const TCHAR *bk_path)
{
  if(PathIsRoot(bk_path))
    {
      return false;
    }

  BOOL    res = TRUE;
  TCHAR  *buf;
  size_t  len;

  len = _tcslen(bk_path) + 1;

  buf = (TCHAR *)alloca(len * sizeof(TCHAR));

  _sntprintf_s(buf, len, _TRUNCATE, L"%s", bk_path);

  PathStripToRoot(buf);

  switch (GetDriveType(buf))
    {
      case DRIVE_RAMDISK:
      case DRIVE_REMOTE:
      case DRIVE_FIXED:
      case DRIVE_NO_ROOT_DIR:
        break;
      case DRIVE_UNKNOWN:
      case DRIVE_CDROM:
        res = FALSE;
        break;
      default:
        break;
    }

  return res;
}

/* ----------------------------------------------------------------- */
BOOL namelist::RootExists(const TCHAR *path)
{
  TCHAR  *tmp;
  size_t  len;

  len = _tcslen(path)  + 1;

  tmp = (TCHAR *)alloca(len * sizeof(TCHAR));

  _tcscpy_s(tmp, len, path);

  PathStripToRoot(tmp);

  return PathFileExists(tmp);
}

/* ----------------------------------------------------------------- */
BOOL namelist::RootCmp(const TCHAR *path)
{
  TCHAR  *tmp1;
  TCHAR  *tmp2;
  size_t  len1;
  size_t  len2;

  len1 = _tcslen(path)  + 1;
  len2 = _tcslen(m_dir) + 1;

  tmp1 = (TCHAR *)alloca(len1 * sizeof(TCHAR));
  tmp2 = (TCHAR *)alloca(len2 * sizeof(TCHAR));

  _tcscpy_s(tmp1, len1, path);
  _tcscpy_s(tmp2, len2, m_dir);

  PathStripToRoot(tmp1);
  PathStripToRoot(tmp2);

  return (_tcsicmp(tmp1, tmp2) == 0);
}

/* ----------------------------------------------------------------- */
void namelist::SetWildcard(const TCHAR *cardlist)
{
  int    i = 0;
  TCHAR *cp;
  TCHAR *ctxt;

  _tcscpy_s(m_cardtemp, MAX_PATH, cardlist);

  m_argc = 0;

  for (cp = _tcstok_s(m_cardtemp, _T(";"), &ctxt); cp; cp = _tcstok_s(NULL, _T(";"), &ctxt))
    {
      if (i >= WC_LIST_MAX)
        {
          break;
        }

      m_argv[i] = cp;
      m_argc++;
      i++;
    }
}

/* ----------------------------------------------------------------- */
void namelist::SeachLock(void)
{
  m_lock = TRUE;
}

/* ----------------------------------------------------------------- */
dirent **namelist::BinSeach(TCHAR* dir, dirent *target)
{
  dirent   *entry;
  dirent  **res   = NULL;
  DWORD     cvs[] = {0, MARK_SVN, MARK_MODIFY, MARK_ADD, MARK_MARGE, MARK_CONF};
  DWORD     i;
  size_t    len;
  dirent   *next = (dirent *)target->param;

  len = __strnlen(dir, MAX_PATH) + 1 + __strnlen(next->d_name, MAX_PATH) + 1;

  entry = (dirent *)alloca(sizeof(dirent) + len * sizeof(TCHAR));

  *entry = *target;

  __snprintf(entry->d_name, len, "%.*s", len - 1, dir);

  ::PathAppend(entry->d_name, next->d_name + next->d_cnt);

  for (i = 0; i < sizeof(cvs) / sizeof(DWORD); i++)
    {
      res = (dirent **)bsearch_s(&entry,
                                 m_namelist,
                                 m_total,
                                 sizeof(dirent *),
                                 (int(*)(void *, const void *, const void *))Compar2,
                                 this);
      if (res != NULL)
        {
          break;
        }
    }

  return res;
}

/* ----------------------------------------------------------------- */
int namelist::Sort(int cur_line)
{
  TCHAR  *d_name;
  dirent *entry;
  size_t  size;

  entry = m_namelist[cur_line];

  size = entry->size - sizeof(dirent);

  d_name = (TCHAR *)alloca(size);

  __snprintf(d_name, size / sizeof(TCHAR), "%s", entry->d_name);

  /* Current folder sort */

  int  type;
  int  order;
  IPT *pt = xPt();

  if (xPtGetSortInfo(pt, m_dir, NULL, &type, &order) == 0)
    {
      SetSortMode(order, type);
    }
  else
    {
      SetSortMode(pt->setting.sort_order, pt->setting.sort_part);
    }

  qsort_s(m_namelist,
          m_total,
          sizeof(dirent *),
          (int(*)(void *, const void *, const void *))Compar0,
          this);

  if (cur_line >= m_total)
    {
      return 0;
    }

  for (int i = 0; i < m_total; i++)
    {
      if (_tcscmp(d_name, m_namelist[i]->d_name) == 0)
        {
          cur_line = i;
          break;
        }
    }

  return cur_line;
}

/* ----------------------------------------------------------------- */
void namelist::SetSortMode(int order, int kind)
{
  m_kind = kind;

  if (m_kind == INAM_KIND_NAME)
    {
      m_name_order = order;
    }
  else if (m_kind == INAM_KIND_TIME)
    {
      m_time_order = order;
    }
  else if (m_kind == INAM_KIND_SIZE)
    {
      m_size_order = order;
    }
  else if (m_kind == INAM_KIND_EXT)
    {
      m_extn_order = order;
    }
  else
    {
      /* do nothing */
    }
}

/* ----------------------------------------------------------------- */
void namelist::GetSortMode(int *order, int *kind)
{
  if (m_kind == INAM_KIND_NAME)
    {
      *order = m_name_order;
    }
  else if (m_kind == INAM_KIND_TIME)
    {
      *order = m_time_order;
    }
  else if (m_kind == INAM_KIND_SIZE)
    {
      *order = m_size_order;
    }
  else if (m_kind == INAM_KIND_EXT)
    {
      *order = m_extn_order;
    }
  else
    {
      /* do nothing */
    }

  if (kind)
    {
      *kind = m_kind;
    }
}

/* ----------------------------------------------------------------- */
TCHAR *namelist::GetCurrentDirectory(void)
{
  return m_dir;
}

/* ----------------------------------------------------------------- */
int namelist::StepCount(void)
{
  return SimpleQueGetCount(handle_step);
}

/* ----------------------------------------------------------------- */
int namelist::BackCount(void)
{
  return SimpleQueGetCount(handle_back);
}

/* ----------------------------------------------------------------- */
int namelist::Clear(void)
{
  return SimpleQueClear(handle_step);
}

/* ----------------------------------------------------------------- */
BOOL namelist::IsMark(void)
{
  BOOL res = FALSE;

  for (int i = 0; i < m_total; i++) {
    if (m_namelist[i]->bookMark) {
      res = TRUE;
      break;
    }
  }

  return res;
}
