/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: namelist.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _NAMELIST_H_
#define _NAMELIST_H_

/* ----------------------------------------------------------------- *
 * Included files
 * ----------------------------------------------------------------- */

#include "dirent.h"
#include "icon.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* 定義一覧 */

#define INAM_MODE_NORMAL  0
#define INAM_MODE_SEACH   1
#define ULARGEINT         ULARGE_INTEGER

/* ----------------------------------------------------------------- *
 * Class of use
 * ----------------------------------------------------------------- */

class namelist;

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

enum {
  INAM_KIND_EXT,
  INAM_KIND_TIME,
  INAM_KIND_SIZE,
  INAM_KIND_NAME,
};

typedef struct TACTION_T {
  BOOL     *cancel;
  HWND      hWnd;
  DWORD     time;
  int       argc;
  TCHAR   **argv;
  namelist *base;
  int       s_mode;
  dirent   *entry;
  int       stat;
}
TACTION;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */
class namelist
{
public:
  namelist();
  ~namelist();

public:
  virtual void  Cancel(void);
  virtual void  NamelistAction(void*) {}

public:
  int       Scandir(const TCHAR  dir[],
                    const TCHAR *prev_dir = NULL,
                    void        *param = NULL,
                    int          seach_mode = INAM_MODE_NORMAL);
  TCHAR    *GetCurrentDirectory(void);
  void      Push(const TCHAR *path);
  TCHAR    *Back(const TCHAR **back_path = NULL);
  TCHAR    *Step(void);
  BOOL      IsPushDir(const TCHAR *bk_path);
  BOOL      RootExists(const TCHAR *path);
  BOOL      RootCmp(const TCHAR *path);
  int       GetTotalFile(void) { return m_total; }
  void      SetWildcard(const TCHAR *cardlist);
  void      SeachLock(void);
  int       GetFileCount() { return m_fcnt; }
  int       GetDirCount() { return m_dcnt; }
  int       GetTotalCount() { return m_dcnt + m_fcnt; }
  ULARGEINT GetTotalFileSize() { return m_fsize; }
  dirent  **BinSeach(TCHAR *dir, dirent *namelist);
  int       Sort(int cur_line);
  void      SetSortMode(int order, int kind);
  void      GetSortMode(int *order, int *kind);
  int       StepCount(void);
  int       BackCount(void);
  int       Clear(void);
  BOOL      IsMark(void);

  /* Directry history(new) */

  void  *handle_back;
  void  *handle_step;

protected:
  enum {
    WC_LIST_MAX = 8,
  };

protected:
  dirent  **m_namelist;
  int       m_total;
  int       m_fcnt;
  int       m_dcnt;
  TCHAR     m_dir[MAX_PATH];
  int       m_seach_mode;
  BOOL      m_cancel;
  //
  TCHAR*    m_argv[WC_LIST_MAX];
  TCHAR     m_argc;
  TCHAR     m_cardtemp[MAX_PATH];
  ULARGEINT m_fsize;
  BOOL      m_isRemote;

public:
  BOOL      m_lock;

public:
  TCHAR     mCopeingFileName[MAX_PATH];
  TCHAR     mCopeingCurrentDirectory[MAX_PATH];
  int       mCopeingFileIndex;
  int       mCopeingCounter;

public:
  int       m_name_order;
  int       m_time_order;
  int       m_size_order;
  int       m_extn_order;
  int       m_kind;
};

#endif /* _NAMELIST_H_ */
