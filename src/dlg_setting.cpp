/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_setting.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    ディレクトリの作成
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "xpt.h"
#include "resource.h"
#include "logf.h"
#include "dirent.h"
#include "icon.h"
#include "com_button.h"
#include "dlg_setting.h"

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private parameters
 * ----------------------------------------------------------------- */

/* Font size info */

static int  mLogPixelSy = 72;
static int  mMaxPixelSy = 72;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
xComboBoxEx::xComboBoxEx()
{
  _sntprintf_s(mFontName, _countof(mFontName), _TRUNCATE, L"%s", L"ＭＳ ゴシック");
  mFontSize = -12;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::DrawItemA(const DRAWITEMSTRUCT *pDIS)
{
  xTmpDC dc(pDIS->hDC);
  IPT   *pt = xPt();
  int    data;
  int    top;
  TCHAR *tmp;

  tmp = GetText((int)pDIS->itemID);

  if (tmp == NULL) {
    LOGE("malloc");
    return -1;
  }

  data = mFontSize;

  data = MulDiv(mLogPixelSy, data, mMaxPixelSy);

  dc.SetSelectFont(tmp, -data);

  data = dc.tm.tmHeight;

  top = pDIS->rcItem.top + pDIS->rcItem.bottom - data;
  top = (top * 10 + 10) / 2;
  top = top / 10;

  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(0xFF000000);

  if (pDIS->itemAction == ODA_DRAWENTIRE) {
    dc.FillRect(&pDIS->rcItem, pt->color.base_bar);
  }
  else if (pDIS->itemAction == ODA_SELECT) {
    if (pDIS->itemState & ODS_SELECTED) {
      dc.FillRect(&pDIS->rcItem, pt->color.selected);
    }
    else {
      dc.FillRect(&pDIS->rcItem, pt->color.base_bar);
    }
  }
  else if (pDIS->itemAction == ODA_FOCUS) {
    dc.Rectangle(&pDIS->rcItem, pt->color.selected_frame, pt->color.selected);
  }
  else {
    LOGE("*** %x:%x:%x", pDIS->itemAction, pDIS->itemState, pDIS->itemID);
  }

  dc.TextOut(pDIS->rcItem.left + 2, top, tmp);

  free(tmp);

  return 1;
}

/* ----------------------------------------------------------------- */
TCHAR *xComboBoxEx::GetText(int idx)
{
  if (idx < 0) {
    idx = GetCurSel();

    if (idx < 0) {
      return NULL;
    }
  }

  int len = GetLBTextLen(idx);

  if (len == 0) {
    LOGE("GetLBTextLen");
    return NULL;
  }

  TCHAR *tmp = (TCHAR *)malloc((len + 1) * sizeof(TCHAR));

  if (tmp == NULL) {
    LOGE("malloc");
    return NULL;
  }

  GetLBText(idx, tmp);

  return tmp;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::GetFontName(TCHAR *name, size_t bsize, int idx)
{
  TCHAR *tmp = GetText(idx);

  if (tmp == NULL) {
    return -1;
  }

  _sntprintf_s(name, bsize, _TRUNCATE, L"%s", tmp);

  free(tmp);

  return 0;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::GetFontSize(int idx)
{
  TCHAR *tmp = GetText(idx);

  if (tmp == NULL) {
    return -1;
  }

  int data = _tcstoul(tmp, NULL, 10);

  free(tmp);

  return data > 0 ? MulDiv(mLogPixelSy, data, mMaxPixelSy) : -1;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::DrawItemB(const DRAWITEMSTRUCT *pDIS)
{
  xTmpDC dc(pDIS->hDC);
  IPT   *pt = xPt();
  int    data;
  int    top;
  TCHAR *tmp;

  tmp = GetText((int)pDIS->itemID);

  if (tmp == NULL) {
    LOGE("malloc");
    return -1;
  }

  data = _tcstoul(tmp, NULL, 10);
  data = MulDiv(mLogPixelSy, data, mMaxPixelSy);

  dc.SetSelectFont(mFontName, -data);

  data = dc.tm.tmHeight;

  top = pDIS->rcItem.top + pDIS->rcItem.bottom - data;
  top = (top * 10 + 10) / 2;
  top = top / 10;

  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(0xFF000000);

  if (pDIS->itemAction == ODA_DRAWENTIRE)
    {
      dc.FillRect(&pDIS->rcItem, pt->color.base_bar);
    }
  else if (pDIS->itemAction == ODA_SELECT)
    {
      if (pDIS->itemState & ODS_SELECTED)
        {
          dc.FillRect(&pDIS->rcItem, pt->color.selected);
        }
      else
        {
          dc.FillRect(&pDIS->rcItem, pt->color.base_bar);
        }
    }
  else if (pDIS->itemAction == ODA_FOCUS)
    {
      dc.Rectangle(&pDIS->rcItem, pt->color.selected_frame, pt->color.selected);
    }
  else
    {
      LOGI("*** ERROR:%x:%x:%x", pDIS->itemAction, pDIS->itemState, pDIS->itemID);
    }

  dc.TextOut(pDIS->rcItem.left, top, tmp);

  free(tmp);

  return 1;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::DrawItem(const DRAWITEMSTRUCT *pDIS)
{
  if (pDIS->itemID == (UINT)(~0))
    {
      return 0;
    }

  if (pDIS->CtlID == IDC_COMBO4
   || pDIS->CtlID == IDC_COMBO6
   || pDIS->CtlID == IDC_COMBO8
   || pDIS->CtlID == IDC_COMBO10
   || pDIS->CtlID == IDC_COMBO12)
    {
      return DrawItemA(pDIS);
    }

  if (pDIS->CtlID == IDC_COMBO5
   || pDIS->CtlID == IDC_COMBO7
   || pDIS->CtlID == IDC_COMBO9
   || pDIS->CtlID == IDC_COMBO11
   || pDIS->CtlID == IDC_COMBO13)
    {
      return DrawItemB(pDIS);
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::MeasureItem(MEASUREITEMSTRUCT *pMIS)
{
  if (pMIS->itemID == (UINT)(~0))
    {
      return 0;
    }

  if (pMIS->CtlID == IDC_COMBO5
   || pMIS->CtlID == IDC_COMBO7
   || pMIS->CtlID == IDC_COMBO9
   || pMIS->CtlID == IDC_COMBO11
   || pMIS->CtlID == IDC_COMBO13)
    {
      UINT item_data = GetItemInt(pMIS->itemID);

      xDC  dc(mhWnd);

      dc.SetSelectFont(mFontName, item_data);

      if (item_data > 0)
        {
          pMIS->itemHeight = dc.tm.tmHeight + dc.tm.tmAscent + 4;
        }
    }

  if (pMIS->CtlID == IDC_COMBO4
   || pMIS->CtlID == IDC_COMBO6
   || pMIS->CtlID == IDC_COMBO8
   || pMIS->CtlID == IDC_COMBO10
   || pMIS->CtlID == IDC_COMBO12)
    {
      TCHAR *tmp = GetText(pMIS->itemID);

      xDC  dc(mhWnd);

      dc.SetSelectFont(tmp, mFontSize);

      pMIS->itemHeight = dc.tm.tmHeight + dc.tm.tmAscent + 4;
    }

  return 1;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::Command(int id, UINT codeNotify)
{
  TCHAR *tmp;

  switch (codeNotify) {
  case CBN_SELCHANGE:
    tmp = GetText();

    if (tmp == NULL) {
      break;
    }

    _sntprintf_s(mFontName, LF_FACESIZE, _TRUNCATE, L"%s", tmp);

    free(tmp);

    break;

  case CBN_ERRSPACE:
    LOGE("CBN_ERRSPACE");
    break;

  default:
    break;
  }

  return 1;
}

/* ----------------------------------------------------------------- */
int xComboBoxEx::GetItemInt(UINT itemID)
{
  int len  = GetLBTextLen(itemID);

  if (len == 0)
    {
      return 0;
    }

  TCHAR *tmp = (TCHAR *)malloc((len + 1) * sizeof(TCHAR));

  if (tmp == NULL)
    {
      LOGE("malloc");
      return 0;
    }

  GetLBText(itemID, tmp);

  int data = _tcstoul(tmp, NULL, 10);

  free(tmp);

  return data > 0 ? MulDiv(mLogPixelSy, data, mMaxPixelSy) : -1;
}

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
xStaticEx::xStaticEx()
{
  IPT *pt = xPt();

  index = pt->setting.sort_part * 2 + (pt->setting.sort_order > 0 ? 0 : 1);

  column = pt->setting.column - 1;
}

/* ----------------------------------------------------------------- */
void xStaticEx::OnPaint(void)
{
  xPaintDC  dc(mhWnd);
  RECT      rc;
  IPT      *pt = xPt();

  GetClientRect(&rc);
  dc.FillRect(&rc, pt->color.base_bar);

  int  x = rc.left;
  int  y = rc.top + 1;

  TCHAR buf[32];

  GetText(buf, 32);

  if (buf[0] == L'I') {
    y += ((rc.bottom - rc.top) - 16) / 2;

    IcoDrawIconFromIndex(dc, x, y, 26 + index, TRUE);
  }
  else if (buf[0] == L'C') {
    y += ((rc.bottom - rc.top) - 16) / 2;

    IcoDrawIconFromIndex(dc, x, y, 79 + column, TRUE);
  }
  else {
    dc.SetSelectFont(pt->fonts[4].name, -MulDiv(mLogPixelSy, 9, mMaxPixelSy));
    dc.SetBkMode(TRANSPARENT);

    y += ((rc.bottom - rc.top ) - dc.tm.tmHeight) / 2;

    dc.SetTextColor(pt->color.title);
    dc.TextOut(x, y, buf);
  }
}

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
dlg_setting::dlg_setting(HWND hParent) : xDialog(_T("IDD_SETTING_DIALOG"), hParent)
{
  mpCmbo1 = new xComboBox;
  mpCmbo2 = new xComboBox;
  mpCmbo3 = new xComboBox;

  /* New combo box control */

  for (int i = 0; i < DLG_SETTING_EDIT_CTRL_NUM; i++)
    {
      mpCmboA[i] = new xComboBoxEx;
      mpCmboB[i] = new xComboBoxEx;
    }

  for (int i = 0; i < DLG_SETTING_STATE_CTRL_NUM; i++)
    {
      mpState[i] = new xStaticEx;
    }

  mpICons  = new xStaticEx;
  mpColumn = new xStaticEx;

  mpButton1 = new com_button;
  mpButton2 = new com_button;

  IcoSetPsMode(&mpButton1->icon_no, &mpButton2->icon_no);
}

/* ----------------------------------------------------------------- */
dlg_setting::~dlg_setting()
{
  /* Delete control */

  delete mpCmbo1;
  delete mpCmbo2;
  delete mpCmbo3;

  for (int i = 0; i < DLG_SETTING_EDIT_CTRL_NUM; i++)
    {
      delete mpCmboA[i];
      delete mpCmboB[i];
    }

  for (int i = 0; i < DLG_SETTING_STATE_CTRL_NUM; i++)
    {
      delete mpState[i];
    }

  delete mpICons;
  delete mpColumn;

  delete mpButton1;
  delete mpButton2;
}

/* ----------------------------------------------------------------- */
int CALLBACK EnumFontFamProc(const LOGFONT        *lf,
                             const TEXTMETRIC FAR *tm,
                             DWORD                 FontType,
                             LPARAM                lParam)
{
  if (lf->lfFaceName[0] == '@' || lf->lfItalic || !(FontType & TRUETYPE_FONTTYPE))
    {
      return 1;
    }

  dlg_setting *dlg = (dlg_setting *)lParam;

  if ((lf->lfPitchAndFamily & 3) == FIXED_PITCH)
    {
      for (int i = 0; i < DLG_SETTING_EDIT_CTRL_NUM; i++)
        {
          dlg->mpCmboA[i]->InsertString(-1, lf->lfFaceName);
        }
    }
  else
    {
      dlg->mpCmboA[0]->InsertString(-1, lf->lfFaceName);
      dlg->mpCmboA[2]->InsertString(-1, lf->lfFaceName);
      dlg->mpCmboA[4]->InsertString(-1, lf->lfFaceName);
    }

  return 1;
}

/* ----------------------------------------------------------------- */
void dlg_setting::SetOkElement(void)
{
  /* Get font name */

  for (int i = 0; i < DLG_SETTING_EDIT_CTRL_NUM; i++) {
    int idx = mpCmboA[i]->GetCurSel();

    if (idx < 0) {
      continue;
    }

    int len = mpCmboA[i]->GetLBTextLen(idx);

    if (len == 0) {
      continue;
    }

    TCHAR *tmp = (TCHAR *)malloc((len + 1) * sizeof(TCHAR));

    if (tmp == NULL) {
      continue;
    }

    mpCmboA[i]->GetLBText(idx, tmp);

    _sntprintf_s(mFontName[i], LF_FACESIZE, _TRUNCATE, L"%s", tmp);

    free(tmp);
  }

  /* Get font size */

  for (int i = 0; i < DLG_SETTING_EDIT_CTRL_NUM; i++) {
    int idx = mpCmboB[i]->GetCurSel();

    if (idx < 0) {
      continue;
    }

    mFontSize[i] = (int)mpCmboB[i]->GetItemData(idx);
  }
}

/* ----------------------------------------------------------------- */
void dlg_setting::SetListFontName(xComboBoxEx *cmb_a, xComboBoxEx *cmb_b, const TCHAR *cp)
{
  int idx = cmb_a->FindString(0, cp);

  if (idx < 0)
    {
      idx = 0;
    }

  cmb_a->SetCurSel(idx);

  _sntprintf_s(cmb_b->mFontName, _countof(cmb_b->mFontName), _TRUNCATE, L"%s", cp);
}

/* ----------------------------------------------------------------- */
void dlg_setting::SetListFontSize(xComboBoxEx *cmb_b, xComboBoxEx *cmb_a, int font_size)
{
  int size = -MulDiv(mMaxPixelSy, font_size, mLogPixelSy);

  cmb_b->InsertString(0, L"8");
  cmb_b->InsertString(1, L"9");
  cmb_b->InsertString(2, L"10");
  cmb_b->InsertString(3, L"11");
  cmb_b->InsertString(4, L"12");
  cmb_b->InsertString(5, L"14");

  cmb_b->SetItemData(0, -MulDiv(mLogPixelSy,  8, mMaxPixelSy));
  cmb_b->SetItemData(1, -MulDiv(mLogPixelSy,  9, mMaxPixelSy));
  cmb_b->SetItemData(2, -MulDiv(mLogPixelSy, 10, mMaxPixelSy));
  cmb_b->SetItemData(3, -MulDiv(mLogPixelSy, 11, mMaxPixelSy));
  cmb_b->SetItemData(4, -MulDiv(mLogPixelSy, 12, mMaxPixelSy));
  cmb_b->SetItemData(5, -MulDiv(mLogPixelSy, 14, mMaxPixelSy));

  TCHAR buf[4];

  _sntprintf_s(buf, _countof(buf), _TRUNCATE, L"%d", size);

  int idx = cmb_b->FindString(0, buf);

  if (idx < 0)
    {
      idx = 0;
    }
  else
    {
      cmb_a->mFontSize = font_size;
    }

  cmb_b->SetCurSel(idx);

  cmb_b->mFontSize = size;
}

/* ----------------------------------------------------------------- */
BOOL dlg_setting::OnInitDialog(HWND hwndFocus, LPARAM lParam)
{
  IPT *pt = xPt();

  CenterWindow();   // 真ん中に移動

  Attach(mpCmbo1, IDC_COMBO1);
  Attach(mpCmbo2, IDC_COMBO2);
  Attach(mpCmbo3, IDC_COMBO3);
  Attach(mpCmboA[0], IDC_COMBO4);
  Attach(mpCmboA[1], IDC_COMBO6);
  Attach(mpCmboA[2], IDC_COMBO8);
  Attach(mpCmboA[3], IDC_COMBO10);
  Attach(mpCmboA[4], IDC_COMBO12);
  Attach(mpCmboB[0], IDC_COMBO5);
  Attach(mpCmboB[1], IDC_COMBO7);
  Attach(mpCmboB[2], IDC_COMBO9);
  Attach(mpCmboB[3], IDC_COMBO11);
  Attach(mpCmboB[4], IDC_COMBO13);
  Attach(mpState[0], IDC_STATIC1);
  Attach(mpState[1], IDC_STATIC2);
  Attach(mpState[2], IDC_STATIC3);
  Attach(mpState[3], IDC_STATIC4);
  Attach(mpState[4], IDC_STATIC5);
  Attach(mpState[5], IDC_STATIC6);
  Attach(mpState[6], IDC_STATIC7);
  Attach(mpState[7], IDC_STATIC10);
  Attach(mpICons, IDC_STATIC8);
  Attach(mpColumn, IDC_STATIC9);
  Attach(mpButton1, IDOK2);
  Attach(mpButton2, IDCANCEL2);

  /* settings */

  mpCmbo1->InsertString(0, _T("1st"));
  mpCmbo1->InsertString(1, _T("2nd"));
  mpCmbo1->InsertString(2, _T("3rd"));
  mpCmbo1->InsertString(3, _T("4th"));
  mpCmbo1->InsertString(4, _T("5th"));
  mpCmbo1->InsertString(5, _T("6th"));

  if (pt->setting.column > 0)
    {
      mpCmbo1->SetCurSel(pt->setting.column - 1);
    }
  mpCmbo2->InsertString(0, _T("Ascending"));
  mpCmbo2->InsertString(1, _T("Descending"));
  if (pt->setting.sort_order > 0)
    {
      mpCmbo2->SetCurSel(0);
    }
  else
    {
      mpCmbo2->SetCurSel(1);
    }
  mpCmbo3->InsertString(0, _T("Extension"));
  mpCmbo3->InsertString(1, _T("Date and time"));
  mpCmbo3->InsertString(2, _T("Size"));
  mpCmbo3->InsertString(3, _T("Name"));
  mpCmbo3->SetCurSel(pt->setting.sort_part);

  /* Get font list */

  xDC dc(hwndFocus);

  /* Get LOGPIXELSY */

  mLogPixelSy = GetDeviceCaps(dc, LOGPIXELSY);
  mMaxPixelSy = 72;

  /* Set fonts */

  LOGFONT lf = {0};

  EnumFontFamiliesEx(dc, &lf, EnumFontFamProc, (LONG_PTR)this, 0);

  for (int i = 0; i < DLG_SETTING_EDIT_CTRL_NUM; i++) {
    SetListFontName(mpCmboA[i], mpCmboB[i], mFontName[i]);
    SetListFontSize(mpCmboB[i], mpCmboA[i], mFontSize[i]);
  }

  for (int i = 0; i < DLG_SETTING_EDIT_CTRL_NUM; i++) {
    mpCmboA[i]->mFontSize = mpCmboB[i]->GetFontSize();
    mpCmboA[i]->GetFontName(mpCmboB[i]->mFontName);
  }

  return TRUE;
}

/* ----------------------------------------------------------------- */
void dlg_setting::OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  IPT         *pt = xPt();
  int          idx = 0;
  xComboBoxEx *cmb_a;
  xComboBoxEx *cmb_b;

  switch (id) {
  case IDC_COMBO1:
    if (codeNotify != CBN_SELCHANGE) {
      break;
    }
    mpColumn->column = mpCmbo1->GetCurSel();
    mpColumn->Invalidate();
    break;

  case IDC_COMBO2:
  case IDC_COMBO3:
    if (codeNotify != CBN_SELCHANGE) {
      break;
    }
    mpICons->index = mpCmbo3->GetCurSel() * 2 + mpCmbo2->GetCurSel();
    mpICons->Invalidate();
    break;

  case IDOK:
  case IDOK2:
    pt->setting.column     = mpCmbo1->GetCurSel() + 1;
    pt->setting.sort_order = mpCmbo2->GetCurSel();

    if (pt->setting.sort_order == 0) {
        pt->setting.sort_order = 1;
    }
    else {
        pt->setting.sort_order = -1;
    }

    pt->setting.sort_part = mpCmbo3->GetCurSel();

    SetOkElement();
    EndDialog(IDOK);

    break;
  case IDCANCEL:
  case IDCANCEL2:
    EndDialog(IDCANCEL);
    break;

  case IDC_COMBO13:
    idx++;
  case IDC_COMBO11:
    idx++;
  case IDC_COMBO9:
    idx++;
  case IDC_COMBO7:
    idx++;
  case IDC_COMBO5:
    if (codeNotify != CBN_SELCHANGE) {
      break;
    }

    cmb_a = mpCmboA[idx];
    cmb_b = mpCmboB[idx];

    cmb_a->mFontSize = cmb_b->GetFontSize();

    cmb_a->Invalidate();

    break;

  case IDC_COMBO12:
    idx++;
  case IDC_COMBO10:
    idx++;
  case IDC_COMBO8:
    idx++;
  case IDC_COMBO6:
    idx++;
  case IDC_COMBO4:
    if (codeNotify != CBN_SELCHANGE) {
      break;
    }

    cmb_a = mpCmboA[idx];
    cmb_b = mpCmboB[idx];

    cmb_a->GetFontName(cmb_b->mFontName);

    cmb_b->Invalidate();

    break;

  default:
    break;
  }
}

/* ----------------------------------------------------------------- */
void dlg_setting::SetLogFont(int no, const TCHAR *fnam, int size)
{
  _sntprintf_s(mFontName[no], _countof(mFontName[no]), _TRUNCATE, L"%s", fnam);
  mFontSize[no] = size;
}

/* ----------------------------------------------------------------- */
void dlg_setting::GetLogFont(int no, TCHAR *face, int len, int *size)
{
  _sntprintf_s(face, len, _TRUNCATE, L"%s", mFontName[no]);
  *size = mFontSize[no];
}

/* ----------------------------------------------------------------- */
void dlg_setting::OnPaint(void)
{
  xPaintDC  dc(mhWnd);
  RECT      rc;
  IPT      *pt = xPt();
  int       x;
  int       y;

  GetClientRect(&rc);

  dc.SetSelectFont(pt->fonts[4].name, -MulDiv(mLogPixelSy, 9, mMaxPixelSy));
  dc.SetBkMode(TRANSPARENT);
  dc.FillRect(&rc, pt->color.base_bar);

  x = rc.left + 32;
  y = rc.top  + 8;

  dc.SetTextColor(pt->color.title);
  dc.TextOut(x, y, L"Set the font for each field.");

  IcoDrawIconFromIndex(dc, rc.left + 8, y, 9, TRUE);
}

/* ----------------------------------------------------------------- */
LRESULT dlg_setting::Message(UINT msg, WPARAM wParam, LPARAM lParam)
{
  LRESULT res = 0;

  switch(msg) {
  case WM_LBUTTONDOWN:
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, lParam);
    break;
  default:
    res = xDialog::Message(msg, wParam, lParam);
    break;
  }

  return res;
}
