/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: icon_beta.c
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
 */

/* ----------------------------------------------------------------- *
 * Included files
 * ----------------------------------------------------------------- */

#include <windows.h>
#include <commctrl.h>
#include <tchar.h>
#include "xpt.h"
#include "logf.h"
#include "dirent.h"
#include "icon.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

#define ICONSIZE  (16)

/**********************************************************************
 * Private types
 *********************************************************************/

typedef struct _ICONICON_T_ {
  //
  HIMAGELIST  handle;
  int         index;
}
ICONICON;

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

static HIMAGELIST _hImage    = NULL;
static ICONICON   _temp_dir  = {NULL, -1};
static ICONICON   _temp_file = {NULL, -1};

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static void DrawIconFromIndex(HIMAGELIST image, HDC hDC, int x, int y, int index, UINT style)
{
  int  cx;
  int  cy;

  ImageList_GetIconSize(image, &cx, &cy);

  if (ImageList_GetImageCount(image) > index) {
    ImageList_DrawEx(image,
                     index,
                     hDC,
                     x,
                     y,
                     cx,
                     cy,
                     CLR_NONE,
                     CLR_NONE,
                     style);
  }
}

/* ----------------------------------------------------------------- */
static TCHAR *PathFindFileName(TCHAR *str)
{
  TCHAR *cp = str;

  for (int i = 0; str[i] && i < MAX_PATH; i++) {
    if (str[i] == '/' || str[i] == '\\') {
      cp = &str[i + 1];
    }
  }

  return cp;
}

/* ----------------------------------------------------------------- */
static HANDLE GetInstance(dirent *entry, DWORD *d_no)
{
  SHFILEINFO  shfi;
  HANDLE      handle;
  UINT        flags = SHGFI_SMALLICON | SHGFI_SYSICONINDEX | SHGFI_OVERLAYINDEX | SHGFI_ICON;

  handle = (HIMAGELIST)SHGetFileInfo(entry->d_name,
                                     FILE_ATTRIBUTE_ARCHIVE,
                                     &shfi,
                                     sizeof(SHFILEINFO),
                                     flags);
  if (handle) {
    *d_no = shfi.iIcon;

    DestroyIcon(shfi.hIcon);
  }
  else {
    *d_no = 0;

    handle = _hImage;
  }

  return handle;
}

/* ----------------------------------------------------------------- */
static HANDLE GetRemoteInstance(dirent *entry, DWORD *d_no)
{
  HANDLE  handle;

  if (entry->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
    IcoGetTempDirectoryIconIndex(&handle, d_no);
  }
  else {
    IcoGetTempFileIconIndex(&handle, d_no);
  }

  return handle;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
void IcoInit(void)
{
  if (_hImage == NULL) {
    _hImage = ImageList_LoadImage(GetModuleHandle(NULL),
                                  L"IDB_IMAGELIST_A",
                                  ICONSIZE,
                                  ICONSIZE,
                                  RGB(0, 128, 0),
                                  IMAGE_BITMAP,
                                  LR_CREATEDIBSECTION);
  }

  TCHAR       buf[ MAX_PATH ];
  TCHAR       tmp[ MAX_PATH ];
  SHFILEINFO  shfi;

  /* Get temporary icon of directory */

  GetTempPath(MAX_PATH, buf);

  if (_temp_dir.handle == NULL) {
    ZeroMemory(&shfi, sizeof(shfi));

    _temp_dir.handle = (HIMAGELIST)SHGetFileInfo(buf,
                                                 FILE_ATTRIBUTE_ARCHIVE,
                                                 &shfi,
                                                 sizeof(SHFILEINFO),
                                                 SHGFI_SMALLICON | SHGFI_SYSICONINDEX);
    _temp_dir.index = shfi.iIcon;
  }

  /* Get temporary icon of file */

  if (_temp_file.handle == NULL) {
    GetTempFileName(buf, _T("fs8"), 0, tmp);

    _temp_file.handle = (HIMAGELIST)SHGetFileInfo(tmp,
                                                  FILE_ATTRIBUTE_ARCHIVE,
                                                  &shfi,
                                                  sizeof(SHFILEINFO),
                                                  SHGFI_SMALLICON | SHGFI_SYSICONINDEX);
    _temp_file.index = shfi.iIcon;

    DeleteFile(tmp);
  }
}

/* ----------------------------------------------------------------- */
void IcoFin(void)
{
  if(_hImage) {
    ImageList_Destroy(_hImage);
    _hImage = NULL;
  }
}

/* ----------------------------------------------------------------- */
void *IcoGetImageList(void)
{
  return _hImage;
}

/* ----------------------------------------------------------------- */
void IcoDrawIconFromFile(HDC hDC, int x, int y, dirent *entry, BOOL isActive, BOOL isRemote)
{
  UINT style = isActive ? ILD_NORMAL :ILD_BLEND50;

  /* Display ".." files as an icon */

  if (_tcscmp(PathFindFileName(entry->d_name), _T("..")) == 0) {
    DrawIconFromIndex(_hImage, hDC, x - 4, y, 168, style);
    DrawIconFromIndex(_hImage, hDC, x +12, y, 169, style);
    return;
  }

  /* Displaying normal file an icon */

  DWORD flag = FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_READONLY;

  DrawIconFromIndex((HIMAGELIST)entry->hHandle,
                    hDC,
                    x,
                    y,
                    entry->userData & 0x0000FFFF,
                    INDEXTOOVERLAYMASK((entry->userData & 0x0F000000) >> 24)
                  | (entry->dwFileAttributes & flag) ? ILD_BLEND50 : style);

  /* Display check mark */

  if (isRemote) {
    DrawIconFromIndex(_hImage, hDC, x - 3, y, 45, style);
  }

  int mark_idx = 38;

  if (entry->bookMark) {
    DrawIconFromIndex(_hImage, hDC, x - 15, y - 2, 119, style);
  }
  else if (entry->dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) {
    DrawIconFromIndex(_hImage, hDC, x - 4, y - 1, mark_idx + 2, style);
  }
  else if (entry->dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
    DrawIconFromIndex(_hImage, hDC, x - 4, y - 1, mark_idx + 1, style);
  }
  else if (entry->dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
    DrawIconFromIndex(_hImage, hDC, x - 4, y - 1, mark_idx + 0, style);
  }

  /* Draw git mark */

  int git_idxx = ((entry->userData & 0x00F00000) >> 20);
  int git_idxy = ((entry->userData & 0x000F0000) >> 16);

  x -= 14;
  y += 2;

  if (1 <= git_idxx && git_idxx <= 7) {
    DrawIconFromIndex(_hImage, hDC, x, y, (git_idxx % 8) + 150, style);
  }

  if (1 <= git_idxy && git_idxy <= 7) {
    DrawIconFromIndex(_hImage, hDC, x + 9, y, (git_idxy % 8) + 150, style);
  }

  if (git_idxx == 0 && 0 < git_idxy) {
    DrawIconFromIndex(_hImage, hDC, x, y, 150, style);
  }

  /* 1: ' ' = unmodified
   * 2:  M  = modified
   * 3:  A  = added
   * 4:  D  = deleted
   * 5:  R  = renamed
   * 6:  C  = copied
   * 7:  U  = updated but unmerged
   */
}

/* ----------------------------------------------------------------- */
int IcoDrawIconFromIndex(HDC hDC, int x, int y, int idx, BOOL isActive)
{
  DrawIconFromIndex(_hImage, hDC, x, y, idx, isActive ? ILD_NORMAL : ILD_BLEND50);

  return ICONSIZE;
}

/* ----------------------------------------------------------------- */
void IcoGetTempDirectoryIconIndex(HANDLE *handle, DWORD *index)
{
  *index  = _temp_dir.index;
  *handle = _temp_dir.handle;
}

/* ----------------------------------------------------------------- */
void IcoGetTempFileIconIndex(HANDLE *handle, DWORD *index)
{
  *index  = _temp_file.index;
  *handle = _temp_file.handle;
}

/* ----------------------------------------------------------------- */
HANDLE IcoGetInstance(dirent *entry, DWORD *d_no, BOOL isRemotea)
{
  return isRemotea ? GetRemoteInstance(entry, d_no) : GetInstance(entry, d_no);
}

/* ----------------------------------------------------------------- */
void IcoSetPsMode(int *icon_ok, int *icon_cancel)
{
  IPT  *pt = xPt();

  if (pt->setting.ps_mode) {
    *icon_ok     = 100;
    *icon_cancel = 99;
  }
  else {
    *icon_ok     = 97;
    *icon_cancel = 89;
  }
}
