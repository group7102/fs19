#ifndef _CMD_EXEC_H_
#define _CMD_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Public types
 * ----------------------------------------------------------------- */

typedef void (*PUMP_CALLBACK)(void *p_param);

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

HANDLE OpenProcessToFileHandle(const TCHAR *cmd, DWORD tmo, PUMP_CALLBACK cb, void *p_param);
int OpenProcessToFile(const TCHAR  *cmd,
                      TCHAR        *file,
                      DWORD         len,
                      DWORD         tmo,
                      PUMP_CALLBACK cb,
                      void         *p_param);
char *OpenProcessToMemory(const TCHAR *cmd, DWORD tmo, PUMP_CALLBACK cb, void *p_param);
int ExecProcess(const TCHAR *cmd, DWORD tmo, PUMP_CALLBACK cb, void *p_param);
BOOL ExecCommand(TCHAR *pCmdline);

#ifdef __cplusplus
}
#endif

#endif /* _CMD_EXEC_H_ */
