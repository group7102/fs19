/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: memo.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _MEMO_H_
#define _MEMO_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */
class memo : public xEdit
{
public:
  memo(int no);
  ~memo();

public:
  HWND Create(HWND hParentWnd, int x, int y, int cx, int cy);

public:
  int PumpMessage(void);

protected:
  void OnDestroy(void);

protected:
  xFont *mpFont;

private:
  int  no;
  BYTE sha1_1[20];
  BYTE sha1_2[20];

private:
  int   Update(BOOL is_force = FALSE);
  char *WindowTextToUTF8(size_t *p_len);
  char *ReadFile(size_t *p_len = NULL);
  void  SetUtf8ForWindowText(char *buf);
  int   GetSha1(const PBYTE buf, DWORD len, PBYTE sha1);
  int   FuncEditor(void);
};

#endif /* _MEMO_H_ */
