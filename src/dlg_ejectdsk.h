/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_ejectdsk.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DLG_EJECTDSK_H_
#define _DLG_EJECTDSK_H_

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

class com_button;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class dlg_ejectdsk : public xDialog
{
public:
  BOOL CheckEjectDrive(void);

public:
  dlg_ejectdsk(HWND hParent);
  ~dlg_ejectdsk();

private:
  BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  void OnPaint();

private:
  BOOL DiscEject(TCHAR *drv);
  BOOL DiscLoad(int c);

private:
  com_list   *mpLogList;
  com_button *mpOK;
  com_button *mpCancel;
};

#endif /* _DLG_EJECTDSK_H_ */
