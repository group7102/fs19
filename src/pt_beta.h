/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: pt_beta.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _PT_BETA_H_
#define _PT_BETA_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */
/*
enum PT_FUNCS_T {
  PT_FUNC_NON     = 0,
  PT_FUNC_EDITOR,
  PT_FUNC_LOGDISK,
  PT_FUNC_REFLESH,
  PT_FUNC_EXEC,
  PT_FUNC_BACK,
  PT_FUNC_STEP,
  PT_FUNC_EJECT,
  PT_FUNC_COPY,
  PT_FUNC_PAST,
  PT_FUNC_RENAME,
  PT_FUNC_OPTION,
  PT_FUNC_SELEXE,
  PT_FUNC_DELETE,
  PT_FUNC_INSERT,
  PT_FUNC_APPS,
  PT_FUNC_PROPERTIES,
  PT_FUNC_ATTRIBUTE,
  PT_FUNC_SHORTCUT1,
  PT_FUNC_SHORTCUT2,
  PT_FUNC_SHORTCUT3,
  PT_FUNC_SHORTCUT4,
  PT_FUNC_SHORTCUT5,
  PT_FUNC_SHORTCUT6,
  PT_FUNC_SHORTCUT7,
  PT_FUNC_SETCOPYVIEW,
  PT_FUNC_FIND,
  PT_FUNC_SORT,
  PT_FUNC_COLUMN,
  PT_FUNC_MAX,
};
*/
#define PT_SORT_FOLDERS_NUM 10

/* ----------------------------------------------------------------- *
 * public types
 * ----------------------------------------------------------------- */

typedef struct {
  TCHAR path[MAX_PATH];
  int   type;
  int   order;
  int   column;
}
D_SORT_T;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class ipt
{
public:
  ipt();
  virtual ~ipt();

public:
  bool Read(void);
  bool Write(void);
//void SetPlacement(HWND hWnd);
//int  GetFuncNo(int vk, int ctrl, int shift, int alt);
  void SetSortInfo(TCHAR *path, int column, int type, int order);
  int  GetSortInfo(TCHAR *path, int *column, int *type, int *order);

protected:
  D_SORT_T *ipt::SearchInfo(TCHAR *path);

public:
  static int      OK;

  /* Window */

  static int      x;
  static int      y;
  static int      cx;
  static int      cy;
  static int      show;
  static int      column;
  static int      page_no;
  static int      tool_size;
  static int      line_pitch;
  static int      sort_part;
  static int      sort_order;

  /* Colors */

  static COLORREF COL_BASE_BAR;
  static COLORREF COL_SCROLL_BAR;
  static COLORREF COL_SCROLL_BAR_SEL;
  static COLORREF COL_BASE_BAR_LOW;
  static COLORREF COL_SPLIT_LINE;
  static COLORREF COL_SPLIT_LINE2;
  static COLORREF COL_SPLIT_LINE3 ;
  static COLORREF COL_SPLIT_LINE_TOP;
  static COLORREF COL_SPLIT_LINE_BTM;
  static COLORREF COL_SELECTED;
  static COLORREF COL_SELECTED_FRAME;
  static COLORREF COL_SELECTED_CUR;
  static COLORREF COL_PROGRESS_BAR;
  static COLORREF COL_REMAIN_BAR_BLUE;
  static COLORREF COL_REMAIN_BAR_RED;
  static COLORREF COL_TITLE;
  static COLORREF COL_FILE_ATTR_SYSTEM;
  static COLORREF COL_FILE_ATTR_HIDDEN;
  static COLORREF COL_FILE_ATTR_READONLY;
  static COLORREF COL_FILE_ATTR_DIRECTORY;
  static COLORREF COL_FILE_ATTR_NORMAL;

  /* Font */

  static int      fontsize1;
  static TCHAR    fontname1[MAX_PATH];
  static int      fontsize2;
  static TCHAR    fontname2[MAX_PATH];
  static int      fontsize3;
  static TCHAR    fontname3[MAX_PATH];
  static int      fontsize4;
  static TCHAR    fontname4[MAX_PATH];
  static int      fontsize5;
  static TCHAR    fontname5[MAX_PATH];

  /* Setting */

  static TCHAR    editor[MAX_PATH];
  static TCHAR    shortcut[MAX_PATH];
  static TCHAR    path1[MAX_PATH];
  static TCHAR    path2[MAX_PATH];
  static int      COL_MODE;
  static TCHAR    wclist[MAX_PATH];
  static TCHAR    difftool[MAX_PATH];

  /* Memo */

  static TCHAR    memo_file1[MAX_PATH];
  static TCHAR    memo_file2[MAX_PATH];
  static DWORD    memo_cur1;
  static DWORD    memo_cur2;

  /* List */

  static int      libraly_num;
  static TCHAR    libraly_name[30][MAX_PATH];
  static TCHAR    libraly_path[30][MAX_PATH];
  static int      search_folder_num;
  static TCHAR    search_folders[8][MAX_PATH];
  static int      sort_folder_num;
  static D_SORT_T sort_folders[PT_SORT_FOLDERS_NUM];
};

#endif//_PT_BETA_H_
