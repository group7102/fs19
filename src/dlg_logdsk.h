/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_logdsk.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _DLG_LOGDSK_H_
#define _DLG_LOGDSK_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define MAX_PATH2 (MAX_PATH * 2)

/* ----------------------------------------------------------------- *
 * class of use
 * ----------------------------------------------------------------- */

class com_list;
class com_edit;
class com_button;
class iFont;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */
class dlg_logdsk : public xDialog
{
public:
  void  SetCurrent(const TCHAR *name);
  TCHAR logdrv[MAX_PATH2];

public:
  dlg_logdsk(HWND hParent);
  ~dlg_logdsk();

private:
  BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  void OnPaint(void);
  void SetList(void);

protected:
  LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam);

private:
  com_list   *mpLogList;
  com_edit   *mpEdit;
  com_button *mpOK;
  com_button *mpCancel;
  xFont      *mpFont;
};

#endif /* _DLG_LOGDSK_H_ */
