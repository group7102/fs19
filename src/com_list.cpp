/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: com_list.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "ssnprintf.h"
#include "xpt.h"
#include "logf.h"
#include "icon.h"
#include "savedir.h"
#include "history.h"
#include "com_list.h"

/* ----------------------------------------------------------------- */
com_list::com_list()
{
  date_time = HitGetLocalDateTime();
}

/* ----------------------------------------------------------------- */
com_list::~com_list()
{
}

/* ----------------------------------------------------------------- *
 * ドライブ情報追加
 * ----------------------------------------------------------------- */
void com_list::SetLogData(int index, const TCHAR *pRootPath, int type)
{
  InsertString(index, pRootPath);
  SetItemData(index, type);
}

/* ----------------------------------------------------------------- *
 * オーナー描画の処理
 * ----------------------------------------------------------------- */
int com_list::DrawItem(const DRAWITEMSTRUCT *pDIS)
{
  xTmpDC      dc(pDIS->hwndItem, pDIS->hDC);
  xBmpDC      bmp;
  TCHAR       buf[MAX_PATH];
  TCHAR       tmp[MAX_PATH];
  IPT        *pt = xPt();
  SIZE        size;
  int         index;
  SHFILEINFO  shfi;
  HIMAGELIST  handle = 0;
  RECT        rc;
  TCHAR       date_str[50];
  int         xxx = 4;
  int         yy1 = 0;
  int         yy2 = 0;
  int         cx;
  int         cy;

  if (pDIS->itemID == (UINT)(~0)) {
    return 0;
  }

  rc = pDIS->rcItem;

  cx  = rc.right  - rc.left;
  cy  = rc.bottom - rc.top;

  bmp.Create(dc, cx, cy);
  bmp.SetBkMode(TRANSPARENT);
  bmp.SetTextColor(0);

  if (pDIS->itemState & ODS_SELECTED) {
    bmp.Rectangle(0, 0, cx, cy, pt->color.selected_frame, pt->color.selected);
  }
  else {
    bmp.FillRect(0, 0, cx, cy, pt->color.base_bar);
  }

  GetText(pDIS->itemID, buf);

  index = (int)GetItemData(pDIS->itemID);

  if (index < 0) {
    bmp.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

    handle = (HIMAGELIST)::SHGetFileInfo(buf,
                                         0,
                                         &shfi,
                                         sizeof(SHFILEINFO),
                                         SHGFI_DISPLAYNAME | SHGFI_SYSICONINDEX);

    bmp.TextOut(DPIX(40), (cy - bmp.tm.tmHeight) / 2, shfi.szDisplayName);

    ::ImageList_DrawEx(handle,
                       shfi.iIcon,
                       bmp,
                       2,
                       (cy - DPIX(32)) / 2,
                       DPIX(32),
                       DPIY(32),
                       CLR_NONE,
                       CLR_NONE,
                       ILD_NORMAL);
  }
  else {
    bmp.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size, FW_SEMIBOLD);

    yy1 = (cy - bmp.tm.tmHeight * 2) / 3;
    yy2 = yy1 + bmp.tm.tmHeight + yy1;

    CmpLocalDateTime(index, date_time, date_str, _countof(date_str));

    GetTextExtentPoint32(bmp, date_str, (int)_tcslen(date_str), &size);

    _sntprintf_s(tmp, _countof(tmp), _TRUNCATE, L"%s", buf);

    if (!PathIsUNC(tmp) || _tcsncmp(tmp, _T("\\\\wsl"), 5) == 0) {
      UINT flags = SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_DISPLAYNAME;

      handle = (HIMAGELIST)::SHGetFileInfo(tmp,
                                           0,
                                           &shfi,
                                           sizeof(SHFILEINFO),
                                           flags);
    }

    if (handle) {
      ::ImageList_DrawEx(handle,
                         shfi.iIcon,
                         bmp,
                         xxx,
                         yy1,
                         DPIX(16),
                         DPIY(16),
                         CLR_NONE,
                         CLR_NONE,
                         ILD_NORMAL);
      xxx += DPIX(24);

      bmp.TextOut(xxx, yy1, shfi.szDisplayName);
    }
    else {
      IcoDrawIconFromIndex(bmp, xxx, yy1, 20, TRUE);

      xxx += DPIX(24);

      bmp.TextOut(xxx, yy1, PathFindFileName(tmp));

      HitDelete(index);
    }

    PathCompactPath(bmp, tmp,  cx - xxx - size.cx);

    PathRemoveBackslash(tmp);
    PathRemoveFileSpec(tmp);

    bmp.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);
    bmp.TextOut(xxx, yy2, tmp);
    bmp.TextOut(cx - (size.cx + DPIX(4)), yy1, date_str);

    int   mark = 0;
    DWORD date = 0;

    HitGetAt(index, &mark, &date);

    if (mark != 0) {
      IcoDrawIconFromIndex(bmp, 2, 2, 44, TRUE);
    }
  }

  dc.BitBlt(rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, bmp, 0, 0, SRCCOPY);

  return 1;
}

/* ----------------------------------------------------------------- *
 * オーナー描画コントロールの寸法を設定
 * ----------------------------------------------------------------- */
int com_list::MeasureItem(MEASUREITEMSTRUCT *pMIS)
{
  xDC   dc(mhWnd);
  IPT  *pt = xPt();
  TCHAR path[MAX_PATH];

  dc.SetSelectFont(pt->fonts[1].name, pt->fonts[1].size);

  GetText(pMIS->itemID, path);

  if (PathIsRoot(path))
    {
      if (dc.tm.tmHeight < 32)
        {
          pMIS->itemHeight = DPIY(38);
        }
      else
        {
          pMIS->itemHeight = dc.tm.tmHeight + DPIY(20);
        }
    }
  else
    {
      if (dc.tm.tmHeight < DPIY(20))
        {
          pMIS->itemHeight = DPIY(20);
        }
      else
        {
          pMIS->itemHeight = dc.tm.tmHeight + DPIY(4);
        }

      pMIS->itemHeight *= 2;
    }

  return 1;
}

/* ----------------------------------------------------------------- */
void com_list::FillRect(HDC hDC, const RECT *pRC, COLORREF fg)
{
  FillRect(hDC, pRC, fg, fg);
}

/* ----------------------------------------------------------------- */
void com_list::FillRect(HDC hDC, const RECT *pRC, COLORREF pen, COLORREF fg)
{
  xTmpDC  dc(hDC);

  dc.Rectangle(pRC, pen, fg);
}

/* ----------------------------------------------------------------- */
void com_list::CmpLocalDateTime(int    idx,
                                DWORD  date_time,
                                TCHAR *date_str,
                                size_t len)
{
  int   mark = 0;
  DWORD date = 0;

  if (date_time == 0 || HitGetAt(idx, &mark, &date) == NULL) {
    _stprintf_s(date_str, len, L"???");
    return;
  }

  date_time -= date;

  if (date_time == 0) {
    _stprintf_s(date_str, len, L"now");
  }
  else if (date_time < 60) {
    if (date_time == 1) {
      _stprintf_s(date_str, len, L"%d second ago", date_time);
    }
    else {
      _stprintf_s(date_str, len, L"%d seconds ago", date_time);
    }
  }
  else if (date_time < 3600) {
    date_time /= 60;

    if (date_time == 1) {
      _stprintf_s(date_str, len, L"%d minute ago", date_time);
    }
    else {
      _stprintf_s(date_str, len, L"%d minutes ago", date_time);
    }
  }
  else if (date_time < 86400) {
    date_time /= 3600;

    if (date_time == 1) {
      _stprintf_s(date_str, len, L"%d hour ago", date_time);
    }
    else {
      _stprintf_s(date_str, len, L"%d hours ago", date_time);
    }
  }
  else if (date_time < 2592000) {
    date_time /= 86400;

    if (date_time == 1) {
      _stprintf_s(date_str, len, L"%d day ago", date_time);
    }
    else {
      _stprintf_s(date_str, len, L"%d days ago", date_time);
    }
  }
  else {
    _stprintf_s(date_str, len, L"More than a month ago");
  }
}
