/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_rename.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    リネームダイアログ
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "ssnprintf.h"
#include "resource.h"
#include "sub_funcs.h"
#include "icon.h"
#include "xpt.h"
#include "com_button.h"
#include "dlg_rename.h"

/* ----------------------------------------------------------------- */
void dlg_rename::SetFileInfo(dirent* entry)
{
  _tcscpy_s(m_dir,  MAX_PATH, entry->d_name);
  _tcscpy_s(m_name, MAX_PATH, entry->d_name);

  PathRemoveFileSpec(m_dir);

  m_entry = *entry;
}

/* ----------------------------------------------------------------- */
void dlg_rename::OnPaint()
{
  xPaintDC  dc(mhWnd);
  RECT      rc;
  IPT      *pt = xPt();
  dirent*   entry;
  size_t    len;

  GetClientRect(&rc);

  dc.SetTextColor(pt->color.title);
  dc.SetBkColor(pt->color.base_bar);
  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  dc.FillRect(&rc, pt->color.base_bar);

  dc.TextOut(rc.left + 28, rc.top + 4, L"Rename");
  IcoDrawIconFromIndex(dc, 4, rc.top + 4, 5, TRUE);

  rc.left  += DPIX(16);
  rc.right -= DPIX(16);
  rc.top   += DPIY(40);

  len = _tcslen(m_name) + 1;

  entry = (dirent *)alloca(sizeof(dirent) + len * sizeof(TCHAR));

  memcpy(entry, &m_entry, sizeof(dirent));

  _tcscpy_s(entry->d_name, len, m_name);

  DispFileStatus(dc, &rc, entry, pt->fonts[1].name, pt->fonts[1].size);
}

/* ----------------------------------------------------------------- */
dlg_rename::dlg_rename(HWND hParent) : xDialog(_T("IDD_RENAMEDLG"), hParent)
{
  mpFont   = new xFont;
  mpOK     = new com_button;
  mpCancel = new com_button;

  IcoSetPsMode(&mpOK->icon_no, &mpCancel->icon_no);
}

/* ----------------------------------------------------------------- */
dlg_rename::~dlg_rename()
{
  delete mpFont;
  delete mpOK;
  delete mpCancel;
}

/* ----------------------------------------------------------------- */
BOOL dlg_rename::OnInitDialog(HWND hwndFocus, LPARAM lParam)
{
  IPT *pt = xPt();

  /* 真ん中に移動 */

  CenterWindow();

  mpFont->Create(pt->fonts[1].name, pt->fonts[1].size);

  mEdit.Attach(GetDlgItem(IDC_EDIT1));

  mEdit.SetFocus();
  mEdit.SetText(PathFindFileName(m_name));
  mEdit.SetSel(0, (int)(PathFindExtension(m_name) - PathFindFileName(m_name)));
  mEdit.SetFont(*mpFont);

  Attach(*mpOK,     IDOK2);
  Attach(*mpCancel, IDCANCEL2);

  return 0;
}

/* ----------------------------------------------------------------- */
void dlg_rename::OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  TCHAR buf[MAX_PATH];

  switch (id)
    {
      case IDOK:
      case IDOK2:
        mEdit.GetText(buf, MAX_PATH);
        _tcscpy_s(rename, MAX_PATH, m_dir);
        PathAppend(rename, buf);
        EndDialog(IDOK);
        break;

      case IDCANCEL:
      case IDCANCEL2:
        EndDialog(IDCANCEL);
        break;

      default:
        break;
    }
}

/* ----------------------------------------------------------------- */
LRESULT dlg_rename::Message(UINT msg, WPARAM wParam, LPARAM lParam)
{
  LRESULT res = 0;

  switch(msg) {
  case WM_LBUTTONDOWN:
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, lParam);
    break;
  default:
    res = xDialog::Message(msg, wParam, lParam);
    break;
  }

  return res;
}
