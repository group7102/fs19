/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: icom_beta.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
 */

/* ----------------------------------------------------------------- *
 * Included files
 * ----------------------------------------------------------------- */

#include "_box.h"
#include "dirent.h"
#include "sub_funcs.h"
#include "icon_beta.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

#define ICONSIZE        16
#define ICON_DPI_SIZE   DPIX(ICONSIZE)

/**********************************************************************
 * Private types
 *********************************************************************/

typedef struct _ICONICON_T_ {
  //
  HIMAGELIST  handle;
  int         index;
}
ICONICON;

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

static HIMAGELIST _hImage = NULL;
static ICONICON   _temp_dir  = {NULL, -1};
static ICONICON   _temp_file = {NULL, -1};

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static HIMAGELIST GetImageList(void)
{
  if (_hImage == NULL)
    {
      _hImage = ImageList_LoadImage(::GetModuleHandle(NULL),
                                    L"IDB_IMAGELIST_A",
                                    ICONSIZE,
                                    ICONSIZE,
                                    RGB(0, 128, 0),
                                    IMAGE_BITMAP,
                                    LR_CREATEDIBSECTION);
    }

  return _hImage;
}

/* ----------------------------------------------------------------- */
void iico::initialization(void)
{
  if (_temp_dir.handle && _temp_file.handle)
    {
      return;
    }

  TCHAR       buf[ MAX_PATH ];
  TCHAR       tmp[ MAX_PATH ];
  SHFILEINFO  shfi;

  /* Get temporary icon of directory */

  GetTempPath(MAX_PATH, buf);

  ZeroMemory(&shfi, sizeof(shfi));

  _temp_dir.handle = (HIMAGELIST)::SHGetFileInfo(
    buf,
    FILE_ATTRIBUTE_ARCHIVE,
    &shfi,
    sizeof(SHFILEINFO),
    SHGFI_SMALLICON | SHGFI_SYSICONINDEX/* | SHGFI_USEFILEATTRIBUTES*/);

  _temp_dir.index = shfi.iIcon;

  /* Get temporary icon of file */

  GetTempFileName(buf, _T("fs8"), 0, tmp);

  _temp_file.handle = (HIMAGELIST)::SHGetFileInfo(
    tmp,
    FILE_ATTRIBUTE_ARCHIVE,
    &shfi,
    sizeof(SHFILEINFO),
    SHGFI_SMALLICON | SHGFI_SYSICONINDEX/* | SHGFI_USEFILEATTRIBUTES*/);

  _temp_file.index = shfi.iIcon;

  DeleteFile(tmp);
}

/* ----------------------------------------------------------------- */
void iico::destroy(void)
{
  if(_hImage)
    {
      ImageList_Destroy(_hImage);
      _hImage = NULL;
    }
}

/* ----------------------------------------------------------------- */
void GetTempDirectoryIconIndex(HANDLE *handle, DWORD *index)
{
  *index  = _temp_dir.index;
  *handle = _temp_dir.handle;
}

/* ----------------------------------------------------------------- */
void GetTempFileIconIndex(HANDLE *handle, DWORD *index)
{
  *index  = _temp_file.index;
  *handle = _temp_file.handle;
}

/* ----------------------------------------------------------------- */
iico::iico()
{
}

/* ----------------------------------------------------------------- */
iico::iico(HDC hDC)
{
  attach(hDC);
}

/* ----------------------------------------------------------------- */
iico::~iico()
{
}

/* ----------------------------------------------------------------- */
void iico::draw(int x, int y, int idx, BOOL isActive)
{
  HIMAGELIST  hImange = ::GetImageList();
  int         cx;
  int         cy;

  ImageList_GetIconSize(hImange, &cx, &cy);

  if (::ImageList_GetImageCount(hImange) > idx)
    {
      ::ImageList_DrawEx(hImange,
                         idx,
                         mhDC,
                         x,
                         y,
                         cx,
                         cy,
                         rgbBk,
                         rgbFg,
                         isActive ? ILD_NORMAL : ILD_BLEND50);
    }
}

/* ----------------------------------------------------------------- */
void iico::attach(HDC hDC)
{
  mhDC   = hDC;
  rgbBk  = CLR_NONE;
  rgbFg  = CLR_NONE;
  fStyle = ILD_NORMAL;
}

/* ----------------------------------------------------------------- */
iico::operator void* () const
{
  return GetImageList();
}

/* ----------------------------------------------------------------- */
void iico::draw(int x, int y, dirent *entry, BOOL isActive, BOOL isRemote)
{
  HIMAGELIST  image;
  DWORD       index, index2;
  DWORD       sub_index;
  xTmpDC      dc(mhDC);
  UINT        style = ILD_BLEND50;
  iico        ico(dc);
  DWORD       flag;

  flag = FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_READONLY;

  if (!isActive)
    {
    }
  else if (entry->dwFileAttributes & flag)
    {
    }
  else
    {
      style = ILD_NORMAL;
    }

  if (_tcscmp(PathFindFileName(entry->d_name), _T("..")) == 0)
    {
      ico.draw(x, y, 85, isActive);
      return;
    }

  image     = (HIMAGELIST)entry->hHandle;
  index     =  entry->userData & 0x0000FFFF;
  index2    = (entry->userData & 0x00FF0000) >> 16;
  sub_index = (entry->userData & 0x0F000000) >> 24;

  dc.ImageList_DrawEx(image,
                      index,
                      x, y,
                      ICON_DPI_SIZE, ICON_DPI_SIZE,
                      INDEXTOOVERLAYMASK(sub_index) | style);

  /* チェックマーク表示 */

  if (isRemote)
    {
      ico.draw(x - 3, y, 45, isActive);
    }

  int mark_idx = 38;

  if (entry->bookMark)
    {
      ico.draw(x - 3, y, 44, isActive);
    }
  else if (entry->dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)
    {
      ico.draw(x - 4, y - 1, mark_idx + 2, isActive);
    }
  else if (entry->dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)
    {
      ico.draw(x - 4, y - 1, mark_idx + 1, isActive);
    }
  else if (entry->dwFileAttributes & FILE_ATTRIBUTE_READONLY)
    {
      ico.draw(x - 4, y - 1, mark_idx + 0, isActive);
    }

  /* git マーク表示 */

  mark_idx = 46;

  switch (index2)
    {
      case 1:
        ico.draw(x, y, mark_idx + 0, isActive);
        break;
      case 2:
        ico.draw(x, y, mark_idx + 1, isActive);
        break;
      case 3:
        ico.draw(x, y, mark_idx + 2, isActive);
        break;
      case 4:
        ico.draw(x, y, mark_idx + 3, isActive);
        break;
      case 5:
        ico.draw(x, y, mark_idx + 4, isActive);
        break;
      case 6:
        ico.draw(x, y, mark_idx + 0, isActive);
        break;
      default:
        break;
    }
}

/* ----------------------------------------------------------------- */
HANDLE iico::get_instance(dirent *entry, DWORD *d_no, BOOL isRemotea)
{
  return isRemotea ? get_remote_instance(entry, d_no) : get_instance(entry, d_no);
}

/* ----------------------------------------------------------------- */
HANDLE iico::get_instance(dirent *entry, DWORD *d_no)
{
  SHFILEINFO  shfi;
  HANDLE      handle;
  UINT        flags = SHGFI_SMALLICON | SHGFI_SYSICONINDEX | SHGFI_OVERLAYINDEX | SHGFI_ICON;

  handle = (HIMAGELIST)::SHGetFileInfo(entry->d_name,
                                       FILE_ATTRIBUTE_ARCHIVE,
                                       &shfi,
                                       sizeof(SHFILEINFO),
                                       flags);
  if (handle)
    {
      *d_no = shfi.iIcon;

      ::DestroyIcon(shfi.hIcon);
    }
  else
    {
      *d_no = 0;

      handle = _hImage;
    }

  return handle;
}

/* ----------------------------------------------------------------- */
HANDLE iico::get_remote_instance(dirent *entry, DWORD *d_no)
{
  HANDLE  handle;
  DWORD   attr = 0;

  if (entry->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
    {
      *d_no  = _temp_dir.index;
      handle = _temp_dir.handle;
    }
  else
    {
      if (GetShortcutInfo(entry->d_name, NULL, &attr))
        {
          if (attr & FILE_ATTRIBUTE_DIRECTORY)
            {
              *d_no  = _temp_dir.index;
              handle = _temp_dir.handle;
            }
          else
            {
              *d_no  = _temp_file.index;
              handle = _temp_file.handle;
            }
        }
      else
        {
          *d_no  = _temp_file.index;
          handle = _temp_file.handle;
        }
    }

  return handle;
}
