/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: logf.cpp
 *  Created  : 08/06/04 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _LOGF_H_
#define _LOGF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <tchar.h>

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define LOGE(fmt, ...) __logf(L"E: %s(%d) " fmt "\n", \
                              NULL, \
                              logf_strstr_w(_T(__FILE__)), \
                              __LINE__, \
                              ##__VA_ARGS__)

#define LOGI(fmt, ...) __logf(L"I: %s(%d) " fmt "\n", \
                              NULL, \
                              logf_strstr_w(_T(__FILE__)), \
                              __LINE__, \
                              ##__VA_ARGS__)

#define LOGW(fmt, ...) __logf(L"W: %s(%d) " fmt "\n", \
                              NULL, \
                              logf_strstr_w(_T(__FILE__)), \
                              __LINE__, \
                              ##__VA_ARGS__)

#define LOGA(fmt, ...) __logf(NULL, \
                              "%s(%d) " fmt "\n", \
                              logf_strstr_a(__FILE__), \
                              __LINE__, \
                              ##__VA_ARGS__)

/* ----------------------------------------------------------------- *
 * Functions
 * ----------------------------------------------------------------- */

int __logf(const TCHAR *wfmt, const char *fmt, ...);

const TCHAR *logf_strstr_w(const TCHAR *str);
const char  *logf_strstr_a(const char *str);

#ifdef __cplusplus
}
#endif

#endif /* _LOGF_H_ */
