/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: main_app.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "savedir.h"
#include "resource.h"
#include "_simple_que.h"
#include "icon.h"
#include "xpt.h"
#include "logf.h"
#include "copy_funcs.h"
#include "sub_app.h"
#include "ContextMenu.h"
#include "dlg_setting.h"
#include "view_base.h"
#include "namelist.h"
#include "view_seach.h"
#include "view_shortcut.h"
#include "view_directory.h"
#include "sub_funcs.h"
#include "dlg_mkdir.h"
#include "dlg_rename.h"
#include "dlg_find.h"
#include "com_list.h"
#include "dlg_ejectdsk.h"
#include "dlg_logdsk.h"
#include "dlg_attribute.h"
#include "dlg_history.h"
#include "view_tree.h"
#include "timerid.h"
#include "xgit.h"
#include "cmd_exec.h"
#include "sub_functions.h"
#include "command.h"
#include "main_app.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

#define FIND_DATA         WIN32_FIND_DATA
#define DWM_BB_ENABLE     0x00000001
#define DWM_BB_BLURREGION 0x00000002

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

typedef struct _DWM_BLURBEHIND
{
  DWORD dwFlags;
  BOOL  fEnable;
  HRGN  hRgnBlur;
  BOOL  fTransitionOnMaximized;
}
DWM_BLURBEHIND, *PDWM_BLURBEHIND;

typedef HRESULT (WINAPI *DwmExFrameClientArea)(HWND hwnd, const MARGINS *pMarInset);
typedef HRESULT (WINAPI *DwmBlurBehindWindow)(HWND hwnd, const DWM_BLURBEHIND *pBlurBehind);

typedef struct _GIT_PARAM
{
  D_NODE     *d_node;
  HWND        h_wnd;
  ULONGLONG   time;
  view_seach *view;
}
GIT_PARAM;

/* Key function */

typedef struct {
  //
  int   vk;
  int   ctl;
  int   sft;
  int   alt;
  int   f_no;
}
KEYS_T;

enum PT_FUNCS_T {
  PT_FUNC_NON     = 0,
  PT_FUNC_EDITOR,
  PT_FUNC_LOGDISK,
  PT_FUNC_REFLESH,
  PT_FUNC_EXEC,
  PT_FUNC_BACK,
  PT_FUNC_STEP,
  PT_FUNC_EJECT,
  PT_FUNC_COPY,
  PT_FUNC_PAST,
  PT_FUNC_RENAME,
  PT_FUNC_OPTION,
  PT_FUNC_SELEXE,
  PT_FUNC_DELETE,
  PT_FUNC_INSERT,
  PT_FUNC_APPS,
  PT_FUNC_PROPERTIES,
  PT_FUNC_ATTRIBUTE,
  PT_FUNC_SHORTCUT1,
  PT_FUNC_SHORTCUT2,
  PT_FUNC_SHORTCUT3,
  PT_FUNC_SHORTCUT4,
  PT_FUNC_SHORTCUT5,
  PT_FUNC_SHORTCUT6,
  PT_FUNC_SHORTCUT7,
  PT_FUNC_SHORTCUT8,
  PT_FUNC_SHORTCUT9,
  PT_FUNC_FIND,
  PT_FUNC_SORT,
  PT_FUNC_COLUMN,
  PT_FUNC_MAX,
};

/* ----------------------------------------------------------------- *
 * Private data
 * ----------------------------------------------------------------- */

/* Key function */

KEYS_T keys[] = {
  {VK_RETURN, 0, 1, 0, PT_FUNC_EDITOR},
  {'L',       0, 0, 0, PT_FUNC_LOGDISK},
  {221,       0, 0, 0, PT_FUNC_LOGDISK},
  {VK_F5,     0, 0, 0, PT_FUNC_REFLESH},
  {'Z',       0, 0, 0, PT_FUNC_EXEC},
  {VK_RETURN, 1, 0, 0, PT_FUNC_BACK},
  {VK_DELETE, 1, 0, 0, PT_FUNC_STEP},
  {'E',       0, 0, 0, PT_FUNC_EJECT},
  {VK_INSERT, 1, 0, 0, PT_FUNC_COPY},
  {VK_INSERT, 0, 1, 0, PT_FUNC_PAST},
  {VK_F2,     0, 0, 0, PT_FUNC_RENAME},
  {'X',       0, 0, 0, PT_FUNC_SELEXE},
  {VK_INSERT, 0, 0, 0, PT_FUNC_INSERT},
  {VK_DELETE, 0, 0, 0, PT_FUNC_DELETE},
  {VK_APPS,   0, 0, 0, PT_FUNC_APPS},
  {'A',       0, 0, 0, PT_FUNC_ATTRIBUTE},
  {VK_F1,     0, 0, 0, PT_FUNC_SHORTCUT1},
  {VK_F11,    0, 0, 0, PT_FUNC_SHORTCUT2},
  {'Q',       0, 0, 0, PT_FUNC_SHORTCUT3},
  {'G',       0, 0, 0, PT_FUNC_SHORTCUT4},
  {'F',       0, 0, 0, PT_FUNC_FIND},
  {'1',       0, 0, 0, PT_FUNC_SORT},
  {'2',       0, 0, 0, PT_FUNC_SORT},
  {'3',       0, 0, 0, PT_FUNC_SORT},
  {'4',       0, 0, 0, PT_FUNC_SORT},
  {'1',       0, 1, 0, PT_FUNC_COLUMN},
  {'2',       0, 1, 0, PT_FUNC_COLUMN},
  {'3',       0, 1, 0, PT_FUNC_COLUMN},
  {'4',       0, 1, 0, PT_FUNC_COLUMN},
  {'D',       0, 0, 0, PT_FUNC_SHORTCUT5},
  {'D',       0, 1, 0, PT_FUNC_SHORTCUT5},
  {'D',       1, 0, 0, PT_FUNC_SHORTCUT5},
  {'M',       0, 0, 0, PT_FUNC_SHORTCUT6},
  {'L',       0, 0, 0, PT_FUNC_SHORTCUT7},
  {226,       0, 0, 0, PT_FUNC_APPS},
  {VK_F12,    0, 0, 0, PT_FUNC_SHORTCUT8},
  {VK_UP,     1, 0, 0, PT_FUNC_SHORTCUT9},
  {VK_DOWN,   1, 0, 0, PT_FUNC_SHORTCUT9},
  {0,         0, 0, 0, PT_FUNC_NON},
};

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static int GetFuncNo(int vk, int ctrl, int shift, int alt)
{
  int   i;
  int   f_no = 0;

  ctrl  = (ctrl > 0);
  shift = (shift > 0);
  alt   = (alt > 0);

  for (i = 0; keys[i].vk; i++)
    {
      if (keys[i].vk  == vk
       && keys[i].ctl == ctrl
       && keys[i].sft == shift
       && keys[i].alt == alt)
        {
          f_no = keys[i].f_no;
        }
    }

  return f_no;
}

/* ----------------------------------------------------------------- */
static void DwmExtendFrameIntoClientArea(HWND hwnd, const MARGINS *pMarInset)
{
  HMODULE dll = LoadLibrary(TEXT("dwmapi.dll"));

  if (!dll)
    {
      return;
    }

  DwmExFrameClientArea dwmEfica;

  dwmEfica = reinterpret_cast <DwmExFrameClientArea> (GetProcAddress(dll,
                                                      "DwmExtendFrameIntoClientArea"));
  dwmEfica(hwnd, pMarInset);

  FreeLibrary(dll);
}

/* ----------------------------------------------------------------- */
static void DwmEnableBlurBehindWindow(HWND hwnd, const DWM_BLURBEHIND *pBlurBehind)
{
  HMODULE dll = LoadLibrary(TEXT("dwmapi.dll"));

  if (!dll)
    {
      return;
    }

  DwmBlurBehindWindow dwmEfica;

  dwmEfica = reinterpret_cast <DwmBlurBehindWindow> (GetProcAddress(dll,
                                                     "DwmEnableBlurBehindWindow"));

  dwmEfica(hwnd, pBlurBehind);

  FreeLibrary(dll);
}

/* ----------------------------------------------------------------- *
 * Public function prototypes
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
main_app::main_app()
{
  mpFuncs = (PT_FUNC_TYPE *)::HeapAlloc(GetProcessHeap(),
                                        HEAP_NO_SERIALIZE,
                                        sizeof(PT_FUNC_TYPE *) * (PT_FUNC_MAX) * 2);

  mpFuncs[PT_FUNC_NON]         = &main_app::Dummy;
  mpFuncs[PT_FUNC_EDITOR]      = &main_app::FuncEditor;
  mpFuncs[PT_FUNC_LOGDISK]     = &main_app::FuncLogDsk;
  mpFuncs[PT_FUNC_REFLESH]     = &main_app::FuncRefresh;
  mpFuncs[PT_FUNC_EXEC]        = &main_app::FuncExec;
  mpFuncs[PT_FUNC_BACK]        = &main_app::FuncBack;
  mpFuncs[PT_FUNC_STEP]        = &main_app::FuncStep;
  mpFuncs[PT_FUNC_EJECT]       = &main_app::FuncEject;
  mpFuncs[PT_FUNC_COPY]        = &main_app::FuncCopy;
  mpFuncs[PT_FUNC_PAST]        = &main_app::FuncPast;
  mpFuncs[PT_FUNC_RENAME]      = &main_app::FuncRename;
  mpFuncs[PT_FUNC_OPTION]      = &main_app::FuncOption;
  mpFuncs[PT_FUNC_SELEXE]      = &main_app::FuncSelExe;
  mpFuncs[PT_FUNC_DELETE]      = &main_app::FuncDelete;
  mpFuncs[PT_FUNC_INSERT]      = &main_app::FuncInsert;
  mpFuncs[PT_FUNC_APPS]        = &main_app::FuncApps;
  mpFuncs[PT_FUNC_PROPERTIES]  = &main_app::FuncProperties;
  mpFuncs[PT_FUNC_ATTRIBUTE]   = &main_app::FuncAttribute;
  mpFuncs[PT_FUNC_SHORTCUT1]   = &main_app::FuncShortcut1;
  mpFuncs[PT_FUNC_SHORTCUT2]   = &main_app::FuncShortcut2;
  mpFuncs[PT_FUNC_SHORTCUT3]   = &main_app::FuncShortcut3;
  mpFuncs[PT_FUNC_SHORTCUT4]   = &main_app::FuncShortcut4;
  mpFuncs[PT_FUNC_SHORTCUT5]   = &main_app::FuncShortcut5;
  mpFuncs[PT_FUNC_SHORTCUT6]   = &main_app::FuncShortcut6;
  mpFuncs[PT_FUNC_SHORTCUT7]   = &main_app::FuncShortcut7;
  mpFuncs[PT_FUNC_SHORTCUT8]   = &main_app::FuncShortcut8;
  mpFuncs[PT_FUNC_SHORTCUT9]   = &main_app::FuncShortcut9;
  mpFuncs[PT_FUNC_FIND]        = &main_app::FuncFind;
  mpFuncs[PT_FUNC_SORT]        = &main_app::FuncSort;
  mpFuncs[PT_FUNC_COLUMN]      = &main_app::FuncColumn;

  m_desktopfolder[0] = '\0';

  /* Do not issue OS messages */

  mOsMessageMode = SetErrorMode(SEM_NOOPENFILEERRORBOX | SEM_FAILCRITICALERRORS);

  OleInitialize(NULL);

  mhNextClipWnd = NULL;

  mStyle = WS_POPUP
//       | WS_BORDER
         | WS_SIZEBOX
//       | WS_SYSMENU
//       | WS_CAPTION
//       | WS_SYSMENU
//       | WS_THICKFRAME
//       | WS_MINIMIZEBOX
//       | WS_MAXIMIZEBOX
         | WS_VISIBLE
         | 0;

//mExStyle = WS_EX_OVERLAPPEDWINDOW;
}

/* ----------------------------------------------------------------- */
main_app::~main_app()
{
  SetErrorMode(mOsMessageMode);

  ::HeapFree(GetProcessHeap(), HEAP_NO_SERIALIZE, mpFuncs);

  OleUninitialize();
}

/* ----------------------------------------------------------------- */
void main_app::OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  POINT     pos;

  GetCursorPos(&pos);

  if (id == ISHORTCUT_BASE_ID)
    {
      switch (codeNotify)
        {
          case ISHORTCUTID_SETTING:
            FuncOption();
            break;
          case ISHORTCUTID_BACK:
            Back();
            break;
          case ISHORTCUTID_STEP:
            Step();
            break;
          case ISHORTCUTID_COPY:
            FuncCopy();
            break;
          case ISHORTCUTID_PAST:
            FuncPast();
            break;
          case ISHORTCUTID_RENAME:
            FuncRename();
            break;
          case ISHORTCUTID_DELETE:
            FuncDelete();
            break;
          case ISHORTCUTID_PROPERTIES:
            FuncProperties();
            break;
          case ISHORTCUTID_REFRESH:
            FuncRefresh();
            break;
          case ISHORTCUTID_R_FOLDER:
            DoContextMenu(pos.x, pos.y, GetSelected());
            break;
          case ISHORTCUTID_EJECT:
            FuncEject();
            break;
          case ISHORTCUTID_MIN:
            ShowWindow(SW_SHOWMINIMIZED);
            break;
          case ISHORTCUTID_MAX:
          case ISHORTCUTID_RESTORE:
            FuncShortcut8();
            break;
          case ISHORTCUTID_EXIT:
            Destroy();
            break;
          case ISHORTCUTID_DESKTOP:
            Desktop();
            break;
          case ISHORTCUTID_ATTRIBUTE:
            FuncAttribute();
            break;
        }
    }
  else if (id == IDIRECTORY_BASE_ID && codeNotify == IDIRECTORYID_R_FOLDER)
    {
      DoContextMenu(pos.x, pos.y, GetSelected());
    }
  else if (id == ISEACHVIEW_BASE_ID)
    {
      if (codeNotify != 0xFFFF)
        {
          DoContextMenu(pos.x, pos.y);
        }
      else
        {
          DoContextMenu(pos.x, pos.y, GetSelected());
        }
    }
  else if (id == ICOPY_BASE_ID)
    {
      LetsCopy((TCOPYACTION *)hwndCtl, codeNotify);
    }
}

/* ----------------------------------------------------------------- */
#ifdef _DEBUG
int _tmain()
#else
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, PSTR szCmdLine, int iCmdShow)
#endif
{
#if 0
  HANDLE hEnum;
  DWORD  res;
  DWORD  usize = 16384;
  DWORD  entries = -1;

  res = WNetOpenEnum(RESOURCE_CONTEXT, RESOURCETYPE_ANY, RESOURCEUSAGE_ALL, NULL, &hEnum);

  if (res != NO_ERROR)
    {
      LOGE("WNetOpenEnum:%X", res);
    }

  LPNETRESOURCE netr = (LPNETRESOURCE)malloc(usize);

  if (netr)
    {
      for (;;)
        {
          memset(netr, 0, usize);

          res = WNetEnumResource(hEnum,
                                 &entries,
                                 netr,
                                 &usize);
          if (res == NO_ERROR)
            {
              for (int i = 0; i < entries; i++)
                {
                  LOGI("===");
                  LOGI("%s", netr[i].lpLocalName);
                  LOGI("%s", netr[i].lpRemoteName);
                  LOGI("%s", netr[i].lpComment);
                  LOGI("%s", netr[i].lpProvider);
                }
            }
          else if (res == ERROR_NO_MORE_ITEMS)
            {
              break;
            }
          else
            {
              LOGE("WNetEnumResource:%X", res);
              break;
            }
        }

      free(netr);
    }

  res = WNetCloseEnum(hEnum);

  if (res != NO_ERROR)
    {
      LOGE("WNetCloseEnum:%X", res);
    }

  {
    DWORD  Length       = 1024;
    LPWSTR lpRemoteName = (LPWSTR)malloc(Length);

    if (lpRemoteName)
      {
        res = WNetGetConnection(L"\\\\wsl$\\",
                                lpRemoteName,
                                &Length);
        if (res == NO_ERROR)
          {
            LOGI("%s", lpRemoteName);
          }
        else
          {
            LOGE("ERROR:%X", res);
          }

        free(lpRemoteName);
      }
  }

  {
    DWORD  Length       = 1024;
    LPWSTR lpRemoteName = (LPWSTR)malloc(Length);

    if (lpRemoteName)
      {
        res = WNetGetUniversalName(L"Z:\\", REMOTE_NAME_INFO_LEVEL, lpRemoteName, &Length);

        if (res == NO_ERROR)
          {
            LOGI("%s:%d", lpRemoteName, Length);
          }
        else
          {
            LOGE("ERROR:%X", res);
          }

        free(lpRemoteName);
      }
  }
#endif

  main_app *app;
  IPT      *pt = xPtInit();

  _tsetlocale(LC_ALL, L"");  /* Defines the locale */

  IcoInit();

  LOGI("Wakeup");

  app = new main_app;

#ifdef _DEBUG
  app->Initialize();
#else
  app->Initialize(hInstance);
#endif
  app->Create();

  app->SetWindowPlacement(&pt->place);

  if (pt->place.showCmd == SW_SHOWMAXIMIZED) {
    app->mTitleHeight = TITLE_HEIGHT_MAXIMIZED;
  }
  else {
    app->mTitleHeight = TITLE_HEIGHT_NORMAL;
  }

  app->ShowWindow();

  app->Run();

  delete app;

  savedir_delete();

  IcoFin();

  xPtFin();

  LOGI("Termination");

  return 0;
}

/* ----------------------------------------------------------------- */
BOOL main_app::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  IPT *pt = xPt();

  mp_seach_view1->SetColumn(pt->setting.column);
  mp_seach_view2->SetColumn(pt->setting.column);

  SetFonts(pt->fonts[0].name, pt->fonts[0].size,
           pt->fonts[1].name, pt->fonts[1].size,
           pt->fonts[2].name, pt->fonts[2].size,
           pt->fonts[3].name, pt->fonts[3].size);

  mhNextClipWnd = SetClipboardViewer(mhWnd);

#if 0
  MARGINS   margins;

  margins.cyTopHeight    = TITLE_HEIGHT;
  margins.cyBottomHeight = 0;
  margins.cxLeftWidth    = 0;
  margins.cxRightWidth   = 0;

  DwmExtendFrameIntoClientArea(mhWnd, &margins);
#endif

  return TRUE;
}

/* ----------------------------------------------------------------- */
void main_app::OnShowWindow(BOOL fShow, UINT status)
{
}

/* ----------------------------------------------------------------- */
void main_app::DoContextMenu(int x, int y)
{
  POINT   pos;
  TCHAR  *dir;
  dirent *entry;
  dirent *top;
  int     cnt;
  int     i;
  TCHAR **names;
  TCHAR  *cp;

  pos.x = x;
  pos.y = y;

  dir = GetCurrentDirectory();

  cnt = GetMarkedNum(&top);

  names = (TCHAR **)HeapAlloc(GetProcessHeap(),
                              HEAP_NO_SERIALIZE,
                              sizeof(TCHAR *) * (cnt + 1));

  for (entry = top, i = 0; i < cnt; i++)
    {
      cp = entry->d_name + _tcslen(dir);

      while (*cp == '\\')
        {
          cp++;
        }

      names[i] = cp;
      entry = entry->next;
    }

  if (cnt == 1)
    {
      if (_tcscmp(names[0], _T("..")) == 0)
        {
          ContextMenu(mhWnd, pos.x, pos.y, dir, _T(""));
        }
      else
        {
          ContextMenu(mhWnd, pos.x, pos.y, dir, names[0]);
        }
    }
  else
    {
      ContextMenuWithCount(mhWnd, pos.x, pos.y, dir, names, cnt);
    }

  ::HeapFree(GetProcessHeap(), HEAP_NO_SERIALIZE, names);

}

/* ----------------------------------------------------------------- */
void main_app::DoContextMenu(int x, int y, const TCHAR *target)
{
  ContextMenu(mhWnd, x, y, (TCHAR *)target, _T(""));
}

/* ----------------------------------------------------------------- */
void main_app::OnInitMenuPopup(HMENU hMenu, UINT item, BOOL fSystemMenu)
{
  ContextMenuMsg(WM_INITMENUPOPUP,
                 (WPARAM)(HMENU)(hMenu),
                 MAKELPARAM((item), (fSystemMenu)));
}

/* ----------------------------------------------------------------- */
void main_app::OnDrawItem(const DRAWITEMSTRUCT *lpDrawItem)
{
  if (lpDrawItem->CtlType == ODT_LISTBOX)
    {
      HMENU hMenu = 0;

      if (lpDrawItem->CtlID != (UINT)102)
        {
          return;
        }

      xControls *ctrl = (xControls *)::GetWindowLongPtr(lpDrawItem->hwndItem,
                                                        GWLP_USERDATA);
      if (ctrl)
        {
          ctrl->DrawItem(lpDrawItem);
        }

      return;
    }

  ContextMenuMsg(WM_DRAWITEM,
                 (WPARAM)(((const DRAWITEMSTRUCT *)lpDrawItem)->CtlID),
                 (LPARAM)(const DRAWITEMSTRUCT *)(lpDrawItem));

  sub_app::OnDrawItem(lpDrawItem);
}

/* ----------------------------------------------------------------- */
void main_app::OnMeasureItem(MEASUREITEMSTRUCT *lpMeasureItem)
{
  if (lpMeasureItem->CtlType == ODT_LISTBOX)
    {
      xDC  dc(mhWnd);
      IPT *pt = xPt();

      dc.SetSelectFont(pt->fonts[0].name, pt->fonts[0].size);

      lpMeasureItem->itemHeight = dc.tm.tmHeight + 6;

      return;
    }

  ContextMenuMsg(WM_MEASUREITEM,
                 (WPARAM)(((MEASUREITEMSTRUCT *)lpMeasureItem)->CtlID),
                 (LPARAM)(MEASUREITEMSTRUCT *)(lpMeasureItem));
}

/* ----------------------------------------------------------------- */
void main_app::OnDestroy(void)
{
  IPT *pt = xPt();

  pt->setting.tool_size = m_tool_size;

  GetCurrentDirectorys(pt->setting.path1, pt->setting.path2, MAX_PATH);

  pt->setting.page_no = GetCurrentPageNo();

  KillTimer(mhWnd, ISCROLL_TIMERID);
  KillTimer(mhWnd, ISUBAPP_TIMEID);
  KillTimer(mhWnd, ISUBAPP_SSSS);
  KillTimer(mhWnd, IINFO_LEAVE_TIMERID);
  KillTimer(mhWnd, IINFO_WAIT_TIMERID);
  KillTimer(mhWnd, ISEACH_TIMERID);

  if (mhNextClipWnd)
    {
      ChangeClipboardChain(mhWnd, mhNextClipWnd);
    }

  /* Save window size */

  GetWindowPlacement(&pt->place);

  _boxApp::OnDestroy();
}

/* ----------------------------------------------------------------- */
int main_app::FuncEditor(TCHAR *param)
{
  IPT    *pt = xPt();
  TCHAR  *buf;
  dirent *entry = GetCurrentEntry();
  size_t  len;

  len = _tcslen(entry->d_name) + _tcslen(pt->setting.editor) + 16;

  buf = (TCHAR *)alloca(len * sizeof(TCHAR));

  if (_tcscmp(PathFindFileName(entry->d_name), _T("..")) != 0
   && !(entry->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
   && !PathIsRoot(entry->d_name))
    {
      _sntprintf_s(buf, len, _TRUNCATE, L"%s \"%s\"", pt->setting.editor, entry->d_name);
      ExecCommand(buf);
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncLogDsk(TCHAR *param)
{
  dlg_logdsk  dlg(mhWnd);

  dlg.SetCurrent(GetCurrentDirectory());

  if (dlg.Modal() ==IDOK)
    {
      SetDirectory(dlg.logdrv, NULL, 1);
      Invalidate();
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncRefresh(TCHAR *param)
{
  Refresh();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncExec(TCHAR *param)
{
  dirent*   entry;
  view_base base;

  entry = GetCurrentEntry();

  if (entry && !PathIsRoot(entry->d_name))
    {
      base.ShellExecute(entry->d_name);
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncBack(TCHAR *param)
{
  Back();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncStep(TCHAR *param)
{
  Step();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncEject(TCHAR *param)
{
  dlg_ejectdsk  dlg(mhWnd);

  if (dlg.CheckEjectDrive())
    {
      StopNotification();

      dlg.Modal();

      SetDirectorys();
      Invalidate();

      StartNotification();
    }
  else
    {
      MessageBox(_T("There is no drive that can be ejected!"),
                 _T("eject..."),
                 MB_ICONINFORMATION);
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncCopy(TCHAR *param)
{
  dirent *entry;
  HDROP   hDrop;
  int     num;

  num = GetMarkedNum(&entry);

  if (num == 0) {
    LOGW("GetMarkedNum");
    return -1;
  }

  LOGI("FuncCopyNum:%d", num);

  hDrop = (HDROP)MakeHDropFiles2nd(entry);

  if (hDrop == NULL) {
    LOGE("MakeHDropFiles2nd");
    return -1;
  }

  if (OpenClipboard(mhWnd) == 0) {
    GlobalFree(hDrop);
  }
  else {
    EmptyClipboard();
    SetClipboardData(CF_HDROP, hDrop);
    CloseClipboard();
  }

  LOGI("FuncCopyEnd");

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncPast(TCHAR *param)
{
  if (m_lock == FALSE)
    {
      if (CheckClips(mhWnd) > 0)
        {
          m_lock = TRUE;
          StartCopyInfo();
          m_lock = FALSE;
        }
    }
  else
    {
      MessageBox(_T("Copying! Can't run"), _T("Copy..."), MB_ICONERROR);
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncRename(TCHAR *param)
{
  dirent     *entry;
  dlg_rename  dlg(mhWnd);
  TCHAR       buf[MAX_PATH];
  TCHAR       title[] = L"rename...";
  TCHAR       msg01[] = L"Cannot be changed!\r";
  TCHAR       msg02[] = L"A file with the same name exists.";

  entry = GetCurrentEntry();

  if (entry == NULL)
    {
      goto errout;
    }

  if (_tcscmp(PathFindFileName(entry->d_name), _T("..")) == 0)
    {
      goto errout;
    }

  if (PathIsRoot(entry->d_name))
    {
      goto errout;
    }

  dlg.SetFileInfo(entry);

  if (dlg.Modal() != IDOK)
    {
      goto errout;
    }

  if (PathFileExists(dlg.rename))
    {
      _stprintf_s(buf, MAX_PATH, L"\"%s\"\r\r%s%s", dlg.rename, msg01, msg02);
      MessageBox(buf, title, MB_ICONSTOP);
      goto errout;
    }

  if (!::MoveFile(entry->d_name, dlg.rename))
    {
      _stprintf_s(buf, MAX_PATH, L"\"%s\"\r\r%s", dlg.rename, msg01);
      MessageBox(buf, title, MB_ICONSTOP);
      goto errout;
    }

  SetDirectory(GetCurrentDirectory(), dlg.rename);

  Invalidate();

errout:
  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncOption(TCHAR *param)
{
  dlg_setting dlg(mhWnd);
  IPT        *pt = xPt();

  for (int i = 0; i < 5; i++) {
    dlg.SetLogFont(i, pt->fonts[i].name, pt->fonts[i].size);
  }

  if (!dlg.Modal())
    {
      return 0;
    }

  for (int i = 0; i < 5; i++) {
    dlg.GetLogFont(i, pt->fonts[i].name, _countof(pt->fonts[i].name), &pt->fonts[i].size);
  }

  SetFonts(pt->fonts[0].name, pt->fonts[0].size,
           pt->fonts[1].name, pt->fonts[1].size,
           pt->fonts[2].name, pt->fonts[2].size,
           pt->fonts[3].name, pt->fonts[3].size);

  mp_seach_view1->SetColumn(pt->setting.column);
  mp_seach_view2->SetColumn(pt->setting.column);

  mp_dir_view1->my_namelist->SetSortMode(pt->setting.sort_order, pt->setting.sort_part);
  mp_dir_view2->my_namelist->SetSortMode(pt->setting.sort_order, pt->setting.sort_part);

  Sort();
  Invalidate();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncSelExe(TCHAR *param)
{
  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncDelete(TCHAR *param)
{
  int     count;
  dirent* entry;

  count = GetMarkedNum(&entry);

  if (count > 0)
    {
      ShDelete(mhWnd, entry, count);
      SetDirectorys();
      Invalidate();
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncInsert(TCHAR *param)
{
  dlg_mkdir dlg(mhWnd);
  IPT      *pt = xPt();

  dlg.SetFont(pt->fonts[1].name, pt->fonts[1].size);
  dlg.SetDirectory(GetCurrentDirectory());

  if (dlg.Modal() == IDOK)
    {
      SetDirectory(GetCurrentDirectory(), dlg.GetSel());
      Invalidate();
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncFind(TCHAR *param)
{
  dlg_find  dlg(mhWnd);

  if (dlg.Modal() == IDOK)
    {
      SelectSeachMode(dlg.m_target);
      Invalidate();
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncSort(TCHAR *param)
{
  int   order;
  int   kind;
  int   new_kind;
  IPT  *pt = xPt();

  mp_act_seach->GetSortMode(&order, &kind);

  switch (*param)
    {
      case '1':
        new_kind = INAM_KIND_EXT;
        break;
      case '2':
        new_kind = INAM_KIND_TIME;
        break;
      case '3':
        new_kind = INAM_KIND_SIZE;
        break;
      default:
        new_kind = INAM_KIND_NAME;
        break;
    }

  if (kind == new_kind)
    {
      order *= -1;
    }

  xPtSetSortInfo(pt, mp_act_seach->GetCurrentDirectory(), -1, new_kind, order);

  Sort();
  Invalidate();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncColumn(TCHAR *param)
{
  int  column;
  IPT *pt = xPt();

  switch (*param)
    {
      case '1':
        column = 1;
        break;
      case '2':
        column = 2;
        break;
      case '3':
        column = 3;
        break;
      default:
        column = 4;
        break;
    }

  xPtSetSortInfo(pt,
                 mp_act_seach->GetCurrentDirectory(),
                 column,
                 -1,
                 0);

  mp_act_seach->SetColumn(column);

  Invalidate();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut1(TCHAR *param)
{
  SetDirectory(GetDesktopFolder());
  Invalidate();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut2(TCHAR *param)
{
  IPT  *pt = xPt();
  TCHAR buf[MAX_PATH];
  TCHAR tmp[MAX_PATH];

  ::GetCurrentDirectory(MAX_PATH, tmp);
  ::SetCurrentDirectory(GetCurrentDirectory());

  wsprintf(buf,
           _T("%s -GREPMODE -GOPT=SP2K -GREPDLG -GFILE=\"*.c;*.cpp;*.h\" -GFOLDER=\"%s\""),
           pt->setting.editor,
           GetCurrentDirectory());

  ExecCommand(buf);

  ::SetCurrentDirectory(tmp);

  return 0;
}

/* ----------------------------------------------------------------- */
static int PumpMessage(HWND hWnd)
{
  MSG   msg;
  int   res = 0;

  while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
    {
      if (msg.message == WM_QUIT)
        {
          res = -1;
          break;
        }
      else if (msg.message == WM_KEYDOWN)
        {
          if (msg.wParam == VK_ESCAPE)
            {
              if (::MessageBox(hWnd,
                               _T("Under analysis, would you like to cancel?"),
                               _T("What are you doing?"),
                               MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
                {
                  res = -1;
                  break;
                }
            }
        }

      if (GetMessage(&msg, NULL, 0, 0) <= 0)
        {
          break;
        }

      ::TranslateMessage(&msg);
      ::DispatchMessage (&msg);
    }

  return res;
}

/* ----------------------------------------------------------------- */
static char *strtoutf8(char *target, size_t len, char *cp)
{
  char *outp = target;

  if (*cp != '\"')
    {
      return cp;
    }

  cp++;

  while (*cp != '\0')
    {
      if (*cp == '\"')
        {
          break;
        }

      if (*cp == '\\')
        {
          *outp++ = (unsigned char)strtol(cp + 1, &cp, 8);
        }
      else
        {
          *outp++ = *cp++;
        }
    }

  *outp = '\0';

  return target;
}

/* ----------------------------------------------------------------- */
void PumpCallback(void *p_param)
{
  main_app *app = (main_app *)p_param;

  app->mp_act_seach1->GScanSet(NULL);
  app->mp_act_seach2->GScanSet(NULL);

  app->PumpMessage();
}

/* ----------------------------------------------------------------- */
char *main_app::OpenProcessToMemory(TCHAR *param)
{
  LOGI("%s", param);

  return ::OpenProcessToMemory(param, 100, PumpCallback, this);
}

/* ----------------------------------------------------------------- */
int main_app::OpenProcessToFile(TCHAR *param, TCHAR *outp)
{
  LOGI("%s", param);

  return ::OpenProcessToFile(param, outp, MAX_PATH, 100, PumpCallback, this);
}

/* ----------------------------------------------------------------- */
int main_app::xgit_status(TCHAR *dir)
{
  char *buff = OpenProcessToMemory(L"wsl.exe git status -s");

  if (buff == NULL)
    {
      return 0;
    }

  TCHAR tmbuf[MAX_PATH];
  TCHAR fpath[MAX_PATH];
  char  cc   [MAX_PATH];
  char *last = NULL;
  char *cp;

  for (cp = strtok_s(buff, "\r\n", &last); cp; cp = strtok_s(NULL, "\r\n", &last)) {
    if (*cp == '?' && *(cp + 1)  == '?') {
      continue;
    }

    MultiByteToWideChar(CP_UTF8,
                        0U,
                        strtoutf8(cc, MAX_PATH, cp + 3),
                        -1,
                        tmbuf,
                        MAX_PATH);

    GetFullPathName(tmbuf, MAX_PATH, fpath, NULL);

    D_NODE *d_node = savedir(fpath);

    /* 0: ' ' = unmodified
     * 1:  M  = modified
     * 2:  A  = added
     * 3:  D  = deleted
     * 4:  R  = renamed
     * 5:  C  = copied
     * 6:  U  = updated but unmerged
     */

    switch (*cp) {
    case 'M':
      d_node->g_idx = 2;
      break;
    case 'A':
      d_node->g_idx = 3;
      break;
    case 'D':
      d_node->g_idx = 4;
      break;
    case 'R':
      d_node->g_idx = 5;
      break;
    case 'C':
      d_node->g_idx = 6;
      break;
    case 'U':
      d_node->g_idx = 7;
      break;
    default:
      d_node->g_idx = 0;
      break;
    }

    d_node->g_idx <<= 4;

    switch (*(cp + 1)) {
    case 'M':
      d_node->g_idx |= 2;
      break;
    case 'A':
      d_node->g_idx |= 3;
      break;
    case 'D':
      d_node->g_idx |= 4;
      break;
    case 'R':
      d_node->g_idx |= 5;
      break;
    case 'C':
      d_node->g_idx |= 6;
      break;
    case 'U':
      d_node->g_idx |= 7;
      break;
    default:
      break;
    }
  }

  free(buff);

  return 0;
}

/* ----------------------------------------------------------------- */
static int SavedirNodeCallback2nd(D_NODE *p_node, void *p_param)
{
  p_node->param = 0;

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::xgit_ls_files(TCHAR *dir)
{
  char *buff = OpenProcessToMemory(L"wsl.exe git ls-files");

  if (buff == NULL)
    {
      return -1;
    }

  /* Set git status to file */

  D_NODE *p_top = savedir(dir);

  savedir_search(p_top, SavedirNodeCallback2nd, NULL);

  TCHAR tmbuf[MAX_PATH];
  TCHAR fpath[MAX_PATH];
  char  cc   [MAX_PATH];
  char *last = NULL;
  char *cp;

  for (cp = strtok_s(buff, "\r\n", &last); cp; cp = strtok_s(NULL, "\r\n", &last))
    {
      MultiByteToWideChar(CP_UTF8,
                          0U,
                          strtoutf8(cc, MAX_PATH, cp),
                          -1,
                          tmbuf,
                          MAX_PATH);

      GetFullPathName(tmbuf, MAX_PATH, fpath, NULL);

      D_NODE *d_node = savedir(fpath);

      d_node->g_idx = 1;
    }

  /* Cleanup */

  free(buff);

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::xgit_log(TCHAR *path, char *hash1, char *hash2)
{
  hash1[0] = '\0';
  hash2[0] = '\0';

  TCHAR tmbuf[MAX_PATH];

  _stprintf_s(tmbuf,
              MAX_PATH,
              L"wsl.exe git log -n 2 --oneline --pretty=format:'%%H' -- %s",
              path);

  char *buff = OpenProcessToMemory(tmbuf);

  if (buff == NULL) {
    return -1;
  }

  char *last = NULL;
  char *cp;
  int   i = 0;
  char *hash[2] = {hash1, hash2};

  for (cp = strtok_s(buff, "\r\n", &last); cp; cp = strtok_s(NULL, "\r\n", &last)) {
    snprintf(hash[i++], 41, "%s", cp);
  }

  free(buff);

  return i;
}

/* ----------------------------------------------------------------- */
int main_app::xgit_log_to_file(TCHAR *path, TCHAR *out)
{
  TCHAR tmbuf[MAX_PATH];

#if 0
  _stprintf_s(tmbuf,
              MAX_PATH,
              _T("wsl.exe git log"
                 " --oneline"
                 " --date=format:'%%y/%%m/%%d %%H:%%M'"
                 " --pretty=format:'%%cd*%%s %%an <%%ae>' -- %s"),
              path);
#else
  _stprintf_s(tmbuf,
              MAX_PATH,
              _T("wsl.exe git log"
                 " --oneline"
                 " --date=format:'%%y/%%m/%%d %%H:%%M'"
                 " --pretty=format:'%%H*%%cd*(%%an)*<%%ae>*%%s' -- %s"),
              path);
#endif

  return OpenProcessToFile(tmbuf, out);
}

/* ----------------------------------------------------------------- */
int main_app::xgit_rev_parse(TCHAR *path, size_t len)
{
  TCHAR tmbuf[MAX_PATH];

  _stprintf_s(tmbuf,
              MAX_PATH,
              L"wsl.exe git rev-parse --show-prefix");

  char *buff = OpenProcessToMemory(tmbuf);

  if (buff == NULL)
    {
      return -1;
    }

  char *last = NULL;
  char *cp;

  if ((cp = strtok_s(buff, "\r\n", &last)) == NULL)
    {
      path[0] = L'\0';
    }
  else
    {
      MultiByteToWideChar(CP_UTF8,
                          0U,
                          cp,
                          -1,
                          path,
                          (int)len);
    }

  free(buff);

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::xgit_diff(TCHAR *path, char *hash, TCHAR *temp)
{
  TCHAR tmbuf[MAX_PATH];

  if (hash == NULL)
    {
      _stprintf_s(tmbuf,
                  MAX_PATH,
                  L"wsl.exe git show HEAD:%s",
                  path);
    }
  else
    {
      if (hash[0] == '\0') {
        return -1;
      }

      TCHAR buf[41];
      int   i;

      for (i = 0; i < _countof(buf); i++) {
        buf[i] = hash[i];
      }

      _stprintf_s(tmbuf,
                  MAX_PATH,
                  L"wsl.exe git show %s:%s",
                  buf,
                  path);

      LOGI("%s", tmbuf);
    }

  return OpenProcessToFile(tmbuf, temp);
}

/* ----------------------------------------------------------------- */
int main_app::xgit_get_rev_parse_path(dirent *entry, TCHAR *path, size_t len)
{
  /* get git prefix */

  TCHAR  prefix[MAX_PATH];

  if (xgit_rev_parse(prefix, MAX_PATH) < 0)
    {
      return -1;
    }

  return _stprintf_s(path, len, L"%s%s", prefix, PathFindFileName(entry->d_name));
}

/* ----------------------------------------------------------------- */
int main_app::xgit_diff_2nd(dirent *entry, TCHAR *hash1, TCHAR *hash2, TCHAR **temp)
{
  TCHAR path[MAX_PATH];

  if (xgit_get_rev_parse_path(entry, path, MAX_PATH) < 0) {
    return -1;
  }

  TCHAR  tmbuf[MAX_PATH];
  int    cnt;
  TCHAR *hash;

  for (cnt = 0; cnt < 2; cnt++) {
    if (cnt == 0) {
      hash = hash1;
    }
    else {
      hash = hash2;
    }

    if (hash[0] == L'\0') {
      break;
    }

    __snprintf(tmbuf,
               _countof(tmbuf),
               "wsl.exe git show %s:%s",
               hash,
               path);

    if (OpenProcessToFile(tmbuf, temp[cnt]) < 0) {
      break;
    }
  }

  return cnt;
}

/* ----------------------------------------------------------------- */
int main_app::xgit_diff_2nd(dirent *entry, uint64_t *hash, TCHAR **temp)
{
  TCHAR path[MAX_PATH];

  if (xgit_get_rev_parse_path(entry, path, MAX_PATH) < 0)
    {
      return -1;
    }

  TCHAR tmbuf[MAX_PATH];
  int   cnt;

  for (cnt = 0; cnt < 2; cnt++)
    {
      if (hash[cnt] == 0)
        {
          break;
        }

      _stprintf_s(tmbuf,
                  MAX_PATH,
                  L"wsl.exe git show %llx:%s",
                  hash[cnt],
                  path);

      if (OpenProcessToFile(tmbuf, temp[cnt]) < 0)
        {
          break;
        }
    }

  return cnt;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut3(TCHAR *param)
{
  const TCHAR *message = NULL;

  /* Move current directory */

  TCHAR  bkbuf[MAX_PATH];
  TCHAR *dir;

  dir = GetCurrentDirectory();

  ::GetCurrentDirectory(MAX_PATH, bkbuf);
  ::SetCurrentDirectory(dir);

  /* Change scan mode */

  mp_act_seach1->GScanMode(TRUE);
  mp_act_seach2->GScanMode(TRUE);

  if (xgit_ls_files(dir))
    {
      message = L"It's not managed by git.";
      goto errout;
    }

  /* Set git status to file */

  if (xgit_status(dir))
    {
      message = L"It's not managed by git.";
      goto errout;
    }

  /* It was successful */

errout:
  /* Return scanmode */

  mp_act_seach1->GScanMode(FALSE);
  mp_act_seach2->GScanMode(FALSE);

  if (message)
    {
      MessageBox(message, L"Compare files", MB_ICONSTOP);
    }

  /* Return current directory */

  ::SetCurrentDirectory(bkbuf);

  Refresh();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut5(TCHAR *param)
{
  const TCHAR *message = NULL;
  IPT         *pt = xPt();
  bool         is_option = (HIBYTE(GetKeyState(VK_CONTROL)) || HIBYTE(GetKeyState(VK_SHIFT)));

  /* Move current directory */

  TCHAR *dir = GetCurrentDirectory();
  TCHAR  bkbuf[MAX_PATH];

  ::GetCurrentDirectory(MAX_PATH, bkbuf);
  ::SetCurrentDirectory(dir);

  /* Change scan mode */

  mp_act_seach1->GScanMode(TRUE);
  mp_act_seach2->GScanMode(TRUE);

  /* get git prefix */

  TCHAR  prefix[MAX_PATH];

  if (xgit_rev_parse(prefix, MAX_PATH) < 0)
    {
      message = L"It's not managed by git.";
      LOGE("%s", message);
      goto errout;
    }

  /* Make git relative path */

  dirent *entry = GetCurrentEntry();

  TCHAR *fname = PathFindFileName(entry->d_name);

  if (_tcscmp(fname, _T("..")) == 0)
    {
      message = L"\"..\" cannot be specified!";
      LOGE("%s", message);
      goto errout;
    }

  TCHAR  path[MAX_PATH];

  _stprintf_s(path, MAX_PATH, L"%s%s", prefix, fname);

  /* Get git log hash */

  char hash1[41];
  char hash2[41];
  int  num;

  num = xgit_log(PathFindFileName(entry->d_name), hash1, hash2);

  if (num < 0)
    {
      message = L"It's not managed by git.";
      LOGE("%s", message);
      goto errout;
    }

  /* Creating temporary file name */

  TCHAR tempname[MAX_PATH];

  if (xgit_diff(path, is_option ? hash2 : NULL, tempname) < 0)
    {
      message = L"File to be compared could not be obtained!";
      LOGE("%s", message);
      goto errout;
    }

  /* Launch diff tool as child process */

  size_t len = 0;

  len += (__strnlen(pt->setting.difftool, MAX_PATH) + 1);
  len += (__strnlen(tempname, MAX_PATH) + 1);
  len += (__strnlen(fname, MAX_PATH) + 1);

  TCHAR *tmbuf = (TCHAR *)alloca(len * sizeof(TCHAR));

  __snprintf(tmbuf,
             len,
             L"%s %s %s",
             pt->setting.difftool,
             tempname,
             fname);

  if (ExecProcess(tmbuf, 80, PumpCallback, this) < 0)
    {
      message = L"Merge tool failed to start!";
      LOGE("%s", message);
    }

  /* Delete temporary file */

  DeleteFile(tempname);

errout:
  /* Return scanmode */

  mp_act_seach1->GScanMode(FALSE);
  mp_act_seach2->GScanMode(FALSE);

  if (message)
    {
      MessageBox(message, L"Compare files", MB_ICONSTOP);
    }

  /* Return current directory */

  ::SetCurrentDirectory(bkbuf);

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut6(TCHAR *param)
{
  mp_act_seach->MemoMode(TRUE);

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::LogListMode(TCHAR *fnam, dirent *entry)
{
  uint64_t hash[] = {0, 0};
  int      ret = 0;
  int      cnt;
  TCHAR    temp1[MAX_PATH] = {0};
  TCHAR    temp2[MAX_PATH] = {0};
  TCHAR   *temp[] = {temp1, temp2};
  IPT     *pt = xPt();
  TCHAR    hash1[41];
  TCHAR    hash2[41];

  for (;;)
    {
      hash[0] = 0;
      hash[1] = 0;

      hash1[0] = '\0';
      hash2[0] = '\0';

      /* Release the standby state of the screen opposite the current one */

      if (mp_act_seach == mp_act_seach1)
        {
          mp_act_seach2->GScanMode(FALSE);
        }
      else
        {
          mp_act_seach1->GScanMode(FALSE);
        }

      if ((ret = mp_act_seach->LogListMode(fnam, hash1, hash2)) <= 0)
        {
          break;
        }

      /* Release the standby state of the screen opposite the current one */

      if (mp_act_seach == mp_act_seach1)
        {
          mp_act_seach2->GScanMode(TRUE);
        }
      else
        {
          mp_act_seach1->GScanMode(TRUE);
        }

      /* Get the file to compare. */

      memset(temp1, 0, sizeof(temp1));
      memset(temp2, 0, sizeof(temp2));

//    cnt = xgit_diff_2nd(entry, hash, temp);
      cnt = xgit_diff_2nd(entry, hash1, hash2, temp);

      TCHAR  tmbuf[MAX_PATH];
      TCHAR *target1;
      TCHAR *target2;

      if (cnt == 2)
        {
          target1 = temp[0];
          target2 = temp[1];
        }
      else if (cnt == 1)
        {
          target1 = entry->d_name;
          target2 = temp[0];
        }
      else
        {
          break;
        }

      /* Launch the comparison tool */

      _stprintf_s(tmbuf, MAX_PATH, L"%s %s %s", pt->setting.difftool, target2, target1);

      int res = ExecProcess(tmbuf, 80, PumpCallback, this);

      /* Delete template files */

      if (temp[0] != L'\0')
        {
          DeleteFile(temp[0]);
        }

      if (temp[1] != L'\0')
        {
          DeleteFile(temp[1]);
        }

      if (res < 0)
        {
          LOGE("ExecProcess");
          break;
        }
    }

  return ret;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut7(TCHAR *param)
{
  TCHAR  bkbuf[MAX_PATH];
  TCHAR *dir;
  int    ret = 0;

  const TCHAR *message = NULL;

  /* Move current directory */

  dir = GetCurrentDirectory();

  ::GetCurrentDirectory(MAX_PATH, bkbuf);
  ::SetCurrentDirectory(dir);

  dirent *entry = GetCurrentEntry();

  TCHAR *fname = PathFindFileName(entry->d_name);

  if (_tcscmp(fname, _T("..")) == 0)
    {
      goto errout;
    }

  /* Change scan mode */

  mp_act_seach1->GScanMode(TRUE);
  mp_act_seach2->GScanMode(TRUE);

  /* Get git log */

  TCHAR fout[MAX_PATH];

  ret = xgit_log_to_file(fname, fout);

  if (ret < 0)
    {
      message = L"It's not managed by git.";
    }
  else
    {
      ret = LogListMode(fout, entry);

      DeleteFile(fout);

      if (ret < 0)
        {
          return ret;
        }
    }

  /* Release scan mode */

  mp_act_seach1->GScanMode(FALSE);
  mp_act_seach2->GScanMode(FALSE);

errout:
  /* Return scanmode */

  if (message)
    {
      MessageBox(message, L"Log files", MB_ICONSTOP);
    }

  /* Return current directory */

  ::SetCurrentDirectory(bkbuf);

  return ret;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut8(TCHAR *param)
{
  WINDOWPLACEMENT place;

  GetWindowPlacement(&place);

  if (place.showCmd == SW_NORMAL) {
    mTitleHeight = TITLE_HEIGHT_MAXIMIZED;
    ShowWindow(SW_MAXIMIZE);
  }
  else {
    mTitleHeight = TITLE_HEIGHT_NORMAL;
    ShowWindow(SW_NORMAL);
  }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::Desktop(void)
{
  CoInitializeEx(NULL, COINIT_MULTITHREADED );

  IUnknown        *pShell = NULL;
  IShellDispatch4 *pDisp4 = NULL;

  HRESULT hr = CoCreateInstance(CLSID_Shell,
                                NULL,
                                CLSCTX_INPROC_SERVER,
                                IID_PPV_ARGS(&pShell));
  if(!SUCCEEDED(hr)) {
    LOGE("CoCreateInstance");
    goto errout;
  }

  hr = pShell->QueryInterface(IID_IShellDispatch4, (void**)&pDisp4);

  if(!SUCCEEDED(hr)) {
    LOGE("QueryInterface");
    goto errout;
  }

  hr = pDisp4->ToggleDesktop();

  if(!SUCCEEDED(hr)) {
    LOGE("ToggleDesktop");
  }

errout:
  if (pDisp4) {
    pDisp4->Release();
  }

  if (pShell) {
    pShell->Release();
  }

  CoUninitialize();

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut9(TCHAR *param)
{
  dlg_history dlg(mhWnd);

  dlg.SetSimpleQueHandle(mp_act_seach->handle_back, mp_act_seach->handle_step);

  if (dlg.Modal() != IDOK) {
    return 0;
  }

  if (dlg.mpCurrentPath) {
    mp_act_seach->SetDirectory2(dlg.mpCurrentPath);
    mp_act_seach->Push(dlg.mpCurrentPath);
  }

  return 0;
}

/* ----------------------------------------------------------------- */
static int SavedirNodeCallback(D_NODE *p_node, void *p_param)
{
  p_node->param = 0;

  return 0;
}

/* ----------------------------------------------------------------- */
static int GitNodeCallback(GIT_NODE *p_node, const TCHAR *git_dir, void *p_param)
{
  GIT_PARAM *param  = (GIT_PARAM *)p_param;
  int        result = 0;

  D_NODE *p_top = param->d_node;

  if (p_top == NULL)
    {
      /* Clear parameter for git files */

      param->d_node = p_top = savedir(git_dir);

      savedir_search(p_top, SavedirNodeCallback, p_param);
    }

  /* Compare git managed files with real files */

  D_NODE *d_node = savedir(p_node->path);

  d_node->gtime = p_node->mtime;

  memcpy(d_node->sha1, p_node->sha1, sizeof(d_node->sha1));

  if (p_node->dev   == 0
   && p_node->inode == 0
   && p_node->mode  == 0xE000
   && p_node->uid   == 0
   && p_node->guid  == 0
   && p_node->size  == 0)
    {
      d_node->g_idx = 1;
    }
  else if (p_node->inode == 0
       && (p_node->dev == 0xFFFFFFFF || p_node->dev == 2 || p_node->dev == 0x19))
    {
      /* Configure files added to git */

      d_node->g_idx = 4;
    }
  else
    {
      /* Compare update time */

      int res = check_git_params(p_node);

      if (res == 0)
        {
          d_node->g_idx = 2;
        }
      else if (res > 0)
        {
          d_node->g_idx = 3;
        }
      else
        {
          /* Error! */
        }

      if (param->time == 0)
        {
          param->time = ::GetTickCount64();
        }

      ULONGLONG d = (::GetTickCount64() - param->time) / 60;

      if (d > 0)
        {
          param->time += d * 60;

          result = PumpMessage(param->h_wnd);

          param->view->GScanSet(p_node);
        }
    }

  /* Set git parameters tracing back to top directories */

  D_NODE *p_parent = d_node;

  while (p_parent)
    {
      if (p_parent->g_idx < d_node->g_idx)
        {
          p_parent->g_idx = d_node->g_idx;
        }

      if (p_parent == p_top)
        {
          break;
        }

      p_parent = p_parent->parent;
    }

  return result;
}

/* ----------------------------------------------------------------- */
int main_app::FuncShortcut4(TCHAR *param)
{
#if 0
  param = GetCurrentDirectory();

  D_NODE *d_node = savedir(param);

  GIT_PARAM git_param;

  git_param.d_node = NULL;
  git_param.h_wnd  = mhWnd;
  git_param.view   = mp_act_seach;
  git_param.time   = 0;

  git_param.view->GScanMode(TRUE);

  scan_git_dir(param, true, GitNodeCallback, (void *)&git_param);

  git_param.view->GScanMode(FALSE);

  Refresh();
#endif
  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncProperties(TCHAR *param)
{
  SHELLEXECUTEINFO  sei;
  dirent*           entry;

  entry = GetCurrentEntry();

  ZeroMemory(&sei, sizeof(sei));

  sei.cbSize = sizeof(sei);
  sei.lpFile = entry->d_name;
  sei.lpVerb = TEXT("properties");
  sei.fMask  = SEE_MASK_INVOKEIDLIST | SEE_MASK_NOCLOSEPROCESS;

  ShellExecuteEx(&sei);

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncAttribute(TCHAR *param)
{
  dlg_attribute  dlg(mhWnd);
  dirent        *entry;

  GetMarkedNum(&entry);

  if (entry)
    {
      dlg.SetAttribute(entry);

      if (dlg.Modal() == IDOK)
        {
          dlg.SetAttributes();
          SetDirectory(GetCurrentDirectory(), entry->d_name);
          Invalidate();
        }
    }

  return 0;
}

/* ----------------------------------------------------------------- */
int main_app::FuncApps(TCHAR *param)
{
  POINT pos;

  GetCurrentPosition((int *)&pos.x, (int *)&pos.y);

  ClientToScreen(mhWnd, &pos);

  DoContextMenu(pos.x, pos.y);

  return 0;
}

/* ----------------------------------------------------------------- */
void main_app::OnKeyDown(UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
  int f_no = GetFuncNo(vk,
                       HIBYTE(GetKeyState(VK_CONTROL)),
                       HIBYTE(GetKeyState(VK_SHIFT)),
                       0);

  (this->*(mpFuncs[f_no]))((TCHAR *)&vk);
}

/* ----------------------------------------------------------------- */
HBRUSH main_app::OnCtlColorEditr(HDC hdc, HWND hwndChild, int type)
{
  return _boxMsgCore::OnCtlColorEditr(hdc, hwndChild, type);
}

/* ----------------------------------------------------------------- */
TCHAR *main_app::GetDesktopFolder(void)
{
  LPITEMIDLIST  pidlist;

  if (m_desktopfolder[0] == '\0')
    {
      SHGetSpecialFolderLocation(NULL, CSIDL_DESKTOPDIRECTORY, &pidlist);
      SHGetPathFromIDList(pidlist, m_desktopfolder);
      CoTaskMemFree(pidlist);
    }

  return m_desktopfolder;
}

/* ----------------------------------------------------------------- */
static void SetEntry(dirent *entry, FIND_DATA *fd)
{
  entry->dwFileAttributes = fd->dwFileAttributes;
  entry->nFileSizeHigh    = fd->nFileSizeHigh;
  entry->nFileSizeLow     = fd->nFileSizeLow;

  StrCpy(entry->cAlternateFileName, fd->cAlternateFileName);
}

/* ----------------------------------------------------------------- */
static dirent *theSetEntry(HANDLE hHeap, dirent *entry, const TCHAR *name)
{
  HANDLE    hFile;
  size_t    size;
  dirent*   new_entry;
  FIND_DATA fd;

  size = sizeof(dirent) + (_tcslen(name) + 1) * sizeof(TCHAR);

  new_entry = (dirent *)HeapAlloc(hHeap, HEAP_NO_SERIALIZE, size);

  if (new_entry == NULL)
    {
      return NULL;
    }

  ZeroMemory(new_entry, size);

  hFile = ::FindFirstFile(name, &fd);

  if (INVALID_HANDLE_VALUE != hFile)
    {
      new_entry->next = entry;
      new_entry->size = (DWORD)size;

      SetEntry(new_entry, &fd);

      StrCpy(new_entry->d_name, name);
    }

  ::FindClose(hFile);

  return new_entry;
}

/* ----------------------------------------------------------------- */
static dirent *theSetEntry(HANDLE hHeap, dirent *entry, const TCHAR *dir, FIND_DATA *fd)
{
  size_t  size;
  dirent *new_entry;

  size = sizeof(dirent)
       + (_tcslen(dir)
       + 1
       + _tcslen(fd->cFileName) + 1) * sizeof(TCHAR);

  new_entry = (dirent *)HeapAlloc(hHeap, HEAP_NO_SERIALIZE, size);

  if (new_entry == NULL)
    {
      return NULL;
    }

  ZeroMemory(new_entry, size);

  new_entry->next = entry;
  new_entry->size = (DWORD)size;

  SetEntry(new_entry, fd);

  StrCpy(new_entry->d_name, dir);
  PathAddBackslash(new_entry->d_name);
  StrCat(new_entry->d_name, fd->cFileName);

  return new_entry;
}

/* ----------------------------------------------------------------- */
static dirent *open_sub(HANDLE hHeap, dirent *in_top)
{
  HANDLE    hFile;
  FIND_DATA fd;
  dirent*   top_entry = in_top->next;
  TCHAR     buf[MAX_PATH];

  wsprintf(buf, _T("%s\\*.*"), in_top->d_name);

  hFile = ::FindFirstFile(buf, &fd);

  if (INVALID_HANDLE_VALUE == hFile)
    {
      return top_entry;
    }

  do
    {
      if (_tcscmp(fd.cFileName, _T(".")) == 0 || _tcscmp(fd.cFileName, _T("..")) == 0)
        {
          continue;
        }

      top_entry = theSetEntry(hHeap, top_entry, in_top->d_name, &fd);

      if (top_entry == NULL)
        {
          break;
        }
    }
  while(::FindNextFile(hFile, &fd));

  ::FindClose(hFile);

  return top_entry;
}

/* ----------------------------------------------------------------- */
static time_t From_FILETIME_to_time_t(FILETIME *from_time)
{
  /* Constant for converting from windows time to linux time */

  const time_t unix_epoch_offset = 116444736000000000LL;

  union _tag {
    FILETIME times;
    time_t   qpart;
  }
  *update = (union _tag *)from_time;

  return (update->qpart - unix_epoch_offset) / 10000000;
}

/* ----------------------------------------------------------------- */
static BOOL FileTimeCmp(time_t time, TCHAR *fname)
{
  HANDLE    hFile;
  FILETIME  from_mtime;    // Last write date
  time_t    to_mtime;
  BOOL      res = FALSE;

  hFile = CreateFile(fname, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

  if (INVALID_HANDLE_VALUE == hFile)
    {
      return res;
    }

  ::GetFileTime(hFile, NULL, NULL, &from_mtime);

  CloseHandle(hFile);

  to_mtime = From_FILETIME_to_time_t(&from_mtime);

  if (difftime(time, to_mtime) > 0)
    {
      res = TRUE;
    }

  return res;
}

/* ----------------------------------------------------------------- *
 * Read drop file list from clipboard
 * ----------------------------------------------------------------- */
int main_app::ReadClip(void)
{
  HDROP   hDrop;
  HANDLE  hHeap;
  int     cnt;
  int     i;
  dirent *entry;
  dirent *top_entry = NULL;
  TCHAR*  dir;
  int     len;
  FILE   *fp;
  TCHAR   buf[MAX_PATH * 2];
  TCHAR   tmp[MAX_PATH * 4];
  errno_t err;

  if (!IsClipboardFormatAvailable(CF_HDROP))
    {
      return 0;
    }

  OpenClipboard(mhWnd);

  hDrop = (HDROP)GetClipboardData(CF_HDROP);

  if (hDrop == NULL)
    {
      return 0;
    }

  hHeap = HeapCreate(HEAP_NO_SERIALIZE, 10000, 0);

  if (hHeap)
    {
      /* Get number of files */

      cnt = ::DragQueryFile(hDrop, (unsigned)-1, NULL, 0);

      for (i = 0; i < cnt; i++)
        {
          ::DragQueryFile(hDrop, i, buf, MAX_PATH);

          top_entry = theSetEntry(hHeap, top_entry, buf);
        }
    }

  GlobalUnlock(hDrop);
  CloseClipboard();

  dir = GetCurrentDirectory();

  len = (int)(PathFindFileName(top_entry->d_name) - top_entry->d_name);

  GetTempPath(MAX_PATH, buf);

  GetTempFileName(buf, _T("fs8"), 0, tmp);

  err = _tfopen_s(&fp, tmp, _T("w"));

  if (err)
    {
      for (entry = top_entry; entry; entry = entry->next)
        {
          if (entry->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
              entry->next = open_sub(hHeap, entry);
            }
          else
            {
              _tcscpy_s(buf, MAX_PATH, dir);
              PathAddBackslash(buf);
              _tcscat_s(buf, MAX_PATH, entry->d_name + len);

              if (PathFileExists(buf))
                {
                  if (FileTimeCmp(entry->mtime, buf) > 0)
                    {
                    }
                }

              ShCopy(mhWnd, entry->d_name, buf);

              _ftprintf(fp, L"\"%s\" \"%s\"\n", entry->d_name, buf);
            }
        }

      fclose(fp);
    }

  if (hHeap)
    {
      HeapDestroy(hHeap);
    }

  DeleteFile(tmp);

  SetDirectory();
  Invalidate();

  return 0;
}

/* ----------------------------------------------------------------- */
void main_app::OnDropFiles(HDROP hDrop)
{
  int             i;
  int             cnt;
  size_t          len  = 0;
  TCHAR*          pTop = NULL;
  TCHAR*          cp;
  TCHAR           buf[MAX_PATH];
  SHFILEOPSTRUCT  fileOp;

  /* Get number of files */

  cnt = ::DragQueryFile(hDrop, (unsigned)-1, NULL, 0);

  /* Calculate region size */

  for (i = 0; i < cnt; i++)
    {
      ::DragQueryFile(hDrop, i, buf, MAX_PATH);

      len += _tcslen(buf);
      len++;   /* delimiter */
    }

  cp = pTop = (TCHAR *)HeapAlloc(GetProcessHeap(),
                                 HEAP_NO_SERIALIZE,
                                 len * sizeof(TCHAR));

  if (cp == NULL)
    {
      return;
    }

  for(i = 0; i < cnt; i++)
    {
      ::DragQueryFile(hDrop, i, buf, MAX_PATH);

      len = _tcslen(buf) + 1;

      _tcscpy_s(cp, len, buf);

      cp += len;
    }

  /* Put double NULL character */

  *cp = '\0';

  /* Set value of SHFILEOPSTRUCT structure member */

  fileOp.hwnd                   = mhWnd;
  fileOp.wFunc                  = FO_COPY;
  fileOp.pFrom                  = pTop;
  fileOp.pTo                    = GetCurrentDirectory();
  fileOp.fAnyOperationsAborted  = FALSE;
  fileOp.hNameMappings          = NULL;
  fileOp.lpszProgressTitle      = NULL;
  fileOp.fFlags =
//    FOF_ALLOWUNDO |
      FOF_RENAMEONCOLLISION |
//    FOF_NOCONFIRMATION |
//    FOF_NOERRORUI |
//    FOF_SILENT |
//    FOF_NOCONFIRMATION |
      0;

  ::HeapFree(GetProcessHeap(), HEAP_NO_SERIALIZE, pTop);
}

/* ----------------------------------------------------------------- */
void main_app::LetsCopy(TCOPYACTION *act, int id)
{
  if(id == TCOPYACTION_EXEC)
    {
    }
  else if(id == TCOPYACTION_COMP)
    {
      CompCopyInfo(act);
    }
  else if(id == TCOPYACTION_END)
    {
      EndCopyInfo();
    }
  else if(id == TCOPYACTION_START)
    {
    }
}
