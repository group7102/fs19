/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_find.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    ファインダイアログ
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "ssnprintf.h"
#include "xpt.h"
#include "resource.h"
#include "sub_funcs.h"
#include "icon.h"
#include "logf.h"
#include "com_button.h"
#include "dlg_find.h"

/* ----------------------------------------------------------------- */
dlg_find::dlg_find(HWND hParent) : xDialog(_T("IDD_FIND"), hParent)
{
  mpFont = new xFont;
  mpCmbo = new xComboBox;

  mpOK     = new com_button;
  mpCancel = new com_button;

  IcoSetPsMode(&mpOK->icon_no, &mpCancel->icon_no);
}

/* ----------------------------------------------------------------- */
dlg_find::~dlg_find()
{
  if (mpFont)
    {
      delete mpFont;
    }

  if (mpCmbo)
    {
      delete mpCmbo;
    }

  delete mpOK;
  delete mpCancel;
}

/* ----------------------------------------------------------------- */
BOOL dlg_find::OnInitDialog(HWND hwndFocus, LPARAM lParam)
{
  IPT *pt = xPt();

  CenterWindow();   // 真ん中に移動

  mpFont->Create(pt->fonts[4].name, pt->fonts[4].size);

  Attach(mpCmbo,   IDC_COMBO1);
  Attach(mpOK,     IDOK2);
  Attach(mpCancel, IDCANCEL2);

  mpCmbo->SetFont(*mpFont);

  SetList();

  mpCmbo->SetCurSel(0);
  mpCmbo->SetFocus();

  return 0;
}

/* ----------------------------------------------------------------- */
void dlg_find::OnCommand(int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
    {
      case IDOK:
      case IDOK2:
        SaveList();
        mpCmbo->GetText(m_target, MAX_PATH);

        if (m_target[0] == '\0')
          {
            EndDialog(IDCANCEL);
          }
        else
          {
            EndDialog(IDOK);
          }

        break;

      case IDCANCEL:
      case IDCANCEL2:
        EndDialog(IDCANCEL);
        break;

      case IDC_COMBO1:
        if (codeNotify == CBN_SELCHANGE)
          {
            mpCmbo->ShowDropdown(TRUE);
          }
        else if (codeNotify == CBN_DBLCLK)
          {
          }
        else if (codeNotify == CBN_SETFOCUS)
          {
          }
        else if (codeNotify == CBN_KILLFOCUS)
          {
          }
        else if (codeNotify == CBN_EDITCHANGE)
          {
            mpCmbo->ShowDropdown(FALSE);
          }
        else if (codeNotify == CBN_EDITUPDATE)
          {
          }
        else if (codeNotify == CBN_DROPDOWN)
          {
          }
        else if (codeNotify == CBN_CLOSEUP)
          {
          }
        else if (codeNotify == CBN_SELENDOK)
          {
          }
        else if (codeNotify == CBN_SELENDCANCEL)
          {
          }

        break;

      default:
        break;
    }
}

/* ----------------------------------------------------------------- */
void dlg_find::SetList(void)
{
  IPT   *pt = xPt();
  TCHAR  wclist[MAX_PATH];
  TCHAR *cp;
  TCHAR *contex;

  _tcscpy_s(wclist, MAX_PATH, pt->setting.wclist);

  cp = _tcstok_s(wclist, _T(" "), &contex);

  for(; cp; cp = _tcstok_s(NULL, _T(" "), &contex))
    {
      mpCmbo->InsertString(-1, cp);
    }
}

/* ----------------------------------------------------------------- */
void dlg_find::SaveList(void)
{
  IPT    *pt = xPt();
  TCHAR   text[MAX_PATH];
  TCHAR   wc[MAX_PATH];
  int     i;
  size_t  size;

  mpCmbo->GetText(text, MAX_PATH);

  size = _tcslen(text) + 2;

  if (size >= MAX_PATH || text[0] == '\0')
    {
      return;
    }

  _tcscpy_s(pt->setting.wclist, MAX_PATH, text);

  for (i = 0; i < mpCmbo->GetCount(); i++)
    {
      mpCmbo->GetLBText(i, wc);

      size += (_tcslen(wc) + 2);

      if (size > MAX_PATH)
        {
          break;
        }

      if (_tcsicmp(wc, text) == 0)
        {
          continue;
        }

      _tcscat_s(pt->setting.wclist, MAX_PATH, _T(" "));
      _tcscat_s(pt->setting.wclist, MAX_PATH, wc);
    }
}

/* ----------------------------------------------------------------- */
void dlg_find::OnPaint()
{
  xPaintDC dc(mhWnd);
  RECT     rc;
  IPT     *pt = xPt();

  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(pt->color.title);
  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  GetClientRect(&rc);

  dc.FillRect(&rc, pt->color.base_bar);

  dc.TextOut(rc.left + 26, rc.top + 4, L"Specify the character string to be searched");
  IcoDrawIconFromIndex(dc, 4, rc.top + 4, 104, TRUE);
}

/* ----------------------------------------------------------------- */
LRESULT dlg_find::Message(UINT msg, WPARAM wParam, LPARAM lParam)
{
  LRESULT res = 0;

  switch(msg) {
  case WM_LBUTTONDOWN:
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, lParam);
    break;
  default:
    res = xDialog::Message(msg, wParam, lParam);
    break;
  }

  return res;
}
