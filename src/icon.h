/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: icom_beta.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 */

#ifndef _ICON_BETA_H_
#define _ICON_BETA_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

void   IcoInit(void);
void   IcoFin(void);
void  *IcoGetImageList(void);
void   IcoDrawIconFromFile(HDC hDC, int x, int y, dirent *entry, BOOL isActive, BOOL isRemote);
int    IcoDrawIconFromIndex(HDC hDC, int x, int y, int idx, BOOL isActive);
void   IcoGetTempDirectoryIconIndex(HANDLE *handle, DWORD *index);
void   IcoGetTempFileIconIndex(HANDLE *handle, DWORD *index);
HANDLE IcoGetInstance(dirent *entry, DWORD *d_no, BOOL isRemotea);
void   IcoSetPsMode(int *icon_ok, int *icon_cancel);

#ifdef __cplusplus
}
#endif

#endif /* _ICON_BETA_H_ */
