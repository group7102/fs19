/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: icom_beta.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
 */

#ifndef _ICON_BETA_H_
#define _ICON_BETA_H_

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

#define MARK_CHECK	52

/* ----------------------------------------------------------------- *
 * Public types
 * ----------------------------------------------------------------- */

enum {
  MARK_GIT = 2,
  MARK_CVS,
  MARK_SVN,
  MARK_MODIFY,
  MARK_ADD,
  MARK_MARGE,
  MARK_CONF,
};

/* ----------------------------------------------------------------- */
class iico
{
public:
  void draw(int x, int y, int idx = 0, BOOL isActive = TRUE);
  void draw(int x, int y, dirent *entry, BOOL isActive, BOOL isRemote = FALSE);
  void attach(HDC hDC = NULL);

public:
  HANDLE get_instance(dirent *entry, DWORD *d_no, BOOL isRemotea);
  HANDLE get_instance(dirent *entry, DWORD *d_no);
  HANDLE get_remote_instance(dirent *entry, DWORD *d_no);

public:
  iico();
  iico(HDC hDC);
  ~iico();

public:
  operator void* () const;

public:
  void      initialization(void);
  void      destroy(void);

private:
  HDC       mhDC;
  COLORREF  rgbBk;
  COLORREF  rgbFg;
  UINT      fStyle;
};

/* ----------------------------------------------------------------- *
 * Public function prototypes
 * ----------------------------------------------------------------- */

void GetTempDirectoryIconIndex( HANDLE *handle, DWORD *index );
void GetTempFileIconIndex( HANDLE *handle, DWORD *index );

#endif /* _ICON_BETA_H_ */
