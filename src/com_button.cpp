/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: com_button.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "xpt.h"
#include "xDcBase.h"
#include "icon.h"
#include "logf.h"
#include "com_button.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

#define TEXT_MAX_LEN  (32)

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
com_button::com_button()
{
  icon_no = 97;
}

/* ----------------------------------------------------------------- */
com_button::~com_button()
{
}

/* ----------------------------------------------------------------- *
 * Send message of up and down key to parent
 * ----------------------------------------------------------------- */
void com_button::OnKeyDown(UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
  switch (vk) {
  case VK_PRIOR:
  case VK_NEXT:
  case VK_UP:
  case VK_DOWN:
    GetParent()->SendMessage(WM_COMMAND,
                             GET_WM_COMMAND_MPS(::GetDlgCtrlID(mhWnd),
                             mhWnd,
                             ICOMBUTTON_KEY | (vk & 0xFF)));
    break;

  default:
    GetParent()->SendMessage(WM_COMMAND,
                             GET_WM_COMMAND_MPS(::GetDlgCtrlID(mhWnd),
                             mhWnd,
                             ICOMBUTTON_KEY | (vk & 0xFF)));
    break;
  }
}

/* ----------------------------------------------------------------- *
 * Owner drawing process
 * ----------------------------------------------------------------- */
int com_button::DrawItem(const DRAWITEMSTRUCT *pDIS)
{
  xTmpDC    dc(pDIS->hwndItem, pDIS->hDC);
  TCHAR     buf[TEXT_MAX_LEN];
  IPT      *pt = xPt();
  RECT      rc;
  SIZE      size;
  int       left;
  COLORREF  color_frame;

  if (pDIS->itemID == (UINT)(~0)) {
    return 0;
  }

  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(0xFF000000);
  dc.SetSelectFont(pt->fonts[4].name, pt->fonts[4].size);

  rc = pDIS->rcItem;

  GetText(buf, TEXT_MAX_LEN);

  dc.GetTextExtentPoint32(buf, &size);

  left = ((rc.right - rc.left) - (size.cx + 32)) / 2;

  if ((pDIS->itemState & 0xFF)== (ODS_FOCUS | ODS_SELECTED)) {
    color_frame = pt->color.selected_frame;
  }
  else {
    color_frame = pt->color.base_bar;
  }

  dc.Rectangle(&pDIS->rcItem, color_frame, pt->color.base_bar);
  IcoDrawIconFromIndex(dc, left, (rc.bottom - rc.top - 16) / 2, icon_no, TRUE);

  rc.left   = left + 24;
  rc.top    = (rc.bottom - rc.top - size.cy) / 2;
  rc.right  = rc.left + size.cx;
  rc.bottom = rc.top + size.cy;

  dc.DrawText(buf, &rc, /*DT_HIDEPREFIX*/0);

#if 0
  if (pDIS->itemAction == ODA_DRAWENTIRE) {
    dc.Rectangle(&pDIS->rcItem, pt->color.base_bar, pt->color.base_bar);
    IcoDrawIconFromIndex(dc, left, top1, icon_no, TRUE);
    dc.TextOut(left + 24, top2, buf);
  }
  else if (pDIS->itemAction == ODA_FOCUS) {
    dc.Rectangle(&pDIS->rcItem, pt->color.selected_frame, pt->color.selected_frame);
    IcoDrawIconFromIndex(dc, left, top1, icon_no, TRUE);
    dc.TextOut(left + 24, top2, buf);
  }

  if (ODA_FOCUS == pDIS->itemAction) {
    LOGI("ODA_FOCUS:%X:%X", pDIS->itemAction, pDIS->itemState);
  }

  if (ODA_DRAWENTIRE == pDIS->itemAction) {
    LOGI("ODA_DRAWENTIRE:%X:%X", pDIS->itemAction, pDIS->itemState);
  }

  if (ODA_SELECT & pDIS->itemAction) {
    LOGI("ODA_SELECT:%X:%X", pDIS->itemAction, pDIS->itemState);
  }

  if (pDIS->itemState & ODS_SELECTED)
    {
      FillRect(dc, &rc, RGB(0, 0, 255));
      y = 1;
    }
  else if (pDIS->itemState & ODS_FOCUS)
    {
      FillRect(dc, &rc, RGB(0, 0, 192));
      y = 1;
    }
  else
    {
      FillRect(dc, &rc, RGB(128, 128, 128));
      y = 0;
    }

  height = rc.bottom - rc.top;

  rc.top += y;
  rc.bottom += y;

  IcoDrawIconFromIndex(dc, rc.left + 2, rc.top + 2, 18, TRUE);

  dc.DrawText(buf, &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
#endif

  return 1;
}

/* ----------------------------------------------------------------- */
static COLORREF Brend(COLORREF col, int alph)
{
  int   r = GetRValue(col);
  int   g = GetGValue(col);
  int   b = GetBValue(col);

  r *= alph;
  g *= alph;
  b *= alph;
  r /= 100;
  g /= 100;
  b /= 100;
  alph = 100 - alph;
  r += (0xFF * alph / 100);
  g += (0xFF * alph / 100);
  b += (0xFF * alph / 100);

  if (r > 255) {
    r = 255;
  }

  if (g > 255) {
    g = 255;
  }

  if (b > 255) {
    b = 255;
  }

  return RGB(r, g ,b);
}

/* ----------------------------------------------------------------- */
void com_button::FillRect(HDC hDC, const RECT *pRC, COLORREF fg)
{
  int     i;
  int     h;
  int     alph;
  xTmpDC  dc(hDC);

  h = pRC->bottom - pRC->top;

  dc.RoundRect(pRC, 4, 4, fg, fg);

  for (i = 0; i < h - 2; i++) {
    alph = i * 16 / (h - 1) + 9;

    dc.Line(pRC->left + 1,
            i + pRC->top + 1,
            pRC->right - 1,
            i + pRC->top + 1,
            Brend(fg, alph));
  }
}
