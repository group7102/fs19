/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: timerid.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */

#ifndef _TIMERID_H_
#define _TIMERID_H_

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

enum _TIMERID_E_ {
  ISCROLL_TIMERID = 0x1124,
  ISUBAPP_TIMEID  = 0x1201,
  ISUBAPP_SSSS    = 0x1202,
  IINFO_LEAVE_TIMERID,
  IINFO_WAIT_TIMERID,
  ISEACH_TIMERID,
};

#endif /* _TIMERID_H_ */
