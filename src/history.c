/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: history.c
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include <windows.h>
#include <shlwapi.h>
#include <tchar.h>
#include <malloc.h>
#include "xpt.h"
#include "logf.h"
#include "history.h"

/* ----------------------------------------------------------------- *
 * pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private data
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Private functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static int Compare(const void *target1, const void *target2)
{
  struct EVENT_T *event1 = (struct EVENT_T *)target1;
  struct EVENT_T *event2 = (struct EVENT_T *)target2;

  int res = 0;

  if (event1->mark > event2->mark) {
    res = -1;
  }
  else if (event1->mark < event2->mark) {
    res = 1;
  }

  if (res) {
    return res;
  }

  if (event1->date > event2->date) {
    res = -1;
  }
  else if (event1->date < event2->date) {
    res = 1;
  }

  return res;
}

/* ----------------------------------------------------------------- */
static int SearchEvent(const TCHAR *path)
{
  struct HISTORY_T *his = &xPt()->history;

  int idx;

  for (idx = 0; idx < his->num && idx < IPT_HISTORY_EVENT_NUM; idx++) {
    if (_tcsicmp(his->events[idx].path, path) == 0) {
      break;
    }
  }

  return idx;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
void HitPush(const TCHAR *path)
{
  struct HISTORY_T *his = &xPt()->history;

  int idx = SearchEvent(path);

  /* When the buffer is full, it overwrites the data at the end of the buffer */

  if (idx >= IPT_HISTORY_EVENT_NUM) {
    his->num = IPT_HISTORY_EVENT_NUM - 1;
    idx = his->num;
  }

  int mark = 0;

  if (idx == his->num) {
    _sntprintf_s(his->events[idx].path,
                 _countof(his->events[idx].path),
                 _TRUNCATE,
                 L"%s",
                 path);
    his->num++;
  }
  else {
    /* Keep the checkmarks of existing data so that the newest data comes first */

    mark = his->events[idx].mark;
  }

  /* Set the new time */

  his->events[idx].mark = 1;
  his->events[idx].date = HitGetLocalDateTime();

  /* Sort by newest chronological order */

  qsort((void*)his->events, his->num, sizeof(struct EVENT_T), Compare);

  his->events[0].mark = mark;
}

/* ----------------------------------------------------------------- */
const TCHAR *HitGetAt(int idx, int *mark, DWORD *date)
{
  struct HISTORY_T *his = &xPt()->history;

  if (idx >= his->num) {
    return NULL;
  }

  *mark = his->events[idx].mark;
  *date = his->events[idx].date;

  return his->events[idx].path;
}

/* ----------------------------------------------------------------- */
const TCHAR *HitSetAt(int idx, int mark)
{
  struct HISTORY_T *his = &xPt()->history;

  if (idx >= his->num) {
    return NULL;
  }

  his->events[idx].mark = mark;

  return his->events[idx].path;
}

/* ----------------------------------------------------------------- */
const TCHAR *HitGetEventFromPath(const TCHAR *path, int *mark, DWORD *date)
{
  return HitGetAt(SearchEvent(path), mark, date);
}

/* ----------------------------------------------------------------- */
DWORD HitGetLocalDateTime(void)
{
  SYSTEMTIME      tm;
  ULARGE_INTEGER  ttt;

  GetLocalTime(&tm);

  SystemTimeToFileTime(&tm, (LPFILETIME)&ttt);

  ttt.QuadPart /= 10000000;

  return (DWORD)ttt.QuadPart;
}

/* ----------------------------------------------------------------- */
void HitDelete(int idx)
{
  struct HISTORY_T *his = &xPt()->history;

  if (his->num == 0 || his->num <= idx) {
    return;
  }

  his->num--;

  for (int i = idx; i < his->num; i++) {
    his->events[i] = his->events[i + 1];
  }
}
