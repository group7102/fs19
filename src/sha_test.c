#include <stdio.h>
#include <openssl/sha.h> // SHA Library

int compute_sha256(unsigned char * src, unsigned int src_len, unsigned char * buffer) {
  SHA256_CTX c;
  SHA256_Init(&c);
  SHA256_Update(&c, src, src_len);
  SHA256_Final(buffer, &c);
  return 0;
}

int main(int argc, char **argv)
{
  SHA256_CTX  ctx;
  SHA256_CTX  ctx_bk;
  uint8_t     buf[SHA256_DIGEST_LENGTH + 1];

  SHA256_Init(&ctx);

  SHA256_Update(&ctx, "AAA", 3);
  SHA256_Update(&ctx, "BBB", 3);
  SHA256_Update(&ctx, "CCC", 3);

  ctx_bk = ctx;

  SHA256_Final(buf, &ctx);

  printf("%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x\n",
         buf[ 0], buf[ 1], buf[ 2], buf[ 3], buf[ 4], buf[ 5], buf[ 6], buf[ 7],
         buf[ 8], buf[ 9], buf[10], buf[11], buf[12], buf[13], buf[14], buf[15],
         buf[16], buf[17], buf[18], buf[19], buf[20], buf[21], buf[22], buf[23],
         buf[24], buf[25], buf[26], buf[27], buf[28], buf[29], buf[31], buf[31]);

  /* Retry */

  SHA256_Init(&ctx);

  SHA256_Transform(&ctx, buf);

//SHA256_Update(&ctx, "DDD", 3);

  SHA256_Final(buf, &ctx);

  printf("%s\n", SHA256(

  printf("%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x\n",
         buf[ 0], buf[ 1], buf[ 2], buf[ 3], buf[ 4], buf[ 5], buf[ 6], buf[ 7],
         buf[ 8], buf[ 9], buf[10], buf[11], buf[12], buf[13], buf[14], buf[15],
         buf[16], buf[17], buf[18], buf[19], buf[20], buf[21], buf[22], buf[23],
         buf[24], buf[25], buf[26], buf[27], buf[28], buf[29], buf[31], buf[31]);

  ctx = ctx_bk;

//SHA256_Update(&ctx, "DDD", 3);

  SHA256_Final(buf, &ctx);

  printf("%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x"
         "%02x%02x%02x%02x%02x%02x%02x%02x\n",
         buf[ 0], buf[ 1], buf[ 2], buf[ 3], buf[ 4], buf[ 5], buf[ 6], buf[ 7],
         buf[ 8], buf[ 9], buf[10], buf[11], buf[12], buf[13], buf[14], buf[15],
         buf[16], buf[17], buf[18], buf[19], buf[20], buf[21], buf[22], buf[23],
         buf[24], buf[25], buf[26], buf[27], buf[28], buf[29], buf[31], buf[31]);

  return 0;
}
