/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: dlg_mkdir.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    ディレクトリの作成
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef _DLG_MKDIR_H_
#define _DLG_MKDIR_H_

/* ----------------------------------------------------------------- *
 * class of use
 * ----------------------------------------------------------------- */

class xFont;
class com_edit;

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class dlg_mkdir : public xDialog
{
public:
  void   SetFont(const TCHAR *name, int size);
  void   SetDirectory(const TCHAR *name);
  TCHAR *GetSel(void);

public:
  dlg_mkdir(HWND hParent);
  ~dlg_mkdir();

private:
  BOOL OnInitDialog(HWND hwndFocus, LPARAM lParam);
  void OnCommand(int id, HWND hwndCtl, UINT codeNotify);
  void OnPaint();

private:
  void DispTitle(int mode);

private:
  LRESULT Message(UINT msg, WPARAM wParam, LPARAM lParam);

private:
  com_edit *mpEdit;
  xFont    *mpFont;
  int       m_mode;
  TCHAR     m_dir[MAX_PATH];
  TCHAR     m_buf[MAX_PATH * 2];

private:
  com_button *mpOK;
  com_button *mpCancel;
};

#endif /* _DLG_MKDIR_H_ */
