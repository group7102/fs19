/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: view_directory.cpp
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *    
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include "_box.h"
#include "dirent.h"
#include "icon.h"
#include "xDcBase.h"
#include "view_base.h"
#include "xpt.h"
#include "namelist.h"
#include "view_directory.h"

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

#define ICON_SIZE       16
#define HENABLE         ((void*)-3)
#define OFFSET_TOP      4
#define OFFSET_BTM      4
#define OFFSET_OFFSET   3
#define OFFSET_INTERVAL 6

/* ----------------------------------------------------------------- *
 * Private types
 * ----------------------------------------------------------------- */

enum {
  BACK_ARROW  = 21,
  BREAK_ARROW = 22,
  BREAK_LINE  = 42,
  UP_FOLDER   = 23,
  POP_FOLDER  = 24,
  PUSH_FOLDER = 25,
  NONE_LINE   = 1,
};

/* ----------------------------------------------------------------- */
view_directory::view_directory(int base) : view_base(base)
{
  Update();

  ::GetCurrentDirectory(MAX_PATH, m_directory);

  m_prev_id     = -1;
  m_action      = -1;
  m_enable      = FALSE;
  m_d_cnt       = 0;
  mIsTop        = TRUE;
  m_step_enable = TRUE;
  m_back_enable = TRUE;
}

/* ----------------------------------------------------------------- */
void view_directory::Draw(HWND hWnd, HDC hDC, int x, int y, int cx, int cy)
{
  xTmpDC  dc(hDC);
  xBmpDC  bmp;
  IPT    *pt = xPt();
  RECT   *rc;

  bmp.Create(dc, cx, cy);
  bmp.SetSelectFont(m_fontname, m_fontsize);

  SetMiRect(bmp, bmp.tm.tmAveCharWidth, cx, cy);

  bmp.FillRect(0, 0, cx, cy, pt->color.base_bar);

  bmp.Line(cx - 2, 0, cx - 2, cy, pt->color.split_line);

  bmp.SetBkMode(TRANSPARENT);

  if (m_enable)
    {
      if (m_focus)
        {
          bmp.SetTextColor(0);
        }
      else
        {
          bmp.SetTextColor(Brend(0, 50));
        }

      rc = &mi_lines[6].rc;

#if 0
      bmp.Rectangle(rc->left,
                    0,
                    cx - rc->left - 8/* - 72*/,
                    cy,
                    pt->color.split_line,
                    pt->color.base_bar);
#else
      bmp.Rectangle(2,
                    0,
                    cx - 8,
                    cy,
                    pt->color.split_line,
                    pt->color.base_bar);
#endif

      DrawMain(bmp, m_prev_id, cx, cy, m_focus);
    }
  else
    {
      DrawMain2(bmp, cy);
    }

  dc.BitBlt(x, y, cx, cy, bmp, 0, 0, SRCCOPY);

  mhWnd = hWnd;
  mhDC  = bmp;
  m_x   = x;
  m_y   = y;
  m_cx  = cx;
  m_cy  = cy;
}

/* ----------------------------------------------------------------- */
void view_directory::Redraw(void)
{
  if (mhDC)
    {
      xDC   dc(mhWnd);

      Draw(mhWnd, dc, m_x, m_y, m_cx, m_cy);
    }
}

/* ----------------------------------------------------------------- */
void view_directory::DrawMain(HDC hDC, int id, int width, int height, BOOL active)
{
  int     i;
  int     y;
  int     offset;
  xTmpDC  dc(hDC);
  IPT    *pt = xPt();
  BOOL    enable = active;

  for (i = 0; i < TB_MAX_LINE && mi_lines[i].handle; i++)
    {
      y = 1;

      if (mi_lines[i].id == IDIRECTORYID_NULL)
        {
          offset = 0;
        }
      else
        {
          if (mi_lines[i].id == IDIRECTORYID_BACK && !m_back_enable)
            {
            }
          else if ((mi_lines[i].id == IDIRECTORYID_STEP || mi_lines[i].id == IDIRECTORYID_DOWN)
                && !m_step_enable)
            {
            }
          else if (mi_lines[i].id == IDIRECTORYID_L_FOLDER && !m_up_enable)
            {
            }
          else if ((m_action == -1 && id == i) || m_action == i)
            {
              FillRect(dc, &mi_lines[i].rc, pt->color.selected, pt->color.selected_frame);

              if (m_action == i)
                {
                  y = 3;
                }
            }

          offset = OFFSET_OFFSET;
        }

      if (mi_lines[i].handle != HENABLE)
        {
          if (mi_lines[i].id == IDIRECTORYID_BACK)
            {
              enable = active == FALSE ? FALSE : m_back_enable;
            }
          else if (mi_lines[i].id == IDIRECTORYID_STEP || mi_lines[i].id == IDIRECTORYID_DOWN)
            {
              enable = active == FALSE ? FALSE : m_step_enable;
            }
          else if (mi_lines[i].id == IDIRECTORYID_L_FOLDER)
            {
              enable = active == FALSE ? FALSE : m_up_enable;
            }
          else
            {
              enable = active;
            }
          IcoDrawIconFromIndex(dc,
                               mi_lines[i].rc.left + offset,
                               (height - ICON_SIZE) / 2 + y,
                               mi_lines[i].index,
                               enable);
          offset += (ICON_SIZE + OFFSET_INTERVAL);
        }

      if (mi_lines[i].name[0])
        {
          dc.TextOut(mi_lines[i].rc.left + offset,
                     (height - dc.tm.tmHeight) / 2 + y,
                     mi_lines[i].name);
        }
    }

  y = (height - ICON_SIZE) / 2 + y;

  int   order;
  int   kind;

  my_namelist->GetSortMode(&order, &kind);

  switch (kind)
    {
      case INAM_KIND_EXT:
        if (order > 0) {
          IcoDrawIconFromIndex(dc, 8, y, 26, active);
        }
        else {
          IcoDrawIconFromIndex(dc, 8, y, 27, active);
        }
        break;
      case INAM_KIND_TIME:
        if (order > 0) {
          IcoDrawIconFromIndex(dc, 8, y, 29, active);
        }
        else {
          IcoDrawIconFromIndex(dc, 8, y, 28, active);
        }
        break;
      case INAM_KIND_SIZE:
        if (order > 0) {
          IcoDrawIconFromIndex(dc, 8, y, 31, active);
        }
        else {
          IcoDrawIconFromIndex(dc, 8, y, 30, active);
        }
        break;
      case INAM_KIND_NAME:
      default:
        if (order > 0) {
          IcoDrawIconFromIndex(dc, 8, y, 32, active);
        }
        else {
          IcoDrawIconFromIndex(dc, 8, y, 33, active);
        }
        break;
    }

#if 0
  IcoDrawIconFromIndex(dc, width - 24, y,  8, active);
  IcoDrawIconFromIndex(dc, width - 48, y,  9, active);
#endif
}

/* ----------------------------------------------------------------- */
void view_directory::DrawMain2(HDC hDC, int height)
{
  xTmpDC  dc(hDC);
  int     i;
  int     x;
  int     y  = (height - dc.tm.tmHeight) / 2;
  int     yd = (height - ICON_SIZE) / 2;

  dc.SetTextColor(Brend(0, 50));

  for (i = 6; i < TB_MAX_LINE && mi_lines[i].handle; i++)
    {
      x = mi_lines[i].rc.left - mi_lines[6].rc.left + OFFSET_INTERVAL;

      if (mi_lines[i].handle == HENABLE)
        {
          dc.TextOut(x, y, mi_lines[i].name);
        }
      else
        {
          if (mi_lines[i].index == BREAK_LINE)
            {
              IcoDrawIconFromIndex(dc, x, yd, BREAK_LINE, FALSE);
            }
          else
            {
              IcoDrawIconFromIndex(dc, x, yd, BREAK_ARROW, FALSE);
            }
        }
    }
}

/* ----------------------------------------------------------------- */
view_directory::~view_directory()
{
}

/* ----------------------------------------------------------------- */
int view_directory::SetMiiSubRect(HDC          hDC,
                                  int          idx,
                                  int          x,
                                  int          cy,
                                  WORD         id,
                                  int          offset,
                                  const TCHAR *str)
{
  SIZE        size      = {0, 0};
  int         icon_size = 0;
  ICONICON2  *lines;

  lines = &mi_lines[idx];

  if (str)
    {
      _tcscpy_s(lines->name, MAX_PATH, str);
      ::GetTextExtentPoint32(hDC, str, (int)_tcslen(str), &size);
    }

  if (lines->handle != HENABLE)
    {
      icon_size = ICON_SIZE;

      if (size.cx)
        {
          icon_size += OFFSET_INTERVAL;
        }
    }

  lines->rc.left   = x;
  lines->rc.top    = OFFSET_TOP;
  lines->rc.right  = x + offset + icon_size + size.cx + offset;
  lines->rc.bottom = cy - OFFSET_BTM;
  lines->id        = id;

  return lines->rc.right;
}

/* ----------------------------------------------------------------- */
int view_directory::SetMiRect(HDC hDC, int width, int cx, int cy)
{
  SHFILEINFO  shfi;
  TCHAR      *tmp;
  TCHAR      *cp;
  int         x = 32;
  int         i;
  int         j;
  int         len;
  int         top;
  TCHAR      *context;

  cx -= 8;

  len = (int)_tcslen(m_directory) + 1;

  tmp = (TCHAR *)malloc(len * sizeof(TCHAR));

  _tcscpy_s(tmp, len, m_directory);

  for (i = 0; i < TB_MAX_LINE; i++)
    {
      mi_lines[i].handle = IcoGetImageList();
    }

  mi_lines[0].index = PUSH_FOLDER;
  mi_lines[1].index = NONE_LINE;
  mi_lines[2].index = POP_FOLDER;
  mi_lines[3].index = BREAK_LINE;
  mi_lines[4].index = UP_FOLDER;
  mi_lines[5].index = NONE_LINE;

  x = SetMiiSubRect(hDC, 0, x, cy, IDIRECTORYID_BACK,     OFFSET_OFFSET);
  x = SetMiiSubRect(hDC, 1, x, cy, IDIRECTORYID_NULL);
  x = SetMiiSubRect(hDC, 2, x, cy, IDIRECTORYID_STEP,     OFFSET_OFFSET);
  x = SetMiiSubRect(hDC, 3, x, cy, IDIRECTORYID_DOWN);
  x = SetMiiSubRect(hDC, 4, x, cy, IDIRECTORYID_L_FOLDER, OFFSET_OFFSET);
  x = SetMiiSubRect(hDC, 5, x, cy, IDIRECTORYID_NULL);

  _tcscpy_s(mi_lines[4].folder, MAX_PATH, _T(".."));

  top = 6;

  for (i = top, cp = _tcstok_s(tmp, L"\\", &context); cp; cp = _tcstok_s(NULL, L"\\", &context))
    {
      if (i == top)
        {
          if (PathIsRoot(m_directory))
            {
              mi_lines[i].index = 74/*NONE_LINE*/;
            }
          else
            {
              mi_lines[i].index = 75;
            }
        }
      else
        {
          mi_lines[i].index = BREAK_ARROW;
        }

      x = SetMiiSubRect(hDC, i, x, cy, IDIRECTORYID_NULL);
      i++;

      mi_lines[i].handle = HENABLE;

      if (i == top + 1)
        {
          if (PathIsUNC(m_directory))
            {
              _stprintf_s(mi_lines[i].folder, MAX_PATH, _T("\\\\%s"), cp);
              cp = mi_lines[i].folder;
            }
          else
            {
              _tcscpy_s(mi_lines[i].folder, MAX_PATH, cp);

              PathAddBackslash(mi_lines[i].folder);
              ::SHGetFileInfo(mi_lines[i].folder,
                              FILE_ATTRIBUTE_ARCHIVE,
                              &shfi,
                              sizeof(SHFILEINFO),
                              SHGFI_DISPLAYNAME);
              cp = shfi.szDisplayName;
            }
        }
      else
        {
          _tcscpy_s(mi_lines[i].folder, MAX_PATH, mi_lines[i - 2].folder);
          ::PathAppend(mi_lines[i].folder, cp);
        }

      x = SetMiiSubRect(hDC, i, x, cy, IDIRECTORYID_L_FOLDER, OFFSET_OFFSET, cp);
      i++;
    }

  if (m_d_cnt)
    {
      mi_lines[i].index = BREAK_ARROW;
    }
  else
    {
      mi_lines[i].index = BREAK_LINE;
    }

  x = SetMiiSubRect(hDC, i, x, cy, IDIRECTORYID_NULL);
  i++;

  for (; i < TB_MAX_LINE - 1; i++)
    {
      mi_lines[i].handle = NULL;
    }

  if (m_enable)
    {
      for (i = top + 1, len = 0; i < TB_MAX_LINE - 1; i += 2)
        {
          if (mi_lines[i + 2].handle == NULL || cx > (x - len))
            {
              break;
            }

          len = mi_lines[i].rc.right - mi_lines[top].rc.left;
        }

      if (len > 0)
        {
          for (i--, j = 0; i < TB_MAX_LINE; i++, j++)
            {
              mi_lines[j + top] = mi_lines[i];

              if (mi_lines[j + top].handle != NULL)
                {
                  mi_lines[j + top].rc.left  -= len;
                  mi_lines[j + top].rc.right -= len;
                }
            }

          mi_lines[top].index = BACK_ARROW;
        }
    }
  else
    {
      for (i = top + 1, len = 0; i < TB_MAX_LINE - 1; i += 2)
        {
          if (mi_lines[i + 2].handle == NULL || cx > (x - len - mi_lines[top].rc.right))
            {
              break;
            }

          len = mi_lines[i].rc.right - mi_lines[top].rc.left;
        }

      for (i--, j = 0; i < TB_MAX_LINE; i++, j++)
        {
          mi_lines[j + top] = mi_lines[i];

          if (mi_lines[j + top].handle != NULL)
            {
              mi_lines[j + top].rc.left  -= len;
              mi_lines[j + top].rc.right -= len;
            }
        }
    }

  free(tmp);

  return 1;
}

/* ----------------------------------------------------------------- */
int view_directory::GetToolActionID(int x, int y)
{
  int   i;
  int   target = -1;
  POINT pos;

  pos.x = x - m_x;
  pos.y = y - m_y;

  for (i = 0; i < TB_MAX_LINE; i++)
    {
      if (mi_lines[i].handle == NULL)
        {
          continue;
        }

      if (PtInRect(&mi_lines[i].rc, pos))
        {
          target = i;
          break;
        }
    }

  return target;
}

/* ----------------------------------------------------------------- */
void view_directory::OnMouseMove(int x, int y)
{
  int   id = GetToolActionID(x, y);

  if (id != m_prev_id)
    {
      m_prev_id = id;
      Redraw();
    }
}

/* ----------------------------------------------------------------- */
void view_directory::OnMouseLeave(void)
{
  m_prev_id = -1;
  Redraw();
}

/* ----------------------------------------------------------------- */
void view_directory::OnLButtonDown(int x, int y)
{
  m_action = GetToolActionID(x, y);
  Redraw();
}

/* ----------------------------------------------------------------- */
void view_directory::OnRButtonDown(int x, int y)
{
  int   action = GetToolActionID(x, y);

  if (action > 2)
    {
      m_action = action;
      Redraw();
    }
}

/* ----------------------------------------------------------------- */
void view_directory::OnLButtonUp(int x, int y)
{
  if (m_action == GetToolActionID(x, y) && m_action >= 0)
    {
      SendID(mi_lines[m_action].id);
    }

  m_action = -1;
  Redraw();
}

/* ----------------------------------------------------------------- */
void view_directory::OnRButtonUp(int x, int y)
{
  if (m_action == GetToolActionID(x, y) && m_action >= 0)
    {
      SendID(mi_lines[m_action].id | IDIRECTORY_R_MASK);
    }

  m_action = -1;
  Redraw();
}

/* ----------------------------------------------------------------- */
int view_directory::Update(void)
{
  return 0;
}

/* ----------------------------------------------------------------- */
TCHAR *view_directory::GetSelected(void)
{
  int   action = 0;

  if (m_action >= 0 && m_action < TB_MAX_LINE)
    {
      action = m_action;
    }

  return mi_lines[action].folder;
}

/* ----------------------------------------------------------------- */
void view_directory::SetDirectory1(const TCHAR *dir, int d_cnt, BOOL redraw)
{
  _tcscpy_s(m_directory, MAX_PATH, dir);

  m_d_cnt = d_cnt;

  if (redraw)
    {
      Redraw();
    }
}

/* ----------------------------------------------------------------- */
void view_directory::SetEnable(BOOL enable, BOOL redraw)
{
  m_enable = enable;
  if (redraw)
    {
      Redraw();
    }
}

/* ----------------------------------------------------------------- */
int view_directory::GetHeight(HWND hWnd)
{
  xDC   dc(hWnd);

  return GetHeight(dc);
}

/* ----------------------------------------------------------------- */
int view_directory::GetHeight(HDC hDC)
{
  xTmpDC  dc(hDC);
  int     height;

  dc.SetSelectFont(m_fontname, m_fontsize);

  if (m_enable)
    {
      if (dc.tm.tmHeight > ICON_SIZE)
        {
          height = OFFSET_TOP
                 + OFFSET_OFFSET
                 + dc.tm.tmHeight
                 + OFFSET_OFFSET
                 + OFFSET_BTM;
        }
      else
        {
          height = OFFSET_TOP
                 + OFFSET_OFFSET
                 + ICON_SIZE
                 + OFFSET_OFFSET
                 + OFFSET_BTM;
        }
    }
  else
    {
      height = OFFSET_TOP + dc.tm.tmHeight + OFFSET_BTM;
    }

  return height;
}

/* ----------------------------------------------------------------- */
TCHAR *view_directory::GetSelected2(void)
{
  return mi_lines[m_action + 1].handle != NULL ? mi_lines[m_action + 1].folder : NULL;
}

/* ----------------------------------------------------------------- */
void view_directory::OnMouseHover(void)
{
  m_prev_id = -1;
  Redraw();
}

/* ----------------------------------------------------------------- */
void view_directory::SetTop(BOOL isTop)
{
  mIsTop = isTop;
}

/* ----------------------------------------------------------------- */
void view_directory::SetStepBackAndUpFocus(BOOL step_enable,
                                           BOOL back_enable,
                                           BOOL up_enable)
{
  m_back_enable = back_enable;
  m_step_enable = step_enable;
  m_up_enable   = up_enable;
}
