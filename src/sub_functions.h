#ifndef _SUB_FUNCTIONS_H_
#define _SUB_FUNCTIONS_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------- *
 * Pre-processor definitions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Public types
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

HGLOBAL MakeHDropFiles2nd(dirent *top_entry);

#ifdef __cplusplus
}
#endif

#endif /* _SUB_FUNCTIONS_H_ */
