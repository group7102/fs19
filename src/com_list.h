/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: com_list.h
 *  Created  : 08/06/04(水) 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#ifndef __COM_LIST_H__
#define __COM_LIST_H__

/* ----------------------------------------------------------------- *
 * Class
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
class com_list : public xListBox
{
public:
  void  SetLogData(int index, const TCHAR *pDispName, int type = -1);

public:
  com_list();
  ~com_list();

private:
  int   DrawItem(const DRAWITEMSTRUCT *lpDrawItem);
  int   MeasureItem(MEASUREITEMSTRUCT *lpMeasureItem);

private:
  void  DispDirectory(HDC hDC, int x, int cy, int height, TCHAR *str);

protected:
  void  FillRect(HDC hDC, const RECT *pRC, COLORREF fg);
  void  FillRect(HDC hDC, const RECT *pRC, COLORREF pen, COLORREF fg);
  void  com_list::CmpLocalDateTime(int    idx,
                                   DWORD  date_time,
                                   TCHAR *date_str,
                                   size_t len);
private:
  DWORD date_time;

};

#endif /* _COM_LIST_H_ */
