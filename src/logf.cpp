/*
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 * Copyright(C) 2004 ****, Inc.
 *
 *  File Name: logf.cpp
 *  Created  : 08/06/04 08:28:31
 *
 *  Function
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  $Revision: 1.1.1.1 $
 *  $Date: 2008/07/27 10:32:49 $
 *  $Author: poko $
 * --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 */
#include <windows.h>
#include <tchar.h>
#include <shlwapi.h>
#include <stdio.h>
#include "logf.h"

/* ----------------------------------------------------------------- *
 * Static parameters
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- *
 * Static function
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
static FILE *log_open(void)
{
  static int   _first = 1;
  static TCHAR _basename[MAX_PATH] = _T("");

  if (_basename[0] == _T('\0'))
    {
      GetModuleFileName(NULL, _basename, MAX_PATH);

      PathRenameExtension(_basename, _T(".log"));
    }

  FILE *fp;

  if (_tfopen_s(&fp, _basename, _T("a")) != 0)
    {
      return NULL;
    }

  if (_first)
    {
      SYSTEMTIME  systime;

      GetLocalTime(&systime);

      _ftprintf(fp,
                _T( "------ First log. %02d-%02d-%02d %02d:%02d:%02d ------\n"),
                systime.wYear % 100,
                systime.wMonth,
                systime.wDay,
                systime.wHour,
                systime.wMinute,
                systime.wSecond);

      _first = 0;
    }

  return fp;
}

/* ----------------------------------------------------------------- *
 * Public functions
 * ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
void logf(const TCHAR *wfmt, const char *fmt, ...)
{
  FILE  *fp;

  if ((fp = log_open()) == NULL)
    {
      return;
    }

  va_list  ap;

  va_start(ap, fmt);

  if (wfmt) {
    _vftprintf(fp, wfmt, ap);
  }
  else if (fmt) {
    vfprintf(fp, fmt, ap);
  }

  fflush(fp);

  fclose(fp);

#ifdef _DEBUG
  if (wfmt) {
    _vtprintf(wfmt, ap);
  }
  else if (fmt) {
    vprintf(fmt, ap);
  }
  fflush(stdout);
#endif

  va_end(ap);
}
